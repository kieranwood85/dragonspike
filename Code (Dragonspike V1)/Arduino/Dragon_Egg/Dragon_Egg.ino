#include <Wire.h>
#include <RHDatagram.h>
#include <RH_RF95.h>
#include <DragonEgg.h>
#include <SPI.h>
#include <SdFat.h>
#include <MemoryFree.h>
#include <Adafruit_LIS3DH.h>
#include <Adafruit_MAX31865.h>

// Network Addresses
#define UAV_ADDRESS 1
#define EGG_ADDRESS 2

// SD Config
#define SD_CS 5                 // SD card chip select pin
#define SD_FREQ 24              // SD card max SPI frequency

// RTD Config
#define RTD_CS 6                // RTD temperature sensor amplifier chip select pin
#define RTD_RREF 430.0
#define RTD_NOMINAL 100.0

// Seismic Config
#define SEISMIC_SAMPLE_LENGTH 160  
#define SEISMIC_SAMPLE_RATE 0.0125   
#define SEISMIC_LOWER_CUTOFF_FREQ 0.5     
#define SEISMIC_UPPER_CUTOFF_FREQ 20  
#define SEISMIC_EVENT_THRESHOLD 1.3
#define SEISMIC_BACKGROUND_THRESHOLD 1.1
#define SEISMIC_MAX_EVENTS 255
#define SEISMIC_EVENT_TOP_SAMPLES 8

// Manager Config
#define MANAGER_SAMPLES_PER_FILE 1024
#define MANAGER_EVENTS_PER_FILE 1024
#define MANAGER_BACKUP_TIME 120

// Radio Config
#define RFM_CS 8                // RFM95 radio chip select pin
#define RFM_IRQ 3               // RFM95 radio interrupt pin
#define RFM_FREQ 915            // RFM95 radio frequency in MHz
#define RFM_PWR 23              // RFM95 radio power in dB
#define RFM_BUF 5               // Radio manager packet buffer length
#define RFM_HB_TIME 5          // Nominal time in seconds between heartbeats

// Start-up Config
#define START_LOWER_THRESHOLD 4
#define START_UPPER_THRESHOLD 7
#define START_FALL_TIME 0.3
#define START_BACKUP_TIME 0

// Acceleration Read Function Prototype
void readLIS3DH(float *x, float *y, float *z);

// Temperature Read Function Prototype
float readMAX31865();

// SD Card System Object
SdFat sd;

// Accelerometer
Adafruit_LIS3DH accel = Adafruit_LIS3DH();

// Temperatue Amplifier
Adafruit_MAX31865 rtd = Adafruit_MAX31865(RTD_CS);

// Singleton instance of the radio driver
RH_RF95 driver(RFM_CS,RFM_IRQ);

// Class to manage message delivery and receipt, using the driver declared above
RHDatagram datagram(driver, EGG_ADDRESS);

// Dragon Manager
DragonManager systemManager(SEISMIC_MAX_EVENTS,MANAGER_SAMPLES_PER_FILE,MANAGER_EVENTS_PER_FILE,&sd);

// Dragon Radio
DragonRadio radioManager(&datagram,RFM_BUF);

// Seismic Monitor
SeismicMonitor sampler(SEISMIC_SAMPLE_LENGTH,SEISMIC_SAMPLE_RATE,SEISMIC_LOWER_CUTOFF_FREQ,SEISMIC_UPPER_CUTOFF_FREQ,SEISMIC_BACKGROUND_THRESHOLD,readLIS3DH);

// Seismic Event Detector
SeismicEventDetector eventDetector(SEISMIC_EVENT_THRESHOLD,SEISMIC_EVENT_TOP_SAMPLES);

// Timers
DragonTimer heartbeatTimer(1000000*RFM_HB_TIME);
DragonTimer backupTimer(1000000*MANAGER_BACKUP_TIME);

void setup() {

  // Initialise serial, SD card and accelerometer communications
  Serial.begin(38400);
  accel.begin(0x18);
  rtd.begin(MAX31865_2WIRE);

  // Attempt to start the radio manager
  if (!datagram.init()) {
    Serial.println("Radio Initialisation Failed");
  } else {
    Serial.println("Initialisation Successful");
  }

  // Initialise SD card system
  if (!sd.begin(SD_CS,SD_SCK_MHZ(SD_FREQ))) {
    Serial.println("SD Initialisation Failed");
    sd.initErrorHalt();
  }

  // Set radio power and frequency
  driver.setTxPower(RFM_PWR, false);
  driver.setFrequency(RFM_FREQ);

  // Set accelerometer range
  accel.setRange(LIS3DH_RANGE_8_G);

  //Initialise system
  uint32_t initSampleID, initEventID;
  systemManager.begin(&initSampleID,&initEventID);
  eventDetector.begin(initEventID);
  sampler.begin(initSampleID,0,0);

  // Start-Up Timer
  DragonTimer fallTimer(1000000*START_FALL_TIME);
  DragonTimer timeoutTimer(1000000*START_BACKUP_TIME);

  //Wait until end of freefall
  float accMag = 10, x, y, z;
  bool fallTrig = false;
  bool timeoutTrig = false;
  
  while (!fallTrig && !timeoutTrig) {
    
    readLIS3DH(&x,&y,&z);
    accMag = sqrt(x*x+y*y+z*z);
    
    if (accMag > START_LOWER_THRESHOLD) {
      fallTimer.reset();
    }
    
    if (fallTimer.update()) {
      fallTrig = true;
    }

    if (timeoutTimer.update()) {
      timeoutTrig = true;
    }
    
  }

  while (fallTrig && !timeoutTrig) {

    readLIS3DH(&x,&y,&z);
    accMag = sqrt(x*x+y*y+z*z);

    if (accMag > START_UPPER_THRESHOLD) {
      fallTrig = false;
    }

    if (timeoutTimer.update()) {
      timeoutTrig = true;
    }
    
  }

}

void handlePacket(DragonPacket recvPacket) {

  switch (recvPacket.msgID) {

    case DragonRadio::ID::RequestSeismicEvent:
    {
      
      DragonMessage_RequestSeismicEvent recvMessage(&recvPacket);
      recvMessage.debug();
      
      SeismicEvent event = systemManager.loadEvent(recvMessage.ID);
      
      if (event.nTopSamples > 0) {
        DragonMessage_SeismicEventSummary sendMessage(event.ID,event.startTime,event.endTime,event.energy,event.PGA);
        DragonPacket sendPacket = sendMessage.pack();
        radioManager.send(&sendPacket,UAV_ADDRESS);
        datagram.waitPacketSent();
      }
      
    }
    break;

    case DragonRadio::ID::RequestSeismicEventTopSamples:
    {
      
      DragonMessage_RequestSeismicEventTopSamples recvMessage(&recvPacket);
      recvMessage.debug();
      
      SeismicEvent event = systemManager.loadEvent(recvMessage.ID);
      
      if (event.nTopSamples > 0) {
        
        if (recvMessage.numSamples == 1) {
          
          SeismicSample sample = systemManager.loadSample(event.topSampleIDs[event.nTopSamples-1]);
          
          if (sample.ID > 0) {
            DragonMessage_SeismicSampleSummary sendMessage(sample.ID,sample.startTime,sample.endTime,sample.rate,sample.energy,sample.PGA);
            DragonPacket sendPacket = sendMessage.pack();
            radioManager.send(&sendPacket,UAV_ADDRESS);
            datagram.waitPacketSent();
          }
          
        } else {
          
          uint8_t numSamples = std::min(std::min((uint8_t)recvMessage.numSamples,(uint8_t)8),(uint8_t)event.nTopSamples);
          uint8_t counter = 0;
          uint32_t IDs[numSamples];
          uint32_t startTimes[numSamples];
          uint32_t endTimes[numSamples];
          float rates[numSamples];
          float energies[numSamples];
          float PGAs[numSamples];
          
          for (int i=0; i < numSamples; i++) {
            
            SeismicSample sample = systemManager.loadSample(event.topSampleIDs[event.nTopSamples-1-i]);
            
            if (sample.ID > 0) {
              IDs[counter] = sample.ID;
              startTimes[counter] = sample.startTime;
              endTimes[counter] = sample.endTime;
              rates[counter] = sample.rate;
              energies[counter] = sample.energy;
              PGAs[counter] = sample.PGA;
              counter++;
            }
            
          }
          
          if (counter > 0) {
            DragonMessage_SeismicSampleSummaryMulti sendMessage(counter,IDs,startTimes,endTimes,rates,energies,PGAs);
            DragonPacket sendPacket = sendMessage.pack();
            radioManager.send(&sendPacket,UAV_ADDRESS);
            datagram.waitPacketSent();
          }
          
        }
      } 
    }
    break;

    case DragonRadio::ID::DeleteTopSeismicEvents:
    {
      
      DragonMessage_DeleteTopSeismicEvents recvMessage(&recvPacket);
      recvMessage.debug();

      systemManager.deleteTopEvent();
      
    }
    break;

    case DragonRadio::ID::RequestSeismicSampleFFT:
    {
      
      DragonMessage_RequestSeismicSampleFFT recvMessage(&recvPacket);
      recvMessage.debug();
      
      uint16_t numBins = std::min((uint8_t)recvMessage.numBins,(uint8_t)16);
      float freqs[numBins], mags[numBins];
      
      SeismicSample sample = systemManager.loadSample(recvMessage.ID);
      
      if (sample.ID > 0) {
        sample.computeFFT(recvMessage.axis,freqs,mags,&numBins);
        DragonMessage_SeismicSampleFFT sendMessage(sample.ID,numBins,freqs,mags);
        DragonPacket sendPacket = sendMessage.pack();
        radioManager.send(&sendPacket,UAV_ADDRESS);
        datagram.waitPacketSent();
      }
      
    }
    break;

    case DragonRadio::ID::RequestSeismicSampleFFTMulti:
    {
      
      DragonMessage_RequestSeismicSampleFFTMulti recvMessage(&recvPacket);
      recvMessage.debug();
      
      uint8_t numSamples = std::min((uint8_t)recvMessage.numSamples,(uint8_t)24);
      uint16_t numBins = std::min((uint16_t)recvMessage.numBins,(uint16_t)floor(24/numSamples));
      uint32_t IDs[numSamples];
      float freqs[numBins*numSamples], mags[numBins*numSamples];
      uint16_t bins[numSamples];
      uint8_t counter = 0;

      for (int i=0; i < numSamples; i++) {
            
        SeismicSample sample = systemManager.loadSample(recvMessage.IDs[i]);
            
        if (sample.ID > 0) {
          uint16_t bins = numBins;
          IDs[counter] = sample.ID;
          sample.computeFFT(recvMessage.axis,freqs+counter*numBins,mags+counter*numBins,&bins);
          counter++;
        }
            
      }
      
      if (counter > 0) {
        DragonMessage_SeismicSampleFFTMulti sendMessage(counter,IDs,numBins,freqs,mags);
        DragonPacket sendPacket = sendMessage.pack();
        radioManager.send(&sendPacket,UAV_ADDRESS);
        datagram.waitPacketSent();
      }
      
    }
    break;

    default:
    break;
        
  }
  
}

void loop() {

  //Update the seismic sampler
  sampler.update();
  //Update the radio manager
  radioManager.update();

  //If a new sample window has been recorded
  if (sampler.available()) {

    //Get the sample window and save to SD card
    SeismicSample sample = sampler.getSample();
    systemManager.saveSample(&sample);     

    //Update the event detector

    eventDetector.update(&sample,&sampler);

    //If a new event has been recorded
    if (eventDetector.available()) {

      //Get the event and save to SD card
      SeismicEvent event = eventDetector.getEvent();
      systemManager.saveEvent(&event);

      //Make a backup of the top events log if backup time has elapsed
      if (backupTimer.update()) {
        systemManager.backup();
      }
      
    }

    if (!radioManager.available()) {

      if (heartbeatTimer.update()) {

      uint8_t numRSAMs = 48;
      float RSAMs[numRSAMs];
      SeismicEvent topEvent = systemManager.getTopEvent();
      sampler.getRSAM(&numRSAMs,RSAMs);
      
      DragonMessage_Heartbeat heartbeat(topEvent.ID,numRSAMs,RSAMs,1);
      DragonPacket packet = heartbeat.pack();
      radioManager.send(&packet,UAV_ADDRESS);

      }
      
    } else {

      //While a new dragon packet is available in the buffer
      while (radioManager.available()) {

        //Get the oldest dragon packet from the manager
        DragonPacket packet = radioManager.read();
        //Handle the packet
        handlePacket(packet);

      }
      
    }

  }
  
}

void readLIS3DH(float *x, float *y, float *z) {

  accel.read();
  
  *x = 9.8*accel.x_g;
  *y = 9.8*accel.y_g;
  *z = 9.8*accel.z_g;
  
}

float readMAX31865() {

  rtd.readRTD();
  return rtd.temperature(RTD_NOMINAL,RTD_RREF);
  
}
