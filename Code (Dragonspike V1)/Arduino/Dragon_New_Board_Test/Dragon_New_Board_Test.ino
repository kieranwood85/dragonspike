#include <Wire.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>
#include "RTClib.h"

#define DS3231_32K 9
#define DS3231_SQW 12

Adafruit_MMA8451 MMA = Adafruit_MMA8451();
RTC_DS3231 rtc;

char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

void setup() {

  pinMode(DS3231_32K,INPUT_PULLUP);
  pinMode(DS3231_SQW,INPUT_PULLUP);

  Serial.begin(9600);
  while (!Serial);
  
  Serial.println("Dragon New Board Test");

  if (! MMA.begin()) {
    Serial.println("MMA8451 FAILED");
    while (true);
  }
  Serial.println("MMA8451 FOUND!");

  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (true);
  } else {
    Serial.println("DS3231 FOUND!");
  }

  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));

  MMA.setRange(MMA8451_RANGE_2_G);
  MMA.setDataRate(MMA8451_DATARATE_50_HZ);

}

void loop() {
  
  MMA.read();
  sensors_event_t event;
  MMA.getEvent(&event);

  delay(1);

  DateTime time = rtc.now();

  Serial.print(time.year(), DEC);
  Serial.print('/');
  Serial.print(time.month(), DEC);
  Serial.print('/');
  Serial.print(time.day(), DEC);
  Serial.print(" (");
  Serial.print(daysOfTheWeek[time.dayOfTheWeek()]);
  Serial.print(") ");
  Serial.print(time.hour(), DEC);
  Serial.print(':');
  Serial.print(time.minute(), DEC);
  Serial.print(':');
  Serial.print(time.second(), DEC);
  Serial.println();
  
  Serial.print("X: "); Serial.print(event.acceleration.x); Serial.println();
  Serial.print("Y: "); Serial.print(event.acceleration.y); Serial.println();
  Serial.print("Z: "); Serial.print(event.acceleration.z); Serial.println();
  Serial.println("m/s^2 ");

  Serial.println();
  delay(1000);
  
}
