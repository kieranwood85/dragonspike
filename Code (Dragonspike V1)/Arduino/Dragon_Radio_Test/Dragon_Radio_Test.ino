#include <Wire.h>
#include <RHDatagram.h>
#include <RH_RF95.h>
#include <Dragon.h>
#include <SPI.h>
#include <SdFat.h>

// Network Addresses
#define UAV_ADDRESS 1

// SD Config
#define SD_CS 5
#define SD_FREQ 24 

// Radio Config
#define RFM_CS 8
#define RFM_IRQ 3
#define RFM_FREQ 915
#define RFM_PWR 23
#define RFM_BUF 4

// SD Card System Object
SdFat sd;

// Singleton instance of the radio driver
RH_RF95 driver(RFM_CS,RFM_IRQ);

// Class to manage message delivery and receipt, using the driver declared above
RHDatagram datagram(driver, UAV_ADDRESS);

// Dragon Radio
DragonRadio radioManager(&datagram,RFM_BUF);

void setup() {
  
  Serial.begin(9600);
  
  if (!datagram.init()) {
    Serial.println("Initialisation Failed");
  } else {
    Serial.println("Initialisation Successful");
  }

    if (!sd.begin(SD_CS,SD_SCK_MHZ(SD_FREQ))) {
    Serial.println("SD Initialisation Failed");
    sd.initErrorHalt();
  }

  driver.setTxPower(RFM_PWR, false);
  driver.setFrequency(RFM_FREQ);
  
}

char fileName[50];

void loop() {

  delay(300);
  DragonMessage_RequestSeismicEvent message1(20);
  DragonPacket packet1 = message1.pack();
  message1.debug();
  radioManager.send(&packet1,2);

  delay(300);
  uint32_t IDs[4] = {1,2,3,4};
  DragonMessage_RequestSeismicEventMulti message2(4,IDs);
  DragonPacket packet2 = message2.pack();
  message2.debug();
  radioManager.send(&packet2,2);

}
