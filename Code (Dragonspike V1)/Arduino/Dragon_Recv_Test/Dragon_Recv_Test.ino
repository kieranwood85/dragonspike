#include <Wire.h>
#include <RHDatagram.h>
#include <RH_RF95.h>
#include <DragonEgg.h>
#include <SPI.h>
#include <SdFat.h>
#include <MemoryFree.h>
#include <Adafruit_LIS3DH.h>
#include <Adafruit_MAX31865.h>

// Network Addresses
#define UAV_ADDRESS 1
#define EGG_ADDRESS 2

// SD Config
#define SD_CS 5                 // SD card chip select pin
#define SD_FREQ 24              // SD card max SPI frequency

// RTD Config
#define RTD_CS 6                // RTD temperature sensor amplifier chip select pin
#define RTD_RREF 430.0
#define RTD_NOMINAL 100.0

// Seismic Config
#define SEISMIC_SAMPLE_LENGTH 30  
#define SEISMIC_SAMPLE_RATE 0.0125   
#define SEISMIC_LOWER_CUTOFF_FREQ 1     
#define SEISMIC_UPPER_CUTOFF_FREQ 20  
#define SEISMIC_EVENT_THRESHOLD 1.5
#define SEISMIC_BACKGROUND_THRESHOLD 1.3
#define SEISMIC_MAX_EVENTS 255
#define SEISMIC_EVENT_TOP_SAMPLES 8

// Manager Config
#define MANAGER_SAMPLES_PER_FILE 1024
#define MANAGER_EVENTS_PER_FILE 1024

// Radio Config
#define RFM_CS 8                // RFM95 radio chip select pin
#define RFM_IRQ 3               // RFM95 radio interrupt pin
#define RFM_FREQ 915            // RFM95 radio frequency in MHz
#define RFM_PWR 23              // RFM95 radio power in dB
#define RFM_BUF 5               // Radio manager packet buffer length

// Acceleration Read Function Prototype
void readLIS3DH(float *x, float *y, float *z);

// Temperature Read Function Prototype
float readMAX31865();

// SD Card System Object
SdFat sd;

// Accelerometer
Adafruit_LIS3DH accel = Adafruit_LIS3DH();

// Temperatue Amplifier
Adafruit_MAX31865 rtd = Adafruit_MAX31865(RTD_CS);

// Singleton instance of the radio driver
RH_RF95 driver(RFM_CS,RFM_IRQ);

// Class to manage message delivery and receipt, using the driver declared above
RHDatagram datagram(driver, EGG_ADDRESS);

// Dragon Manager
DragonManager systemManager(SEISMIC_MAX_EVENTS,MANAGER_SAMPLES_PER_FILE,MANAGER_EVENTS_PER_FILE,&sd);

// Dragon Radio
DragonRadio radioManager(&datagram,RFM_BUF);

// Seismic Monitor
SeismicMonitor sampler(SEISMIC_SAMPLE_LENGTH,SEISMIC_SAMPLE_RATE,SEISMIC_LOWER_CUTOFF_FREQ,SEISMIC_UPPER_CUTOFF_FREQ,SEISMIC_BACKGROUND_THRESHOLD,readLIS3DH);

// Seismic Event Detector
SeismicEventDetector eventDetector(SEISMIC_EVENT_THRESHOLD,SEISMIC_EVENT_TOP_SAMPLES);

void setup() {

  // Initialise serial, SD card and accelerometer communications
  Serial.begin(38400);
  while (!Serial);
  accel.begin(0x18);
  rtd.begin(MAX31865_2WIRE);

  // Attempt to start the radio manager
  if (!datagram.init()) {
    Serial.println("Radio Initialisation Failed");
  } else {
    Serial.println("Initialisation Successful");
  }

  // Initialise SD card system
  if (!sd.begin(SD_CS,SD_SCK_MHZ(SD_FREQ))) {
    Serial.println("SD Initialisation Failed");
    sd.initErrorHalt();
  }

  // Set radio power and frequency
  driver.setTxPower(RFM_PWR, false);
  driver.setFrequency(RFM_FREQ);

  // Set accelerometer range
  accel.setRange(LIS3DH_RANGE_8_G);

  //Begin Dragon Manager
  systemManager.begin();

}

void handlePacket(DragonPacket recvPacket) {

  switch (recvPacket.msgID) {

    case DragonRadio::ID::RequestSeismicEvent:
    {
      DragonMessage_RequestSeismicEvent recvMessage(&recvPacket);
      recvMessage.debug();
    }
    break;

    case DragonRadio::ID::RequestSeismicEventMulti:
    {
      DragonMessage_RequestSeismicEventMulti recvMessage(&recvPacket);
      recvMessage.debug();
    }
    break;

    case DragonRadio::ID::RequestSeismicSample:
    {
      DragonMessage_RequestSeismicSample recvMessage(&recvPacket);
      recvMessage.debug();
    }
    break;

    case DragonRadio::ID::RequestSeismicSampleMulti:
    {
      DragonMessage_RequestSeismicSampleMulti recvMessage(&recvPacket);
      recvMessage.debug();
    }
    break;

    case DragonRadio::ID::RequestSeismicEventTopSamples:
    {
      DragonMessage_RequestSeismicEventTopSamples recvMessage(&recvPacket);
      recvMessage.debug();
    }
    break;

    case DragonRadio::ID::DeleteTopSeismicEvents:
    {
      DragonMessage_DeleteTopSeismicEvents recvMessage(&recvPacket);
      recvMessage.debug();
    }
    break;

    case DragonRadio::ID::RequestSeismicSampleFFT:
    {
      DragonMessage_RequestSeismicSampleFFT recvMessage(&recvPacket);
      recvMessage.debug();
    }
    break;

    default:
    break;
        
  }
  
}

void loop() {
  
  //Update the radio manager
  radioManager.update();

  //While a new dragon packet is available in the buffer
  while (radioManager.available()) {

    //Get the oldest dragon packet from the manager
    DragonPacket packet = radioManager.read();
    //Handle the packet
    handlePacket(packet);

  }

}

void readLIS3DH(float *x, float *y, float *z) {

  accel.read();
  
  *x = 9.8*accel.x_g;
  *y = 9.8*accel.y_g;
  *z = 9.8*accel.z_g;
  
}

float readMAX31865() {

  rtd.readRTD();
  return rtd.temperature(RTD_NOMINAL,RTD_RREF);
  
}
