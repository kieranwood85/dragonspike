#include <Wire.h>
#include <RHDatagram.h>
#include <RH_RF95.h>
#include <Dragon.h>
#include <SPI.h>
#include <SdFat.h>

// Network Addresses
#define UAV_ADDRESS 1

// SD Config
#define SD_CS 5
#define SD_FREQ 24 

// Radio Config
#define RFM_CS 8
#define RFM_IRQ 3
#define RFM_FREQ 915
#define RFM_PWR 23
#define RFM_BUF 5

// SD Card System Object
SdFat sd;

// Singleton instance of the radio driver
RH_RF95 driver(RFM_CS,RFM_IRQ);

// Class to manage message delivery and receipt, using the driver declared above
RHDatagram datagram(driver, UAV_ADDRESS);

// Dragon Radio
DragonRadio radioManager(&datagram,RFM_BUF);

void setup() {
  
  Serial.begin(9600);
  
  if (!datagram.init()) {
    Serial.println("Initialisation Failed");
  } else {
    Serial.println("Initialisation Successful");
  }

    if (!sd.begin(SD_CS,SD_SCK_MHZ(SD_FREQ))) {
    Serial.println("SD Initialisation Failed");
    sd.initErrorHalt();
  }

  driver.setTxPower(RFM_PWR, false);
  driver.setFrequency(RFM_FREQ);
  
}

void handlePacket(DragonPacket recvPacket) {

  switch (recvPacket.msgID) {

    case DragonRadio::ID::Heartbeat:
    {
      
      DragonMessage_Heartbeat recvMessage(&recvPacket);
      recvMessage.debug();
      recvMessage.log("LOG.txt");
      
      if (recvMessage.topEventID > 0 ) {
        DragonMessage_RequestSeismicEvent sendMessage(recvMessage.topEventID);
        DragonPacket sendPacket = sendMessage.pack();
        delay(100);
        radioManager.send(&sendPacket,recvPacket.sendID);
        datagram.waitPacketSent();
      }
      
    }
    break;

    case DragonRadio::ID::SeismicEventSummary:
    {
      
      DragonMessage_SeismicEventSummary recvMessage(&recvPacket);
      recvMessage.debug();
      recvMessage.log("LOG.txt");
      
      DragonMessage_RequestSeismicEventTopSamples sendMessage(recvMessage.ID,10);
      DragonPacket sendPacket = sendMessage.pack();
      delay(100);
      radioManager.send(&sendPacket,recvPacket.sendID);
      datagram.waitPacketSent(); 
      
    }
    break;

    case DragonRadio::ID::SeismicSampleSummary:
    {
      
      DragonMessage_SeismicSampleSummary recvMessage(&recvPacket);
      recvMessage.debug();
      recvMessage.log("LOG.txt");
      
      if (recvMessage.ID > 0) {
        DragonMessage_RequestSeismicSampleFFT sendMessage(recvMessage.ID,0,4);
        DragonPacket sendPacket = sendMessage.pack();
        delay(100);
        radioManager.send(&sendPacket,recvPacket.sendID);
        datagram.waitPacketSent();
      }
      
    }
    break;

    case DragonRadio::ID::SeismicSampleSummaryMulti:
    {
      
      DragonMessage_SeismicSampleSummaryMulti recvMessage(&recvPacket);
      recvMessage.debug();
      recvMessage.log("LOG.txt");

      uint8_t numSamples = std::min((uint8_t)recvMessage.numSamples,(uint8_t)24);
      uint16_t numBins = (uint16_t)floor(24/numSamples);

      DragonMessage_RequestSeismicSampleFFTMulti sendMessage(numSamples,recvMessage.IDs,0,numBins);
      DragonPacket sendPacket = sendMessage.pack();
      delay(100);
      radioManager.send(&sendPacket,recvPacket.sendID);
      datagram.waitPacketSent();
      
    }
    break;

    case DragonRadio::ID::SeismicSampleFFT:
    {
      
      DragonMessage_SeismicSampleFFT recvMessage(&recvPacket);
      recvMessage.debug();
      recvMessage.log("LOG.txt");

      DragonMessage_DeleteTopSeismicEvents sendMessage(1);
      DragonPacket sendPacket = sendMessage.pack();
      delay(100);
      radioManager.send(&sendPacket,recvPacket.sendID);
      datagram.waitPacketSent();
      
    }
    break;

    case DragonRadio::ID::SeismicSampleFFTMulti:
    {
      
      DragonMessage_SeismicSampleFFTMulti recvMessage(&recvPacket);
      recvMessage.debug();
      recvMessage.log("LOG.txt");

      DragonMessage_DeleteTopSeismicEvents sendMessage(1);
      DragonPacket sendPacket = sendMessage.pack();
      delay(100);
      radioManager.send(&sendPacket,recvPacket.sendID);
      datagram.waitPacketSent();
      
    }
    break;

    default:
    break;
        
  }
  
}
void loop() {

  //Update the radio manager
  radioManager.update();
  
  while (radioManager.available()) {

    //Get the oldest dragon packet from the manager
    DragonPacket packet = radioManager.read();
    //Handle the packet
    handlePacket(packet);
    
  }
  
}
