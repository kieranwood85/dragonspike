/*	
	Dragon Egg library for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#ifndef DragonEgg_h

#define DragonEgg_h
#include "DragonManager/DragonManager.h"
#include "DragonSeismic/DragonSeismic.h"
#include "DragonUtils/DragonUtils.h"
#include "DragonRadio/DragonRadio.h"

#endif