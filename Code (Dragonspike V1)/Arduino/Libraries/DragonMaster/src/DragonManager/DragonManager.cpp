/*	
	Dragon manager class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#include "Arduino.h"
#include "DragonManager.h"
#include "math.h"

DragonManager::DragonManager(uint8_t numTopEvents, uint32_t samplesPerFile, uint32_t eventsPerFile, SdFat *sdObject)
	:nTopEvents(numTopEvents)
	,samplesPerFile(samplesPerFile)
	,eventsPerFile(eventsPerFile)
	,nSamples(0)
	,nEvents(0)
	,sd(sdObject)
{
	
}

void DragonManager::begin(uint32_t *initSampleID = NULL,uint32_t *initEventID = NULL) {
	
	SdFile file;
	createFileSystem();
	
	if (sd->exists("SAMPLES/INDEXES.dat")) {
		file.open("SAMPLES/INDEXES.dat",O_CREAT | O_WRONLY | O_APPEND);
		file.seekEnd();
		*initSampleID = floor((file.curPosition()+1) / 6) + 1;
		file.close();
	} else {
		*initSampleID = 1;
	}
	
	if (sd->exists("EVENTS/INDEXES.dat")) {
		file.open("EVENTS/INDEXES.dat",O_CREAT | O_WRONLY | O_APPEND);
		file.seekEnd();
		*initEventID = floor((file.curPosition()+1) / 6) + 1;
		file.close();
	} else {
		*initEventID = 1;
	}
	
	if (sd->exists("EVENTS/BACKUP.dat")) {
		
		file.open("EVENTS/BACKUP.dat",O_RDONLY);
		
		uint8_t buf[8];
		while (file.read(buf,8) == 8) {
			
			uint32_t ID;
			float energy;
			energyIDPair pair;
			memcpy(&(pair.ID),buf,4);
			memcpy(&(pair.energy),buf+4,4);
			
			if (topEvents.size() < nTopEvents) {
				topEvents.insert(pair);
			}
		}
		
		file.close();
		
	}
}

void DragonManager::backup() {
	
	SdFile file;
	sd->remove("EVENTS/BACKUP.dat");
	file.open("EVENTS/BACKUP.dat",O_CREAT | O_WRONLY);
	
	if (topEvents.size() > 0) {
		
		std::multiset<energyIDPair>::iterator it = topEvents.end();
		
		uint8_t buf[8];
		while (it != topEvents.begin()) {
			--it;
			energyIDPair pair = *it;
			memcpy(buf,&(pair.ID),sizeof(uint32_t));
			memcpy(buf+4,&(pair.energy),sizeof(float));
			file.write(buf,8);
		}
		
	}
	
	file.close();
	
}

void DragonManager::saveSample(SeismicSample *sample) {
	
	uint8_t index = 0;
	uint8_t buf[25];
	uint16_t fileNum;
	uint32_t fileIndex;
	char filename[50];
	
	memcpy(buf+index,&(sample->ID),sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(buf+index,&(sample->length),sizeof(uint8_t)); index += sizeof(uint8_t);
	memcpy(buf+index,&(sample->rate),sizeof(float)); index += sizeof(float);
	memcpy(buf+index,&(sample->energy),sizeof(float)); index += sizeof(float);
	memcpy(buf+index,&(sample->PGA),sizeof(float)); index += sizeof(float);
	memcpy(buf+index,&(sample->startTime),sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(buf+index,&(sample->endTime),sizeof(uint32_t)); index += sizeof(uint32_t);
	
	fileNum = (uint16_t) (1 + nSamples / samplesPerFile);
    
    snprintf(filename,sizeof(filename),"SAMPLES/%08x.dat",fileNum);
	
	SdFile file;
	file.open(filename,O_WRONLY | O_CREAT | O_APPEND);
	file.seekEnd();
	
	fileIndex = file.curPosition();
	
	file.write(buf,25);
	file.write(sample->xData,sample->length*sizeof(float));
	file.write(sample->yData,sample->length*sizeof(float));
	file.write(sample->zData,sample->length*sizeof(float));
	file.close();
	
	registerSample(sample->ID, fileNum, fileIndex);
	
}

void DragonManager::saveEvent(SeismicEvent *event) {
	
	uint8_t index = 0;
	uint8_t buf[29];
	uint16_t fileNum;
	uint32_t fileIndex;
	char filename[50];
	
	memcpy(buf+index,&(event->ID),sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(buf+index,&(event->startID),sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(buf+index,&(event->endID),sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(buf+index,&(event->startTime),sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(buf+index,&(event->endTime),sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(buf+index,&(event->energy),sizeof(float)); index += sizeof(float);
	memcpy(buf+index,&(event->PGA),sizeof(float)); index += sizeof(float);
	memcpy(buf+index,&(event->nTopSamples),sizeof(uint8_t)); index += sizeof(uint8_t);
	
	fileNum = (uint16_t) (1 + nEvents / eventsPerFile);
	
	snprintf(filename,sizeof(filename),"EVENTS/%08x.dat",fileNum);
	
	SdFile file;
	file.open(filename,O_WRONLY | O_CREAT | O_APPEND);
	file.seekEnd();
	
	fileIndex = file.curPosition();
	
	file.write(buf,29);
	file.write(event->topSampleIDs,event->nTopSamples*sizeof(uint32_t));
	file.close();
	
	registerEvent(event->ID, fileNum, fileIndex);
	
	energyIDPair pair;
	
	pair.ID = event->ID;
	pair.energy = event->energy;
	
	topEvents.insert(pair);
	
	if (topEvents.size() > nTopEvents) {
		topEvents.erase(topEvents.begin());
	}
	
}

SeismicEvent DragonManager::getTopEvent() {
	
	if (topEvents.size() > 0) {
		
		std::multiset<energyIDPair>::iterator it = topEvents.end();
		--it;
		energyIDPair pair = *it;
		
		SeismicEvent event = loadEvent(pair.ID);
		return event;
		
	} else {
		
		SeismicEvent event;
		return event;
		
	}
	
}

void DragonManager::deleteTopEvent() {
	
	if (topEvents.size() > 0) {
		
		std::multiset<energyIDPair>::iterator it = topEvents.end();
		--it;
		topEvents.erase(it);
		
	}
	
}

SeismicEvent DragonManager::loadEvent(uint32_t requestID) {
	
	char filename[50];
	uint8_t header[29]; 
	
	uint32_t ID;
	uint32_t startID;
	uint32_t endID;
	uint32_t startTime;
	uint32_t endTime;
	float energy;
	float PGA;
	uint8_t nTopSamples;
	
	uint16_t fileNum;
	uint32_t fileIndex;
	
	SdFile file;
	file.open("EVENTS/INDEXES.dat", O_RDONLY);
	file.seekEnd();
	
	if (file.curPosition() < 6*requestID) {
		SeismicEvent event;
		file.close();
		return event;
	} else {
		file.seekSet(6*requestID);
		file.read(&fileNum,2);
		file.read(&fileIndex,4);
		file.close();
	}
	
	snprintf(filename,sizeof(filename),"EVENTS/%08x.dat",fileNum);
	file.open(filename, O_RDONLY);
	file.seekSet(fileIndex);
	
	file.read(header,29);
	
	uint32_t index = 0;
	memcpy(&ID,header+index,sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(&startID,header+index,sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(&endID,header+index,sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(&startTime,header+index,sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(&endTime,header+index,sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(&energy,header+index,sizeof(float)); index += sizeof(float);
	memcpy(&PGA,header+index,sizeof(float)); index += sizeof(float);
	memcpy(&nTopSamples,header+index,sizeof(uint8_t)); index += sizeof(uint8_t);
	
	uint32_t topSampleIDs[nTopSamples];
	file.read(topSampleIDs,nTopSamples*sizeof(uint32_t));
	
	file.close();
	
	SeismicEvent event(ID,startID,endID,startTime,endTime,energy,PGA,nTopSamples,topSampleIDs);
	return event;
	
}

SeismicSample DragonManager::loadSample(uint32_t requestID) {
	
	char filename[50];
	uint8_t header[25]; 
	
	uint32_t ID;
	uint8_t length;
	float rate;
	float energy;
	float PGA;
	uint32_t startTime;
	uint32_t endTime;
	
	uint16_t fileNum;
	uint32_t fileIndex;
	
	SdFile file;
	file.open("SAMPLES/INDEXES.dat", O_RDONLY);
	file.seekEnd();
	
	if (file.curPosition() < 6*requestID) {
		SeismicSample sample;
		file.close();
		return sample;
	} else {
		file.seekSet(6*requestID);
		file.read(&fileNum,2);
		file.read(&fileIndex,4);
		file.close();
	}
	
	snprintf(filename,sizeof(filename),"SAMPLES/%08x.dat",fileNum);
	file.open(filename, O_RDONLY);
	file.seekSet(fileIndex);
	
	file.read(header,25);
	
	uint32_t index = 0;
	memcpy(&ID,header+index,sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(&length,header+index,sizeof(uint8_t)); index += sizeof(uint8_t);
	memcpy(&rate,header+index,sizeof(float)); index += sizeof(float);
	memcpy(&energy,header+index,sizeof(float)); index += sizeof(float);
	memcpy(&PGA,header+index,sizeof(float)); index += sizeof(float);
	memcpy(&startTime,header+index,sizeof(uint32_t)); index += sizeof(uint32_t);
	memcpy(&endTime,header+index,sizeof(uint32_t)); index += sizeof(uint32_t);
	
	float xData[length];
	float yData[length];
	float zData[length];
	
	file.read(xData,length);
	file.read(yData,length);
	file.read(zData,length);
	
	file.close();
	
	SeismicSample sample(ID,length,rate,energy,PGA,startTime,endTime,xData,yData,zData);
	return sample;
	
} 

void DragonManager::registerSample(uint32_t ID, uint16_t fileNum, uint32_t fileIndex) {
	
	SdFile file;
	
	file.open("SAMPLES/INDEXES.dat", O_WRONLY);
	file.seekEnd();
	
	uint32_t pos = file.curPosition();
	
	if (pos < 6*ID) {
		
		uint32_t offset = 6*ID - pos;
		padZeros(&file,offset);
		
	} else {
		
		file.seekSet(6*ID);
		
	}
	
	uint8_t buf[6];
	memcpy(buf,&fileNum,2);
	memcpy(buf+2,&fileIndex,4);
	
	file.write(buf,6);
	file.close();
	
	nSamples++;
	
}

void DragonManager::registerEvent(uint32_t ID, uint16_t fileNum, uint32_t fileIndex) {
	
	SdFile file;
	
	file.open("EVENTS/INDEXES.dat", O_WRONLY);
	file.seekEnd();
	
	uint32_t pos = file.curPosition();
	
	if (pos < 6*ID) {
		
		uint32_t offset = 6*ID - pos;
		padZeros(&file,offset);
		
	} else {
		
		file.seekSet(6*ID);
		
	}
	
	uint8_t buf[6];
	memcpy(buf,&fileNum,2);
	memcpy(buf+2,&fileIndex,4);
	
	file.write(buf,6);
	file.close();
	
	nEvents++;
	
}

void DragonManager::padZeros(SdFile *file, uint32_t numZeros) {
	
	file->seekEnd();
	
	uint32_t bufSize = std::min(numZeros,(uint32_t)127);
	uint8_t buf[bufSize];
	
	for (int i = 0; i < bufSize; i++) {
		buf[i] = 0;
	}
	
	while (numZeros > 0) {
		
		file->write(buf,std::min(bufSize,numZeros));
		numZeros -= std::min(bufSize,numZeros);
		
	}
	
}

void DragonManager::createFileSystem() {
	
	SdFile file;
	
	sd->chdir("/");
	
	if (!sd->exists("SYSTEM")) {
		sd->mkdir("SYSTEM");
	}
	
	if (!sd->exists("SAMPLES")) {
		sd->mkdir("SAMPLES");
	}
	
	if (!sd->exists("EVENTS")) {
		sd->mkdir("EVENTS");
	}
	
	if (!sd->exists("MSG_HISTORY")) {
		sd->mkdir("MSG_HISTORY");
	}
	
	if (!sd->exists("SAMPLES/INDEXES.dat")) {
		file.open("SAMPLES/INDEXES.dat",O_CREAT | O_WRONLY);
		file.close();
	}
	
	if (!sd->exists("EVENTS/INDEXES.dat")) {
		file.open("EVENTS/INDEXES.dat",O_CREAT | O_WRONLY);
		file.close();
	}
	
	
}