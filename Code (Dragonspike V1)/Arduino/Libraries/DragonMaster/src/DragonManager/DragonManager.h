/*	
	Dragon manager class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#ifndef DragonManager_h

#define DragonManager_h

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#include "DragonSeismic/SeismicSample.h"
#include "DragonSeismic/SeismicEvent.h"
#include "SdFat.h"
#include "ArduinoSTL.h"
#include <set>
#include <iterator>

class DragonManager {
	
	private:
	
		SdFat *sd;
		
		struct energyIDPair {
			uint32_t ID;
			float energy;
		};
		
		friend bool operator<(energyIDPair a, energyIDPair b) {
			if (a.energy < b.energy) {
				return true;
			} else {
				return false;
			}
		}
	
		std::multiset<energyIDPair> topEvents;
		uint8_t nTopEvents;
		
		uint32_t nSamples, samplesPerFile;
		uint32_t nEvents, eventsPerFile;
		
		void createFileSystem();
		void padZeros(SdFile *file, uint32_t numZeros);
		void registerSample(uint32_t ID, uint16_t fileNum, uint32_t fileIndex);
		void registerEvent(uint32_t ID, uint16_t fileNum, uint32_t fileIndex);
		
	
	public:
	
		DragonManager(uint8_t numTopEvents, uint32_t samplesPerFile, uint32_t eventsPerFile, SdFat *sdObject);
		void begin(uint32_t *initSampleID, uint32_t *initEventID);
		void saveSample(SeismicSample *sample);
		void saveEvent(SeismicEvent *event);
		void backup();
		SeismicSample loadSample(uint32_t requestID);
		SeismicEvent loadEvent(uint32_t requestID);
		SeismicEvent getTopEvent();
		void deleteTopEvent();
	
};

#endif