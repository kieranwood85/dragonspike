/*
    Dragon radio library for Arduino
    Created by Samuel P. Pownall, March 2019
    Autocoded using MATLAB
*/

#ifndef DragonRadio_h

#define DragonRadio_h
#include "RHDatagram.h"
#include "DragonPacket.h"
#include <list>
#include <iterator>
#include "Messages/DragonMessage_Heartbeat.h"
#include "Messages/DragonMessage_RequestSeismicEvent.h"
#include "Messages/DragonMessage_SeismicEventSummary.h"
#include "Messages/DragonMessage_RequestSeismicEventMulti.h"
#include "Messages/DragonMessage_SeismicEventSummaryMulti.h"
#include "Messages/DragonMessage_RequestSeismicSample.h"
#include "Messages/DragonMessage_SeismicSampleSummary.h"
#include "Messages/DragonMessage_RequestSeismicSampleMulti.h"
#include "Messages/DragonMessage_SeismicSampleSummaryMulti.h"
#include "Messages/DragonMessage_RequestSeismicEventTopSamples.h"
#include "Messages/DragonMessage_RequestTopSeismicEvents.h"
#include "Messages/DragonMessage_DeleteTopSeismicEvents.h"
#include "Messages/DragonMessage_RequestSeismicSampleFFT.h"
#include "Messages/DragonMessage_RequestSeismicSampleFFTMulti.h"
#include "Messages/DragonMessage_SeismicSampleFFT.h"
#include "Messages/DragonMessage_SeismicSampleFFTMulti.h"

class DragonRadio {

    private:

        RHDatagram *manager;
        std::list<DragonPacket> packetBuf;
        uint8_t bufSize;

    public:

        DragonRadio(RHDatagram *radioManager, uint8_t bufSize);
        void send(DragonPacket *packet, uint8_t address);
        void update();
        bool available();
        DragonPacket read();

        enum ID {
			Heartbeat=1,
			RequestSeismicEvent=2,
			SeismicEventSummary=3,
			RequestSeismicEventMulti=4,
			SeismicEventSummaryMulti=5,
			RequestSeismicSample=6,
			SeismicSampleSummary=7,
			RequestSeismicSampleMulti=8,
			SeismicSampleSummaryMulti=9,
			RequestSeismicEventTopSamples=10,
			RequestTopSeismicEvents=11,
			DeleteTopSeismicEvents=12,
			RequestSeismicSampleFFT=13,
			RequestSeismicSampleFFTMulti=14,
			SeismicSampleFFT=15,
			SeismicSampleFFTMulti=16,
		};

};

#endif