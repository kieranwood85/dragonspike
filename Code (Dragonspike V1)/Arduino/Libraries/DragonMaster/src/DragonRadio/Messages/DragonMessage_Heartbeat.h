/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_Heartbeat
    ID: 1
    Description: Heartbeat message for checking alive status of Dragon Eggs

    Fields:
        
		Name: topEventID
		Type: uint32_t
		Description: Top event ID

		Name: numRSAMs
		Type: uint8_t
		Description: Number of RSAM samples available

		Name: RSAMs
		Type: float[numRSAMs]
		Description: RSAM samples

		Name: status
		Type: uint8_t
		Description: Dragon Egg status flags

	Autocoded using MATLAB
*/

#ifndef DragonMessage_Heartbeat_h

#define DragonMessage_Heartbeat_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_Heartbeat {

    public:

        const uint8_t msgID = 1;
        uint8_t sendID, recvID, size, sequence;
       
        uint32_t topEventID;
        uint8_t numRSAMs;
        float *RSAMs;
        uint8_t status;

        // Construct a Heartbeat message from a DragonPacket
        DragonMessage_Heartbeat(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&topEventID,packet->payload+index,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(&numRSAMs,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
			RSAMs = new float[numRSAMs];
			memcpy(RSAMs,packet->payload+index,numRSAMs*sizeof(float)); index += numRSAMs*sizeof(float);
			memcpy(&status,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
        }
        
        // Construct a Heartbeat message from individual fields
        DragonMessage_Heartbeat(uint32_t topEventID,uint8_t numRSAMs,float *RSAMs,uint8_t status)
            :size(0+1*sizeof(uint32_t)+1*sizeof(uint8_t)+numRSAMs*sizeof(float)+1*sizeof(uint8_t))
			,topEventID(topEventID)
			,numRSAMs(numRSAMs)
			,RSAMs(new float[numRSAMs])
			,status(status)
		{
			memcpy(this->RSAMs,RSAMs,numRSAMs*sizeof(float));
		}

        // Pack a Heartbeat message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&topEventID,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(buf+index,&numRSAMs,sizeof(uint8_t)); index += sizeof(uint8_t);
			memcpy(buf+index,RSAMs,numRSAMs*sizeof(float)); index += numRSAMs*sizeof(float);
			memcpy(buf+index,&status,sizeof(uint8_t)); index += sizeof(uint8_t);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: Heartbeat"));
            Serial.println(F("ID: 1"));
            Serial.println(F("Fields:"));

			Serial.print(F("	topEventID: "));
            Serial.print(topEventID,10); Serial.println();

			Serial.print(F("	numRSAMs: "));
            Serial.print(numRSAMs,10); Serial.println();

			Serial.print(F("	RSAMs: "));
            for (int i=0; i<numRSAMs; i++) {
				Serial.print(RSAMs[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	status: "));
            Serial.print(status,10); Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: Heartbeat"));
            file.println(F("ID: 1"));
            file.println(F("Fields:"));

			file.print(F("	topEventID: "));
            file.print(topEventID,10); file.println();

			file.print(F("	numRSAMs: "));
            file.print(numRSAMs,10); file.println();

			file.print(F("	RSAMs: "));
            for (int i=0; i<numRSAMs; i++) {
				file.print(RSAMs[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	status: "));
            file.print(status,10); file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_Heartbeat() {

			delete [] RSAMs;

        }

};

#endif