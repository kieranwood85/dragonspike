/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_RequestSeismicEvent
    ID: 2
    Description: Request seismic event data for a single event

    Fields:
        
		Name: ID
		Type: uint32_t
		Description: Request event with this ID

	Autocoded using MATLAB
*/

#ifndef DragonMessage_RequestSeismicEvent_h

#define DragonMessage_RequestSeismicEvent_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_RequestSeismicEvent {

    public:

        const uint8_t msgID = 2;
        uint8_t sendID, recvID, size, sequence;
       
        uint32_t ID;

        // Construct a RequestSeismicEvent message from a DragonPacket
        DragonMessage_RequestSeismicEvent(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&ID,packet->payload+index,sizeof(uint32_t)); index += sizeof(uint32_t);
        }
        
        // Construct a RequestSeismicEvent message from individual fields
        DragonMessage_RequestSeismicEvent(uint32_t ID)
            :size(0+1*sizeof(uint32_t))
			,ID(ID)
		{
		}

        // Pack a RequestSeismicEvent message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&ID,sizeof(uint32_t)); index += sizeof(uint32_t);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: RequestSeismicEvent"));
            Serial.println(F("ID: 2"));
            Serial.println(F("Fields:"));

			Serial.print(F("	ID: "));
            Serial.print(ID,10); Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: RequestSeismicEvent"));
            file.println(F("ID: 2"));
            file.println(F("Fields:"));

			file.print(F("	ID: "));
            file.print(ID,10); file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_RequestSeismicEvent() {


        }

};

#endif