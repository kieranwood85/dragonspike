/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_RequestSeismicSample
    ID: 6
    Description: Request seismic sample data for a single sample

    Fields:
        
		Name: ID
		Type: uint32_t
		Description: Request sample with this ID

	Autocoded using MATLAB
*/

#ifndef DragonMessage_RequestSeismicSample_h

#define DragonMessage_RequestSeismicSample_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_RequestSeismicSample {

    public:

        const uint8_t msgID = 6;
        uint8_t sendID, recvID, size, sequence;
       
        uint32_t ID;

        // Construct a RequestSeismicSample message from a DragonPacket
        DragonMessage_RequestSeismicSample(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&ID,packet->payload+index,sizeof(uint32_t)); index += sizeof(uint32_t);
        }
        
        // Construct a RequestSeismicSample message from individual fields
        DragonMessage_RequestSeismicSample(uint32_t ID)
            :size(0+1*sizeof(uint32_t))
			,ID(ID)
		{
		}

        // Pack a RequestSeismicSample message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&ID,sizeof(uint32_t)); index += sizeof(uint32_t);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: RequestSeismicSample"));
            Serial.println(F("ID: 6"));
            Serial.println(F("Fields:"));

			Serial.print(F("	ID: "));
            Serial.print(ID,10); Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: RequestSeismicSample"));
            file.println(F("ID: 6"));
            file.println(F("Fields:"));

			file.print(F("	ID: "));
            file.print(ID,10); file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_RequestSeismicSample() {


        }

};

#endif