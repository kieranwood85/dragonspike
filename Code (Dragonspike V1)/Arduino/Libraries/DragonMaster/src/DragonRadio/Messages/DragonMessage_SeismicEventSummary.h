/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_SeismicEventSummary
    ID: 3
    Description: Seismic event summary for a single event

    Fields:
        
		Name: ID
		Type: uint32_t
		Description: Event ID

		Name: startTime
		Type: uint32_t
		Description: Event start time

		Name: endTime
		Type: uint32_t
		Description: Event end time

		Name: energy
		Type: float
		Description: Event energy

		Name: PGA
		Type: float
		Description: Event peak ground acceleration

	Autocoded using MATLAB
*/

#ifndef DragonMessage_SeismicEventSummary_h

#define DragonMessage_SeismicEventSummary_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_SeismicEventSummary {

    public:

        const uint8_t msgID = 3;
        uint8_t sendID, recvID, size, sequence;
       
        uint32_t ID;
        uint32_t startTime;
        uint32_t endTime;
        float energy;
        float PGA;

        // Construct a SeismicEventSummary message from a DragonPacket
        DragonMessage_SeismicEventSummary(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&ID,packet->payload+index,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(&startTime,packet->payload+index,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(&endTime,packet->payload+index,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(&energy,packet->payload+index,sizeof(float)); index += sizeof(float);
			memcpy(&PGA,packet->payload+index,sizeof(float)); index += sizeof(float);
        }
        
        // Construct a SeismicEventSummary message from individual fields
        DragonMessage_SeismicEventSummary(uint32_t ID,uint32_t startTime,uint32_t endTime,float energy,float PGA)
            :size(0+1*sizeof(uint32_t)+1*sizeof(uint32_t)+1*sizeof(uint32_t)+1*sizeof(float)+1*sizeof(float))
			,ID(ID)
			,startTime(startTime)
			,endTime(endTime)
			,energy(energy)
			,PGA(PGA)
		{
		}

        // Pack a SeismicEventSummary message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&ID,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(buf+index,&startTime,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(buf+index,&endTime,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(buf+index,&energy,sizeof(float)); index += sizeof(float);
			memcpy(buf+index,&PGA,sizeof(float)); index += sizeof(float);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: SeismicEventSummary"));
            Serial.println(F("ID: 3"));
            Serial.println(F("Fields:"));

			Serial.print(F("	ID: "));
            Serial.print(ID,10); Serial.println();

			Serial.print(F("	startTime: "));
            Serial.print(startTime,10); Serial.println();

			Serial.print(F("	endTime: "));
            Serial.print(endTime,10); Serial.println();

			Serial.print(F("	energy: "));
            Serial.print(energy,10); Serial.println();

			Serial.print(F("	PGA: "));
            Serial.print(PGA,10); Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: SeismicEventSummary"));
            file.println(F("ID: 3"));
            file.println(F("Fields:"));

			file.print(F("	ID: "));
            file.print(ID,10); file.println();

			file.print(F("	startTime: "));
            file.print(startTime,10); file.println();

			file.print(F("	endTime: "));
            file.print(endTime,10); file.println();

			file.print(F("	energy: "));
            file.print(energy,10); file.println();

			file.print(F("	PGA: "));
            file.print(PGA,10); file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_SeismicEventSummary() {


        }

};

#endif