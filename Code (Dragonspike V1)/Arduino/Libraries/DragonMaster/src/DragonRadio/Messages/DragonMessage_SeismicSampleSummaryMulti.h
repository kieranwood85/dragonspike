/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_SeismicSampleSummaryMulti
    ID: 9
    Description: Seismic sample summary for multiple samples

    Fields:
        
		Name: numSamples
		Type: uint8_t
		Description: Number of samples in message

		Name: IDs
		Type: uint32_t[numSamples]
		Description: List of sample IDs

		Name: startTimes
		Type: uint32_t[numSamples]
		Description: List of sample start times

		Name: endTimes
		Type: uint32_t[numSamples]
		Description: List of sample end times

		Name: rate
		Type: float[numSamples]
		Description: List of sample rates

		Name: energies
		Type: float[numSamples]
		Description: List of sample energies

		Name: PGAs
		Type: float[numSamples]
		Description: List of sample peak ground accelerations

	Autocoded using MATLAB
*/

#ifndef DragonMessage_SeismicSampleSummaryMulti_h

#define DragonMessage_SeismicSampleSummaryMulti_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_SeismicSampleSummaryMulti {

    public:

        const uint8_t msgID = 9;
        uint8_t sendID, recvID, size, sequence;
       
        uint8_t numSamples;
        uint32_t *IDs;
        uint32_t *startTimes;
        uint32_t *endTimes;
        float *rate;
        float *energies;
        float *PGAs;

        // Construct a SeismicSampleSummaryMulti message from a DragonPacket
        DragonMessage_SeismicSampleSummaryMulti(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&numSamples,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
			IDs = new uint32_t[numSamples];
			memcpy(IDs,packet->payload+index,numSamples*sizeof(uint32_t)); index += numSamples*sizeof(uint32_t);
			startTimes = new uint32_t[numSamples];
			memcpy(startTimes,packet->payload+index,numSamples*sizeof(uint32_t)); index += numSamples*sizeof(uint32_t);
			endTimes = new uint32_t[numSamples];
			memcpy(endTimes,packet->payload+index,numSamples*sizeof(uint32_t)); index += numSamples*sizeof(uint32_t);
			rate = new float[numSamples];
			memcpy(rate,packet->payload+index,numSamples*sizeof(float)); index += numSamples*sizeof(float);
			energies = new float[numSamples];
			memcpy(energies,packet->payload+index,numSamples*sizeof(float)); index += numSamples*sizeof(float);
			PGAs = new float[numSamples];
			memcpy(PGAs,packet->payload+index,numSamples*sizeof(float)); index += numSamples*sizeof(float);
        }
        
        // Construct a SeismicSampleSummaryMulti message from individual fields
        DragonMessage_SeismicSampleSummaryMulti(uint8_t numSamples,uint32_t *IDs,uint32_t *startTimes,uint32_t *endTimes,float *rate,float *energies,float *PGAs)
            :size(0+1*sizeof(uint8_t)+numSamples*sizeof(uint32_t)+numSamples*sizeof(uint32_t)+numSamples*sizeof(uint32_t)+numSamples*sizeof(float)+numSamples*sizeof(float)+numSamples*sizeof(float))
			,numSamples(numSamples)
			,IDs(new uint32_t[numSamples])
			,startTimes(new uint32_t[numSamples])
			,endTimes(new uint32_t[numSamples])
			,rate(new float[numSamples])
			,energies(new float[numSamples])
			,PGAs(new float[numSamples])
		{
			memcpy(this->IDs,IDs,numSamples*sizeof(uint32_t));
			memcpy(this->startTimes,startTimes,numSamples*sizeof(uint32_t));
			memcpy(this->endTimes,endTimes,numSamples*sizeof(uint32_t));
			memcpy(this->rate,rate,numSamples*sizeof(float));
			memcpy(this->energies,energies,numSamples*sizeof(float));
			memcpy(this->PGAs,PGAs,numSamples*sizeof(float));
		}

        // Pack a SeismicSampleSummaryMulti message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&numSamples,sizeof(uint8_t)); index += sizeof(uint8_t);
			memcpy(buf+index,IDs,numSamples*sizeof(uint32_t)); index += numSamples*sizeof(uint32_t);
			memcpy(buf+index,startTimes,numSamples*sizeof(uint32_t)); index += numSamples*sizeof(uint32_t);
			memcpy(buf+index,endTimes,numSamples*sizeof(uint32_t)); index += numSamples*sizeof(uint32_t);
			memcpy(buf+index,rate,numSamples*sizeof(float)); index += numSamples*sizeof(float);
			memcpy(buf+index,energies,numSamples*sizeof(float)); index += numSamples*sizeof(float);
			memcpy(buf+index,PGAs,numSamples*sizeof(float)); index += numSamples*sizeof(float);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: SeismicSampleSummaryMulti"));
            Serial.println(F("ID: 9"));
            Serial.println(F("Fields:"));

			Serial.print(F("	numSamples: "));
            Serial.print(numSamples,10); Serial.println();

			Serial.print(F("	IDs: "));
            for (int i=0; i<numSamples; i++) {
				Serial.print(IDs[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	startTimes: "));
            for (int i=0; i<numSamples; i++) {
				Serial.print(startTimes[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	endTimes: "));
            for (int i=0; i<numSamples; i++) {
				Serial.print(endTimes[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	rate: "));
            for (int i=0; i<numSamples; i++) {
				Serial.print(rate[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	energies: "));
            for (int i=0; i<numSamples; i++) {
				Serial.print(energies[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	PGAs: "));
            for (int i=0; i<numSamples; i++) {
				Serial.print(PGAs[i],10); Serial.print(F(" | "));
			} Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: SeismicSampleSummaryMulti"));
            file.println(F("ID: 9"));
            file.println(F("Fields:"));

			file.print(F("	numSamples: "));
            file.print(numSamples,10); file.println();

			file.print(F("	IDs: "));
            for (int i=0; i<numSamples; i++) {
				file.print(IDs[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	startTimes: "));
            for (int i=0; i<numSamples; i++) {
				file.print(startTimes[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	endTimes: "));
            for (int i=0; i<numSamples; i++) {
				file.print(endTimes[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	rate: "));
            for (int i=0; i<numSamples; i++) {
				file.print(rate[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	energies: "));
            for (int i=0; i<numSamples; i++) {
				file.print(energies[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	PGAs: "));
            for (int i=0; i<numSamples; i++) {
				file.print(PGAs[i],10); file.print(F(" | "));
			} file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_SeismicSampleSummaryMulti() {

			delete [] IDs;
			delete [] startTimes;
			delete [] endTimes;
			delete [] rate;
			delete [] energies;
			delete [] PGAs;

        }

};

#endif