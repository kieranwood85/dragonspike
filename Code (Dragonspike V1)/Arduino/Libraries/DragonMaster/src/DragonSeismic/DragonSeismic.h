/*	
	Dragon Egg seismic library for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#ifndef DragonSeismic_h

#define DragonSeismic_h
#include "SeismicMonitor.h"
#include "SeismicEventDetector.h"
#include "SeismicSample.h"
#include "SeismicEvent.h"

#endif