/*	
	Seismic event class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#include "Arduino.h"
#include "SeismicEvent.h"

SeismicEvent::SeismicEvent(uint32_t ID, uint32_t startID, uint32_t endID, uint32_t startTime, uint32_t endTime, float energy, float PGA, uint8_t nTopSamples, uint32_t *topSampleIDs)
	:ID(ID)
	,startID(startID)
	,endID(endID)
	,startTime(startTime)
	,endTime(endTime)
	,energy(energy)
	,PGA(PGA)
	,nTopSamples(nTopSamples)
	,topSampleIDs(new uint32_t[nTopSamples])
{
	memcpy(this->topSampleIDs,topSampleIDs,nTopSamples*sizeof(uint32_t));
}

SeismicEvent::SeismicEvent(const SeismicEvent &event) 
	:ID(event.ID)
	,startID(event.startID)
	,endID(event.endID)
	,startTime(event.startTime)
	,endTime(event.endTime)
	,energy(event.energy)
	,PGA(event.PGA)
	,nTopSamples(event.nTopSamples)
	,topSampleIDs(new uint32_t[event.nTopSamples])
{
	memcpy(topSampleIDs,event.topSampleIDs,event.nTopSamples*sizeof(uint32_t));
}

SeismicEvent::SeismicEvent()
	:ID(0)
	,startID(0)
	,endID(0)
	,startTime(0)
	,endTime(0)
	,energy(0)
	,PGA(0)
	,nTopSamples(0)
	,topSampleIDs(new uint32_t[1])
{
	topSampleIDs[0] = 0;
}

SeismicEvent::~SeismicEvent() {
	
	delete [] topSampleIDs;
	
}