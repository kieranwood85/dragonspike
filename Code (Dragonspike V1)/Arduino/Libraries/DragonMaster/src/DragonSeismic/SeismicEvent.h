/*	
	Seismic event class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#ifndef SeismicEvent_h
#define SeismicEvent_h

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#include "Arduino.h"

class SeismicEvent {
	
	public:
	
		uint8_t nTopSamples;
		uint32_t *topSampleIDs;
		uint32_t ID, startID, endID, startTime, endTime;
		float energy, PGA;
	
		SeismicEvent(uint32_t ID, uint32_t startID, uint32_t endID, uint32_t startTime, uint32_t endTime, float energy, float PGA, uint8_t nTopSamples, uint32_t *topSampleIDs);
		SeismicEvent(const SeismicEvent &event);
		SeismicEvent();
		~SeismicEvent();
	
};

#endif