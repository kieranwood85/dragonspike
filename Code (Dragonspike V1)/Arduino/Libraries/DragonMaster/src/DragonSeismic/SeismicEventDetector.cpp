/*	
	Seismic event detector class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#include "Arduino.h"
#include "SeismicEventDetector.h"

SeismicEventDetector::SeismicEventDetector(float eventThreshold, uint8_t nTopSamples)
	:threshold(eventThreshold)
	,startID(0)
	,endID(0)
	,energy(0)
	,PGA(0)
	,thisID(1)
	,counter(0)
	,nTopSamples(nTopSamples)
	,lastTriggered(false)
	,isAvailable(false)
{
	
}

void SeismicEventDetector::update(SeismicSample *sample, SeismicMonitor *monitor) {
	
	float triggerEnergy = threshold*monitor->getMeanEnergy();
	energyIDPair pair;
	
	pair.ID = sample->ID;
	pair.energy = sample->energy;
	
	if (!lastTriggered && sample->energy >= triggerEnergy) {
		
		counter = 1;
		startID = sample->ID;
		endID = sample->ID;
		startTime = sample->startTime;
		endTime = sample->endTime;
		energy = sample->energy;
		PGA = sample->PGA;
		topSamples.insert(pair);
		lastTriggered = true;
		
	} else if (sample->energy >= triggerEnergy) {
		
		endID = sample->ID;
		endTime = sample->endTime;
		energy += sample->energy;
		
		topSamples.insert(pair);
		
		if (sample->PGA > PGA) {
			PGA = sample->PGA;
		}
		
		if (topSamples.size() > nTopSamples) {
			topSamples.erase(topSamples.begin());
		}
		
		counter++;
		lastTriggered = true;
		
	} else if (lastTriggered && sample->energy < triggerEnergy) {
		
		lastTriggered = false;
		isAvailable = true;
		
		std::multiset<energyIDPair>::iterator it = topSamples.begin();
		uint32_t topSampleIDs[topSamples.size()];
		
		for (int i = 0; i < topSamples.size(); i++) {
			pair = *it;
			topSampleIDs[i] = pair.ID;
			++it;
		}
		
		delete event;
		event = new SeismicEvent(thisID,startID,endID,startTime,endTime,energy,PGA,topSamples.size(),topSampleIDs);
		
		topSamples.clear();
		thisID++;
		
	} else {
		
		lastTriggered = false;
		
	}
	
}

void SeismicEventDetector::begin(uint32_t initID) {
	
	lastTriggered = false;
	isAvailable = false;
	thisID = initID;
	
}

bool SeismicEventDetector::available() {
	
	return isAvailable;
	
}

SeismicEvent SeismicEventDetector::getEvent() {
	
	isAvailable = false;
	return *event;
	
}

SeismicEventDetector::~SeismicEventDetector() {
	
	delete event;
	
}