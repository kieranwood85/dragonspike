/*	
	Seismic event detector class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#ifndef SeismicEventDetector_h

#define SeismicEventDetector_h
#include "Arduino.h"
#include "SeismicSample.h"
#include "SeismicMonitor.h"
#include "SeismicEvent.h"
#include "ArduinoSTL.h"
#include <set>
#include <iterator>

class SeismicEventDetector {
		
	private: 
		
		struct energyIDPair {
			uint32_t ID;
			float energy;
		};
		
		friend bool operator<(energyIDPair a, energyIDPair b) {
			if (a.energy < b.energy) {
				return true;
			} else {
				return false;
			}
		}
		
		std::multiset<energyIDPair> topSamples;
		uint8_t nTopSamples;
		
		SeismicEvent *event;
		uint32_t startID, endID, thisID, startTime, endTime, counter;
		float energy, PGA, threshold;
		bool lastTriggered, isAvailable;
		
	public:
	
		SeismicEventDetector(float eventThreshold, uint8_t nTopSamples);
		~SeismicEventDetector();
		void update(SeismicSample *sample,SeismicMonitor *monitor);
		bool available();
		void begin(uint32_t initID);
		SeismicEvent getEvent();
	
};

#endif