/*	
	Dragon Egg seismic monitor class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#include "Arduino.h"
#include "SeismicMonitor.h"

SeismicMonitor::SeismicMonitor(uint8_t length, float sampleRate, float lowerCutoffFreq, float upperCutoffFreq, float backgroundThreshold, void (*accFunc)(float*,float*,float*)) 
	:xFilter(new DragonFilter(DragonFilter::Type::BandPass,0,sampleRate,lowerCutoffFreq,upperCutoffFreq))
	,yFilter(new DragonFilter(DragonFilter::Type::BandPass,0,sampleRate,lowerCutoffFreq,upperCutoffFreq))
	,zFilter(new DragonFilter(DragonFilter::Type::BandPass,0,sampleRate,lowerCutoffFreq,upperCutoffFreq))
	,sampleTimer(new DragonTimer(1000000*sampleRate))
	,RSAMTimer(new DragonTimer(1000000*600))
	,mags(new float[length])
	,xData(new float[length])
	,yData(new float[length])
	,zData(new float[length])
	,isAvailable(false)
	,rate(sampleRate)
	,totalEnergy(0)
	,meanEnergy(0)
	,threshold(backgroundThreshold)
	,counter(0)
	,meanN(0)
	,PGA(0)
	,RSAM(0)
	,RSAMN(0)
	,sampleLength(length)
	,thisID(1)
	,readAccel(accFunc)
{

}

void SeismicMonitor::begin(uint32_t initSampleID, uint32_t initMeanN, float initMeanEnergy) {
	
	isAvailable = false;
	counter = 0;
	totalEnergy = 0;
	PGA = 0;
	thisID = initSampleID;
	meanN = initMeanN;
	meanEnergy = initMeanEnergy;
	sampleTimer->reset();
	RSAMTimer->reset();
	
}

void SeismicMonitor::update() {
	
	if (sampleTimer->update()) {
		
		if (counter == 0) {
			startTime = sampleTimer->getMillis();
		}
		
		float xAcc,yAcc,zAcc,thisEnergy,thisAcc;
		readAccel(&xAcc,&yAcc,&zAcc);
		
		xAcc = xFilter->filter(xAcc);
		yAcc = yFilter->filter(yAcc);
		zAcc = zFilter->filter(zAcc);
		
		thisEnergy = (pow(xAcc,2)+pow(yAcc,2)+pow(zAcc,2))*rate;
		thisAcc = sqrt(thisEnergy/rate);
		
		RSAM += (thisAcc - RSAM)/(RSAMN+1);
		RSAMN++;
		
		if (thisAcc > PGA) {
			PGA = thisAcc;
		}
		
		xData[counter] = xAcc;
		yData[counter] = yAcc;
		zData[counter] = zAcc;
		
		mags[counter] = sqrt(thisEnergy/rate);
		
		totalEnergy += thisEnergy;
		
		if (counter < sampleLength-1) {
			
			counter++;
			
		} else {
			
			if (meanN == 0) {
				meanEnergy += (totalEnergy - meanEnergy)/(meanN+1);
			} else {
				meanEnergy += (std::min((float)totalEnergy,(float)meanEnergy*threshold) - meanEnergy)/(meanN+1);
			}
			
			meanN++;
			
			delete sample;
			sample = new SeismicSample(thisID,sampleLength,rate,totalEnergy,PGA,startTime,sampleTimer->getMillis(),xData,yData,zData);
			counter = 0;
			isAvailable = true;
			totalEnergy = 0;
			PGA = 0;
			thisID++;;
			
		}
		
	}
	
	if (RSAMTimer->update()) {
		
		if (RSAMList.size() == 48) {
			RSAMList.pop_front();
		}
		
		RSAMList.push_back(RSAM);
		
		RSAM = 0;
		RSAMN = 0;
		
	}
	
}

bool SeismicMonitor::available() {
	
	return isAvailable;
	
}

float SeismicMonitor::getMeanEnergy() {
	
	return meanEnergy;
	
}

void SeismicMonitor::getRSAM(uint8_t *numRSAM, float *bufRSAM) {

	*numRSAM = std::min((uint8_t)RSAMList.size(),(uint8_t)*numRSAM);
	
	std::deque<float>::iterator it = RSAMList.begin();
	
	for (int i=0; i < *numRSAM; i++) {
		bufRSAM[i] = *it;
		++it;
	}
	
}

SeismicSample SeismicMonitor::getSample() {
	
	isAvailable = false;
	return *sample;
	
}

SeismicMonitor::~SeismicMonitor(){
	
	delete sampleTimer;
	delete xFilter;
	delete yFilter;
	delete zFilter;
	delete sample;
	
	delete [] mags;
	delete [] xData;
	delete [] yData;
	delete [] zData;
	
}