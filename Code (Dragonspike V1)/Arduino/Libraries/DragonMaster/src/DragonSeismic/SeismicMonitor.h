/*	
	Dragon Egg seismic monitor class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#ifndef SeismicMonitor_h

#define SeismicMonitor_h
#include "Arduino.h"
#include "SeismicSample.h"
#include "DragonUtils/DragonFilter.h"
#include "DragonUtils/DragonTimer.h"
#include <queue>
#include <iterator>

class SeismicMonitor {
	
	private:
	
		DragonTimer *sampleTimer, *RSAMTimer;
		DragonFilter *xFilter, *yFilter, *zFilter;
		SeismicSample *sample;
		
		std::deque<float> RSAMList;
		
		void (*readAccel)(float*,float*,float*);
		float *mags, *xData, *yData, *zData, RSAM;
		
		bool isAvailable;
		float rate, totalEnergy, meanEnergy, threshold, PGA;
		uint8_t counter, sampleLength;
		uint32_t startTime, thisID, meanN, RSAMN;
	
	public:
	
		SeismicMonitor(uint8_t length, float sampleRate, float lowerCutoffFreq, float upperCutoffFreq, float backgroundThreshold, void (*accFunc)(float*,float*,float*));
		~SeismicMonitor();
		
		void update();
		void begin(uint32_t initSampleID, uint32_t initMeanN, float initMeanEnergy);
		bool available();
		float getMeanEnergy();
		void getRSAM(uint8_t *numRSAM, float *bufRSAM);
		SeismicSample getSample();
	
};

#endif