/*	
	Dragon Egg seismic sample class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#include "Arduino.h"
#include "SeismicSample.h"
#include "arduinoFFT.h"
#include <math.h>
#include <set>
#include <iterator>

SeismicSample::SeismicSample(uint32_t ID, uint8_t length, float rate, float energy, float PGA, uint32_t startTime, uint32_t endTime, float *xData, float *yData, float *zData)
	:ID(ID)
	,length(length)
	,rate(rate)
	,energy(energy)
	,PGA(PGA)
	,startTime(startTime)
	,endTime(endTime)
	,xData(new float[length])
	,yData(new float[length])
	,zData(new float[length])
{
	memcpy(this->xData,xData,length);
	memcpy(this->yData,yData,length);
	memcpy(this->zData,zData,length);
}

SeismicSample::SeismicSample(const SeismicSample &sample) 
	:ID(sample.ID)
	,length(sample.length)
	,rate(sample.rate)
	,energy(sample.energy)
	,PGA(sample.PGA)
	,startTime(sample.startTime)
	,endTime(sample.endTime)
	,xData(new float[length])
	,yData(new float[length])
	,zData(new float[length])
{
	memcpy(xData,sample.xData,length);
	memcpy(yData,sample.yData,length);
	memcpy(zData,sample.zData,length);
}

SeismicSample::SeismicSample()
	:ID(0)
	,length(0)
	,rate(0)
	,energy(0)
	,PGA(0)
	,startTime(0)
	,endTime(0)
	,xData(new float[1])
	,yData(new float[1])
	,zData(new float[1])
{
	xData[0] = 0;
	yData[0] = 0;
	zData[0] = 0;
}

void SeismicSample::computeFFT(uint8_t axis, float *frequencies, float *magnitudes, uint16_t *numBins) {
	
	uint32_t lengthFFT = SeismicSample::ceilPower2(length);
	float vReal[lengthFFT], vImag[lengthFFT];
	float *accData;
	
	std::multiset<freqMagPair> outputFFT;
	std::fill_n(vReal,lengthFFT,0);
	std::fill_n(vImag,lengthFFT,0);
	
	switch (axis) {
		
		case 0:
		accData = xData;
		break;
		
		case 1:
		accData = yData;
		break;
		
		case 2:
		accData = zData;
		break;
		
		default:
		accData = xData;
		break;
		
	}
	
	memcpy(vReal,accData,length);
	arduinoFFT FFT = arduinoFFT(vReal, vImag, lengthFFT, 1/rate);
	FFT.DCRemoval();
	
	FFT.Windowing(FFT_WIN_TYP_HAMMING,FFT_FORWARD);
	FFT.Compute(FFT_FORWARD);
	
	for (int i = 0; i < lengthFFT/2; i++) {
		freqMagPair pair;
		pair.freq = M_PI*i/(rate*lengthFFT);
		pair.mag = sqrt(vReal[i]*vReal[i] + vImag[i]*vImag[i]);
		outputFFT.insert(pair);
	}
	
	std::multiset<freqMagPair>::iterator it = outputFFT.end();
	int i = 0;
	
	while (it != outputFFT.begin() && i < *numBins) {
		--it;
		freqMagPair pair = *it;
		frequencies[i] = pair.freq;
		magnitudes[i] = pair.mag;
		i++;
	}
	
	*numBins = i;
	
}

uint32_t SeismicSample::ceilPower2(uint32_t value) {
	
	value--;
	value |= value >> 1;
	value |= value >> 2;
	value |= value >> 4;
	value |= value >> 8;
	value |= value >> 16;
	value++;
	
	return value;
	
}

SeismicSample::~SeismicSample() {

	delete [] xData;
	delete [] yData;
	delete [] zData;

}

