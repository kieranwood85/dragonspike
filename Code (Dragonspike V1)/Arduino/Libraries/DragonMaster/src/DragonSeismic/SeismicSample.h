/*	
	Dragon Egg seismic sample class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#ifndef SeismicSample_h

#define SeismicSample_h
#include <Arduino.h>

class SeismicSample {
	
	private:
	
		struct freqMagPair {
			float freq;
			float mag;
		};
		
		friend bool operator<(freqMagPair a, freqMagPair b) {
			if (a.mag < b.mag) {
				return true;
			} else {
				return false;
			}
		}
	
	public:
	
		uint8_t length;
		float rate, energy, PGA;
		float *xData, *yData, *zData;
		uint32_t startTime, endTime, ID;
	
		SeismicSample(uint32_t ID, uint8_t length, float rate, float energy, float PGA, uint32_t startTime, uint32_t endTime, float *xData, float *yData, float *zData);
		SeismicSample(const SeismicSample &sample);
		SeismicSample();
		~SeismicSample();
		void computeFFT(uint8_t axis, float *frequencies, float *magnitudes, uint16_t *numBins);
		uint32_t ceilPower2(uint32_t value);

};

#endif