/*	
	Dragon filter class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#include "Arduino.h"
#include "DragonFilter.h"
#include "math.h"

DragonFilter::DragonFilter(uint8_t type, float x_init, float sampleRate, float cutoffFreq)
	:x_last(x_init)
	,x_filt_hp(x_init)
	,x_filt_lp(x_init)
	,filterType(type)
{
	switch (filterType) {
		
		case DragonFilter::Type::LowPass:
			computeAlphaLowPass(sampleRate,cutoffFreq);
			break;
			
		case DragonFilter::Type::HighPass:
			computeAlphaHighPass(sampleRate,cutoffFreq);
			break;
			
		default:
			alpha_lp = 1;
			alpha_hp = 1;
			break;
		
	}
}

DragonFilter::DragonFilter(uint8_t type, float x_init, float sampleRate, float lowerCutoffFreq, float upperCutoffFreq)
	:x_last(x_init)
	,x_filt_hp(x_init)
	,x_filt_lp(x_init)
	,filterType(type)
{
	switch (filterType) {
		
		case DragonFilter::Type::BandPass:
			computeAlphaHighPass(sampleRate,lowerCutoffFreq);
			computeAlphaLowPass(sampleRate,upperCutoffFreq);
			break;
		
		case DragonFilter::Type::BandStop:
			computeAlphaHighPass(sampleRate,upperCutoffFreq);
			computeAlphaLowPass(sampleRate,lowerCutoffFreq);
			break;
			
		default:
			alpha_lp = 1;
			alpha_hp = 1;
			break;
		
	}
}

float DragonFilter::filter(float x) {
	
	float x_filt = x;
	
	if (filterType == DragonFilter::Type::LowPass || filterType >= 2) {
		
		x_filt_lp = (1-alpha_lp)*x_filt_lp + alpha_lp*x_filt;
		x_filt = x_filt_lp;
		
	}
	
	if (filterType == DragonFilter::Type::HighPass || filterType >= 2) {
		
		x_filt_hp = alpha_hp*(x_filt_hp + x_filt - x_last);
		x_filt = x_filt_hp;
		
	}
	
	x_last = x;
	
	return x_filt;
	
}

void DragonFilter::tune(float sampleRate, float cutoffFreq) {
	
	switch (filterType) {
		
		case DragonFilter::Type::LowPass:
			computeAlphaLowPass(sampleRate,cutoffFreq);
			break;
			
		case DragonFilter::Type::HighPass:
			computeAlphaHighPass(sampleRate,cutoffFreq);
			break;
			
		default:
			break;
		
	}
	
}

void DragonFilter::tune(float sampleRate, float lowerCutoffFreq, float upperCutoffFreq) {
	
	switch (filterType) {
		
		case DragonFilter::Type::BandPass:
			computeAlphaHighPass(sampleRate,lowerCutoffFreq);
			computeAlphaLowPass(sampleRate,upperCutoffFreq);
			break;
		
		case DragonFilter::Type::BandStop:
			computeAlphaHighPass(sampleRate,upperCutoffFreq);
			computeAlphaLowPass(sampleRate,lowerCutoffFreq);
			break;
		
		default:
			break;
		
	}
	
}

void DragonFilter::reset(float x_init) {
	
	x_last = x_init;
	x_filt_hp = x_init;
	x_filt_lp = x_init;
	
}

void DragonFilter::computeAlphaLowPass(float sampleRate, float cutoffFreq) {
	
	alpha_lp = (2*M_PI*sampleRate*cutoffFreq)/(2*M_PI*sampleRate*cutoffFreq+1);
	
}

void DragonFilter::computeAlphaHighPass(float sampleRate, float cutoffFreq) {
	
	alpha_hp = 1/(2*M_PI*sampleRate*cutoffFreq+1);
	
}