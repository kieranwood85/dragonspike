/*	
	Dragon filter class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#ifndef DragonFilter_h
#define DragonFilter_h

#include "Arduino.h"

class DragonFilter {
	
	private:
	
		float alpha_hp, alpha_lp, x_last, x_filt_hp, x_filt_lp;
		uint8_t filterType;
		void computeAlphaLowPass(float sampleRate, float cutoffFreq);
		void computeAlphaHighPass(float sampleRate, float cutoffFreq);
		
	public:
	
		DragonFilter(uint8_t type, float x_init, float sampleRate, float lowerCutoffFreq, float upperCutoffFreq);
		DragonFilter(uint8_t type, float x_init, float sampleRate, float cutoffFreq);
		enum Type{LowPass = 0, HighPass = 1, BandPass = 2, BandStop = 3};
		float filter(float x);
		void tune(float sampleRate, float cutoffFreq);
		void tune(float sampleRate, float lowerCutoffFreq, float upperCutoffFreq);
		void reset(float x_init);
	
};

#endif