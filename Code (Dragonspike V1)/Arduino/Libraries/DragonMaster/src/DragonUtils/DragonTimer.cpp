/*	
	Dragon timer class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#include "Arduino.h"
#include "DragonTimer.h"

DragonTimer::DragonTimer(uint32_t duration)
	:maxTime(4294967295)
	,interval(duration)
	,overflows(0)
{
	
	uint32_t initTime = micros();
	thisTimeMillis = millis();
	lastTime = initTime;
	thisTime = initTime;
	targetTime = (initTime + interval) % maxTime;
	
}

bool DragonTimer::update() {
	
	lastTime = thisTime;
	thisTime = micros() % maxTime;
	thisTimeMillis = millis();
	
	if (thisTime < lastTime) {
		overflows++;
	}
	
	if ((thisTime >= targetTime && lastTime <= targetTime) || (thisTime >= targetTime && lastTime >= thisTime)) {
		
		if (thisTime - lastTime >= interval) {
			targetTime = (thisTime + interval) % maxTime;
		} else {
			targetTime = (targetTime + interval) % maxTime;
		}
		
		return true;
		
	} else {
		
		return false;
		
	}
	
}

uint32_t DragonTimer::getMicros() {
	
	return thisTime;
	
}

uint32_t DragonTimer::getMillis() {
	
	return thisTimeMillis;
	
}

void DragonTimer::reset() {
	
	uint32_t initTime = micros();
	lastTime = initTime;
	thisTime = initTime;
	targetTime = (initTime + interval) % maxTime;
	
}