/*	
	Dragon timer class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#ifndef DragonTimer_h
#define DragonTimer_h

#include "Arduino.h"

class DragonTimer {
	
	private:
	
		uint32_t lastTime, thisTime, targetTime, maxTime, interval, thisTimeMillis;
		uint32_t overflows;
		
	public:
		
		DragonTimer(uint32_t duration);
		bool update();
		uint32_t getMicros();
		uint32_t getMillis();
		uint32_t readDuration();
		void reset();
	
};

#endif