/*	
	Dragon packet class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#include "DragonPacket.h"
#include "Arduino.h"

DragonPacket::DragonPacket(uint8_t *buf, uint8_t len, uint8_t from, uint8_t to, uint8_t seq) 
	:sendID(from)
	,recvID(to)
	,sequence(seq)
	,size(len-1)
{
	
	payload = new uint8_t[size];
	memcpy(&msgID,buf,1);
	memcpy(payload,buf+1,size);
	
}

DragonPacket::DragonPacket(uint8_t *buf, uint8_t id, uint8_t size) 
	:msgID(id)
	,size(size)
{
	
	payload = new uint8_t[size];
	memcpy(payload,buf,size);
	
}
    
DragonPacket::DragonPacket(const DragonPacket &packet)
    :msgID(packet.msgID)
    ,sendID(packet.sendID)
    ,recvID(packet.recvID)
    ,sequence(packet.sequence)
    ,size(packet.size)
{
    payload = new uint8_t[size];
    memcpy(payload,packet.payload,size);
}
    
DragonPacket::DragonPacket()
	:msgID(0)
	,size(0)
{
		
}

void DragonPacket::send(RHDatagram *manager, uint8_t address) {
	
	uint8_t buf[size+1];
	memcpy(buf,&msgID,1);
	memcpy(buf+1,payload,size);
	
	manager->sendto(buf,size+1,address);
	
}

DragonPacket::~DragonPacket() {
	
	delete [] payload;
	
}
