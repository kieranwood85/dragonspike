/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_DeleteTopSeismicEvents
    ID: 12
    Description: Request deletion of largest event buffer elements

    Fields:
        
		Name: numEvents
		Type: uint8_t
		Description: Delete this number of largest events

	Autocoded using MATLAB
*/

#ifndef DragonMessage_DeleteTopSeismicEvents_h

#define DragonMessage_DeleteTopSeismicEvents_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_DeleteTopSeismicEvents {

    public:

        const uint8_t msgID = 12;
        uint8_t sendID, recvID, size, sequence;
       
        uint8_t numEvents;

        // Construct a DeleteTopSeismicEvents message from a DragonPacket
        DragonMessage_DeleteTopSeismicEvents(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&numEvents,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
        }
        
        // Construct a DeleteTopSeismicEvents message from individual fields
        DragonMessage_DeleteTopSeismicEvents(uint8_t numEvents)
            :size(0+1*sizeof(uint8_t))
			,numEvents(numEvents)
		{
		}

        // Pack a DeleteTopSeismicEvents message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&numEvents,sizeof(uint8_t)); index += sizeof(uint8_t);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: DeleteTopSeismicEvents"));
            Serial.println(F("ID: 12"));
            Serial.println(F("Fields:"));

			Serial.print(F("	numEvents: "));
            Serial.print(numEvents,10); Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: DeleteTopSeismicEvents"));
            file.println(F("ID: 12"));
            file.println(F("Fields:"));

			file.print(F("	numEvents: "));
            file.print(numEvents,10); file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_DeleteTopSeismicEvents() {


        }

};

#endif