/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_RequestSeismicEventMulti
    ID: 4
    Description: Request seismic event data for multiple events

    Fields:
        
		Name: numEvents
		Type: uint8_t
		Description: Number of events being requested

		Name: IDs
		Type: uint32_t[numEvents]
		Description: Request events with these IDs

	Autocoded using MATLAB
*/

#ifndef DragonMessage_RequestSeismicEventMulti_h

#define DragonMessage_RequestSeismicEventMulti_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_RequestSeismicEventMulti {

    public:

        const uint8_t msgID = 4;
        uint8_t sendID, recvID, size, sequence;
       
        uint8_t numEvents;
        uint32_t *IDs;

        // Construct a RequestSeismicEventMulti message from a DragonPacket
        DragonMessage_RequestSeismicEventMulti(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&numEvents,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
			IDs = new uint32_t[numEvents];
			memcpy(IDs,packet->payload+index,numEvents*sizeof(uint32_t)); index += numEvents*sizeof(uint32_t);
        }
        
        // Construct a RequestSeismicEventMulti message from individual fields
        DragonMessage_RequestSeismicEventMulti(uint8_t numEvents,uint32_t *IDs)
            :size(0+1*sizeof(uint8_t)+numEvents*sizeof(uint32_t))
			,numEvents(numEvents)
			,IDs(new uint32_t[numEvents])
		{
			memcpy(this->IDs,IDs,numEvents*sizeof(uint32_t));
		}

        // Pack a RequestSeismicEventMulti message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&numEvents,sizeof(uint8_t)); index += sizeof(uint8_t);
			memcpy(buf+index,IDs,numEvents*sizeof(uint32_t)); index += numEvents*sizeof(uint32_t);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: RequestSeismicEventMulti"));
            Serial.println(F("ID: 4"));
            Serial.println(F("Fields:"));

			Serial.print(F("	numEvents: "));
            Serial.print(numEvents,10); Serial.println();

			Serial.print(F("	IDs: "));
            for (int i=0; i<numEvents; i++) {
				Serial.print(IDs[i],10); Serial.print(F(" | "));
			} Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: RequestSeismicEventMulti"));
            file.println(F("ID: 4"));
            file.println(F("Fields:"));

			file.print(F("	numEvents: "));
            file.print(numEvents,10); file.println();

			file.print(F("	IDs: "));
            for (int i=0; i<numEvents; i++) {
				file.print(IDs[i],10); file.print(F(" | "));
			} file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_RequestSeismicEventMulti() {

			delete [] IDs;

        }

};

#endif