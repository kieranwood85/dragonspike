/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_RequestSeismicEventTopSamples
    ID: 10
    Description: Request top samples in a seismic event

    Fields:
        
		Name: ID
		Type: uint32_t
		Description: Request from event with this ID

		Name: numSamples
		Type: uint8_t
		Description: Number of samples requested

	Autocoded using MATLAB
*/

#ifndef DragonMessage_RequestSeismicEventTopSamples_h

#define DragonMessage_RequestSeismicEventTopSamples_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_RequestSeismicEventTopSamples {

    public:

        const uint8_t msgID = 10;
        uint8_t sendID, recvID, size, sequence;
       
        uint32_t ID;
        uint8_t numSamples;

        // Construct a RequestSeismicEventTopSamples message from a DragonPacket
        DragonMessage_RequestSeismicEventTopSamples(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&ID,packet->payload+index,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(&numSamples,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
        }
        
        // Construct a RequestSeismicEventTopSamples message from individual fields
        DragonMessage_RequestSeismicEventTopSamples(uint32_t ID,uint8_t numSamples)
            :size(0+1*sizeof(uint32_t)+1*sizeof(uint8_t))
			,ID(ID)
			,numSamples(numSamples)
		{
		}

        // Pack a RequestSeismicEventTopSamples message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&ID,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(buf+index,&numSamples,sizeof(uint8_t)); index += sizeof(uint8_t);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: RequestSeismicEventTopSamples"));
            Serial.println(F("ID: 10"));
            Serial.println(F("Fields:"));

			Serial.print(F("	ID: "));
            Serial.print(ID,10); Serial.println();

			Serial.print(F("	numSamples: "));
            Serial.print(numSamples,10); Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: RequestSeismicEventTopSamples"));
            file.println(F("ID: 10"));
            file.println(F("Fields:"));

			file.print(F("	ID: "));
            file.print(ID,10); file.println();

			file.print(F("	numSamples: "));
            file.print(numSamples,10); file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_RequestSeismicEventTopSamples() {


        }

};

#endif