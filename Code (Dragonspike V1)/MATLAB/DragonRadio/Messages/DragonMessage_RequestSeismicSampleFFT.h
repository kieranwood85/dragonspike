/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_RequestSeismicSampleFFT
    ID: 13
    Description: Request FFT of a seismic sample and return top bins

    Fields:
        
		Name: ID
		Type: uint32_t
		Description: Request FFT from sample with this ID

		Name: axis
		Type: uint8_t
		Description: Accelerometor axis to perform FFT on

		Name: numBins
		Type: uint8_t
		Description: Number of top bins to return

	Autocoded using MATLAB
*/

#ifndef DragonMessage_RequestSeismicSampleFFT_h

#define DragonMessage_RequestSeismicSampleFFT_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_RequestSeismicSampleFFT {

    public:

        const uint8_t msgID = 13;
        uint8_t sendID, recvID, size, sequence;
       
        uint32_t ID;
        uint8_t axis;
        uint8_t numBins;

        // Construct a RequestSeismicSampleFFT message from a DragonPacket
        DragonMessage_RequestSeismicSampleFFT(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&ID,packet->payload+index,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(&axis,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
			memcpy(&numBins,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
        }
        
        // Construct a RequestSeismicSampleFFT message from individual fields
        DragonMessage_RequestSeismicSampleFFT(uint32_t ID,uint8_t axis,uint8_t numBins)
            :size(0+1*sizeof(uint32_t)+1*sizeof(uint8_t)+1*sizeof(uint8_t))
			,ID(ID)
			,axis(axis)
			,numBins(numBins)
		{
		}

        // Pack a RequestSeismicSampleFFT message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&ID,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(buf+index,&axis,sizeof(uint8_t)); index += sizeof(uint8_t);
			memcpy(buf+index,&numBins,sizeof(uint8_t)); index += sizeof(uint8_t);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: RequestSeismicSampleFFT"));
            Serial.println(F("ID: 13"));
            Serial.println(F("Fields:"));

			Serial.print(F("	ID: "));
            Serial.print(ID,10); Serial.println();

			Serial.print(F("	axis: "));
            Serial.print(axis,10); Serial.println();

			Serial.print(F("	numBins: "));
            Serial.print(numBins,10); Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: RequestSeismicSampleFFT"));
            file.println(F("ID: 13"));
            file.println(F("Fields:"));

			file.print(F("	ID: "));
            file.print(ID,10); file.println();

			file.print(F("	axis: "));
            file.print(axis,10); file.println();

			file.print(F("	numBins: "));
            file.print(numBins,10); file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_RequestSeismicSampleFFT() {


        }

};

#endif