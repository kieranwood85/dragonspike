/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_RequestSeismicSampleMulti
    ID: 8
    Description: Request seismic sample data for multiple samples

    Fields:
        
		Name: numSamples
		Type: uint8_t
		Description: Number of samples being requested

		Name: IDs
		Type: uint32_t[numSamples]
		Description: Request samples with these IDs

	Autocoded using MATLAB
*/

#ifndef DragonMessage_RequestSeismicSampleMulti_h

#define DragonMessage_RequestSeismicSampleMulti_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_RequestSeismicSampleMulti {

    public:

        const uint8_t msgID = 8;
        uint8_t sendID, recvID, size, sequence;
       
        uint8_t numSamples;
        uint32_t *IDs;

        // Construct a RequestSeismicSampleMulti message from a DragonPacket
        DragonMessage_RequestSeismicSampleMulti(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&numSamples,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
			IDs = new uint32_t[numSamples];
			memcpy(IDs,packet->payload+index,numSamples*sizeof(uint32_t)); index += numSamples*sizeof(uint32_t);
        }
        
        // Construct a RequestSeismicSampleMulti message from individual fields
        DragonMessage_RequestSeismicSampleMulti(uint8_t numSamples,uint32_t *IDs)
            :size(0+1*sizeof(uint8_t)+numSamples*sizeof(uint32_t))
			,numSamples(numSamples)
			,IDs(new uint32_t[numSamples])
		{
			memcpy(this->IDs,IDs,numSamples*sizeof(uint32_t));
		}

        // Pack a RequestSeismicSampleMulti message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&numSamples,sizeof(uint8_t)); index += sizeof(uint8_t);
			memcpy(buf+index,IDs,numSamples*sizeof(uint32_t)); index += numSamples*sizeof(uint32_t);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: RequestSeismicSampleMulti"));
            Serial.println(F("ID: 8"));
            Serial.println(F("Fields:"));

			Serial.print(F("	numSamples: "));
            Serial.print(numSamples,10); Serial.println();

			Serial.print(F("	IDs: "));
            for (int i=0; i<numSamples; i++) {
				Serial.print(IDs[i],10); Serial.print(F(" | "));
			} Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: RequestSeismicSampleMulti"));
            file.println(F("ID: 8"));
            file.println(F("Fields:"));

			file.print(F("	numSamples: "));
            file.print(numSamples,10); file.println();

			file.print(F("	IDs: "));
            for (int i=0; i<numSamples; i++) {
				file.print(IDs[i],10); file.print(F(" | "));
			} file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_RequestSeismicSampleMulti() {

			delete [] IDs;

        }

};

#endif