/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_RequestTopSeismicEvents
    ID: 11
    Description: Request top seismic samples

    Fields:
        
		Name: numEvents
		Type: uint8_t
		Description: Number of events requested

	Autocoded using MATLAB
*/

#ifndef DragonMessage_RequestTopSeismicEvents_h

#define DragonMessage_RequestTopSeismicEvents_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_RequestTopSeismicEvents {

    public:

        const uint8_t msgID = 11;
        uint8_t sendID, recvID, size, sequence;
       
        uint8_t numEvents;

        // Construct a RequestTopSeismicEvents message from a DragonPacket
        DragonMessage_RequestTopSeismicEvents(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&numEvents,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
        }
        
        // Construct a RequestTopSeismicEvents message from individual fields
        DragonMessage_RequestTopSeismicEvents(uint8_t numEvents)
            :size(0+1*sizeof(uint8_t))
			,numEvents(numEvents)
		{
		}

        // Pack a RequestTopSeismicEvents message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&numEvents,sizeof(uint8_t)); index += sizeof(uint8_t);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: RequestTopSeismicEvents"));
            Serial.println(F("ID: 11"));
            Serial.println(F("Fields:"));

			Serial.print(F("	numEvents: "));
            Serial.print(numEvents,10); Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: RequestTopSeismicEvents"));
            file.println(F("ID: 11"));
            file.println(F("Fields:"));

			file.print(F("	numEvents: "));
            file.print(numEvents,10); file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_RequestTopSeismicEvents() {


        }

};

#endif