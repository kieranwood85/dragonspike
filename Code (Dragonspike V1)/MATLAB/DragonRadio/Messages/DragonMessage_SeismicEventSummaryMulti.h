/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_SeismicEventSummaryMulti
    ID: 5
    Description: Seismic event summary for multiple events

    Fields:
        
		Name: numEvents
		Type: uint8_t
		Description: Number of events in message

		Name: IDs
		Type: uint32_t[numEvents]
		Description: List of event IDs

		Name: startTimes
		Type: uint32_t[numEvents]
		Description: List of event start times

		Name: endTimes
		Type: uint32_t[numEvents]
		Description: List of event end times

		Name: energies
		Type: float[numEvents]
		Description: List of event energies

		Name: PGAs
		Type: float[numEvents]
		Description: List of event peak ground accelerations

	Autocoded using MATLAB
*/

#ifndef DragonMessage_SeismicEventSummaryMulti_h

#define DragonMessage_SeismicEventSummaryMulti_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_SeismicEventSummaryMulti {

    public:

        const uint8_t msgID = 5;
        uint8_t sendID, recvID, size, sequence;
       
        uint8_t numEvents;
        uint32_t *IDs;
        uint32_t *startTimes;
        uint32_t *endTimes;
        float *energies;
        float *PGAs;

        // Construct a SeismicEventSummaryMulti message from a DragonPacket
        DragonMessage_SeismicEventSummaryMulti(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&numEvents,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
			IDs = new uint32_t[numEvents];
			memcpy(IDs,packet->payload+index,numEvents*sizeof(uint32_t)); index += numEvents*sizeof(uint32_t);
			startTimes = new uint32_t[numEvents];
			memcpy(startTimes,packet->payload+index,numEvents*sizeof(uint32_t)); index += numEvents*sizeof(uint32_t);
			endTimes = new uint32_t[numEvents];
			memcpy(endTimes,packet->payload+index,numEvents*sizeof(uint32_t)); index += numEvents*sizeof(uint32_t);
			energies = new float[numEvents];
			memcpy(energies,packet->payload+index,numEvents*sizeof(float)); index += numEvents*sizeof(float);
			PGAs = new float[numEvents];
			memcpy(PGAs,packet->payload+index,numEvents*sizeof(float)); index += numEvents*sizeof(float);
        }
        
        // Construct a SeismicEventSummaryMulti message from individual fields
        DragonMessage_SeismicEventSummaryMulti(uint8_t numEvents,uint32_t *IDs,uint32_t *startTimes,uint32_t *endTimes,float *energies,float *PGAs)
            :size(0+1*sizeof(uint8_t)+numEvents*sizeof(uint32_t)+numEvents*sizeof(uint32_t)+numEvents*sizeof(uint32_t)+numEvents*sizeof(float)+numEvents*sizeof(float))
			,numEvents(numEvents)
			,IDs(new uint32_t[numEvents])
			,startTimes(new uint32_t[numEvents])
			,endTimes(new uint32_t[numEvents])
			,energies(new float[numEvents])
			,PGAs(new float[numEvents])
		{
			memcpy(this->IDs,IDs,numEvents*sizeof(uint32_t));
			memcpy(this->startTimes,startTimes,numEvents*sizeof(uint32_t));
			memcpy(this->endTimes,endTimes,numEvents*sizeof(uint32_t));
			memcpy(this->energies,energies,numEvents*sizeof(float));
			memcpy(this->PGAs,PGAs,numEvents*sizeof(float));
		}

        // Pack a SeismicEventSummaryMulti message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&numEvents,sizeof(uint8_t)); index += sizeof(uint8_t);
			memcpy(buf+index,IDs,numEvents*sizeof(uint32_t)); index += numEvents*sizeof(uint32_t);
			memcpy(buf+index,startTimes,numEvents*sizeof(uint32_t)); index += numEvents*sizeof(uint32_t);
			memcpy(buf+index,endTimes,numEvents*sizeof(uint32_t)); index += numEvents*sizeof(uint32_t);
			memcpy(buf+index,energies,numEvents*sizeof(float)); index += numEvents*sizeof(float);
			memcpy(buf+index,PGAs,numEvents*sizeof(float)); index += numEvents*sizeof(float);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: SeismicEventSummaryMulti"));
            Serial.println(F("ID: 5"));
            Serial.println(F("Fields:"));

			Serial.print(F("	numEvents: "));
            Serial.print(numEvents,10); Serial.println();

			Serial.print(F("	IDs: "));
            for (int i=0; i<numEvents; i++) {
				Serial.print(IDs[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	startTimes: "));
            for (int i=0; i<numEvents; i++) {
				Serial.print(startTimes[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	endTimes: "));
            for (int i=0; i<numEvents; i++) {
				Serial.print(endTimes[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	energies: "));
            for (int i=0; i<numEvents; i++) {
				Serial.print(energies[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	PGAs: "));
            for (int i=0; i<numEvents; i++) {
				Serial.print(PGAs[i],10); Serial.print(F(" | "));
			} Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: SeismicEventSummaryMulti"));
            file.println(F("ID: 5"));
            file.println(F("Fields:"));

			file.print(F("	numEvents: "));
            file.print(numEvents,10); file.println();

			file.print(F("	IDs: "));
            for (int i=0; i<numEvents; i++) {
				file.print(IDs[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	startTimes: "));
            for (int i=0; i<numEvents; i++) {
				file.print(startTimes[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	endTimes: "));
            for (int i=0; i<numEvents; i++) {
				file.print(endTimes[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	energies: "));
            for (int i=0; i<numEvents; i++) {
				file.print(energies[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	PGAs: "));
            for (int i=0; i<numEvents; i++) {
				file.print(PGAs[i],10); file.print(F(" | "));
			} file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_SeismicEventSummaryMulti() {

			delete [] IDs;
			delete [] startTimes;
			delete [] endTimes;
			delete [] energies;
			delete [] PGAs;

        }

};

#endif