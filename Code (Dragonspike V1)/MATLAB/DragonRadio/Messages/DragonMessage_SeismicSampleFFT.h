/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_SeismicSampleFFT
    ID: 15
    Description: FFT of a seismic sample

    Fields:
        
		Name: ID
		Type: uint32_t
		Description: Sample ID

		Name: numBins
		Type: uint8_t
		Description: Number of top bins returned

		Name: frequencies
		Type: float[numBins]
		Description: Centre frequencies of returned bins

		Name: amplitudes
		Type: float[numBins]
		Description: Amplitudes of returned bins

	Autocoded using MATLAB
*/

#ifndef DragonMessage_SeismicSampleFFT_h

#define DragonMessage_SeismicSampleFFT_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_SeismicSampleFFT {

    public:

        const uint8_t msgID = 15;
        uint8_t sendID, recvID, size, sequence;
       
        uint32_t ID;
        uint8_t numBins;
        float *frequencies;
        float *amplitudes;

        // Construct a SeismicSampleFFT message from a DragonPacket
        DragonMessage_SeismicSampleFFT(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&ID,packet->payload+index,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(&numBins,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
			frequencies = new float[numBins];
			memcpy(frequencies,packet->payload+index,numBins*sizeof(float)); index += numBins*sizeof(float);
			amplitudes = new float[numBins];
			memcpy(amplitudes,packet->payload+index,numBins*sizeof(float)); index += numBins*sizeof(float);
        }
        
        // Construct a SeismicSampleFFT message from individual fields
        DragonMessage_SeismicSampleFFT(uint32_t ID,uint8_t numBins,float *frequencies,float *amplitudes)
            :size(0+1*sizeof(uint32_t)+1*sizeof(uint8_t)+numBins*sizeof(float)+numBins*sizeof(float))
			,ID(ID)
			,numBins(numBins)
			,frequencies(new float[numBins])
			,amplitudes(new float[numBins])
		{
			memcpy(this->frequencies,frequencies,numBins*sizeof(float));
			memcpy(this->amplitudes,amplitudes,numBins*sizeof(float));
		}

        // Pack a SeismicSampleFFT message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&ID,sizeof(uint32_t)); index += sizeof(uint32_t);
			memcpy(buf+index,&numBins,sizeof(uint8_t)); index += sizeof(uint8_t);
			memcpy(buf+index,frequencies,numBins*sizeof(float)); index += numBins*sizeof(float);
			memcpy(buf+index,amplitudes,numBins*sizeof(float)); index += numBins*sizeof(float);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: SeismicSampleFFT"));
            Serial.println(F("ID: 15"));
            Serial.println(F("Fields:"));

			Serial.print(F("	ID: "));
            Serial.print(ID,10); Serial.println();

			Serial.print(F("	numBins: "));
            Serial.print(numBins,10); Serial.println();

			Serial.print(F("	frequencies: "));
            for (int i=0; i<numBins; i++) {
				Serial.print(frequencies[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	amplitudes: "));
            for (int i=0; i<numBins; i++) {
				Serial.print(amplitudes[i],10); Serial.print(F(" | "));
			} Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: SeismicSampleFFT"));
            file.println(F("ID: 15"));
            file.println(F("Fields:"));

			file.print(F("	ID: "));
            file.print(ID,10); file.println();

			file.print(F("	numBins: "));
            file.print(numBins,10); file.println();

			file.print(F("	frequencies: "));
            for (int i=0; i<numBins; i++) {
				file.print(frequencies[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	amplitudes: "));
            for (int i=0; i<numBins; i++) {
				file.print(amplitudes[i],10); file.print(F(" | "));
			} file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_SeismicSampleFFT() {

			delete [] frequencies;
			delete [] amplitudes;

        }

};

#endif