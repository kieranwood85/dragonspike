/*
    DragonRadio message class autocoded in MATLAB

    Name: DragonMessage_SeismicSampleFFTMulti
    ID: 16
    Description: FFT of a seismic sample

    Fields:
        
		Name: numSamples
		Type: uint8_t
		Description: Number of samples returned

		Name: IDs
		Type: uint32_t[numSamples]
		Description: Sample IDs

		Name: numBins
		Type: uint8_t
		Description: Number of top bins returned per sample

		Name: frequencies
		Type: float[numBins*numSamples]
		Description: Centre frequencies of returned bins

		Name: amplitudes
		Type: float[numBins*numSamples]
		Description: Amplitudes of returned bins

	Autocoded using MATLAB
*/

#ifndef DragonMessage_SeismicSampleFFTMulti_h

#define DragonMessage_SeismicSampleFFTMulti_h
#include "Arduino.h"
#include "../DragonPacket.h"
#include "RHDatagram.h"
#include "SdFat.h"

class DragonMessage_SeismicSampleFFTMulti {

    public:

        const uint8_t msgID = 16;
        uint8_t sendID, recvID, size, sequence;
       
        uint8_t numSamples;
        uint32_t *IDs;
        uint8_t numBins;
        float *frequencies;
        float *amplitudes;

        // Construct a SeismicSampleFFTMulti message from a DragonPacket
        DragonMessage_SeismicSampleFFTMulti(DragonPacket *packet)
            :sendID(packet->sendID)
            ,recvID(packet->recvID)
            ,size(packet->size)
            ,sequence(packet->sequence)
        {
            uint8_t index = 0;

			memcpy(&numSamples,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
			IDs = new uint32_t[numSamples];
			memcpy(IDs,packet->payload+index,numSamples*sizeof(uint32_t)); index += numSamples*sizeof(uint32_t);
			memcpy(&numBins,packet->payload+index,sizeof(uint8_t)); index += sizeof(uint8_t);
			frequencies = new float[numBins*numSamples];
			memcpy(frequencies,packet->payload+index,numBins*numSamples*sizeof(float)); index += numBins*numSamples*sizeof(float);
			amplitudes = new float[numBins*numSamples];
			memcpy(amplitudes,packet->payload+index,numBins*numSamples*sizeof(float)); index += numBins*numSamples*sizeof(float);
        }
        
        // Construct a SeismicSampleFFTMulti message from individual fields
        DragonMessage_SeismicSampleFFTMulti(uint8_t numSamples,uint32_t *IDs,uint8_t numBins,float *frequencies,float *amplitudes)
            :size(0+1*sizeof(uint8_t)+numSamples*sizeof(uint32_t)+1*sizeof(uint8_t)+numBins*numSamples*sizeof(float)+numBins*numSamples*sizeof(float))
			,numSamples(numSamples)
			,IDs(new uint32_t[numSamples])
			,numBins(numBins)
			,frequencies(new float[numBins*numSamples])
			,amplitudes(new float[numBins*numSamples])
		{
			memcpy(this->IDs,IDs,numSamples*sizeof(uint32_t));
			memcpy(this->frequencies,frequencies,numBins*numSamples*sizeof(float));
			memcpy(this->amplitudes,amplitudes,numBins*numSamples*sizeof(float));
		}

        // Pack a SeismicSampleFFTMulti message into a generic message for transmission
        DragonPacket pack() {

            uint8_t index = 0;
            uint8_t buf[size];

			memcpy(buf+index,&numSamples,sizeof(uint8_t)); index += sizeof(uint8_t);
			memcpy(buf+index,IDs,numSamples*sizeof(uint32_t)); index += numSamples*sizeof(uint32_t);
			memcpy(buf+index,&numBins,sizeof(uint8_t)); index += sizeof(uint8_t);
			memcpy(buf+index,frequencies,numBins*numSamples*sizeof(float)); index += numBins*numSamples*sizeof(float);
			memcpy(buf+index,amplitudes,numBins*numSamples*sizeof(float)); index += numBins*numSamples*sizeof(float);

            DragonPacket packet(buf,msgID,size);
            return packet;

        }

        // Print debug text over Serial
        void debug() {
        
            Serial.println(F("START DRAGON RADIO DEBUG MESSAGE"));
            Serial.println(F("Name: SeismicSampleFFTMulti"));
            Serial.println(F("ID: 16"));
            Serial.println(F("Fields:"));

			Serial.print(F("	numSamples: "));
            Serial.print(numSamples,10); Serial.println();

			Serial.print(F("	IDs: "));
            for (int i=0; i<numSamples; i++) {
				Serial.print(IDs[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	numBins: "));
            Serial.print(numBins,10); Serial.println();

			Serial.print(F("	frequencies: "));
            for (int i=0; i<numBins*numSamples; i++) {
				Serial.print(frequencies[i],10); Serial.print(F(" | "));
			} Serial.println();

			Serial.print(F("	amplitudes: "));
            for (int i=0; i<numBins*numSamples; i++) {
				Serial.print(amplitudes[i],10); Serial.print(F(" | "));
			} Serial.println();

            Serial.println(F("END DRAGON RADIO DEBUG MESSAGE"));
            Serial.println();

        }

        void log(char *fileName) {

            SdFile file;
            file.open(fileName,O_WRONLY | O_CREAT | O_APPEND);

            file.print(F("START DRAGON RADIO MESSAGE"));
            file.println(F("Name: SeismicSampleFFTMulti"));
            file.println(F("ID: 16"));
            file.println(F("Fields:"));

			file.print(F("	numSamples: "));
            file.print(numSamples,10); file.println();

			file.print(F("	IDs: "));
            for (int i=0; i<numSamples; i++) {
				file.print(IDs[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	numBins: "));
            file.print(numBins,10); file.println();

			file.print(F("	frequencies: "));
            for (int i=0; i<numBins*numSamples; i++) {
				file.print(frequencies[i],10); file.print(F(" | "));
			} file.println();

			file.print(F("	amplitudes: "));
            for (int i=0; i<numBins*numSamples; i++) {
				file.print(amplitudes[i],10); file.print(F(" | "));
			} file.println();
         
            file.println(F("END DRAGON RADIO MESSAGE"));
            file.println();

            file.close();

        }

        // Destructor memory management
        ~DragonMessage_SeismicSampleFFTMulti() {

			delete [] IDs;
			delete [] frequencies;
			delete [] amplitudes;

        }

};

#endif