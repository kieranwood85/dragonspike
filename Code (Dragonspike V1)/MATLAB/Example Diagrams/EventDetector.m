figure;
b = bar([0.2 0 ;0.18 0 ;0.22 0 ;0.5 1.3 ;0.5 0.8 ;0.5 2.6 ;0.5 2.7 ;0.5 1.5 ;0.5 0.8 ;0.5 0.1 ;0.4 0 ;0.3 0 ;0.22 0 ;0.18 0 ;0.2 0],'stacked');
b(1).FaceColor = [0.8 0 0];
b(2).FaceColor = [0 0.8 0];
xlabel('Sample No.');
ylabel('Signal Energy');
hold on;
plot([0 16],[0.5 0.5],'LineWidth',2,'Color',[1 0 0]);
legend('Below Threshold','Above Threshold','Threshold')