classdef DragonRadioGen
%DRAGONRADIOGEN: Dragon Egg messaging protocol code generator
%Description:
%   This program is used to generate the DragonRadio library in the C++ language. Message definitions
%   are provided in the form of an XML document and autocoded into header files. It is inspired by the
%   MAVLINK messaging protocol, but streamlined and integrated with the RadioHead library.
    
    methods(Static, Access=public)
        
        function generate(xmlPath, outputPath)
        %GENERATE: Generates a C++ implementation of the DragonRadio library
        %Description:
        %    Generates a C++ implementation of the DragonRadio library from a set of XML dialect
        %    files. The DragonRadio library allows communication between radio enabled microprocessors
        %    utilising the RadioHead communications library.
        %Arguments:
        %    xmlPath(string): Path to the folder containing XML dialect files
        %    outputPath(string): Path to where the DragonRadio folder will be generated (default = '.')
           
            %Start timer
            timer = tic();
        
            %Handle varying numbers of input arguments
            if nargin == 0
                xmlPath = 'data';
                outputPath = '.';
            elseif nargin == 1
                outputPath = '.';
            elseif nargin > 2
                disp('ERROR: Too many input arguments!');
                return;
            end
            
            fullParsedMsgList = [];
            
            %Create the folder system at the output path
            libraryPath = [outputPath '/DragonRadio'];
            
            if ~exist([libraryPath '/Messages'],'dir')
                mkdir([libraryPath '/Messages']);
            end
            
            %Find all XML files within the specified folder
            xmlList = dir([xmlPath '/*.xml']);
            
            %Perform the following actions for each dialect
            for i = 1:1:size(xmlList)
                fullname = [xmlPath '/' xmlList(i).name];
                f = fopen(fullname,'r');

                %Load the DragonRadio message definition XML document as a DragonStruct
                root = DragonStruct(fullname);
                xmlName = strrep(xmlList(i).name, '.xml', '');

                %Find the message definitions and generate a DragonRadio message class for each
                msgList = root.find('DragonRadio').find('messages').findAll('message');
                parsedMsgList = DragonRadioGen.generateMessageClasses([libraryPath '/Messages'], msgList);

                %Combine message lists from each dialect
                fullParsedMsgList = [fullParsedMsgList parsedMsgList];

                fclose(f);
            end
            
            %Reformat the parsed message list to support the template engine
            parsedMsgList = struct('message',fullParsedMsgList);
            %Generate the DragonRadio header file
            DragonRadioGen.generateRadioClass(libraryPath, parsedMsgList);
            %Copy all fixed files
            DragonRadioGen.copyFixedClasses(libraryPath);
            
            disp(['DragonRadio Implementation generated successfully! (' num2str(toc(timer)) ' seconds)']);
            
        end
        
    end
    
    methods(Static,Access=private)
        
        function parsedMsgList = generateMessageClasses(libraryPath, msgList)
        %GENERATEMESSAGECLASSES: Generates a DragonRadio message class per message in msgList
        %Description:
        %    Generates a class file per message in msgList in the folder specified by messagePath.
        %Arguments:
        %    msgPath(string): Path to the folder where message files will be generated
        %    msgList(DragonStruct): Array of messages to generate classes from


            %Create an empty cell array for parsed messages
            parsedMsgList = [];

            %Load the template for DragonRadio message classes
            templateFile = fopen('message_template.txt','r');
            template = char(fread(templateFile,[1 inf]));
            fclose(templateFile);

            %Generate a DragonRadio class file for each DragonRadio message in the XML file
            for i = 1:1:size(msgList,2)
                parsedMsg = DragonRadioGen.generateClassFromMsg(libraryPath, msgList(i), template);
                if ~isempty(parsedMsg)
                    parsedMsgList = cat(2,parsedMsgList,parsedMsg);
                end
            end

        end
        
        function parsedMsg = generateClassFromMsg(libraryPath, msg, template)
        %GENERATECLASSFROMMSG: Generates a DragonRadio message class
        %Description:
        %    Generates a class file by injecting the data stored in msg into a template.
        %Arguments:
        %    msgPath(string): Path to the folder where the message file will be generated
        %    msg(DragonStruct): Message to generate the class file from
        %    template(string): Template used with DragonString to produce a message class

            parsedMsg = [];

            %Look-up structure of data sizes
            typeSize = struct('double',8,'int64_t',8,'uint64_t',8,'int32_t',4,...
                'uint32_t',4,'float',4,'int16_t',2,'uint16_t',2,'char',1,'int8_t',1,'uint8_t',1);

            %Get top level message attributes
            parsedMsg.msgid = msg.attributes.id;
            parsedMsg.name = msg.attributes.name;
            
            %Throw away messages with IDs that are too high
            if parsedMsg.msgid > 255
                parsedMsg = [];
                return;
            end

            %Get description if available, otherwise set to 'No description available'
            if ~isempty(msg.find('description'))
                parsedMsg.desc = strrep(msg.find('description').text,'\n','');
            else
                parsedMsg.desc = 'No description available';
            end

            %Find all fields until the extension tag
            fields = msg.findAll('field');
            %Define struct for parsed fields
            parsedFields = struct('type',{},'name',{},'desc',{},'arraySize',{},'isArray',{},'ID',{});

            %Parse all fields in the message and add to parsedFields
            parsedMsg.msglen = 0;
            for i=1:1:size(fields,2)

                %Get name of field
                fieldName = fields(i).attributes.name;

                %Get description if available, otherwise set to 'No description available'
                if fields(i).hasText
                    fieldDesc = fields(i).text;
                else
                    fieldDesc = 'No description available';
                end

                %Get the array size of the field
                fieldType = strsplit(fields(i).attributes.type,'[');
                if size(fieldType,2) > 1
                    arraySize = strsplit(fieldType{2},']');
                    arraySize = arraySize(1);
                    isArray = 1;
                else
                    arraySize = '1';
                    isArray = 0;
                end

                %Add this field to parsedFields
                fieldType = fieldType(1);
                parsedFields(i) = struct('type',fieldType,'name',fieldName,'desc',fieldDesc,'arraySize',arraySize,'isArray',isArray,'ID',i);

            end

            %Sort the fields of this message into data type order and store the name of the first
            parsedMsg.orderedFields = parsedFields;
            %Find the number of fields in this message
            parsedMsg.numFields = size(parsedMsg.orderedFields,2);
            %Write the class file for this DragonRadio message
            messageFilename = [libraryPath '/DragonMessage_' parsedMsg.name '.h'];
            disp(['Generating: ' messageFilename]);
            messageFile = fopen(messageFilename,'w');
            fprintf(messageFile,DragonString(template,parsedMsg));
            fclose(messageFile); 

        end
        
        function generateRadioClass(libraryPath, parsedMsgList)
        %GENERATERADIOCLASS: Generates the main DragonRadio class
        %Description:
        %    Generates a class file by injecting the data stored in parsedMsgList into a template.
        %Arguments:
        %    mainPath(string): Path to the folder where the main radio class will be generated
        %    msg(DragonStruct): Message list to generate the class file from
            
            %Load the template for DragonRadio class
            templateFile = fopen('radio_template.txt','r');
            template = char(fread(templateFile,[1 inf]));
            fclose(templateFile);
            
            %Write the main DragonRadio class to file
            disp(['Generating: ' libraryPath '/DragonRadio.h']);
            radioFile = fopen([libraryPath '/DragonRadio.h'],'w');
            fprintf(radioFile,DragonString(template,parsedMsgList));
            fclose(radioFile);
            
        end
        
        function copyFixedClasses(libraryPath)
        %COPYFIXEDCLASSES: Copy master class files into the DragonRadio implementation
        %Description: 
        %    Copies one of each class master file into the DragonRadio implementation. These files do not need to be
        %    generated and are the same for any set of XML files.
        %Arguments:
        %    libraryPath(string): Path to the generated DragonRadio libraries
      
            disp(['Generating: ' libraryPath '/DragonRadio.cpp']);
            copyfile('Masters/DragonRadio.cpp',[libraryPath '/DragonRadio.cpp']);
            disp(['Generating: ' libraryPath '/DragonPacket.h']);
            copyfile('Masters/DragonPacket.h',[libraryPath '/DragonPacket.h']);
            disp(['Generating: ' libraryPath '/DragonPacket.cpp']);
            copyfile('Masters/DragonPacket.cpp',[libraryPath '/DragonPacket.cpp']);
        
        end
        
    end
    
end

