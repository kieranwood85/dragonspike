function [outString,exitChar] = DragonString(inString,schema)
%DRAGONSTRING: The DragonRadio templating engine
%Description:
%    Parses a string and injects data from a schema into it using a simple templating system. Error
%    checking is minimal so use with caution.
%Arguments:
%    inString: The string to be used as the template
%    schema: The structure from which data will be injected into the template
%    startIndex: Index at which to start parsing the template string
%    endIndex: Index at which to stop parsing the template string
%Usage:
%    Variable: ${var}$
%    Repetition: #{array<#>text}#
%    Conditional: ?{condition<?>expression1<?>expression2}?

    %Initialise some variables
    varName = []; condition = []; outString = []; exitChar = 0;
    conditionStart = 0;
    
    %Find all tags in the file and sort them in character index order
    tags = sort([strfind(inString,'${') strfind(inString,'#{') strfind(inString,'?{') strfind(inString,'}#') ...
        strfind(inString,'}?') strfind(inString,'<?>')]);
    
    %Initialise the parser loop
    stringIndex = 1;
    
    %Parse until no more special tags are found
    for tagIndex=1:1:size(tags,2)
        %Ignore any tags that occur before the current string index
        if tags(tagIndex) >= stringIndex;
            %Add text between the current string index and next tag to the output string
            outString = [outString inString(stringIndex:tags(tagIndex)-1)];
            %Determine the tag type and handle appropriately
            switch(inString(tags(tagIndex):tags(tagIndex)+1))
                %Variable start tag
                case '${'
                    [returnedString,exitChar] = variable(inString(tags(tagIndex)+2:end),schema);
                    outString = [outString, returnedString];
                    stringIndex = tags(tagIndex) + exitChar + 1;
                %Repetition start tag
                case '#{'
                    [returnedString,exitChar] = repetition(inString(tags(tagIndex)+2:end),schema);
                    outString = [outString, returnedString];
                    stringIndex = tags(tagIndex) + exitChar + 1;
                %Conditional start tag
                case '?{'
                    [returnedString,exitChar] = conditional(inString(tags(tagIndex)+2:end),schema);
                    outString = [outString, returnedString];
                    stringIndex = tags(tagIndex) + exitChar + 1;
                %Repetition end tag
                case '}#'
                    exitChar = tags(tagIndex) + 1;
                    return;
                %Conditional end tag
                case '}?'
                    exitChar = tags(tagIndex) + 1;
                    return;
                %Handles the 3 character conditional delimiter
                otherwise
                    exitChar = tags(tagIndex) + 2;
                    return;
            end
            
        end
        
    end
    %Add remaining text after the last found tag to the output string
    outString = [outString inString(stringIndex:end)];
end

function [outString,exitChar] = variable(inString,schema)
%VARIABLE: Handles replacement of ${var}$ tags
%Description:
%    Replaces a variable tag with data found in the supplied schema. Throws an error if the input
%    string is not formatted correctly.
%Arguments:
%    inString(string): String to be parsed for a variable name
%    schema(struct): Source of the data used to replace the ${var}$ tag

    outString = [];
    tagStart = strfind(inString,'}$');
    varName = inString(1:tagStart(1)-1);
    exitChar = tagStart(1)+2;
    
    if ~isfield(schema,varName)
        fprintf(2,'ERROR: Field does not exist!\n');
        fprintf(2,'TEMPLATE TEXT: ${%s', inString);
        fprintf(2,'\n');
    elseif isnumeric(schema.(varName))
        outString = num2str(schema.(varName));
    else
        outString = schema.(varName);
    end
    
end

function [outString,exitChar] = repetition(inString,schema)
%REPETITION: Handles replacement of #{array<#>text}# tags
%Description:
%    Replaces a repetition tag with data found in the supplied schema. Throws an error if the input
%    string is not formatted correctly. Supports nesting.
%Arguments:
%    inString(string): String to be parsed for an array name and replacement text
%    schema(struct): Source of the array data

    outString = [];
    tagStart = strfind(inString,'<#>');
    varName = inString(1:tagStart(1)-1);
    varName = strsplit(varName,'(');
    
    if length(varName) > 1
        indexString = strsplit(varName{2},')');
        indexString = indexString{1};
    else
        indexString = '';
    end
    
    varName = varName{1};
    
    if ~isfield(schema,varName) 
        fprintf(2,'ERROR: Field does not exist!\n');
        fprintf(2,'TEXT: %s\n', varName);
        outString = [];
        exitChar = [];
        return;
    end
        
    newSchema = schema.(varName);
        
    if isempty(indexString)
        indexes = 1:size(newSchema,2);
        indexError = 0;
    else
        [indexes,indexError] = parseIndexing(indexString,newSchema);
    end

    if indexError
        fprintf(2,'ERROR: Indexing is invalid!\n');
        fprintf(2,'TEXT: %s(%s)\n',varName,indexString);
        outString = [];
        exitChar = [];
        return;
    end

    for i=1:1:length(indexes);
        j = indexes(i);
        [returnedString,exitChar] = DragonString(inString(tagStart(1)+3:end),newSchema(j));
        outString = [outString returnedString];
        exitChar = exitChar + tagStart(1) + 3;
    end
    
end

function [outString,exitChar] = conditional(inString,schema)
%CONDITIONAL: Handles replacement of ?{condition<?>expression1<?>expression2}? tags
%Description:
%    Replaces a conditional tag with data found in the supplied schema. Throws an error if the input
%    string is not formatted correctly. Does not support nesting.
%Arguments:
%    inString(string): String to be parsed for a condition and expressions
%    schema(struct): Source of the data used by any nested tags

    outString = []; evaluation = -1;
    tagStart = strfind(inString,'<?>');
    [condition,~] = DragonString(inString(1:tagStart(1)-1),schema);
    try
        eval(sprintf('if %s,evaluation=1;,else,evaluation=0;,end',condition));
    catch
        fprintf('ERROR: Condition is not valid!\n');
        fprintf(2,'TEMPLATE TEXT: ?{%s', inString);
        fprintf(2,'\n');
    end
    
    if evaluation == 1
        [outString,~] = DragonString(inString(tagStart(1)+3:end),schema);
    elseif evaluation == 0
        [outString,~] = DragonString(inString(tagStart(2)+3:end),schema);
    end
    
    tagStart = strfind(inString,'}?');
    exitChar = tagStart(1) + 2;
    
end

function [indexes,error] = parseIndexing(indexString,variable)
%PARSEINDEXING: Handles parsing of array access indexing in template text
%Description:
%    Allows for support of indexing into elements of the schema including support for the end
%    operator. Contains simplistic error checking for index out of bounds.
%Arguments:
%    indexString(string): String to be parsed into an array of indexes
%    variable(struct/var): Source of the array being indexed

    indexes = [];
    error = 0;

    indexString = strsplit(indexString,'[');
    indexString = strsplit(indexString{end},']');
    indexString = indexString{1};
    indexStrings = strsplit(indexString,{' ',','});

    for i=1:1:length(indexStrings)

        indexString = strsplit(indexStrings{i},':');

        if length(indexString) > 1

            startIndex = str2double(indexString{1});

            if strfind(indexString{2},'end')

                if strfind(indexString{2},'-')
                    indexString = strsplit(indexString{2},'-');
                    endIndex = size(variable,2)-str2double(indexString{2});
                elseif strfind(indexString{2},'+')
                    indexString = strsplit(indexString{2},'+');
                    endIndex = size(variable,2)+str2double(indexString{2});
                else
                    endIndex = size(variable,2);
                end 

            else
                endIndex = str2double(indexString{2});
            end
            
            if startIndex <= endIndex
                indexes = [indexes startIndex:endIndex];
            else
                error = 1;
            end

        else

            if strfind(indexString{1},'end')

                if strfind(indexString{1},'-')
                    indexString = strsplit(indexString{1},'-');
                    index = size(variable,2)-str2double(indexString{2});
                elseif strfind(indexString{1},'+')
                    indexString = strsplit(indexString{2},'+');
                    index = size(variable,2)+str2double(indexString{2});
                else
                    index = size(variable,2);
                end 

            else
                index = str2double(indexString{1});
            end

            indexes = [indexes index];

        end
    end
    
    if any((indexes > size(variable,2)) | (indexes < 1))
        error = 1;
    end

end
