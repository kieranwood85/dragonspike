/*	
	Dragon packet class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#ifndef DragonPacket_h

#define DragonPacket_h
#include "RHDatagram.h"

class DragonPacket {
	
	public:
	
		uint8_t *payload;
		uint8_t msgID, sendID, recvID, size, sequence;
	
		DragonPacket(uint8_t *buf, uint8_t len, uint8_t from, uint8_t to, uint8_t seq);
		DragonPacket(uint8_t *buf, uint8_t id, uint8_t size);
        DragonPacket(const DragonPacket &packet);
		DragonPacket();
		~DragonPacket();
		void send(RHDatagram *manager, uint8_t address);
	
};

#endif