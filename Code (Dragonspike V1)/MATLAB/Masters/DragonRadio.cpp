/*	
	Dragon radio class for Arduino
	Created by Samuel P. Pownall, March 2019
*/

#include "Arduino.h"
#include "DragonRadio.h"

DragonRadio::DragonRadio(RHDatagram *radioManager, uint8_t bufSize)
	:manager(radioManager)
    ,bufSize(bufSize)
{
	
}

void DragonRadio::send(DragonPacket *packet, uint8_t address) {

	packet->send(manager,address);
	
}

bool DragonRadio::available() {
	
	if (!packetBuf.empty()) {
		return true;
	} else {
		return false;
	}
	
}

DragonPacket DragonRadio::read() {
 
    if (!packetBuf.empty()) {
        DragonPacket packet = packetBuf.back();
        packetBuf.pop_back();
        return packet;
    } else {
        DragonPacket packet;
        return packet;
    }
    
}

void DragonRadio::update() {

	uint8_t len = 251, from, to, seq;
	uint8_t buf[len];
		
    if (!manager->available()) {
        return;
    }
    
	if (manager->recvfrom(buf,&len,&from,&to,&seq)) {
		
		DragonPacket packet(buf,len,from,to,seq);
		
        if (packetBuf.size() == bufSize) {
            packetBuf.pop_back();
        }
        
        packetBuf.push_front(packet);
		
	}

}