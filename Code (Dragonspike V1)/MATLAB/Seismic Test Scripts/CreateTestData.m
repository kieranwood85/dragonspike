filename = 'Seismic Test Data/Output/EQ1.dat';
fileID = fopen(filename,'r');

A = fread(fileID,'single');

fclose(fileID);

filename = 'Seismic Test Data/Output/EQ2.dat';
fileID = fopen(filename,'r');

B = fread(fileID,'single');

fclose(fileID);

filename = 'Seismic Test Data/Output/fromprobe.txt';
fileID = fopen(filename,'r');

sampleStart = [];
sampleEnd = [];
sampleEnergy = [];

eventStart = [];
eventEnd = [];
eventEnergy = [];

tline = fgetl(fileID);
while ischar(tline)
    disp(tline)
    if strfind(tline,'Sample:')
        [data] = sscanf(tline,'Sample: %d,%d,%f');
        sampleStart(end+1) = data(1);
        sampleEnd(end+1) = data(2);
        sampleEnergy(end+1) = data(3);
    else
        [data] = sscanf(tline,'Event: %d,%d,%f');
        eventStart(end+1) = data(1);
        eventEnd(end+1) = data(2);
        eventEnergy(end+1) = data(3);
    end
    tline = fgetl(fileID);
end

C = single(zeros(1000000,1));



data = [A ; B ; C];
X = 0:1/80:350;

figure;
subplot(4,1,1);
bar(1:2:238*2,sampleEnergy.^0.5);
xlim([0 350]);
ylabel('E_s^0^.^5');
subplot(4,1,2); 
plot(X,data(1:3:length(X)*3)); hold on;
plot([32 32],[-1 1],'LineWidth',2,'Color',[0 0.8 0]);
plot([78 78],[-1 1],'LineWidth',2,'Color',[0.8 0 0]);
plot([216 216],[-1 1],'LineWidth',2,'Color',[0 0.8 0]);
plot([282 282],[-1 1],'LineWidth',2,'Color',[0.8 0 0]);
plot([290 290],[-1 1],'LineWidth',2,'Color',[0 0.8 0]);
plot([300 300],[-1 1],'LineWidth',2,'Color',[0.8 0 0]);
ylim([-0.4 0.4]);
ylabel('Channel 1 (m/s^2)');
legend('Acceleration','Event Start','Event End');
subplot(4,1,3); 
plot(X,data(2:3:length(X)*3)); hold on;
plot([32 32],[-1 1],'LineWidth',2,'Color',[0 0.8 0]);
plot([78 78],[-1 1],'LineWidth',2,'Color',[0.8 0 0]);
plot([216 216],[-1 1],'LineWidth',2,'Color',[0 0.8 0]);
plot([282 282],[-1 1],'LineWidth',2,'Color',[0.8 0 0]);
plot([290 290],[-1 1],'LineWidth',2,'Color',[0 0.8 0]);
plot([300 300],[-1 1],'LineWidth',2,'Color',[0.8 0 0]);
ylim([-0.4 0.4]);
ylabel('Channel 2 (m/s^2)');
subplot(4,1,4); 
plot(X,data(3:3:length(X)*3)); hold on;
plot([32 32],[-1 1],'LineWidth',2,'Color',[0 0.8 0]);
plot([78 78],[-1 1],'LineWidth',2,'Color',[0.8 0 0]);
plot([216 216],[-1 1],'LineWidth',2,'Color',[0 0.8 0]);
plot([282 282],[-1 1],'LineWidth',2,'Color',[0.8 0 0]);
plot([290 290],[-1 1],'LineWidth',2,'Color',[0 0.8 0]);
plot([300 300],[-1 1],'LineWidth',2,'Color',[0.8 0 0]);
ylim([-0.4 0.4]);
xlabel('Time (s)');
ylabel('Channel 3 (m/s^2)');


filename = 'Seismic Test Data/Output/testdata.dat';
delete(filename);
fileID = fopen(filename,'w');

fwrite(fileID,data,'single');

fclose(fileID);