%% Loads Raw Seismic Acceleration Data %%

%% Input Data %%

% Folder Name of Earthquake %
earthquakeFolder = 'EQ2';

% Output Folder %
outputFolder = 'Output';

% Conversion to m/s/s %
unitConv = 9.81;

%% Processing %%

% Read data into matrices %
X = unitConv*importdata(['Seismic Test Data/' earthquakeFolder '/C1.txt']);
Y = unitConv*importdata(['Seismic Test Data/' earthquakeFolder '/C2.txt']);
Z = unitConv*importdata(['Seismic Test Data/' earthquakeFolder '/C3.txt']);

% Reshape into 1 dimension %
X = reshape(X',1,[]);
Y = reshape(Y',1,[]);
Z = reshape(Z',1,[]);

% Remove NaN values %
X(isnan(X)) = [];
Y(isnan(Y)) = [];
Z(isnan(Z)) = [];

% Trim data to minimum length channel %
N = min(length(X),min(length(Y),length(Z)));
X = X(1:N);
Y = Y(1:N);
Z = Z(1:N);

% Plot waveforms

figure;
subplot(3,1,1);
plot(X);
subplot(3,1,2);
plot(Y);
subplot(3,1,3);
plot(Z);

% Create data file
fileName = ['Seismic Test Data/' outputFolder '/' earthquakeFolder '.dat'];
delete(fileName);
fileID = fopen(fileName,'w');

A = [X;Y;Z];
A = reshape(A,[],1);

fwrite(fileID,A,'single');

fclose(fileID);