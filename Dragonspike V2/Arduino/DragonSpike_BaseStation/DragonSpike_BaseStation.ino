//start of file
//Basic code example for DragonSpike V2 boards
//Read accelerometer and RTC data and write to SD card
//K Wood
//27/10/20
//
//==========================================================
// GLOBALS (GENERIC)
//==========================================================

// Annoying workaround required because it conflicts with ArduinoSTL (hopefully doesn't cause serious issues!)
#undef min
#undef max

#define LED_ON 1
#define SD_ON 1
#define RTC_ON 1
#define LORA_ON 1
int somethingChanged = 1;       //Change this to force a recompile if only a pre-compiler flag was changed

// Libraries
#include <Chrono.h>             // For scheduled timing control
#include <RHDatagram.h>
#include <RH_RF95.h>
#include "Arduino.h"
#include "math.h"
#include <ArduinoSTL.h>
#include <SdFat.h>
#include "sdios.h"
#include <Adafruit_MMA8451.h>   // For accelerometer
#include <RTClib.h>             // For real time clock
#include <Arduino.h>
#include <SerialCommands.h>


// DragonOS and settings
#include <DragonOS.h>
#define DRAGON_SYSTEMID 1       // Dragon system ID (Greater than zero!)
uint8_t DRAGON_SYSTEMID_INT = 1; // Using INT to ensure recompile when changed
#define DRAGON_RADIO_CACHE 20    // Received object cache size TODO: what is this?
#define DRAGON_TARGET_ID 12      // The target ID to send the data to over LoRa. All devices listen to 0.
#define DRAGON_LISTEN_ALL 0     // All node listen


//==========================================================
// HARDWARE SETUP
//==========================================================
// LED error flash messages
#define LED_PIN 13
int errorLedState = HIGH;
Chrono errorLedChrono;
Chrono blinkChrono;
uint32_t errorLedNormalInteval = 5000;   //time in ms between blink if normal
uint32_t errorLedErrorInteval = 1000;    //time in ms between blink if error
bool setError = false;

// Minute counter
Chrono minuteCounter;
uint32_t minuteCountInteval = 60*1000;
uint32_t totalMins = 0;   //enough for 4000 years

// Battery monitor
#define VBATPIN A7
Chrono batteryCheckChrono;
float latestVoltage = 0.0;
uint32_t batteryCheckInteval = 5*60*1000;   //time between battery updates
float batFullV = 4.15;  //above this value the battery is considered full
float batNormV = 4.0;
float batLowV  = 3.8;
float batCritV = 3.5;   //below this is critical. High power sensors/systems should be turned off/very very infrequent.
//float batEptyV = 3.3;

// RTC Chip settings
Chrono readTimeChrono;
uint32_t timeReadInteval = 2000;  //time between RTC reads in ms
RTC_DS3231 rtc;
float RTCTemp = 0.0;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
uint16_t time_dd = 0;
uint16_t time_mo = 0;
uint16_t time_yy = 0;
uint16_t time_hh = 0;
uint16_t time_mm = 0;
uint16_t time_ss = 0;
bool didRTCReset = false;
bool forceRTCReset = false;

// SD card settings
Chrono writeSDChrono;
uint32_t sdWriteInteval = 2000;   // time between SD writes in ms
#define SD_CS 19                  // Chip select pin (this is for the DragonSpike V2)
#define SD_FREQ 24                // SPI Frequency (MHz)
#define SD_FAT_TYPE 0             // for SdFat/File
SdFat sd;
SPISettings settings;
char mainFileName[32];
char bkupFileName[32];
bool SDInit = false;

// Radio settings
Chrono sendDataChrono;
uint32_t sendDataInteval = 2000;   // time between loraSends
#define RFM_CS 8                // Chip select pin
#define RFM_IRQ 3               // Interrupt pin
#define RFM_FREQ 915.0          // Radio Frequency (MHz)
#define RFM_PWR 23              // Power setting
#define RFM_LBT_TOUT 3000       // LBT timeout in ms
RH_RF95 radioDriver(RFM_CS,RFM_IRQ);
RHDatagram datagram(radioDriver,0);


//==========================================================
// SOFTWARE SETUP
//==========================================================
// Dragon OS
DragonRadio radio(&datagram,DRAGON_RADIO_CACHE);
bool autoPollOn = false;
Chrono pollTimer12;
Chrono pollTimer13;
Chrono pollTimer14;
uint32_t pollTimerInteval = 2000;
DateTime latestRecvTime;


//==========================================================
// MEASUREMENT VARIABLES
//==========================================================


//==========================================================
// FUNCTIONS
//==========================================================
void makeNewLogFile(const char *baseName, const char *folderName, bool safeOn=true) {
  if (SDInit) {
    //Check/create the folder
    if (!sd.exists(folderName)) {
      if (!sd.mkdir(folderName)) {
        Serial.println("\n!!! error creating folder !!!\n");
        setError = true;
      }
    }
    //Change the working directory
    if (!sd.chdir(folderName)) {
      Serial.println("\n!!! error changing directory !!!\n");
      setError = true;
    }
    Serial.print("Working in folder: '");Serial.print(folderName);Serial.println("'");
  
    //File suffix started at zero
    int fileN = 0;
    sprintf(mainFileName,"%s_%03dm.dat",baseName,fileN);
    while(sd.exists(mainFileName)) {
      fileN++;
      sprintf(mainFileName,"%s_%03dm.dat",baseName,fileN);
    }
    //Open the file...
    File mainFile = sd.open(mainFileName,FILE_WRITE);
  
    //If the file is available, write a header to it...
    if (mainFile) {
      Serial.print("Created main file: '");Serial.print(mainFileName);Serial.println("'");
      mainFile.print("DragonEgg Main Log. SysID: ");mainFile.println(DRAGON_SYSTEMID);
      mainFile.close();
    }  
    else {
      Serial.println("\n!!! error opening main file !!!\n");
      setError = true;
    }
   
    if (safeOn) {
      //Assuming there will be no conflict for the backup filename if 
      //the mainfile name is unique.
      sprintf(bkupFileName,"%s_%03db.dat",baseName,fileN);
      File backupFile = sd.open(bkupFileName,FILE_WRITE);
      if (backupFile) {
        Serial.print("Created bkup file: '");Serial.print(bkupFileName);Serial.println("'");
        backupFile.print("DragonEgg Backup Log. SysID: ");backupFile.println(DRAGON_SYSTEMID);
        backupFile.close();
      }
      else {
        Serial.println("\n!!! error opening backup file !!!\n");
        setError = true;
      }
    }
  }
}

//Write the char message to the mainFile. If safeOn=true, then
//also write to an identical backup file. Always appends.
//Write to file and a backup. Only one file is open at once, hence 
//file corruption due to power loss cannot affect both files.
void safeWrite(char message[],bool safeOn=true) {
  if (SDInit) {
    File mainFile = sd.open(mainFileName,FILE_WRITE);
    if (mainFile) { 
      mainFile.println(message);
      mainFile.close();
    }  
    else {
      Serial.println("\n!!! error opening main file !!!\n");
      setError = true;
    }
    if (safeOn) {
      //Write to backup file
      File backupFile = sd.open(bkupFileName,FILE_WRITE);
      if (backupFile) {
        backupFile.println(message);
        backupFile.close();
      }  
      else {
        Serial.println("\n!!! error opening backup log file !!!\n");
        setError = true;
      }
    }
  }
}

// Serial commands interface (not needed for eggs)
char serial_command_buffer_[32];
SerialCommands serial_commands_(&Serial, serial_command_buffer_, sizeof(serial_command_buffer_), "\r\n", " ");
#include "serialCmds.h"

//==========================================================
// SETUP/STARTUP
//==========================================================
void setup() {
#if RTC_ON
  //delay(500);
  //Serial.print("Initialise RTC ... ");
  //Check if an RTC is connected
  if (!rtc.begin()) {
    //Serial.println("\n!!! Couldn't start RTC !!!\n");
    setError = true;
  }
  if (forceRTCReset || rtc.lostPower()) {
  //Serial.print("RTC lost power, lets set the time! ... ");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    didRTCReset = true;
  }
  //RTCTemp = DS3231_get_treg();
  //Serial.print("Temp. = ");Serial.print(rtc.getTemperature());Serial.print("C ... ");
  //Serial.println("done");
#endif

  // Start the USB debug port.
  // Pause to allow connection to be established before first messages
  delay(500);
  Serial.begin(9600);
  delay(1000);delay(1000);delay(1000);delay(1000);
  while(!Serial) delay(10);
  Serial.println("Dragon Arrow V2 startup");

  //Required for interacting with the LoRa and SD card.
  datagram.init();

  // Dragon OS setup. Init. the system ID (choose a unique ID per sensor pod)
  Serial.print("Unique Egg ID = "); Serial.println(DRAGON_SYSTEMID);
  DragonOS::setSystemID(DRAGON_SYSTEMID);

  //Setup serial commands (not needed for eggs)
  regAllCmds();

#if LED_ON
  // LED is on during setup. Then slow blink for normal operation, fast blink for an error
  digitalWrite(LED_PIN, HIGH);
  Serial.println("   solid LED    = startup");
  Serial.println("   5s LED blink = normal operation");
  Serial.println("   1s LED blink = error");
#endif

#if RTC_ON
  delay(500);
  /*
  Serial.print("Initialise RTC ... ");
  //Check if an RTC is connected
  if (!rtc.begin()) {
    Serial.println("\n!!! Couldn't start RTC !!!\n");
    setError = true;
  }
  if (rtc.lostPower()) {
  Serial.print("RTC lost power, lets set the time! ... ");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  */
  if (didRTCReset) {
    Serial.print("RTC lost power, lets set the time! ... ");
  }
  DateTime now = rtc.now();
  time_hh = now.hour();
  time_mm = now.minute();
  time_ss = now.second();
  Serial.print("Time:");Serial.print(time_hh);Serial.print(":");Serial.print(time_mm);Serial.print(":");Serial.println(time_ss);
  //RTCTemp = DS3231_get_treg();
  Serial.print("Temp. = ");Serial.print(rtc.getTemperature());Serial.print("C ... ");
  Serial.println("done");
#endif

#if SD_ON
  delay(500);
  Serial.print("Initialise SD ... ");
  settings = SPISettings(1000000*SD_FREQ,MSBFIRST,SPI_MODE0);
  SDInit = sd.begin(SD_CS,settings);
  if (!SDInit) {
    Serial.println("\n!!! Couldn't start SD !!!\n");
    setError = true;
  }
  else {
    Serial.println("done");
    #if RTC_ON
    //SD is ok so make a folder name using the date
    DateTime now = rtc.now();
    char date_format[6] = {'Y','Y','M','M','D','D'};
    makeNewLogFile("BASE",now.toString(date_format),true);
    #else
    makeNewLogFile("BASE","DEFAULT",true);
    #endif
  }
#endif

#if LORA_ON
  delay(500);
  Serial.print("Initialise LoRa...");
  radioDriver.setTxPower(RFM_PWR, false);
  radioDriver.setFrequency(RFM_FREQ);
  radioDriver.setCADTimeout(RFM_LBT_TOUT);
  Serial.print("freq. = ");Serial.print(RFM_FREQ);Serial.print("MHz ... ");
  Serial.println("done");
#endif

  //=====================
  // Specific setup
  //=====================
  //none
  
  delay(1000);
  Serial.println("Startup complete!");
#if LED_ON
  digitalWrite(LED_PIN, LOW);
#endif
  Serial.println("");
}  //end setup()

//==========================================================
// MAIN LOOP
//==========================================================
void loop() {
#if LED_ON
  //See if an error has been set (setError = true) and blink LED appropriately
  checkErrorState();
#endif

  //Display the number of minutes elapsed since power up
  minsElapsed();

  //Check the battery health
  checkBattery();

  //Check for serial commands
  serial_commands_.ReadSerial();

#if RTC_ON
  if (readTimeChrono.hasPassed(timeReadInteval,true)) {
    // Retrieve current time from RTC chip
    DateTime now = rtc.now();
    
    //Store the data into variables
    time_yy = now.year();
    time_mo = now.month();
    time_dd = now.day();
    time_hh = now.hour();
    time_mm = now.minute();
    time_ss = now.second();
  }
#endif

//Keep resetting the polling to be evenly spaced
if (!autoPollOn) {
    pollTimer12.restart(300);
    pollTimer13.restart(150);
    pollTimer14.restart();
}

#if LORA_ON
  //Poll the sensor at a set rate
  if (autoPollOn && pollTimer12.hasPassed(pollTimerInteval,true)) {
    DragonObject_LORACMD createdObject(DRAGON_SYSTEMID_INT,05);
    radio.send(createdObject,12);
    datagram.waitPacketSent();
  }
  //Poll the sensor at a set rate
  if (autoPollOn && pollTimer13.hasPassed(pollTimerInteval,true)) {
    DragonObject_LORACMD createdObject(DRAGON_SYSTEMID_INT,05);
    radio.send(createdObject,13);
    datagram.waitPacketSent();
  }
  //Poll the sensor at a set rate
  if (autoPollOn && pollTimer14.hasPassed(pollTimerInteval,true)) {
    DragonObject_LORACMD createdObject(DRAGON_SYSTEMID_INT,05);
    radio.send(createdObject,14);
    datagram.waitPacketSent();
  }

  // Update the radio (this is a DragonOS function which reads the RD modem and
  // attempts to recognise Dragon OS objects from the stream.
  radio.update();
  while (radio.available()) {
    latestRecvTime = rtc.now();
    switch (radio.available()) {
      case 0:
      break;
   
      case DragonObject_ACK_LORACMD::getObjectID():
      {
        DragonObject_ACK_LORACMD object;
        if (radio.recv(object)) {
          Serial.println("Received ACK_LORACMD package");
          //Do something here!        
        }
      }
      break;
  
      case DragonObject_MAX_ACCEL::getObjectID():
      {
        DragonObject_MAX_ACCEL object;
        if (radio.recv(object)) {
          Serial.print("Received MAX_ACCEL package from D:");
          Serial.print(object.getFields()->DEVID);Serial.print(",X");
          Serial.print(object.getFields()->MAX_X);Serial.print(",Y");
          Serial.print(object.getFields()->MAX_Y);Serial.print(",Z");
          Serial.print(object.getFields()->MAX_Z);Serial.print(",T");
          Serial.println(object.getFields()->UnixT);
          //Write to SD
          char writeBuf[64];
          int sz = sprintf(writeBuf,"%lu,MAX_ACCEL,%i,%5.2f,%5.2f,%5.2f,%lu",latestRecvTime.unixtime(),object.getFields()->DEVID,object.getFields()->MAX_X,object.getFields()->MAX_Y,object.getFields()->MAX_Z,object.getFields()->UnixT);
          safeWrite(writeBuf,true);      
        }
      }
      break;
  
      case DragonObject_RTC_TEMP::getObjectID():
      {
        DragonObject_RTC_TEMP object;
        if (radio.recv(object)) {
          Serial.print("Received RTC_TEMP package from D:");
          Serial.print(object.getFields()->DEVID);Serial.print(",T");
          Serial.print(object.getFields()->Temp);Serial.print("C,T");
          Serial.println(object.getFields()->UnixT);
          //Write to SD
          char writeBuf[64];
          int sz = sprintf(writeBuf,"%lu,RTC_TEMP,%i,%5.2f,%lu",latestRecvTime.unixtime(),object.getFields()->DEVID,object.getFields()->Temp,object.getFields()->UnixT);
          safeWrite(writeBuf,true);        
        }
      }
      break;
      
      case DragonObject_LAST_CO2::getObjectID():
      {
        DragonObject_LAST_CO2 object;
        if (radio.recv(object)) {
          Serial.print("Received LAST_CO2 package from D:");
          Serial.print(object.getFields()->DEVID);Serial.print(",");
          Serial.print(object.getFields()->CO2);Serial.print("ppm,");
          Serial.print(object.getFields()->UnixT);Serial.print(",");
          Serial.println(object.getFields()->UNQ);
          //Write to SD
          char writeBuf[64];
          int sz = sprintf(writeBuf,"%lu,LAST_CO2,%i,%5.2f,%lu,%i",latestRecvTime.unixtime(),object.getFields()->DEVID,object.getFields()->CO2,object.getFields()->UnixT,object.getFields()->UNQ);
          safeWrite(writeBuf,true);        
        }
      }
      break;
     
    } // end switch radio.available()
  }
 
#endif

}

//==========================================================
// FUNCTIONS
//==========================================================
//Function to make the LED blink with a heartbeat during
//normal operation and flash quickly when an error state has
//been set.
void checkErrorState(void) {
  if (setError) {
    if (errorLedChrono.hasPassed(errorLedErrorInteval,true)) {
      errorLedState = !errorLedState;
      digitalWrite(LED_PIN, errorLedState);
    }
  }
  else {
    //Short blink at longer intevals
    if (errorLedChrono.hasPassed(errorLedNormalInteval,true)) {
      if (!errorLedState) {
        errorLedState = HIGH;
        digitalWrite(LED_PIN, errorLedState);
        blinkChrono.restart();
      }
    }
    if (blinkChrono.hasPassed(100)) {
      errorLedState = LOW;
      digitalWrite(LED_PIN, errorLedState);
      blinkChrono.stop();
    }
  }
}

//Count and display the minutes elapsed since start. This is
//just a basic counter and might drift slightly.
uint32_t minsElapsed(void) {
  if (minuteCounter.hasPassed(minuteCountInteval,true)) {
    totalMins++;
    Serial.print("Mins elapsed: "); Serial.println(totalMins);
    return totalMins;
  }
}

//Check the battery voltage. The returned value is binned according to the settings.
uint8_t checkBattery(void) {
  float measuredVBat = analogRead(VBATPIN);
  measuredVBat *= 2.0;   //remove effect of potential divider
  measuredVBat *= 3.3;   //multiply by reference voltage
  measuredVBat /= 1024;  //divide by bit depth
  latestVoltage = measuredVBat;
  if (batteryCheckChrono.hasPassed(batteryCheckInteval,true)) {
    Serial.print("Battery voltage: "); Serial.print(latestVoltage);Serial.println(" V");
  }
  //Check the health 'code'
  if      (latestVoltage > batFullV) {return 4;}
  else if (latestVoltage > batNormV) {return 3;}
  else if (latestVoltage > batLowV) {return 2;}
  else if (latestVoltage > batCritV) {return 1;}
  else {return 0;}
}

//end of file
