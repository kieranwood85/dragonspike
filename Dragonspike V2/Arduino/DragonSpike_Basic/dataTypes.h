#include <CircularBuffer.h>

//Class to handle data storage buffers. It replicates push and pop procedures
//commonly found in buffers, but it splits the storage into a memory buffer 
//and an SD storage array.

  //The total packets recorded since start. Also acts as an index to the 
  //tail location
  uint32_t nPackets;

  //The number of packets sent so far. Also acts as an index to the current 
  //Tx head location
  uint32_t packetsSent;
  //CircularBuffer<T,100> accelEventLvl_1s;
  

template <class T>
T GetMax (T a, T b) {
    T result;
    result = (a>b)? a : b;
    return (result);
  }
