//start of file
//Basic code example for DragonSpike V2 boards
//Read accelerometer and RTC data and write to SD card
//K Wood
//27/10/20
//
//==========================================================
// GLOBALS (GENERIC)
//==========================================================

// Annoying workaround required because it conflicts with ArduinoSTL (hopefully doesn't cause serious issues!)
#undef min
#undef max

#define DEVEL_ON 0
#define LED_ON 1
#define SD_ON 1
#define ACCEL_ON 1
#define RTC_ON 1
#define LORA_ON 1
#define CO2_ON 0
//The battery is for the on-board sense pin aimed 
//at 1S (4.2V max) lipo/lion batteries.
#define BATT_ON 1


// Libraries
#include <Chrono.h>             // For scheduled timing control
#include <RHDatagram.h>
#include <RH_RF95.h>
#include "Arduino.h"
#include "math.h"
#include <ArduinoSTL.h>
#include <SdFat.h>
#include "sdios.h"
#include <Adafruit_MMA8451.h>   // For accelerometer
#include <RTClib.h>             // For real time clock
#include <Arduino.h>
#include <SerialCommands.h>
#include <Adafruit_SleepyDog.h>
#include <Wire.h>
#include "headers\dragon\mavlink.h"   //For custom serial message protocol


// DragonOS and settings
#include <DragonOS.h>
#define DRAGON_SYSTEMID 12      // Dragon system ID (Greater than zero!)
uint8_t DRAGON_SYSTEMID_INT = 12; // Using INT to ensure recompile when changed
#define DRAGON_RADIO_CACHE 20   // Received object cache size
#define DRAGON_TARGET_ID 1      // The target ID to send the data to over LoRa (i.e. the base station)
#define DRAGON_LISTEN_ALL 0     // All node listen

#if DEVEL_ON
  #include <CircularBuffer.h>
  #include "dataTypes.h"
#endif


//==========================================================
// HARDWARE SETUP
//==========================================================
// LED error flash messages
#define LED_PIN 13
int errorLedState = HIGH;
Chrono errorLedChrono;
Chrono blinkChrono;
uint32_t errorLedNormalInteval = 5000;   //time in ms between blink if normal
uint32_t errorLedErrorInteval = 1000;    //time in ms between blink if error
bool setError = false;

// Minute counter
Chrono minuteCounter;
uint32_t minuteCountInteval = 60*1000;
uint32_t totalMins = 0;   //enough for 4000 years

// Battery monitor
#if BATT_ON
#define VBATPIN A7
Chrono batteryCheckChrono;
float latestVoltage = 0.0;
uint32_t batteryCheckInteval = 1*60*1000;   //time between battery updates in ms
float batFullV = 4.15;  //LVL0 above this value the battery is considered full
float batNormV = 4.0;   //LVL1
float batLowV  = 3.8;   //LVL2
float batCritV = 3.5;   //LVL3 below this is critical. High power sensors/systems should be turned off/very very infrequent.
//float batEptyV = 3.3;   //LVL4 below LVL3, the board should basically hibernate
#endif

#if DEVEL_ON
//Watchdog timeout
uint32_t activityWatchdogTimeout = 4000;    //if this time elapses without the watchdog function being called, the board will reset
#endif

// Accelerometer settings
Chrono readAccelChrono;
uint32_t accelReadInteval = 10;  //time between accelerometer reads in ms
Adafruit_MMA8451 mma = Adafruit_MMA8451();
float accel_x = 0;
float accel_y = 0;
float accel_z = 0;

// RTC Chip settings
Chrono readTimeChrono;
uint32_t timeReadInteval = 1000;  //time between RTC reads in ms
RTC_DS3231 rtc;
float RTCTemp = 0.0;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
uint16_t time_dd = 0;
uint16_t time_mo = 0;
uint16_t time_yy = 0;
uint16_t time_hh = 0;
uint16_t time_mm = 0;
uint16_t time_ss = 0;

// SD card settings
Chrono writeSDChrono;
uint32_t sdWriteInteval = 1000;   // time between SD writes in ms
#define SD_CS 19                  // Chip select pin (this is for the DragonSpike V2)
#define SD_FREQ 24                // SPI Frequency (MHz)
#define SD_FAT_TYPE 0             // for SdFat/File
SdFat sd;
SPISettings settings;
char mainFileName[32];
char bkupFileName[32];
bool SDInit = false;

// Radio settings
Chrono sendDataChrono;
uint32_t sendDataInteval = 1000;   // time between loraSends
#define RFM_CS 8                // Chip select pin
#define RFM_IRQ 3               // Interrupt pin
#define RFM_FREQ 915.0          // Radio Frequency (MHz)
#define RFM_PWR 23              // Power setting
#define RFM_LBT_TOUT 3000       // LBT timeout in ms
RH_RF95 radioDriver(RFM_CS,RFM_IRQ);
RHDatagram datagram(radioDriver,0);

//CO2 Settings
#if CO2_ON
Chrono readCO2Chrono;
uint32_t co2ReadInteval = 1000;  //time between RTC reads in ms
int co2Addr = 0x68;
//int co2Addr = 0x7F;
int latestCO2 = 0;

//UART A on the dragon spike board
#define CO2Serial Serial2
Uart Serial2 (&sercom1, 11, 10, SERCOM_RX_PAD_0, UART_TX_PAD_2);
void SERCOM1_Handler()
{
  Serial2.IrqHandler();
}
byte readCO2[] = {0xFE, 0X44, 0X00, 0X08, 0X02, 0X9F, 0X25};  //Command packet to read Co2 (see app note)
byte response[] = {0,0,0,0,0,0,0};  //create an array to store the response
//multiplier for value. default is 1. set to 3 for K-30 3% and 10 for K-33 ICB
int valMultiplier = 1;
#endif


//==========================================================
// SOFTWARE SETUP
//==========================================================
// Dragon OS
DragonRadio radio(&datagram,DRAGON_RADIO_CACHE);
//DragonStore store(SD_CS,SD_FREQ);

bool sendData_on = true;         //to activate/deactive data sends

/*
// Serial commands interface (not needed for eggs)
char serial_command_buffer_[32];
SerialCommands serial_commands_(&Serial, serial_command_buffer_, sizeof(serial_command_buffer_), "\r\n", " ");
#include "serialCmds.h"
*/


//==========================================================
// MEASUREMENT VARIABLES
//==========================================================
#if DEVEL_ON
//Data structures
struct accelEventLvl_1 {
  char dateTime[20] = "";
  double magnitude = 0.0;
};
CircularBuffer<accelEventLvl_1,100> accelEventLvl_1s;
#endif

//To detect max accel. even within a time period
DateTime maxAccelTime;
float maxAccel = 0.0;
float maxAccel_xRunning = 0.0;
float maxAccel_yRunning = 0.0;
float maxAccel_zRunning = 0.0;
float maxAccel_x = 0.0;
float maxAccel_y = 0.0;
float maxAccel_z = 0.0;
Chrono maxAccelChrono;
uint32_t maxAccelInteval = 1000;    //timeout on detection of maximum accel event.


//==========================================================
// FUNCTIONS
//==========================================================
//Create a new unique filename in the root directory.
//The baseName has a incrementing counter added to ensure every new file is unique.
//The folder name also be set using a string.
//If safeOn==true, then also open a backup file for dual writing.
void makeNewLogFile(const char *baseName, const char *folderName, bool safeOn=true) {
  if (SDInit) {
    //Check/create the folder
    if (!sd.exists(folderName)) {
      if (!sd.mkdir(folderName)) {
        Serial.println("\n!!! error creating folder !!!\n");
        setError = true;
      }
    }
    //Change the working directory
    if (!sd.chdir(folderName)) {
      Serial.println("\n!!! error changing directory !!!\n");
      setError = true;
    }
    Serial.print("Working in folder: '");Serial.print(folderName);Serial.println("'");
  
    //File suffix started at zero
    int fileN = 0;
    sprintf(mainFileName,"%s_%03dm.dat",baseName,fileN);
    while(sd.exists(mainFileName)) {
      fileN++;
      sprintf(mainFileName,"%s_%03dm.dat",baseName,fileN);
    }
    //Open the file...
    File mainFile = sd.open(mainFileName,FILE_WRITE);
  
    //If the file is available, write a header to it...
    if (mainFile) {
      Serial.print("Created main file: '");Serial.print(mainFileName);Serial.println("'");
      mainFile.print("DragonEgg Main Log. SysID: ");mainFile.println(DRAGON_SYSTEMID_INT);
      mainFile.close();
    }  
    else {
      Serial.println("\n!!! error opening main file !!!\n");
      setError = true;
    }
   
    if (safeOn) {
      //Assuming there will be no conflict for the backup filename if 
      //the mainfile name is unique.
      sprintf(bkupFileName,"%s_%03db.dat",baseName,fileN);
      File backupFile = sd.open(bkupFileName,FILE_WRITE);
      if (backupFile) {
        Serial.print("Created bkup file: '");Serial.print(bkupFileName);Serial.println("'");
        backupFile.print("DragonEgg Backup Log. SysID: ");backupFile.println(DRAGON_SYSTEMID_INT);
        backupFile.close();
      }
      else {
        Serial.println("\n!!! error opening backup file !!!\n");
        setError = true;
      }
    }
  }
}

//Write the char message to the mainFile. If safeOn=true, then
//also write to an identical backup file. Always appends.
//Write to file and a backup. Only one file is open at once, hence 
//file corruption due to power loss cannot affect both files.
void safeWrite(char message[],bool safeOn=true) {
  if (SDInit) {
    File mainFile = sd.open(mainFileName,FILE_WRITE);
    if (mainFile) { 
      mainFile.println(message);
      mainFile.close();
    }  
    else {
      Serial.println("\n!!! error opening main file !!!\n");
      setError = true;
    }
    if (safeOn) {
      //Write to backup file
      File backupFile = sd.open(bkupFileName,FILE_WRITE);
      if (backupFile) {
        backupFile.println(message);
        backupFile.close();
      }  
      else {
        Serial.println("\n!!! error opening backup log file !!!\n");
        setError = true;
      }
    }
  }
}


//==========================================================
// SETUP/STARTUP
//==========================================================
void setup() {
  // Start the USB debug port.
  // Pause to allow connection to be established before first messages
  Serial.begin(9600);
  delay(1000);delay(1000);delay(1000);delay(1000);
  //while(!Serial.available()){}
  Serial.println("Dragon Arrow V2 startup");

  //Required for interacting with the LoRa and SD card.
  datagram.init();

  // Dragon OS setup. Init. the system ID (choose a unique ID per sensor pod)
  Serial.print("Unique Egg ID = "); Serial.println(DRAGON_SYSTEMID_INT);
  DragonOS::setSystemID(DRAGON_SYSTEMID_INT);

/*
  //Setup serial commands (not needed for eggs)
  regAllCmds();
*/

#if LED_ON
  // LED is on during setup. Then slow blink for normal operation, fast blink for an error
  digitalWrite(LED_PIN, HIGH);
  Serial.println("   solid LED    = startup");
  Serial.println("   5s LED blink = normal operation");
  Serial.println("   1s LED blink = error");
#endif

#if RTC_ON
  delay(500);
  Serial.print("Initialise RTC ... ");
  //Check if an RTC is connected
  if (!rtc.begin()) {
    Serial.println("\n!!! Couldn't start RTC !!!\n");
    setError = true;
  }
  if (rtc.lostPower()) {
  Serial.print("RTC lost power, lets set the time! ... ");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  //RTCTemp = DS3231_get_treg();
  Serial.print("Temp. = ");Serial.print(rtc.getTemperature());Serial.print("C ... ");
  Serial.println("done");
#endif

#if SD_ON
  delay(500);
  Serial.print("Initialise SD ... ");
  settings = SPISettings(1000000*SD_FREQ,MSBFIRST,SPI_MODE0);
  SDInit = sd.begin(SD_CS,settings);
  if (!SDInit) {
    Serial.println("\n!!! Couldn't start SD !!!\n");
    setError = true;
  }
  else {
    Serial.println("done");
    #if RTC_ON
    //SD is ok so make a folder name using the date
    DateTime now = rtc.now();
    char date_format[6] = {'Y','Y','M','M','D','D'};
    makeNewLogFile("REDEGG",now.toString(date_format),true);
    #else
    makeNewLogFile("REDEGG","DEFAULT",true);
    #endif
  }
#endif

#if ACCEL_ON
  delay(500);
  Serial.print("Initialise accelerometer ... ");
  //Check if an accelerometer is connected
  if (! mma.begin()) {
    Serial.println("\n!!! Couldn't start accelerometer !!!\n");
    setError = true;
  }
  //Set the expected measurement range
  //mma.setRange(MMA8451_RANGE_2_G);
  //Serial.print("Range +-2g ... ");
  //mma.setRange(MMA8451_RANGE_4_G);
  //Serial.print("Range +-4g ... ");
  mma.setRange(MMA8451_RANGE_8_G);
  Serial.print("Range +-8g ... ");
  Serial.println("done");
#endif

#if LORA_ON
  delay(500);
  Serial.print("Initialise LoRa...");
  radioDriver.setTxPower(RFM_PWR, false);
  radioDriver.setFrequency(RFM_FREQ);
  radioDriver.setCADTimeout(RFM_LBT_TOUT);
  Serial.print("freq. = ");Serial.print(RFM_FREQ);Serial.print("MHz ... ");
  // Set secondary universal listen address
  radio.addSecondaryAddress(DRAGON_LISTEN_ALL);
  Serial.println("done");
#endif

  //=====================
  // Specific setup
  //=====================
  // Measurement setup
  maxAccelTime = rtc.now();

#if CO2_ON
  //Start I2C interface
  Wire.begin();
  CO2Serial.begin(9600);
#endif

  delay(1000);
#if DEVEL_ON
  //Start the watchdog timer
  int countdownMS = Watchdog.enable(activityWatchdogTimeout);
#endif

  Serial.println("Startup complete!");
#if LED_ON
  digitalWrite(LED_PIN, LOW);
#endif
  Serial.println("");
}  //end setup()


//==========================================================
// MAIN LOOP
//==========================================================
void loop() {
#if LED_ON
  //See if an error has been set (setError = true) and blink LED appropriately
  checkErrorState();
#endif

#if DEVEL_ON
  //Reset the watchdog timer
  Watchdog.reset();
  //Watchdog.disable();
#endif

  //Display the number of minutes elapsed since power up
  minsElapsed();

#if BATT_ON
  //Check the battery health
  checkBattery();
#endif

#if ACCEL_ON
  if (readAccelChrono.hasPassed(accelReadInteval,true)) {
    mma.read();
    accel_x = mma.x_g;
    accel_y = mma.y_g;
    accel_z = mma.z_g;

    //Check if the latest read is a higher...
    float newAccel = sqrt(accel_x*accel_x + accel_y*accel_y + accel_z*accel_z);
    if (newAccel > maxAccel) {
      maxAccel = newAccel;
      maxAccel_xRunning = accel_x;
      maxAccel_yRunning = accel_y;
      maxAccel_zRunning = accel_z;
    }
    if (maxAccelChrono.hasPassed(maxAccelInteval,true)) {
      maxAccelTime = rtc.now();
      maxAccel_x = maxAccel_xRunning;
      maxAccel_y = maxAccel_yRunning;
      maxAccel_z = maxAccel_zRunning;
      maxAccel = 0.0;
      maxAccel_xRunning = 0.0;
      maxAccel_yRunning = 0.0;
      maxAccel_zRunning = 0.0;
      Serial.print("Max. accel. (g),");Serial.print(maxAccel_x); Serial.print(",");Serial.print(maxAccel_y); Serial.print(",");Serial.println(maxAccel_z);
    }
  }
#endif

#if RTC_ON
  if (readTimeChrono.hasPassed(timeReadInteval,true)) {
    // Retrieve current time from RTC chip
    DateTime now = rtc.now();
    
    //Store the data into variables
    time_yy = now.year();
    time_mo = now.month();
    time_dd = now.day();
    time_hh = now.hour();
    time_mm = now.minute();
    time_ss = now.second();

    /*
    Serial.print(time_yy, DEC);Serial.print('/');
    Serial.print(time_mo, DEC);Serial.print('/');
    Serial.print(time_dd, DEC);Serial.print(" (");
    Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);Serial.print(") ");
    Serial.print(time_hh, DEC);Serial.print(':');
    Serial.print(time_mm, DEC);Serial.print(':');
    Serial.print(time_ss, DEC);Serial.print(" (T ");Serial.print(rtc.getTemperature());Serial.print("C)");
    Serial.println();
    */
  }
#endif

#if CO2_ON
  if (readCO2Chrono.hasPassed(co2ReadInteval,true)) {
    //Start comms with the CO2 sensor
    Wire.beginTransmission(co2Addr);
    Wire.write(0x22);
    Wire.write(0x00);
    Wire.write(0x08);
    Wire.write(0x2A);
    Wire.endTransmission();
    //Wait 10ms for the sensor to process our command.
    delay(10);
    Wire.requestFrom(co2Addr, 4);
    delay(10);
    int i = 0;
    byte buffer[4] = {0, 0, 0, 0};
    while (Wire.available()) {
      buffer[i] = Wire.read();
      Serial.print(buffer[i]);Serial.print(" ");
      i++;
      Serial.println(i);
    }
  
//    latestCO2 = 0;
//    latestCO2 |= buffer[1] & 0xFF;
//    latestCO2 = latestCO2 << 8;
//    latestCO2 |= buffer[2] & 0xFF; 

    latestCO2 = 0;
    latestCO2 = (buffer[1] << 8) + buffer[2];
    
  
    //Checksum
    byte sum = 0; //Checksum Byte
    sum = buffer[0] + buffer[1] + buffer[2]; //Byte addition utilizes overflow
    if (sum == buffer[3]) {
     Serial.print("CO2 read success: ");Serial.println(latestCO2);
    }
    else {
      Serial.println("CO2 read fail");
    }

    //Serial version
    //sendRequest(readCO2);
    //unsigned long valCO2 = getValue(response);
    //Serial.print("Co2 ppm = ");    Serial.println(valCO2);
  }
#endif

#if SD_ON
  if (writeSDChrono.hasPassed(sdWriteInteval,true)) {
    /*
    File myFile = sd.open("test.txt", FILE_WRITE);
    char writeBuf[40];
    int sz = sprintf(writeBuf,"AccelXYZ,%d,%d,%d\n",accel_x,accel_y,accel_z);
    myFile.write(writeBuf,sz);
    sz = sprintf(writeBuf,"TimeYMD:HMS,%d,%d,%d:%d,%d,%d\n",time_yy,time_mo,time_dd,time_hh,time_mm,time_ss);
    myFile.write(writeBuf,sz);
    myFile.close();
    Serial.println("SD Write complete");
    */

    char writeBuf[64];
    int sz = sprintf(writeBuf,"AccelXYZ,%5.2f,%5.2f,%5.2f",accel_x,accel_y,accel_z);
    safeWrite(writeBuf,true);
    sz = sprintf(writeBuf,"TimeYMD:HMS,%d,%d,%d:%d,%d,%d\n",time_yy,time_mo,time_dd,time_hh,time_mm,time_ss);
    safeWrite(writeBuf,true);
    #if CO2_ON
    sz = sprintf(writeBuf,"CO2,%d\n",latestCO2);
    #endif
    safeWrite(writeBuf,true);
    Serial.println("SD write complete");
  }
#endif

#if LORA_ON
  // Update the radio (this is a DragonOS function which reads the RD modem and
  // attempts to recognise Dragon OS objects from the stream
  radio.update();
  switch (radio.available()) {
    case 0:
    break;

    case DragonObject_LORACMD::getObjectID():
    {
      DragonObject_LORACMD object;
      if (radio.recv(object)) {
        Serial.print("Received LORACMD package... ");
        //Interpret the command
        switch (object.getFields()->CMDID) {
          case 1:
          {
            //Start sending measurement data
            Serial.print("start Tx...");
            sendData_on = true;
          }
          break;
          case 2:
          {
            //Stop sending measurement data
            Serial.print("stop Tx...");
            sendData_on = false;
          }
          break;
          case 3:
          {
            //Set send inteval to 1 second (time in ms)
            Serial.print("inteval 1s...");
            sendDataInteval = 1000;
          }
          break;
          case 4:
          {
            //Set send inteval to 60 seconds  (time in ms)
            Serial.print("inteval 60s...");
            sendDataInteval = 60*1000;
          }
          break;
        }
        Serial.println("done");
      }
    }
    break;

    case DragonObject_ACK_LORACMD::getObjectID():
    {
      DragonObject_ACK_LORACMD object;
      if (radio.recv(object)) {
        Serial.println("Received ACK_LORACMD package");
        //Do something here!        
      }
    }
    
  } // end switch radio.available()

  //If the data send is enabled, then start sending at intevals
  if (sendData_on && sendDataChrono.hasPassed(sendDataInteval,true)) {
    if (false) {
    {
    DragonObject_MAX_ACCEL createdObject(DRAGON_SYSTEMID_INT,maxAccel_x,maxAccel_y,maxAccel_z,maxAccelTime.unixtime());
    radio.send(createdObject,DRAGON_TARGET_ID);
    Serial.println("Sent MAX_ACCEL package");
    }
    {
    DateTime now = rtc.now();
    DragonObject_RTC_TEMP createdObject(DRAGON_SYSTEMID_INT,now.unixtime(),rtc.getTemperature());
    radio.send(createdObject,DRAGON_TARGET_ID);
    Serial.println("Sent RTC_TEMP package");
    }
    }

    //Make a blank mavlink message
    mavlink_message_t MavlinkMsg;             //struct to encode MAVLink data into
    uint16_t MavlinkMsg_Length;               //size of the mavlink message
    uint8_t buf[250];                         //buffer to hold packed message before sending

    //Pack the data into the mavlink object
    mavlink_msg_dl_internal_temp_pack(0, 0, &MavlinkMsg,
                                      (uint8_t)1,
                                      (uint8_t)2,
                                      (uint16_t)rtc.getTemperature());
    //Pack into a byte buffer
    MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
    
    
  }
  
#endif

}  //end main loop()


//==========================================================
// FUNCTIONS
//==========================================================
//Function to make the LED blink with a heartbeat during
//normal operation and flash quickly when an error state has
//been set.
void checkErrorState(void) {
  if (setError) {
    if (errorLedChrono.hasPassed(errorLedErrorInteval,true)) {
      errorLedState = !errorLedState;
      digitalWrite(LED_PIN, errorLedState);
    }
  }
  else {
    //Short blink at longer intevals
    if (errorLedChrono.hasPassed(errorLedNormalInteval,true)) {
      if (!errorLedState) {
        errorLedState = HIGH;
        digitalWrite(LED_PIN, errorLedState);
        blinkChrono.restart();
      }
    }
    if (blinkChrono.hasPassed(100)) {
      errorLedState = LOW;
      digitalWrite(LED_PIN, errorLedState);
      blinkChrono.stop();
    }
  }
}

//Count and display the minutes elapsed since start. This is
//just a basic counter and might drift slightly.
uint32_t minsElapsed(void) {
  if (minuteCounter.hasPassed(minuteCountInteval,true)) {
    totalMins++;
    Serial.print("Mins elapsed: "); Serial.println(totalMins);
    return totalMins;
  }
}

/*
//Get the internal chip temperature from the DS3231 RTC board
float DS3231_get_treg(void) {
  int16_t temp;
  Wire.beginTransmission(DS3231_ADDRESS);
  Wire.write(0x11);   //this is the 'command' to retrieve temperature
  Wire.endTransmission();
  Wire.requestFrom(DS3231_ADDRESS,2);
  temp = Wire.read() << 8;
  temp |= Wire.read();
  return (temp/256.0);
}
*/

#if BATT_ON
//Check the battery voltage. The returned value is binned according to the settings.
uint8_t checkBattery(void) {
  float measuredVBat = analogRead(VBATPIN);
  measuredVBat *= 2.0;   //remove effect of potential divider
  measuredVBat *= 3.3;   //multiply by reference voltage
  measuredVBat /= 1024;  //divide by bit depth
  latestVoltage = measuredVBat;
  if (batteryCheckChrono.hasPassed(batteryCheckInteval,true)) {
    Serial.print("Battery voltage: "); Serial.print(latestVoltage);Serial.println(" V");
  }
  //Check the health 'code'
  if      (latestVoltage > batFullV) {return 0;}
  else if (latestVoltage > batNormV) {return 1;}
  else if (latestVoltage > batLowV)  {return 2;}
  else if (latestVoltage > batCritV) {return 3;}
  else {return 4;}
}
bool isBatteryFull(void) {
  return (checkBattery()==1);
}
bool isBatteryNormal(void) {
  return (checkBattery()==2);
}
bool isBatteryLow(void) {
  return (checkBattery()==3);
}
bool isBatteryCritical(void) {
  return (checkBattery()==4);
}
#endif

#if DEVEL_ON
//Go to sleep (low power mode)
uint32_t lowPowerMode(uint32_t sleepTime_ms) {
  //Shut down any files/peripherals first
  //TODO more
  //radio.sleep();

  //Put device to sleep
  uint32_t sleepMS = Watchdog.sleep(sleepTime_ms);

  //Code resumes on wake, so
  //start all files/peripherals again...
  //TODO

  //Reattach USB (might not work 100%)
  #if defined(USBCON) && !defined(USE_TINYUSB)
    USBDevice.attach();
  #endif
}
#endif


#if CO2_ON
void sendRequest(byte packet[])
{
  while(!CO2Serial.available())  //keep sending request until we start to get a response
  {
    CO2Serial.write(readCO2,7);
    delay(50);
  }
  
  int timeout=0;  //set a timeoute counter
  while(CO2Serial.available() < 7 ) //Wait to get a 7 byte response
  {
    timeout++;  
    if(timeout > 10)    //if it takes to long there was probably an error
      {
        while(CO2Serial.available())  //flush whatever we have
          CO2Serial.read();
          
          break;                        //exit and try again
      }
      delay(50);
  }
  
  for (int i=0; i < 7; i++)
  {
    response[i] = CO2Serial.read();
  }  
}

unsigned long getValue(byte packet[])
{
    int high = packet[3];                        //high byte for value is 4th byte in packet in the packet
    int low = packet[4];                         //low byte for value is 5th byte in the packet
    unsigned long val = high*256 + low;                //Combine high byte and low byte with this formula to get value
    return val* valMultiplier;
}
#endif

//end of file
