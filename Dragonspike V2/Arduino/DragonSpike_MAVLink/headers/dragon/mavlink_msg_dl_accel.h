#pragma once
// MESSAGE DL_ACCEL PACKING

#define MAVLINK_MSG_ID_DL_ACCEL 9001


typedef struct __mavlink_dl_accel_t {
 uint32_t accel; /*<  Acceleration (m/s*E6)*/
 uint8_t target_system; /*<  Target System ID*/
 uint8_t source_component; /*<  Source System ID*/
} mavlink_dl_accel_t;

#define MAVLINK_MSG_ID_DL_ACCEL_LEN 6
#define MAVLINK_MSG_ID_DL_ACCEL_MIN_LEN 6
#define MAVLINK_MSG_ID_9001_LEN 6
#define MAVLINK_MSG_ID_9001_MIN_LEN 6

#define MAVLINK_MSG_ID_DL_ACCEL_CRC 39
#define MAVLINK_MSG_ID_9001_CRC 39



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DL_ACCEL { \
    9001, \
    "DL_ACCEL", \
    3, \
    {  { "target_system", NULL, MAVLINK_TYPE_UINT8_T, 0, 4, offsetof(mavlink_dl_accel_t, target_system) }, \
         { "source_component", NULL, MAVLINK_TYPE_UINT8_T, 0, 5, offsetof(mavlink_dl_accel_t, source_component) }, \
         { "accel", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_accel_t, accel) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DL_ACCEL { \
    "DL_ACCEL", \
    3, \
    {  { "target_system", NULL, MAVLINK_TYPE_UINT8_T, 0, 4, offsetof(mavlink_dl_accel_t, target_system) }, \
         { "source_component", NULL, MAVLINK_TYPE_UINT8_T, 0, 5, offsetof(mavlink_dl_accel_t, source_component) }, \
         { "accel", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_accel_t, accel) }, \
         } \
}
#endif

/**
 * @brief Pack a dl_accel message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param target_system  Target System ID
 * @param source_component  Source System ID
 * @param accel  Acceleration (m/s*E6)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_accel_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t target_system, uint8_t source_component, uint32_t accel)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_ACCEL_LEN];
    _mav_put_uint32_t(buf, 0, accel);
    _mav_put_uint8_t(buf, 4, target_system);
    _mav_put_uint8_t(buf, 5, source_component);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_ACCEL_LEN);
#else
    mavlink_dl_accel_t packet;
    packet.accel = accel;
    packet.target_system = target_system;
    packet.source_component = source_component;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_ACCEL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_ACCEL;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DL_ACCEL_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL_LEN, MAVLINK_MSG_ID_DL_ACCEL_CRC);
}

/**
 * @brief Pack a dl_accel message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param target_system  Target System ID
 * @param source_component  Source System ID
 * @param accel  Acceleration (m/s*E6)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_accel_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t target_system,uint8_t source_component,uint32_t accel)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_ACCEL_LEN];
    _mav_put_uint32_t(buf, 0, accel);
    _mav_put_uint8_t(buf, 4, target_system);
    _mav_put_uint8_t(buf, 5, source_component);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_ACCEL_LEN);
#else
    mavlink_dl_accel_t packet;
    packet.accel = accel;
    packet.target_system = target_system;
    packet.source_component = source_component;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_ACCEL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_ACCEL;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DL_ACCEL_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL_LEN, MAVLINK_MSG_ID_DL_ACCEL_CRC);
}

/**
 * @brief Encode a dl_accel struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param dl_accel C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_accel_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_dl_accel_t* dl_accel)
{
    return mavlink_msg_dl_accel_pack(system_id, component_id, msg, dl_accel->target_system, dl_accel->source_component, dl_accel->accel);
}

/**
 * @brief Encode a dl_accel struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dl_accel C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_accel_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_dl_accel_t* dl_accel)
{
    return mavlink_msg_dl_accel_pack_chan(system_id, component_id, chan, msg, dl_accel->target_system, dl_accel->source_component, dl_accel->accel);
}

/**
 * @brief Send a dl_accel message
 * @param chan MAVLink channel to send the message
 *
 * @param target_system  Target System ID
 * @param source_component  Source System ID
 * @param accel  Acceleration (m/s*E6)
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_dl_accel_send(mavlink_channel_t chan, uint8_t target_system, uint8_t source_component, uint32_t accel)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_ACCEL_LEN];
    _mav_put_uint32_t(buf, 0, accel);
    _mav_put_uint8_t(buf, 4, target_system);
    _mav_put_uint8_t(buf, 5, source_component);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_ACCEL, buf, MAVLINK_MSG_ID_DL_ACCEL_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL_LEN, MAVLINK_MSG_ID_DL_ACCEL_CRC);
#else
    mavlink_dl_accel_t packet;
    packet.accel = accel;
    packet.target_system = target_system;
    packet.source_component = source_component;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_ACCEL, (const char *)&packet, MAVLINK_MSG_ID_DL_ACCEL_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL_LEN, MAVLINK_MSG_ID_DL_ACCEL_CRC);
#endif
}

/**
 * @brief Send a dl_accel message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_dl_accel_send_struct(mavlink_channel_t chan, const mavlink_dl_accel_t* dl_accel)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_dl_accel_send(chan, dl_accel->target_system, dl_accel->source_component, dl_accel->accel);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_ACCEL, (const char *)dl_accel, MAVLINK_MSG_ID_DL_ACCEL_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL_LEN, MAVLINK_MSG_ID_DL_ACCEL_CRC);
#endif
}

#if MAVLINK_MSG_ID_DL_ACCEL_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This variant of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_dl_accel_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t target_system, uint8_t source_component, uint32_t accel)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, accel);
    _mav_put_uint8_t(buf, 4, target_system);
    _mav_put_uint8_t(buf, 5, source_component);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_ACCEL, buf, MAVLINK_MSG_ID_DL_ACCEL_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL_LEN, MAVLINK_MSG_ID_DL_ACCEL_CRC);
#else
    mavlink_dl_accel_t *packet = (mavlink_dl_accel_t *)msgbuf;
    packet->accel = accel;
    packet->target_system = target_system;
    packet->source_component = source_component;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_ACCEL, (const char *)packet, MAVLINK_MSG_ID_DL_ACCEL_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL_LEN, MAVLINK_MSG_ID_DL_ACCEL_CRC);
#endif
}
#endif

#endif

// MESSAGE DL_ACCEL UNPACKING


/**
 * @brief Get field target_system from dl_accel message
 *
 * @return  Target System ID
 */
static inline uint8_t mavlink_msg_dl_accel_get_target_system(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  4);
}

/**
 * @brief Get field source_component from dl_accel message
 *
 * @return  Source System ID
 */
static inline uint8_t mavlink_msg_dl_accel_get_source_component(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  5);
}

/**
 * @brief Get field accel from dl_accel message
 *
 * @return  Acceleration (m/s*E6)
 */
static inline uint32_t mavlink_msg_dl_accel_get_accel(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Decode a dl_accel message into a struct
 *
 * @param msg The message to decode
 * @param dl_accel C-struct to decode the message contents into
 */
static inline void mavlink_msg_dl_accel_decode(const mavlink_message_t* msg, mavlink_dl_accel_t* dl_accel)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    dl_accel->accel = mavlink_msg_dl_accel_get_accel(msg);
    dl_accel->target_system = mavlink_msg_dl_accel_get_target_system(msg);
    dl_accel->source_component = mavlink_msg_dl_accel_get_source_component(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DL_ACCEL_LEN? msg->len : MAVLINK_MSG_ID_DL_ACCEL_LEN;
        memset(dl_accel, 0, MAVLINK_MSG_ID_DL_ACCEL_LEN);
    memcpy(dl_accel, _MAV_PAYLOAD(msg), len);
#endif
}
