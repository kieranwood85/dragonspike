#pragma once
// MESSAGE DL_TIME PACKING

#define MAVLINK_MSG_ID_DL_TIME 9002


typedef struct __mavlink_dl_time_t {
 uint32_t time; /*<  UTC timestamp*/
 uint8_t target_system; /*<  Target System ID*/
 uint8_t source_component; /*<  Source System ID*/
} mavlink_dl_time_t;

#define MAVLINK_MSG_ID_DL_TIME_LEN 6
#define MAVLINK_MSG_ID_DL_TIME_MIN_LEN 6
#define MAVLINK_MSG_ID_9002_LEN 6
#define MAVLINK_MSG_ID_9002_MIN_LEN 6

#define MAVLINK_MSG_ID_DL_TIME_CRC 61
#define MAVLINK_MSG_ID_9002_CRC 61



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DL_TIME { \
    9002, \
    "DL_TIME", \
    3, \
    {  { "target_system", NULL, MAVLINK_TYPE_UINT8_T, 0, 4, offsetof(mavlink_dl_time_t, target_system) }, \
         { "source_component", NULL, MAVLINK_TYPE_UINT8_T, 0, 5, offsetof(mavlink_dl_time_t, source_component) }, \
         { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_time_t, time) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DL_TIME { \
    "DL_TIME", \
    3, \
    {  { "target_system", NULL, MAVLINK_TYPE_UINT8_T, 0, 4, offsetof(mavlink_dl_time_t, target_system) }, \
         { "source_component", NULL, MAVLINK_TYPE_UINT8_T, 0, 5, offsetof(mavlink_dl_time_t, source_component) }, \
         { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_time_t, time) }, \
         } \
}
#endif

/**
 * @brief Pack a dl_time message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param target_system  Target System ID
 * @param source_component  Source System ID
 * @param time  UTC timestamp
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_time_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t target_system, uint8_t source_component, uint32_t time)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_TIME_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint8_t(buf, 4, target_system);
    _mav_put_uint8_t(buf, 5, source_component);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_TIME_LEN);
#else
    mavlink_dl_time_t packet;
    packet.time = time;
    packet.target_system = target_system;
    packet.source_component = source_component;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_TIME_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_TIME;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DL_TIME_MIN_LEN, MAVLINK_MSG_ID_DL_TIME_LEN, MAVLINK_MSG_ID_DL_TIME_CRC);
}

/**
 * @brief Pack a dl_time message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param target_system  Target System ID
 * @param source_component  Source System ID
 * @param time  UTC timestamp
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_time_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t target_system,uint8_t source_component,uint32_t time)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_TIME_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint8_t(buf, 4, target_system);
    _mav_put_uint8_t(buf, 5, source_component);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_TIME_LEN);
#else
    mavlink_dl_time_t packet;
    packet.time = time;
    packet.target_system = target_system;
    packet.source_component = source_component;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_TIME_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_TIME;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DL_TIME_MIN_LEN, MAVLINK_MSG_ID_DL_TIME_LEN, MAVLINK_MSG_ID_DL_TIME_CRC);
}

/**
 * @brief Encode a dl_time struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param dl_time C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_time_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_dl_time_t* dl_time)
{
    return mavlink_msg_dl_time_pack(system_id, component_id, msg, dl_time->target_system, dl_time->source_component, dl_time->time);
}

/**
 * @brief Encode a dl_time struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dl_time C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_time_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_dl_time_t* dl_time)
{
    return mavlink_msg_dl_time_pack_chan(system_id, component_id, chan, msg, dl_time->target_system, dl_time->source_component, dl_time->time);
}

/**
 * @brief Send a dl_time message
 * @param chan MAVLink channel to send the message
 *
 * @param target_system  Target System ID
 * @param source_component  Source System ID
 * @param time  UTC timestamp
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_dl_time_send(mavlink_channel_t chan, uint8_t target_system, uint8_t source_component, uint32_t time)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_TIME_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint8_t(buf, 4, target_system);
    _mav_put_uint8_t(buf, 5, source_component);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_TIME, buf, MAVLINK_MSG_ID_DL_TIME_MIN_LEN, MAVLINK_MSG_ID_DL_TIME_LEN, MAVLINK_MSG_ID_DL_TIME_CRC);
#else
    mavlink_dl_time_t packet;
    packet.time = time;
    packet.target_system = target_system;
    packet.source_component = source_component;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_TIME, (const char *)&packet, MAVLINK_MSG_ID_DL_TIME_MIN_LEN, MAVLINK_MSG_ID_DL_TIME_LEN, MAVLINK_MSG_ID_DL_TIME_CRC);
#endif
}

/**
 * @brief Send a dl_time message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_dl_time_send_struct(mavlink_channel_t chan, const mavlink_dl_time_t* dl_time)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_dl_time_send(chan, dl_time->target_system, dl_time->source_component, dl_time->time);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_TIME, (const char *)dl_time, MAVLINK_MSG_ID_DL_TIME_MIN_LEN, MAVLINK_MSG_ID_DL_TIME_LEN, MAVLINK_MSG_ID_DL_TIME_CRC);
#endif
}

#if MAVLINK_MSG_ID_DL_TIME_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This variant of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_dl_time_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t target_system, uint8_t source_component, uint32_t time)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint8_t(buf, 4, target_system);
    _mav_put_uint8_t(buf, 5, source_component);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_TIME, buf, MAVLINK_MSG_ID_DL_TIME_MIN_LEN, MAVLINK_MSG_ID_DL_TIME_LEN, MAVLINK_MSG_ID_DL_TIME_CRC);
#else
    mavlink_dl_time_t *packet = (mavlink_dl_time_t *)msgbuf;
    packet->time = time;
    packet->target_system = target_system;
    packet->source_component = source_component;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_TIME, (const char *)packet, MAVLINK_MSG_ID_DL_TIME_MIN_LEN, MAVLINK_MSG_ID_DL_TIME_LEN, MAVLINK_MSG_ID_DL_TIME_CRC);
#endif
}
#endif

#endif

// MESSAGE DL_TIME UNPACKING


/**
 * @brief Get field target_system from dl_time message
 *
 * @return  Target System ID
 */
static inline uint8_t mavlink_msg_dl_time_get_target_system(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  4);
}

/**
 * @brief Get field source_component from dl_time message
 *
 * @return  Source System ID
 */
static inline uint8_t mavlink_msg_dl_time_get_source_component(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  5);
}

/**
 * @brief Get field time from dl_time message
 *
 * @return  UTC timestamp
 */
static inline uint32_t mavlink_msg_dl_time_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Decode a dl_time message into a struct
 *
 * @param msg The message to decode
 * @param dl_time C-struct to decode the message contents into
 */
static inline void mavlink_msg_dl_time_decode(const mavlink_message_t* msg, mavlink_dl_time_t* dl_time)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    dl_time->time = mavlink_msg_dl_time_get_time(msg);
    dl_time->target_system = mavlink_msg_dl_time_get_target_system(msg);
    dl_time->source_component = mavlink_msg_dl_time_get_source_component(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DL_TIME_LEN? msg->len : MAVLINK_MSG_ID_DL_TIME_LEN;
        memset(dl_time, 0, MAVLINK_MSG_ID_DL_TIME_LEN);
    memcpy(dl_time, _MAV_PAYLOAD(msg), len);
#endif
}
