// REMEMBER TO CHECK FOR CORRECT PIN DEFINITIONS

//==================================================================================================
// GLOBALS (GENERIC)
//==================================================================================================
//Which hardware is installed?
#define RADEGG 0                // If the radiation sensor is connected
#define ACCELEGG 0              // If the accelerometer chip is present
#define GASEGG 0                // If the gas sensors are connected
#define GPSEGG 0                // If the GPS sensor is connected
#define RTC_ENABLED 0           // If the RTC chip is present or not
#define SATCOMMS 0              // If an Iridium satellite TRx is used

#define RECEIVER 1              // Is this deployed or a receiver?
#define DEBUG 1                 // USB serial debug messages on/off (i.e verbose mode)
#define WAIT_FOR_USB_CONNECTION 1 // Only run code once a USB serial connection has been made

// Annoying workaround required because it conflicts with ArduinoSTL (hopefully doesn't cause serious issues!)
#undef min
#undef max

// Hardware libraries for DragonSpikeV2 boards
#include <RHDatagram.h>         // For for packing/handling LoRa transmissions
#include <RH_RF95.h>            // The FeatherM0 LoRa module
#include <Adafruit_GPS.h>       // The GPS module (if installed)
#include <string.h>             //For feather I/O
#include "wiring_private.h"     //For Feather I/O

// DragonOS and settings
#include <DragonOS.h>
#define DRAGON_SYSTEMID 1       // Dragon system ID (Greater than zero!)
#define DRAGON_RADIO_CACHE 5    // Received object cache size TODO: what is this?
#define DRAGON_TARGET_ID 1      // The target ID to send the data to over LoRa

// Other libs
#include <ArduinoSTL.h>         // ArduinoSTL provides C++ standard library features such as vectors
#include <Chrono.h>             // For scheduled timing control

// Radio settings
#define RFM_CS 8                // Chip select pin
#define RFM_IRQ 3               // Interrupt pin
#define RFM_FREQ 868            // Radio Frequency (MHz)
#define RFM_PWR 23              // Power setting
#define RFM_LBT_TOUT 3000       // LBT timeout. 
//By setting a LBT value, the modem automatically waits for the channel to clear without using calling
//an additional specific function (i.e. it is part of the send function). Note to check the result of
//send function since the LBT might fail and hence the message never sends! Need to pass through the
//returned bool from the RF95 driver via the Dragon OS. TODO!

// SD settings
#define SD_CS 19                // Chip select pin (this is for the DragonSpike V2)
#define SD_FREQ 24              // SPI Frequency (MHz)

// LoRa radio setup
RH_RF95 radioDriver(RFM_CS, RFM_IRQ);
RHDatagram datagram(radioDriver, 0);

// Dragon OS
DragonRadio radio(&datagram, DRAGON_RADIO_CACHE);
DragonStore store(SD_CS, SD_FREQ);

// LED error flash messages
#define LED_PIN 13
int errorLedState = HIGH;
Chrono errorLedChrono;
Chrono blinkChrono;
uint32_t errorLedNormal = 5000;
uint32_t errorLedError = 1000;
bool setError = false;

// Minute counter
Chrono minuteCounter;
uint32_t minuteCountTime = 60000;
uint32_t totalMins = 0;

//==================================================================================================
// GLOBALS (SENSOR SPECIFIC) (typically not used on receiver)
//==================================================================================================
#if RADEGG
#endif
#if ACCELEGG
#endif
#if GASEGG
#endif
#if GPSEGG
#endif

#if SATCOMMS
//===========
//If the Iridium satellite modem is enabled then the receiver module will be capable of
//sending packets of data from anywhere on the planet with a reasonable view of the sky.
//Data packets must be compressed to 48 bytes of data.
//===========
#include <IridiumSBD.h>                    //For Iridium Satcom support
#define IRIDIUM_DIAGNOSTICS true           // Change this to see iridium sat modem diagnostics

//SATCOM configuration
#define PACKET_SIZE 48  //Try to keep message short (48 byte=1block, so sent all in one go)
#define STX_BITE 0x02
#define ETX_BITE 0x03

//Outgoing satcomms packet structure.
//Created as a structure to hold various types of data, and then 'unioned' with a
//char array to allow bytewise access.
//
//The "__attribute__((__packed__))" statement just ensures the data within the struct
//stays the same as defined here (ie. the compiler does not optimise or rearrange).
//
//Note: ideally we want the total bytes to be below 48 so a complete message can be sent as one.
typedef struct __attribute__((__packed__)) satDragonSpikeData
{
  byte STX;                   // 1 byte (start message/end message)
  uint16_t bitmask1;          // 2 byte (for any IO or health settings messages (i.e. 2 bytes are 16 1s or 0s)
  int32_t  mostSigT;          // 4 byte The time of the highest counts reported (posix time)
  uint16_t mostSigCounts;     // 2 byte The number of counts at the most significant 10s of time
  byte ETX;                   // 1 byte
  //                      TOTAL: 20 byte less than 48 OK
};
//Outgoing beacon packet union
typedef union satDragonSpikePacket
{
  satDragonSpikeData data;      //"alias 1": memory data to be interpreted as struct containing packet data
  uint8_t packet[PACKET_SIZE];  //"alias 2": memory data to be interpreted as binary packet of bytes
};

// TODO: CHECK THE PINS ARE CORRECT
// Need to set up the serial port to talk with the Iridium modem
// Config SERCOM1 as Serial3 on pins 11, 10 for Iridium
Uart Serial2 (&sercom1, 11, 10, SERCOM_RX_PAD_0, UART_TX_PAD_2);
void SERCOM1_Handler()
{
  Serial2.IrqHandler();
}

//Decleare the satcomms object
IridiumSBD modem(Serial2);

//Make a message object
satDragonSpikePacket messageDragonSpike;

int signalQuality = -1;               // Sat modem signal quality

//Control the rate of sending
Chrono satcommChrono;
uint32_t satcommSendRate = 600000;  //interval transmit. 600000 is 10 mins
bool satSendNow  = false;           //flag to transmit when data is ready
bool satTxActive = false;           //flag to indicate a satellite Tx attempt is in progress
#define SAT_SEND_ON_TIMER 1
#define SAT_SEND_ON_READY 1

//Received messages
#define SAT_RX_ON 1
#define SAT_RX_BUF_LGTH 270

//Sent messages
#define SAT_TX_BUF_LGTH 340
uint8_t satTxBuffer[SAT_TX_BUF_LGTH];

#endif

//==================================================================================================
// SETUP
//==================================================================================================
void setup() {
  // Start the USB debug port.
  // Pause to allow connection to be established before first messages
  Serial.begin(9600);
#if WAIT_FOR_USB_CONNECTION
  while (!Serial) {
    delay(500);
  }
  Serial.println("Start in 4s ...");
#endif

  delay(1000); delay(1000); delay(1000); delay(1000);
  Serial.println("Dragon Arrow V2 startup...");

  // LED is on during setup. Then slow blink for normal operation, fast blink for an error
  Serial.println("   solid LED    = startup");
  Serial.println("   5s LED blink = normal operation");
  Serial.println("   1s LED blink = error");
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);

  // Init. the system ID (choose a unique ID per sensor pod)
  Serial.print("Unique Egg ID = "); Serial.println(DRAGON_SYSTEMID);
  DragonOS::setSystemID(DRAGON_SYSTEMID);

  // Initialise radio driver and datastore (note that radio driver is initiliased directly with RadioHead here)
  // Set radio power, frequency, and LBT timeout
#if DEBUG
  Serial.println("Starting radio...");
  Serial.print("   TxPower     = "); Serial.println(RFM_PWR);
  Serial.print("   Frequency   = "); Serial.println(RFM_FREQ);
  Serial.print("   LBT timeout = "); Serial.println(RFM_LBT_TOUT);
#endif
  datagram.init();
  store.begin();
  radioDriver.setTxPower(RFM_PWR, false);
  radioDriver.setFrequency(RFM_FREQ);
  radioDriver.setCADTimeout(RFM_LBT_TOUT);

  // Set to be a dual reciver
  radio.addSecondaryAddress(33);

#if RADEGG
#endif
#if ACCELEGG
#endif
#if GASEGG
#endif
#if GPSEGG
#endif

#if SATCOMMS
  // Start the serial port connected to the satellite modem
  //TODO: is this needed when using softserial?
  Serial2.begin(19200);

  // TODO: CHECK THE PINS ARE CORRECT
  // Assign pins 10 & 11 SERCOM1 functionality (Seria2)
  pinPeripheral(10, PIO_SERCOM);  //TX (10)
  pinPeripheral(11, PIO_SERCOM);  //RX (11)

  // Begin satellite modem operation
  int satErr;
  Serial.println("SAT: Starting modem...");
  satErr = modem.begin();
  if (satErr != ISBD_SUCCESS)
  {
    Serial.print("Begin failed: error "); Serial.println(satErr);
    if (satErr == ISBD_NO_MODEM_DETECTED)
      Serial.println("No modem detected: check wiring.");
    return;
  }

  //Set up satellite modem
  modem.setPowerProfile(IridiumSBD::USB_POWER_PROFILE); //Low power, 90ma, T>60s
  //modem.setPowerProfile(IridiumSBD::DEFAULT_POWER_PROFILE); //High power, 450ma, T>20s
  //Exprimental
  //modem.adjustATTimeout(20);          // default value = 20 seconds
  //modem.adjustSendReceiveTimeout(300); // default value = 300 seconds

  //Start the timer
  satcommChrono.restart(satcommSendRate);

#endif

  delay(1000);
  Serial.println("Startup complete!");
  digitalWrite(LED_PIN, LOW);
  Serial.println("");
}


//==================================================================================================
// MAIN LOOP
//==================================================================================================
void loop() {
  //Due to a quirk in the setup of the IridiumISB library, whenever there is an interaction with
  //the rockblock, the send/receive functions will block the main thread, but will continue to call
  //the ISBDCallback function. Hence all code except the satcomms is wrapped up in a sub-function which
  //is called either by the main loop or by the ISBDCallback. Either way, most of the code continues to
  //run at all times.
  loop2();

#if SATCOMMS
  //The satellite messages are sent either when there is data waiting, or on a timer.
  //The user can control which (or both) modes are active. If using a timer, and there
  //is more data waiting than can be sent in a single burst, then the newest sensor data
  //is sent.
  if (SAT_SEND_ON_TIMER && (satcommChrono.hasPassed(satcommSendRate, true))) {
    satTxActive = true;
  }
  if (SAT_SEND_ON_READY && satSendNow) {
    satTxActive = true;
  }
  if (satTxActive) {
    Serial.println("SAT: Start Iridium transmission...");

    //Pack the data
#if 0
    satDragonSpike.data.STX = STX_BITE;
    satDragonSpike.data.bitmask1 = 1;   //TODO: some bit manipulation for status IO messages
    satDragonSpike.data.GPS_lat = 12.345;
    satDragonSpike.data.GPS_lon = 54.321;
    satDragonSpike.data.GPS_alt = 111.222;
    satDragonSpike.data.ETX = ETX_BITE;
#endif

    int satErr;
    size_t satTxBufferSize = sizeof(satTxBuffer);
    if (int(satTxBufferSize) > SAT_TX_BUF_LGTH) {
      Serial.println("SAT:ERROR -> more than 50 byte message");
      setError = true;
    }

#if SAT_RX_ON
    uint8_t satRxBuffer[SAT_RX_BUF_LGTH];     //make a receive buffer
    size_t satRxBufferSize = sizeof(satRxBuffer);  //get the size of the recive buffer. The max is 270 bytes.

    satErr = modem.sendReceiveSBDBinary(satTxBuffer, satTxBufferSize, satRxBuffer, satRxBufferSize);
#else
    satErr = modem.sendSBDBinary(satTxBuffer, satTxBufferSize);
#endif

    //At this point the IridiumSBD library blocks until either an error, timeout, or success
    //The library then begins calling ISBDCallback to ensure the other functions continue.
    //Once the blocking is finished, we interpret the result...
    if (satErr != ISBD_SUCCESS)
    {
      Serial.print("SAT: send failed: code "); Serial.println(satErr);
      if (satErr == ISBD_SENDRECEIVE_TIMEOUT) {
        Serial.println("SAT: timeout error, try better view of sky.");
      }
      satTxActive = false;
    }
    else
    {
      Serial.println("SAT: message sent!");
      satTxActive = false;
      Serial.println("SAT: putting modem to sleep.");
      satErr = modem.sleep();   //TODO: need to deifne the sleep pin
      if (satErr != ISBD_SUCCESS)
      {
        Serial.print("SAT: sleep failed: error "); Serial.println(satErr);
        setError = true;
      }

#if SAT_RX_ON
      //TODO: Interpret the received messages in a more meaninful way
      Serial.print("Inbound buffer size is "); Serial.println(satRxBufferSize);
      for (int i = 0; i < satRxBufferSize; ++i)
      {
        Serial.print(satRxBuffer[i], HEX);
      }
      Serial.print("Messages remaining to be retrieved: ");
      Serial.println(modem.getWaitingMessageCount());
#endif
    }
  }
#endif
}

bool ISBDCallback() {
  return loop2();
}

bool loop2() {

  //See if an error has been set (setError = true) and blink LED appropriately
  checkErrorState();
  minsElapsed();

  // Update the radio (this is a DragonOS function which reads the RD modem and
  // attempts to recognise Dragon OS objects from the stream.
  radio.update();

  // Handle received objects if available
  // NOTE: Must scope each case {} or issues will occur
  switch (radio.available()) {
    case 0:
      break;
      case DragonObject_GPSllaT::getObjectID():
      {
        DragonObject_GPSllaT object;
        if (radio.recv(object)) {
          Serial.println("Received GPSllaT package ...");

          // Print the value of each field through object access
          Serial.print("Sender ID     : "); Serial.println(object.getFields()->DEVID);
          Serial.print("Receiver ID   : "); Serial.println(object.getFields()->TGTID);
          Serial.print("Datapacket ID : "); Serial.println(object.getFields()->PKTID);
          Serial.print("Latitude = "); Serial.println(object.getFields()->Lat);
          Serial.print("Longitude = "); Serial.println(object.getFields()->Lon);
          Serial.print("Altitude = "); Serial.println(object.getFields()->Alt);
          Serial.print("N Counts = "); Serial.println(object.getFields()->CPS);
          Serial.print("Time = ");     Serial.println(object.getFields()->Time);

          //Send back the RECEIVEDOK confirmation
          DragonObject_RECEIVEDOK receivedokObj((uint32_t)object.getFields()->DEVID, (uint32_t)object.getFields()->TGTID, (uint32_t)object.getFields()->PKTID);
          radio.send(receivedokObj, (uint32_t)object.getFields()->DEVID);
          datagram.waitPacketSent();

          //Store the data locally
          uint32_t storeID = store.save(object);
          Serial.println("   sent RECEIVEDOK package");
        }
      }
      break;

    case DragonObject_HELLO::getObjectID():
      {
        DragonObject_HELLO object;
        if (radio.recv(object)) {
          Serial.println("Received HELLO package ...");
#if DEBUG
          Serial.print("   Sending ID : "); Serial.println(object.getFields()->DEVID);
          Serial.print("   Target ID  : "); Serial.println(object.getFields()->TGTID);
#endif
          //Send back the IMHERE confirmation
          DragonObject_IMHERE imhereObj((uint32_t)object.getFields()->DEVID, (uint32_t)object.getFields()->TGTID);
          // radioDriver.waitCAD();
          radio.send(imhereObj, (uint32_t)object.getFields()->DEVID);
          datagram.waitPacketSent();
          Serial.println("   sent IMHERE package");

          //If still in range, the egg/spike should start transmitting data shortly.
        }
      }
      break;
  } // end switch radio.available()

  //Perform any data processing/analysis/compression


#if SATCOMMS
  //TODO: Package data into a buffer. This should be kept updated with the latest data
  //to be sent so when the sat comms are activated, the latest data is sent.
  sprintf((char *)satTxBuffer, "Hello world!");
#endif

  return true;    //TODO: this is for the IridiumSBD call back. Set false to cancel an Iridium Tx mid process.
} // end loop2()


//==================================================================================================
// FUNCTIONS
//==================================================================================================
void checkErrorState(void) {
  if (setError) {
    if (errorLedChrono.hasPassed(errorLedError, true)) {
      errorLedState = !errorLedState;
      digitalWrite(LED_PIN, errorLedState);
    }
  }
  else {
    //Short blink at set intevals
    if (errorLedChrono.hasPassed(errorLedNormal, true)) {
      if (!errorLedState) {
        errorLedState = HIGH;
        digitalWrite(LED_PIN, errorLedState);
        blinkChrono.restart();
      }
    }
    if (blinkChrono.hasPassed(100)) {
      errorLedState = LOW;
      digitalWrite(LED_PIN, errorLedState);
      blinkChrono.stop();
    }
  }
}

//Display the minutes elapsed since start. Just to help keep track of things.
//The 'totalMins' is a uint32, which will not overflow until 8000 years.
void minsElapsed(void) {

  if (minuteCounter.hasPassed(minuteCountTime, true)) {
    totalMins++;
    Serial.print("Mins elapsed: "); Serial.println(totalMins);
  }
}

#if SATCOMMS
//Functions for the RockBlock satellite modem. For some buggy reason
//they are required to make the code work.
#if IRIDIUM_DIAGNOSTICS
void ISBDConsoleCallback(IridiumSBD *device, char c)
{
  Serial.write(c);
}

void ISBDDiagsCallback(IridiumSBD *device, char c)
{
  Serial.write(c);
}
#endif
#endif


//eof
