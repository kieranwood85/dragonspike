// REMEMBER TO CHECK FOR CORRECT PIN DEFINITIONS

//==========================================================
// GLOBALS (GENERIC)
//==========================================================
//Which hardware is installed?
#define RADEGG 1                // If the radiation sensor is connected
#define ACCELEGG 0              // If the accelerometer chip is present
#define GASEGG 0                // If the gas sensors are connected
#define GPSEGG 1                // If the GPS sensor is connected
#define RTC_ENABLED 1           // If the RTC chip is present or not

#define RECEIVER 0              // Is this deployed or a receiver?
#define DEBUG 1                 // USB serial debug messages on/off (i.e verbose mode)

// Annoying workaround required because it conflicts with ArduinoSTL (hopefully doesn't cause serious issues!)
#undef min
#undef max

// Hardware libraries for DragonSpikeV2 boards
#include <RHDatagram.h>         // For for packing/handling LoRa transmissions
#include <RH_RF95.h>            // The FeatherM0 LoRa module
#include <Adafruit_GPS.h>       // The GPS module (if installed)
#include <string.h>             //For feather I/O
#include "wiring_private.h"     //For Feather I/O

// DragonOS and settings
#include <DragonOS.h>
#define DRAGON_SYSTEMID 2       // Dragon system ID (Greater than zero!)
#define DRAGON_RADIO_CACHE 5    // Received object cache size TODO: what is this?
#define DRAGON_TARGET_ID 1      // The target ID to send the data to over LoRa

// Other libs
#include <ArduinoSTL.h>         // ArduinoSTL provides C++ standard library features such as vectors
#include <Chrono.h>             // For scheduled timing control

// Radio settings
#define RFM_CS 8                // Chip select pin
#define RFM_IRQ 3               // Interrupt pin
#define RFM_FREQ 868            // Radio Frequency (MHz)
#define RFM_PWR 23              // Power setting
#define RFM_LBT_TOUT 3000       // LBT timeout in ms

// SD settings
#define SD_CS 19                // Chip select pin (this is for the DragonSpike V2)
#define SD_FREQ 24              // SPI Frequency (MHz)

// LoRa radio setup
RH_RF95 radioDriver(RFM_CS,RFM_IRQ);
RHDatagram datagram(radioDriver,0);

// Dragon OS
DragonRadio radio(&datagram,DRAGON_RADIO_CACHE);
DragonStore store(SD_CS,SD_FREQ);

// LED error flash messages
#define LED_PIN 13
int errorLedState = HIGH;
Chrono errorLedChrono;
Chrono blinkChrono;
uint32_t errorLedNormal = 5000;   //time in ms between blink if normal
uint32_t errorLedError = 1000;    //time in ms between blink if error
bool setError = false;

// Receiver detection
Chrono sendHello;
uint32_t helloTimeout = 120000;    //time in ms to check if a receriver is near
bool checkImhereOn = false;
Chrono checkImhere;
uint32_t imhereTimeout = 5000;
bool transmitDataOn = false;

// Minute counter
Chrono minuteCounter;
uint32_t minuteCountTime = 60000;
uint32_t totalMins = 0;

//Send timeout
Chrono sendTimeout;
uint32_t sendTimeoutDur = 5000;  //time in ms to wait for data confirmation
bool packetSentOn = false;


//==========================================================
// GLOBALS (SENSOR SPECIFIC)
//==========================================================
#if RADEGG
  //No need to config. The UART for Serial 1 since it is hardware enabled by default.
  //UART A on the dragon spike board
  #define RadSerial Serial2
  #define RAD_MSG_LEN 300
  char rad_message[RAD_MSG_LEN + 1];
  uint32_t totalRadCounts = 0;
  #define RAD_MSG_STORE_MAX 600
  uint32_t radMsgStoreIdxs[RAD_MSG_STORE_MAX];
  uint32_t radMsgStored = 0;
  uint32_t radMsgSent = 0;
#endif

#if ACCELEGG
// Accelerometer settings

#endif

#if GASEGG
//Gas sensor settings

#endif

#if GPSEGG
//If the GPS sensor is present
//Connect to UART B
//Enable/Disable: GPIO 1
#define GPSSerial Serial1
#define GPS_EN_PIN 14
Uart Serial2 (&sercom1, 11, 10, SERCOM_RX_PAD_0, UART_TX_PAD_2);
void SERCOM1_Handler()
{
  Serial2.IrqHandler();
}
Adafruit_GPS GPS(&GPSSerial);
float latestLat = 0.1;
float latestLon = 0.2;
float latestAlt = 0.3;
Chrono gpsOffChrono;
Chrono gpsOnChrono;
int gpsState = HIGH;
int gpsLockFound = LOW;
uint32_t gpsOffT = 600000;
uint32_t gpsOnT = 180000;  
float countGPS = 0.0;

//DragonOS storage
float field_Lat = 0.0;
float field_Lon = 0.0;
float field_Alt = 0.0;
uint32_t field_time = 0;
uint32_t old_millis = 0.0;
uint32_t field_counts = 0;

#endif




//==========================================================
// SETUP
//==========================================================
void setup() {
  // Start the USB debug port.
  // Pause to allow connection to be established before first messages
  Serial.begin(9600);
  delay(1000);delay(1000);delay(1000);delay(1000);
  Serial.println("Dragon Arrow V2 startup...");

  //Prevent the receiver detection from running immidiately
  checkImhere.stop();
  sendTimeout.stop();

  // LED is on during setup. Then slow blink for normal operation, fast blink for an error
  Serial.println("   solid LED    = startup");
  Serial.println("   5s LED blink = normal operation");
  Serial.println("   1s LED blink = error");
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);

  // Init. the system ID (choose a unique ID per sensor pod)
  Serial.print("Unique Egg ID = "); Serial.println(DRAGON_SYSTEMID);
  DragonOS::setSystemID(DRAGON_SYSTEMID);

  // Initialise radio driver and datastore (note that radio driver is initiliased directly with RadioHead here)
  // Set radio power, frequency, and LBT timeout
  #if DEBUG
    Serial.println("Starting radio...");
    Serial.print("   TxPower     = "); Serial.println(RFM_PWR);
    Serial.print("   Frequency   = "); Serial.println(RFM_FREQ);
    Serial.print("   LBT timeout = "); Serial.println(RFM_LBT_TOUT);
  #endif
  datagram.init();
  store.begin();
  radioDriver.setTxPower(RFM_PWR, false);
  radioDriver.setFrequency(RFM_FREQ);
  radioDriver.setCADTimeout(RFM_LBT_TOUT);

  #if RADEGG
    Serial.println("RADEGG enabled");
    //Open a connection to the gamma sensor
    RadSerial.begin(9600);
  #endif

  #if ACCELEGG
    Serial.println("ACCELEGG enabled");
  
  #endif

  #if GASEGG
    Serial.println("GASEGG enabled");
  
  #endif

  #if GPSEGG
    //Enable the GPS
    Serial.println("GPSEGG enabled");
    // THIS SECTION DEALS WITH PIN DECLARATION. THIS IS A BIT HACKISH SINCE THE CURRENT IDE VESRION DOES NOT FULLY SUPPORT M0's FUNCTIONALITY YET.
    // Assign pins 10 & 11 SERCOM1 functionality (Seria2)
    pinPeripheral(10, PIO_SERCOM);  //TX (10)
    pinPeripheral(11, PIO_SERCOM);  //RX (11)

    pinMode(GPS_EN_PIN, OUTPUT);
    digitalWrite(GPS_EN_PIN, gpsState);
    GPS.begin(9600);
    gpsOffChrono.restart(gpsOffT-2000);             //Start first GPS 2 seconds after now
    GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);   //Enable minimum outputs
    GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);      //Set updates to 1Hz
    gpsState = LOW;
    digitalWrite(GPS_EN_PIN, gpsState);
    
  #endif

  delay(1000);
  Serial.println("Startup complete!");
  digitalWrite(LED_PIN, LOW);
  Serial.println("");
}


//==========================================================
// MAIN LOOP
//==========================================================
void loop() {
  //See if an error has been set (setError = true) and blink LED appropriately
  checkErrorState();
  minsElapsed();

  // Check if there is a data receiver in range
  checkReceiverInRange();
  
  // If a receiver is found, this condition passes and transmissions begin...
  if (transmitDataOn) {

    //If all data packets have been sent, then stop transmitting
    if (radMsgStored == radMsgSent) {
      transmitDataOn = false;
      Serial.println("All packets sent!");
    }
    else {
  
      //If a packet is not currently being sent...
      if (!packetSentOn) {
        Serial.print("   Number of waiting data packets = ");Serial.println(radMsgStored - radMsgSent);
    
        //Load a data packet from storage and send it.
        DragonObject_GPSllaT loadedObject;
        store.load(loadedObject,radMsgStoreIdxs[radMsgSent]);
        radio.send(loadedObject,DRAGON_TARGET_ID);
        datagram.waitPacketSent();
    
        //Start the timeout timer
        sendTimeout.restart();
        packetSentOn = true;
        Serial.print("   Sent data packet #");Serial.println(radMsgSent+1);
      }
  
      //If a packet has been sent, then await a confirmation
      if (packetSentOn) {
        //Only try and receive a 'RECEIVEDOK' package if a data packet was sent
        radio.update();
      
        switch (radio.available()) {
          case 0:
            break;
  
          case DragonObject_RECEIVEDOK::getObjectID():
          {
            DragonObject_RECEIVEDOK object;
            if (radio.recv(object)) {
              sendTimeout.stop();
              packetSentOn = false;
              radMsgSent++;
            }
          }
          break;
        
        } //end switch radio
      }
  
      //If a data packet has been sent, and the timeout is over, then assume the receiver is gone and stop trying to transmit
      if (sendTimeout.hasPassed(sendTimeoutDur,true)) {
        sendTimeout.stop();
        transmitDataOn = false;
        Serial.println("Timout - stopping transmissions");
      }
    }
  }

  #if RADEGG
    //No timing control on RAD sensor. Just wait-for, and read messages as fast as they arrive on the serial buffer.
    //Check for new rad packet
    if (RadSerial.available() > 0 ) {
      byte size = RadSerial.readBytes(rad_message, RAD_MSG_LEN);
      rad_message[size] = 0;
      totalRadCounts++;
      field_counts++;
      Serial.println("New rad message...");
      #if DEBUG
        Serial.print("   Message: ");      Serial.print(rad_message);
        Serial.print("   Total counts: "); Serial.println(totalRadCounts);
      #endif
    }

    //TOFO: Add code to decode and store the radiation value
    //

  #endif

  #if ACCELEGG

  #endif

  #if GASEGG

  #endif

  #if GPSEGG
    //If the off period has elapsed, enable the GPS
    if (gpsOffChrono.hasPassed(gpsOffT,true)) {
      Serial.println("Enable GPS ...");
      gpsState = HIGH;
      digitalWrite(GPS_EN_PIN, gpsState);
      gpsOnChrono.restart();
      countGPS = 0.0;
      //Clear out the input buffer
      while (GPSSerial.available() > 0) {
        char temp = GPSSerial.read();
      }
      GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);   //Enable minimum outputs
      GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);      //Set updates to 1Hz
    }
    //If the GPS is on, and the on time has elapsed, disable the GPS
    if (gpsState && (gpsOnChrono.hasPassed(gpsOnT,true))) {
      gpsState = LOW;
      digitalWrite(GPS_EN_PIN, gpsState);
      //TODO: set some sort of flag to indicate the GPS averaging has finished?
      gpsOnChrono.stop();

      //Save the time-averaged GPS into the DragonOS
      field_Lat = latestLat;
      field_Lon = latestLon;
      field_Alt = latestAlt;
      field_time = millis() - old_millis;
      old_millis = millis();
      gpsLockFound = LOW;

      Serial.println("Disable GPS ...");
      #if DEBUG
        Serial.print("   Averaged Lat:      ");Serial.println(field_Lat);
        Serial.print("   Averaged Lon:      ");Serial.println(field_Lon);
        Serial.print("   Averaged Alt:      ");Serial.println(field_Alt);
        Serial.print("   Counts in period:  ");Serial.println(field_counts);
        Serial.print("   Period in seconds: ");Serial.println(field_time/1000);
      #endif

      //Save the data into the store
      DragonObject_GPSllaT createdObject(DRAGON_SYSTEMID,DRAGON_TARGET_ID,radMsgStored,field_Lat,field_Lon,field_Alt,field_counts,field_time);
      uint32_t storeID = store.save(createdObject);

      //Save the index into an array to keep track of what is stored and the indexes.
      radMsgStoreIdxs[radMsgStored] = storeID;
      radMsgStored++;
      Serial.print("Stored ");Serial.print(radMsgStored);Serial.print(" data packets. ");Serial.print(radMsgStored - radMsgSent);Serial.println(" unsent.");

      //Reset the rad count
      field_counts = 0;
      
    }
    //Always keep reading from the buffer to prevent overflows etc. even when GPS off.
    char c = GPS.read();
    #if GPS_NMEA_ECHO
      if (c) {
        Serial.print(c);
      }
    #endif
    if (GPS.newNMEAreceived()) {
      
      //If first GPS position decoded...
      if (GPS.fix && !gpsLockFound) {
        Serial.println("GPS lock successful ...");
        gpsLockFound = HIGH;

        //Reset the averages
        latestLat = GPS.latitude;
        latestLon = GPS.longitude;
        latestAlt = GPS.altitude;
        
        #if DEBUG
          Serial.print("   Lat: ");Serial.println(latestLat);
          Serial.print("   Lon: ");Serial.println(latestLon);
          Serial.print("   Alt: ");Serial.println(latestAlt);
        #endif
      }
      
      GPS.parse(GPS.lastNMEA());
      //Only average when the GPS is enabled.
      if (gpsState) {
        countGPS = countGPS + 1.0;
        latestLat = ((countGPS-1.0)/countGPS)*latestLat + (GPS.latitude/countGPS);
        latestLon = ((countGPS-1.0)/countGPS)*latestLon + (GPS.longitude/countGPS);
        latestAlt = ((countGPS-1.0)/countGPS)*latestAlt + (GPS.altitude/countGPS);

        //TODO: get the the time from GPS. Store with data and also use to sync RTC clock.
      }
    }
  #endif

  
//TODO delete example code below
/*
  if (sendTestChrono.hasPassed(10000,true)) {    
    // Create some random example data
    // TODO: Replace with measurements from rad/gas sensors
    fieldA = (uint8_t)random(255);
    fieldB = (uint16_t)random(65535);
    fieldC = (uint32_t)random(2147483647);
    fieldD = (float)random(100000)/100;
  
    // Create the object from its fields
    DragonObject_SimpleExample createdObject(fieldA,fieldB,fieldC,fieldD);
  
    // Save the object to the local store
    uint32_t storeID = store.save(createdObject);
  
    // Reload the object from store (proof the system works)
    DragonObject_SimpleExample loadedObject;
    store.load(loadedObject,storeID);
  
    // Make sure that the object was successfully defined (also returned from the store.load() line as bool)
    if (loadedObject.isDefined()) {
  
      // Print the value of each field through object access
      Serial.print("Field A = "); Serial.println(loadedObject.getFields()->fieldA);
      Serial.print("Field B = "); Serial.println(loadedObject.getFields()->fieldB);
      Serial.print("Field C = "); Serial.println(loadedObject.getFields()->fieldC);
      Serial.print("Field D = "); Serial.println(loadedObject.getFields()->fieldD,3);
      Serial.println();
      Serial.println("---------------------------------------------------------------------------------");
      Serial.println();
  
      // Send the object over radio to device with address 2
      //TODO: Use CAD/LBT to deconflict - this should be built into the dragon radio function
      radioDriver.waitCAD();
      radio.send(loadedObject,2);
      
      // Wait until the object is sent (this should become a function of the DragonRadio class in future)
      datagram.waitPacketSent();
    }
  
    // Create some more random example data but for arrays
    sizeA = (uint8_t)random(1,10);
    sizeB = (uint8_t)random(1,10);
    uint16_t arrayA[sizeA];
    uint32_t arrayB[sizeB];
    for (int i=0; i<sizeA; i++) arrayA[i] = (uint16_t)random(65535);
    for (int i=0; i<sizeB; i++) arrayB[i] = (uint32_t)random(2147483647);
  
    // Create the object from its fields
    DragonObject_ArrayExample anotherObject(sizeA,sizeB,arrayA,arrayB);
  
    // Print the value of each field through object access
    Serial.print("Size A = "); Serial.println(anotherObject.getFields()->sizeA);
    Serial.print("Size B = "); Serial.println(anotherObject.getFields()->sizeB);
    
    Serial.print("Array A ="); 
    for (int i = 0; i < anotherObject.getFields()->sizeA; i++) { Serial.print(" | "); Serial.print(anotherObject.getFields()->arrayA[i]); } 
    Serial.println();
    
    Serial.print("Array B ="); 
    for (int i = 0; i < anotherObject.getFields()->sizeB; i++) { Serial.print(" | "); Serial.print(anotherObject.getFields()->arrayB[i]); } 
    Serial.println();
    
    Serial.println();
    Serial.println("---------------------------------------------------------------------------------");
    Serial.println();
  
    // Send the new object over radio to device with address 33 (this is to demonstrate the address group feature on the receiving end)
    //TODO: Use CAD/LBT to deconflict - this should be built into the dragon radio function
    radioDriver.waitCAD();
    radio.send(anotherObject,33);
  }
*/
  
}

//==========================================================
// FUNCTIONS
//==========================================================
void checkErrorState(void) {
  if (setError) {
    if (errorLedChrono.hasPassed(errorLedError,true)) {
      errorLedState = !errorLedState;
      digitalWrite(LED_PIN, errorLedState);
    }
  }
  else {
    //Short blink at set intevals
    if (errorLedChrono.hasPassed(errorLedNormal,true)) {
      if (!errorLedState) {
        errorLedState = HIGH;
        digitalWrite(LED_PIN, errorLedState);
        blinkChrono.restart();
      }
    }
    if (blinkChrono.hasPassed(100)) {
      errorLedState = LOW;
      digitalWrite(LED_PIN, errorLedState);
      blinkChrono.stop();
    }
  }
}

void minsElapsed(void) {
  //Display the minutes elapsed since start. Just to help keep track of things.
  if (minuteCounter.hasPassed(minuteCountTime,true)) {
    totalMins++;
    Serial.print("Mins elapsed: "); Serial.println(totalMins);
  }
}

void checkReceiverInRange(void) {
  //Only send a 'HELLO' package intermittently
  if (sendHello.hasPassed(helloTimeout,true)) {
    Serial.println("Looking for receiver (sent HELLO) ...");
    //Make a 'HELLO' package
    DragonObject_HELLO helloObj((uint32_t)DRAGON_SYSTEMID,(uint32_t)DRAGON_TARGET_ID);
    #if DEBUG
      Serial.print("   Sending ID : "); Serial.println(DRAGON_SYSTEMID);
      Serial.print("   Target ID  : "); Serial.println(DRAGON_TARGET_ID);
    #endif
    //Send it out and await a response
        //radioDriver.waitCAD();
    radio.send(helloObj,DRAGON_TARGET_ID);
    datagram.waitPacketSent();
    
    //Start listening
    checkImhere.restart();
    checkImhereOn = true;
  }

  //If the check timeout has elapsed then stop listening
  if (checkImhere.hasPassed(imhereTimeout,true)) {
    checkImhere.stop();
    checkImhereOn = false;
    Serial.println("No receiver found");
  }
  
  //
  if (checkImhereOn) {
    //Only try and receive a 'IMHERE' package for a short time to conserve power
    radio.update();
    
    switch (radio.available()) {
      case 0:
        break;

      case DragonObject_IMHERE::getObjectID():
      {
        DragonObject_IMHERE object;
        if (radio.recv(object)) {
          // Print the value of each field through object access
          #if DEBUG
            Serial.println("Found receiver (received IMHERE) ...");
            Serial.print("Confirmation sending ID   : "); Serial.println(object.getFields()->DEVID);
            Serial.print("Confirmation receiving ID : "); Serial.println(object.getFields()->TGTID);
          #endif
          
          //Check if the returned IDs match
          if ((object.getFields()->DEVID == DRAGON_SYSTEMID) && (object.getFields()->TGTID == DRAGON_TARGET_ID)) {
            //If a match then data transmissions can start
            checkImhere.stop();
            checkImhereOn = false;
            Serial.println("Receiver found!");
            transmitDataOn = true;
            Serial.println("Starting transmissions ...");
          }
        }
      }
      break;
      
    } //end switch radio
  }
  
}



//eof
