//start of file
//Basic code example for DragonSpike V2 boards
//Read accelerometer and RTC data and write to SD card
//K Wood
//27/10/20
//
//==========================================================
// GLOBALS (GENERIC)
//==========================================================

// Annoying workaround required because it conflicts with ArduinoSTL (hopefully doesn't cause serious issues!)
#undef min
#undef max

#define LED_ON 1
#define SD_ON 1
#define ACCEL_ON 1
#define RTC_ON 1
#define LORA_ON 1


// Hardware libraries
#include <Chrono.h>             // For scheduled timing control
#include <RHDatagram.h>
#include <RH_RF95.h>
#include "Arduino.h"
#include "math.h"
#include <ArduinoSTL.h>
#include <SdFat.h>
//#include <list>
//#include <iterator>
#include <Adafruit_MMA8451.h>   // For accelerometer
#include <RTClib.h>             // For real time clock


// LED error flash messages
#define LED_PIN 13
int errorLedState = HIGH;
Chrono errorLedChrono;
Chrono blinkChrono;
uint32_t errorLedNormal = 5000;   //time in ms between blink if normal
uint32_t errorLedError = 1000;    //time in ms between blink if error
bool setError = false;

// Minute counter
Chrono minuteCounter;
uint32_t minuteCountTime = 60000;
uint32_t totalMins = 0;

// Accelerometer settings
Chrono readAccelChrono;
uint32_t accelReadInteval = 1000;  //time between accelerometer reads in ms
Adafruit_MMA8451 mma = Adafruit_MMA8451();
uint16_t x = 0;
uint16_t y = 0;
uint16_t z = 0;

// RTC Chip settings
Chrono readTimeChrono;
uint32_t timeReadInteval = 1000;  //time between RTC reads in ms
RTC_DS3231 rtc;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
uint16_t dd = 0;
uint16_t mo = 0;
uint16_t yy = 0;
uint16_t hh = 0;
uint16_t mm = 0;
uint16_t ss = 0;

// SD card settings
Chrono writeSDChrono;
uint32_t sdWriteInteval = 1000;   // time between SD writes in ms
#define SD_CS 19                  // Chip select pin (this is for the DragonSpike V2)
#define SD_FREQ 24                // SPI Frequency (MHz)
SdFat sd;
SPISettings settings;

// Radio settings
#define RFM_CS 8                // Chip select pin
#define RFM_IRQ 3               // Interrupt pin
#define RFM_FREQ 868            // Radio Frequency (MHz)
#define RFM_PWR 23              // Power setting
#define RFM_LBT_TOUT 3000       // LBT timeout in ms
RH_RF95 radioDriver(RFM_CS,RFM_IRQ);
RHDatagram datagram(radioDriver,0);


void setup() {
  // Start the USB debug port.
  // Pause to allow connection to be established before first messages
  Serial.begin(9600);
  delay(1000);delay(1000);delay(1000);delay(1000);
  Serial.println("Dragon Arrow V2 startup...");

  //Required for interacting with the LoRa and SD card.
  datagram.init();

#if LED_ON
  // LED is on during setup. Then slow blink for normal operation, fast blink for an error
  Serial.println("   solid LED    = startup");
  Serial.println("   5s LED blink = normal operation");
  Serial.println("   1s LED blink = error");
#endif

#if SD_ON
  delay(500);
  Serial.print("Initialise SD...");
  settings = SPISettings(1000000*SD_FREQ,MSBFIRST,SPI_MODE0);
  bool SDTest = sd.begin(SD_CS,settings);
  if (!SDTest) {
    Serial.println("\n!!! Couldn't start SD !!!\n");
    setError = true;
  }
  Serial.println("done");
#endif

#if ACCEL_ON
  delay(500);
  Serial.print("Initialise accelerometer...");
  //Check if an accelerometer is connected
  if (! mma.begin()) {
    Serial.println("\n!!! Couldn't start accelerometer !!!\n");
    setError = true;
  }
  //Set the expected measurement range
  mma.setRange(MMA8451_RANGE_2_G);
  Serial.println("done");
#endif

#if RTC_ON
  delay(500);
  Serial.print("Initialise RTC...");
  //Check if an RTC is connected
  if (! rtc.begin()) {
    Serial.println("\n!!! Couldn't start RTC !!!\n");
    setError = true;
  }
  if (rtc.lostPower()) {
  Serial.print("RTC lost power, lets set the time!...");
  // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  Serial.println("done");
#endif

#if LORA_ON
  delay(500);
  Serial.print("Initialise LoRa...");
  radioDriver.setTxPower(RFM_PWR, false);
  radioDriver.setFrequency(RFM_FREQ);
  radioDriver.setCADTimeout(RFM_LBT_TOUT);
  Serial.println("done");
#endif

  delay(1000);
  Serial.println("Startup complete!");
#if LED_ON
  digitalWrite(LED_PIN, LOW);
#endif
  Serial.println("");
}

//==========================================================
// MAIN LOOP
//==========================================================
void loop() {
#if LED_ON
  //See if an error has been set (setError = true) and blink LED appropriately
  checkErrorState();
#endif

  //Display the number of minutes elapsed since power up
  minsElapsed();

#if ACCEL_ON
  if (readAccelChrono.hasPassed(accelReadInteval,true)) {
    mma.read();
    Serial.print("X:\t"); Serial.print(mma.x); 
    Serial.print("\tY:\t"); Serial.print(mma.y); 
    Serial.print("\tZ:\t"); Serial.print(mma.z);
    Serial.println();

    //Store the data into variables
    x = mma.x;
    y = mma.y;
    z = mma.z;
  }
#endif

#if RTC_ON
  if (readTimeChrono.hasPassed(timeReadInteval,true)) {
    DateTime now = rtc.now();
    Serial.print(now.year(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.day(), DEC);
    Serial.print(" (");
    Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);
    Serial.print(") ");
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println();

    //Store the data into variables
    yy = now.year();
    mo = now.month();
    dd = now.day();
    hh = now.hour();
    mm = now.minute();
    ss = now.second();
  }
#endif

#if SD_ON
  if (writeSDChrono.hasPassed(sdWriteInteval,true)) {
    File myFile = sd.open("test.txt", FILE_WRITE);
    char writeBuf[40];
    int sz = sprintf(writeBuf,"AccelXYZ,%d,%d,%d\n",x,y,z);
    myFile.write(writeBuf,sz);
    sz = sprintf(writeBuf,"TimeYMD:HMS,%d,%d,%d:%d,%d,%d\n",yy,mo,dd,hh,mm,ss);
    myFile.write(writeBuf,sz);
    myFile.close();
    Serial.println("SD Write complete");
  }
#endif


#if LORA_ON
// TODO
#endif

}

//==========================================================
// FUNCTIONS
//==========================================================
void checkErrorState(void) {
  if (setError) {
    if (errorLedChrono.hasPassed(errorLedError,true)) {
      errorLedState = !errorLedState;
      digitalWrite(LED_PIN, errorLedState);
    }
  }
  else {
    //Short blink at set intevals
    if (errorLedChrono.hasPassed(errorLedNormal,true)) {
      if (!errorLedState) {
        errorLedState = HIGH;
        digitalWrite(LED_PIN, errorLedState);
        blinkChrono.restart();
      }
    }
    if (blinkChrono.hasPassed(100)) {
      errorLedState = LOW;
      digitalWrite(LED_PIN, errorLedState);
      blinkChrono.stop();
    }
  }
}

void minsElapsed(void) {
  //Display the minutes elapsed since start. Just to help keep track of activity.
  if (minuteCounter.hasPassed(minuteCountTime,true)) {
    totalMins++;
    Serial.print("Mins elapsed: "); Serial.println(totalMins);
  }
}

//end of file
