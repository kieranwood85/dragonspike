//start of file
//GasWeb Node code for Gas Sensing - (based on DragonSpike V2 boards)
//Kieran Wood
//kieran.wood@manchester.ac.uk
//27/10/21
//
//Known issues
//-----------------
//Drift on Chrono timers. With so many functions in the main loop
//small blocks eventually can add up to cause the Chrono resets to
//gradually become behind.
//
//==========================================================
// GLOBALS (GENERIC)
//==========================================================

//Turn on/off various features of the hardware
#define WAIT_FOR_USB_TERMINAL 0
#define DEVEL_ON 0
#define LED_ON 1
#define SD_ON 1
#define ACCEL_ON 1
#define RTC_ON 1
#define LORA_ON 1
#define CO2_ON 0
#define SAT_ON 0
#define SERIAL_IN_LOOP 1

//Comms settings
#define IS_SERVER 1
#if IS_SERVER
  #define CLIENT_ADDRESS 1
  #define SERVER_ADDRESS 0
#else
  #define CLIENT_ADDRESS 16    //pick unique ID per node
  #define SERVER_ADDRESS 1    //always send to server at address 1
  #define SD_ON 0             //No SD for Accel only nodes.
#endif


// Libraries
#include <Arduino.h>            // Required before wiring_private.h
#include "wiring_private.h"     // for pinPeripheral() function (part of UART setup)
#include "SafeWrite.h"          // SD card writing
#include "BlinkPlus.h"          // LED blink status
#include <Chrono.h>             // Timing control
#include <Wire.h>               // I2C communications
#include <Adafruit_MMA8451.h>   // For accelerometer
#include <RTClib.h>             // For real time clock
#include "IridiumSBD.h"         // For satellite modem
#include <SPI.h>                // For Lora (on Feather M0)
#include <RH_RF95.h>            // For Lora (on Feather M0)
#include <RHReliableDatagram.h> // For message packing
#include <Adafruit_SleepyDog.h> // For watchdog timer reset
#include "headers\dragon\mavlink.h"   //For custom messages protocol
#include <SerialCommands.h>     // For command line options



//==========================================================
// HARDWARE SETUP
//==========================================================
#if LED_ON
  #define LED_PIN 13
  BlinkPlus blink(LED_PIN);
#endif

// Battery monitor. Define the pin (A7 is the Feather M0 onboard), and potential divider value (2.0 is the Feather M0 onboard)
#define VBATPIN A7
#define VBAT_PD 2.0
Chrono batteryCheckChrono;
float latestVoltage = 0.0;
uint32_t batteryCheckInteval = 1*60*1000;   //time between battery updates (ms)
float batFullV = 4.15;  //above this value the battery is considered full
float batNormV = 4.0;
float batLowV  = 3.8;
float batCritV = 3.5;    //below this is critical. High power sensors/systems should be turned off/very very infrequent.
DateTime voltageValidTime = DateTime(2000, 1, 1, 0, 0);

#if SD_ON
  Chrono writeSDChrono;
  uint32_t sdWriteInteval = 1*60*1000;
  SafeWrite SW(SafeWrite::DRAGON,&Serial);
#endif

#if ACCEL_ON
  // Accelerometer settings
  Chrono readAccelChrono;
  //Time between accelerometer reads (ms). 
  //Minimum is ~10ms and at that rate this will interfere with other slower I2C devices
  uint32_t accelReadInteval = 10;  
  Adafruit_MMA8451 mma = Adafruit_MMA8451();
#endif
//Always define these variables to not break the main loop when hardware is absent
float accel_x = 0;
float accel_y = 0;
float accel_z = 0;
DateTime accelValidTime = DateTime(2000, 1, 1, 0, 0);

#if RTC_ON
  // RTC Chip settings
  Chrono readTimeChrono;
  uint32_t timeReadInteval = 1000;  //time between RTC reads (ms)
  RTC_DS3231 rtc;
  float latestIntenalTemp = 0.0;
  //Just an int to force a recompile when trying to set the RTC chip.
  uint8_t forceRecompile = 1;
#endif
//Always define these variables to not break the main loop when hardware is absent
float RTCTemp = 0.0;
//char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
uint16_t time_dd = 0;
uint16_t time_mo = 0;
uint16_t time_yy = 0;
uint16_t time_hh = 0;
uint16_t time_mm = 0;
uint16_t time_ss = 0;
DateTime tempValidTime = DateTime(2000, 1, 1, 0, 0);

#if SAT_ON
  #define IRIDIUM_DIAGNOSTICS false           // Change this to see iridium sat modem diagnostics
  
  //SATCOM configuration
  #define PACKET_SIZE 48  //Try to keep message short (48 byte=1block, so sent all in one go)
  #define STX_BITE 0x02
  #define ETX_BITE 0x03

  //Set up an additional serial port (UART B on dragon boards) 
  Uart Serial2 (&sercom1, 11, 10, SERCOM_RX_PAD_0, UART_TX_PAD_2);
  void SERCOM1_Handler() {
    Serial2.IrqHandler();
  }

  //Decleare the satcomms object linked to the new serial port
  IridiumSBD satmodem(Serial2);

  Chrono satTxChrono;
  uint32_t satTxInteval = 2*60*1000;  //time beween sat sends (ms)
  bool satTxActive = false;           //flag to indicate a satellite Tx attempt is in progress
  int signalQuality = -1;

  //Sent messages
  #define SAT_TX_BUF_LGTH 340
  uint8_t satTxBuffer[SAT_TX_BUF_LGTH];
#endif

#if CO2_ON
  //CO2 Settings
  Chrono readCO2Chrono;
  uint32_t readCO2Inteval = 1000;   // Time between CO2 reads in ms
  //int co2Addr = 0x7F;               // DS3231 RTC chips also use 0x68
  int co2Addr = 0x70;                 // If K30 has been reconfigured to use address 0x70
  int latestCO2 = 0;
  int uniqueCO2 = 0;
  DateTime co2ValidTime = DateTime(2000, 1, 1, 0, 0);
  #define CO2_PUMP_PIN A1
  #define CO2_LAMP_PIN A2
  #define CO2_BATT_PIN A0

  byte CheckSum(byte * buf, byte count) {
    byte sum=0;
    while (count>0) {
    sum += *buf;
    buf++;
    count--;
    }
    return sum;
  }
#endif

#if LORA_ON
  #define RFM95_CS 8
  #define RFM95_RST 4
  #define RFM95_INT 3
  #define RFM_PWR 23                      // Power setting (23 is max)
  #define RFM_LBT_TOUT 1000               // LBT timeout in ms(used for waitCAD() function)
  #define RF95_FREQ 915.0
  RH_RF95 rf95(RFM95_CS, RFM95_INT);
  uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];   //251 bytes using radiohead libraries
#endif


//==========================================================
// SOFTWARE SETUP
//==========================================================
/// Minute counter
Chrono minuteCountChrono;
uint32_t minuteCountInteval = 60*1000;
uint32_t totalMins = 0;   //uint32 is enough for 4000 years

// Startup flags
bool setupComplete = false;

//Watchdog timer
uint32_t WATCH_TOUT = 4000;

//To detect max accel. even within a time period
float maxAccel_xRunning = 0.0;
float maxAccel_yRunning = 0.0;
float maxAccel_zRunning = 0.0;
//Storage for the max accel. 
float maxAccel = 0.0;
float maxAccel_x = 0.0;
float maxAccel_y = 0.0;
float maxAccel_z = 0.0;
Chrono maxAccelChrono;
uint32_t maxAccelInteval = 1000;    //timeout on detection of maximum accel event.


#if LORA_ON
//The data transmission packet management
RHReliableDatagram linkmanager(rf95, CLIENT_ADDRESS);
Chrono autoSendDataChrono;
uint32_t autoSendDataInteval = 1000;   // time between loraSends
DateTime latestRecvTime = DateTime(2000, 1, 1, 0, 0);
#endif


//Serial commands from terminal
char serial_command_buffer_[32];
SerialCommands serial_commands_(&Serial, serial_command_buffer_, sizeof(serial_command_buffer_), "\r\n", " ");
#include "serialCmds.h"


//Flags to done some things just once in the main loop
bool doOnce = true;


//==========================================================
// FUNCTIONS
//==========================================================

//Count and display the minutes elapsed since start. This is
//just a basic counter and might drift slightly.
uint32_t minsElapsed(void) {
  if (minuteCountChrono.hasPassed(minuteCountInteval,true)) {
    totalMins += 1;
    #if SERIAL_IN_LOOP
    Serial.print("Mins elapsed: "); Serial.println(totalMins);
    #endif
  }
  return totalMins;
}


//Check the battery voltage. The returned value is binned according to the settings.
uint8_t checkBattery(void) {
  float measuredVBat = analogRead(VBATPIN);
  measuredVBat *= VBAT_PD;   //remove effect of potential divider
  measuredVBat *= 3.3;   //multiply by reference voltage
  measuredVBat /= 1024;  //divide by bit depth
  latestVoltage = measuredVBat;
  if (batteryCheckChrono.hasPassed(batteryCheckInteval,true)) {
    #if SERIAL_IN_LOOP
    Serial.print("Battery voltage: "); Serial.print(latestVoltage);Serial.println(" V");
    #endif
  }
  //Check the health 'code'
  if      (latestVoltage > batFullV) {return 4;}
  else if (latestVoltage > batNormV) {return 3;}
  else if (latestVoltage > batLowV)  {return 2;}
  else if (latestVoltage > batCritV) {return 1;}
  else {return 0;}
  #if RTC_ON
    voltageValidTime = rtc.now();
  #endif
}


//==========================================================
// SETUP/STARTUP
//==========================================================
void setup() {
  #if LED_ON
    blink.init();
  #endif

  //Start the USB debug port.
  Serial.begin(9600);
  #if WAIT_FOR_USB_TERMINAL
    while (!Serial) {
      delay(200);
    }
    Serial.println("Start in 4s ...");
  delay(1000);delay(1000);delay(1000);delay(1000);
  #endif
  Serial.println("GasWeb node startup");


  #if RTC_ON
    Serial.print("Initialise RTC... ");
    delay(500);
    //Check if an RTC is connected
    if (!rtc.begin()) {
      Serial.println("\n!!! Couldn't start RTC !!!\n");
      blink.setError(true);
    }
    if (rtc.lostPower()) {
    Serial.print("RTC lost power. Resetting time to compile time");
      // following line sets the RTC to the date & time this sketch was compiled
      rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    }
    Serial.print("Temp. = ");Serial.print(rtc.getTemperature());Serial.print("C");
    Serial.println(" ...done");
  #endif


  #if ACCEL_ON
    Serial.print("Initialise accelerometer ... ");
    delay(500);
    //Check if an accelerometer is connected
    if (! mma.begin()) {
      Serial.println("\n!!! Couldn't start accelerometer !!!\n");
      blink.setError(true);
    }
    //Set the expected measurement range
    //mma.setRange(MMA8451_RANGE_2_G);
    //Serial.print("Range +-2g ... ");
    //mma.setRange(MMA8451_RANGE_4_G);
    //Serial.print("Range +-4g ... ");
    mma.setRange(MMA8451_RANGE_8_G);
    Serial.print("Range +-8g");
    Serial.println(" ...done");
  #endif


  #if SAT_ON
    Serial.print("Initialise sat comms. ... ");
    Serial2.begin(19200);
    pinPeripheral(10, PIO_SERCOM);  //TX (10)
    pinPeripheral(11, PIO_SERCOM);  //RX (11)
    int satErr;
    satErr = satmodem.begin();
    if (satErr != ISBD_SUCCESS) {
      Serial.println("\n!!! Couldn't start sat comms. !!!\n");
      if (satErr == ISBD_NO_MODEM_DETECTED) {
        Serial.println("No modem detected: check wiring.");
      }
      blink.setError(true);
    }

    char version[12];
    satErr = satmodem.getFirmwareVersion(version, sizeof(version));
    if (satErr != ISBD_SUCCESS) {
       Serial.print("FirmwareVersion failed: error ");
       blink.setError(true);
    }
    else {
      Serial.print("Firmware: ");
      Serial.print(version);
      Serial.print("...");
    }
    #if 0
      // Check the signal quality (optional)
      satErr = satmodem.getSignalQuality(signalQuality);
      if (satErr != 0)
      {
        Serial.print("SignalQuality failed: error ");
        blink.setError(true);
      }
      Serial.print("Signal: ");
      Serial.print(signalQuality);
      Serial.print("...");
    #endif
    
    satmodem.setPowerProfile(IridiumSBD::USB_POWER_PROFILE); //Low power, 90ma, T>60s

    Serial.println(" ...done");
  #endif


  #if SD_ON
    Serial.println("Initialise SD...");
    delay(500);
    
    //Need to set CS pin for onboard Lora high when lora not being used
    pinMode(RFM95_CS, OUTPUT); 
    digitalWrite(RFM95_CS, HIGH);
    
    if (!SW.init()) {
      Serial.println("\n!!! Couldn't start SD !!!\n");
      blink.setError(true);
    }
    else {
      Serial.println("SD initialised");
      #if RTC_ON
        //Make a folder name using the date
        DateTime now = rtc.now();
        char date_format[6] = {'Y','Y','M','M','D','D'};
        SW.createnew("GASW",now.toString(date_format),true);
        Serial.print("Using folder name: ");Serial.println(now.toString(date_format));
      #else
        SW.createnew("GASW","DEFAULT",true);
        Serial.print("Using folder name: ");Serial.println("default");
      #endif
    }
  #endif

  
  #if LORA_ON
    Serial.print("Initialise LoRa...");
    delay(500);

    //Set SD CS deliberately high during LoRa init.
    pinMode(19, OUTPUT);      //Need to set CS pin for onboard Lora high when lora not being used
    digitalWrite(19, HIGH);
    
    //Reset the Lora chip
    pinMode(RFM95_RST, OUTPUT);
    digitalWrite(RFM95_RST, LOW);
    delay(20);
    digitalWrite(RFM95_RST, HIGH);
    delay(20);
    //if (!rf95.init()) {
    if (!linkmanager.init()) {
      Serial.println("\n!!! Couldn't start Lora RF !!!\n");
      blink.setError(true);
    }
    else {
      rf95.setTxPower(RFM_PWR, false);
      rf95.setFrequency(RF95_FREQ);
      rf95.setCADTimeout(RFM_LBT_TOUT);
      Serial.print("set frequency 915MHz...");
  
      Serial.println("done");
    }
  #endif


  #if CO2_ON
    Serial.print("Initialise K30...");
    //Start I2C interface to K30 sensor
    Wire.begin();
    pinMode(CO2_PUMP_PIN, OUTPUT);
    digitalWrite(CO2_PUMP_PIN, LOW);
    pinMode(CO2_LAMP_PIN, INPUT);
    pinMode(CO2_BATT_PIN, INPUT);
    //Wait while a lamp input is detected
    while(analogRead(CO2_LAMP_PIN)<1000) {
      delay(10);
    }
    float co2_batt_v = ((float)analogRead(14)/1024.0)*3.3*7.8;
    Serial.print("CO2 Batt: ");Serial.print(co2_batt_v);Serial.print("V");
    Serial.println(" ...done");

    //To change the I2C address to be 0x69 to deconflict with the RTC on 0x68
    //https://electronics.stackexchange.com/questions/581004/how-do-i-edit-the-eeprom-of-a-k30-co2-sensor
    if (false) {
      delay(500); 
      byte changebuf[5] = {0x31, 0x00, 0x00, 0x70, 0x00};
      changebuf[4] = CheckSum(changebuf, 4);
      Wire.beginTransmission(co2Addr);
      for (size_t i = 0; i<sizeof(changebuf); i++) {
        Wire.write(changebuf[i]);
      }
      Wire.endTransmission();
      delay(500);
      Serial.println("Changed K30 I2C address to 0x70");
    }
    
  #endif


  //Setup serial terminal commands (not needed for eggs)
  regAllCmds();

  Serial.print("Initialise Watchdog...");
  int countdownMS = Watchdog.enable(WATCH_TOUT);
  Serial.print(countdownMS);Serial.println("ms timeout...done");

  #if RTC_ON
    DateTime now = rtc.now();
    char date_format[] = {'Y','Y','-','M','M','-','D','D',' ','h','h',':','m','m',':','s','s','\0'};
    Serial.print("DateTime is: ");Serial.println(now.toString(date_format));
  #endif
  Serial.println("Startup complete!");
  Serial.println("");
  setupComplete = true;
}  //end setup()


//==========================================================
// MAIN LOOP
//==========================================================
void loop() {
  if (doOnce) {
    //Need to ensure nothing uses the I2C near to the K30 CO2.
    //It doesn't have a lot of compute, so retrieving the measurment take time 
    //and other I2C messages must not be requested inbetween.
    #if CO2_ON
    readCO2Chrono.restart(300);
    #endif
    
    #if RTC_ON
    readTimeChrono.restart();
    #endif
    doOnce = false;
  }

  //Due to a quirk in the setup of the IridiumISB library, whenever there is an interaction with
  //the rockblock, the send/receive functions will block the main thread, but will continue to call
  //the ISBDCallback function. Hence all code except the satcomms is wrapped up in a sub-function which
  //is called either by the main loop or by the ISBDCallback. Either way, most of the code continues to
  //run at all times.
  if (setupComplete) {
    loop2();
  }
  
  #if SAT_ON
    if (satTxChrono.hasPassed(satTxInteval,true)) {
      satTxActive = true;
    }

    if (satTxActive) {
      Serial.println("SAT: Start Iridium transmission...");
      
      int satErr;
      size_t satTxBufferSize = sizeof(satTxBuffer);
      if (int(satTxBufferSize) > SAT_TX_BUF_LGTH) {
        Serial.println("SAT:ERROR -> more than 50 byte message");
        blink.setError(true);
      }

      satErr = satmodem.sendSBDText("Hello Emma. Just testing the satellite messages.");

      //Sat lib now blocks (calling loop2()) until an error or timeout
      if (satErr != ISBD_SUCCESS) {
        Serial.print("sendSBDText failed: error ");
        Serial.println(satErr);
        if (satErr == ISBD_SENDRECEIVE_TIMEOUT) {
          Serial.println("Try again with a better view of the sky.");
        }
      }
      else {
        //else it worked!
        Serial.println("SAT: message sent!");
      }
      
      satTxActive = false;
    }
  #endif
}

bool ISBDCallback() {
  if (setupComplete) {
    return loop2();
  }
}

bool loop2() {
  #if LED_ON
    blink.update();
  #endif
  minsElapsed();
  checkBattery();
  Watchdog.reset();
  serial_commands_.ReadSerial();

  #if !IS_SERVER
  #if ACCEL_ON
    if (readAccelChrono.hasPassed(accelReadInteval,true)) {
      mma.read();
      accel_x = mma.x_g;
      accel_y = mma.y_g;
      accel_z = mma.z_g;

      //Check if the latest read is a higher...
      float newAccel = sqrt(accel_x*accel_x + accel_y*accel_y + accel_z*accel_z);
      if (newAccel > maxAccel) {
        maxAccel = newAccel;
        maxAccel_xRunning = accel_x;
        maxAccel_yRunning = accel_y;
        maxAccel_zRunning = accel_z;
      }
      if (maxAccelChrono.hasPassed(maxAccelInteval,true)) {
        maxAccel_x = maxAccel_xRunning;
        maxAccel_y = maxAccel_yRunning;
        maxAccel_z = maxAccel_zRunning;
        maxAccel = 0.0;
        maxAccel_xRunning = 0.0;
        maxAccel_yRunning = 0.0;
        maxAccel_zRunning = 0.0;
        #if SERIAL_IN_LOOP
        Serial.print("Max. accel. (g),");Serial.print(maxAccel_x); Serial.print(",");Serial.print(maxAccel_y); Serial.print(",");Serial.println(maxAccel_z);
        #endif
        #if RTC_ON
        accelValidTime = rtc.now();
        #endif
      }
    }
  #endif
  #endif


  #if RTC_ON
    if (readTimeChrono.hasPassed(timeReadInteval,true)) {
      DateTime now = rtc.now();
      time_yy = now.year();
      time_mo = now.month();
      time_dd = now.day();
      time_hh = now.hour();
      time_mm = now.minute();
      time_ss = now.second();
      latestIntenalTemp = rtc.getTemperature();
      #if RTC_ON
      tempValidTime = rtc.now();
      #endif
    }
  #endif

  #if !IS_SERVER
  #if CO2_ON
    if (readCO2Chrono.hasPassed(readCO2Inteval,true)) {
      //Start comms with the CO2 sensor
      Wire.beginTransmission(co2Addr);
      Wire.write(0x22);
      Wire.write(0x00);
      Wire.write(0x08);
      Wire.write(0x2A);
      Wire.endTransmission();
      //Wait 10ms for the sensor to process our command.
      delay(20);
      Wire.requestFrom(co2Addr, 4);
      byte i = 0;
      byte buffer[4] = {0, 0, 0, 0};
      while (Wire.available()) {
        buffer[i] = Wire.read();
        i++;
      }  
      int tmpCO2 = 0;
      tmpCO2 |= buffer[1] & 0xFF;
      tmpCO2 = tmpCO2 << 8;
      tmpCO2 |= buffer[2] & 0xFF;
    
      //Checksum
      byte sum = 0; //Checksum Byte
      sum = buffer[0] + buffer[1] + buffer[2]; //Byte addition utilizes overflow
      if (sum == buffer[3]) {
        #if SERIAL_IN_LOOP
        Serial.print("CO2 read success: ");Serial.println(tmpCO2);
        #endif
        latestCO2 = tmpCO2;
        uniqueCO2 = 1;
        #if RTC_ON
          co2ValidTime = rtc.now();
        #endif
        if (tmpCO2 == 0) {
          readTimeChrono.restart(500);
        }
      }
      else {
        #if SERIAL_IN_LOOP
        Serial.println("CO2 read fail");
        #endif
        uniqueCO2 = 0;
      }
    }
  #endif
  #endif


  #if SD_ON
    if (writeSDChrono.hasPassed(sdWriteInteval,true)) {
      char writeBuf[64];
      #if RTC_ON
        DateTime now = rtc.now();
        sprintf(writeBuf,"%d,%lu,TimeYMD:HMS,%d,%d,%d:%d,%d,%d",CLIENT_ADDRESS,now.unixtime(),time_yy,time_mo,time_dd,time_hh,time_mm,time_ss);
        SW.write(writeBuf);
      #endif
      sprintf(writeBuf,"%d,%lu,Volatge,%f",CLIENT_ADDRESS,voltageValidTime.unixtime(),latestVoltage);
      SW.write(writeBuf);
      
      //If server, don't write local measurements 
      #if !IS_SERVER
        #if ACCEL_ON
          sprintf(writeBuf,"%d,%lu,AccelXYZ,%5.2f,%5.2f,%5.2f",CLIENT_ADDRESS,accelValidTime.unixtime(),accel_x,accel_y,accel_z);
          SW.write(writeBuf);
        #endif
        #if CO2_ON
          sprintf(writeBuf,"%d,%lu,CO2,%d,%d",CLIENT_ADDRESS,co2ValidTime.unixtime(),latestCO2,uniqueCO2);
          SW.write(writeBuf);
        #endif
        #if SERIAL_IN_LOOP
        Serial.println("SD write complete");
        #endif
      #endif
    }
  #endif



  #if LORA_ON
    #if IS_SERVER
      //IF THIS IS THE SERVER
      if (linkmanager.available()) {
        //A message has arrived for the SERVER_ADDRESS
        uint8_t buflen = sizeof(buf);
        uint8_t from;
        //Two options to check for new messages.
        //The Ack one blocks while sending an acknowledgement back to the sender.
        //if (linkmanager.recvfromAck(buf, &buflen, &from)) {
        if (linkmanager.recvfrom(buf, &buflen, &from)) {
          //A datagram addressed to this node has been received.
          //Now decode buffer into a mavlink message
          mavlink_status_t status;
          mavlink_message_t msg;
          for (unsigned int i = 0; i < buflen; i++){
            if (mavlink_parse_char(MAVLINK_COMM_0, buf[i], &msg, &status)) {
              #if SERIAL_IN_LOOP
              Serial.print("Recived from node #");Serial.print(from);
              Serial.print(". MSG ID #");Serial.println(msg.msgid);
              #endif
              #if RTC_ON
                latestRecvTime = rtc.now();
              #endif

              switch(msg.msgid) {
                case MAVLINK_MSG_ID_DL_INTERNAL_TEMP:  //#9000
                  {
                    char writeBuf[64];
                    int sz = sprintf(writeBuf,"%d,%lu,RTC_TEMP,%i",
                                              from,
                                              mavlink_msg_dl_internal_temp_get_time(&msg),
                                              mavlink_msg_dl_internal_temp_get_temp(&msg));
                    SW.write(writeBuf);
                    #if SERIAL_IN_LOOP
                    Serial.println(writeBuf);
                    #endif
                  }
                  break;

                case MAVLINK_MSG_ID_DL_ACCEL:   //#9001
                  {
                    char writeBuf[64];
                    int sz = sprintf(writeBuf,"%d,%lu,ACCEL,%i",
                                                from,
                                                mavlink_msg_dl_accel_get_time(&msg),
                                                mavlink_msg_dl_accel_get_accel(&msg));
                    SW.write(writeBuf);
                    #if SERIAL_IN_LOOP
                    Serial.println(writeBuf);
                    #endif
                  }
                  break;

                case MAVLINK_MSG_ID_DL_CO2:   //#9003
                  {
                    char writeBuf[64];
                    int sz = sprintf(writeBuf,"%d,%lu,CO2,%i,%d",
                                                from,
                                                mavlink_msg_dl_co2_get_time(&msg),
                                                mavlink_msg_dl_co2_get_co2(&msg),
                                                mavlink_msg_dl_co2_get_unique(&msg));
                    SW.write(writeBuf);
                    #if SERIAL_IN_LOOP
                    Serial.println(writeBuf);
                    #endif
                  }
                  break;

                case MAVLINK_MSG_ID_DL_VOLTAGE:   //#9004
                  {
                    char writeBuf[64];
                    int sz = sprintf(writeBuf,"%d,%lu,Voltage,%i",
                                                from,
                                                mavlink_msg_dl_voltage_get_time(&msg),
                                                mavlink_msg_dl_voltage_get_voltage(&msg));
                    SW.write(writeBuf);
                    #if SERIAL_IN_LOOP
                    Serial.println(writeBuf);
                    #endif
                  }
                  break;

                default:
                  break;                
              }
            }
          }
        }
      }

    #else
      //IF THIS IS THE NODE
      if (autoSendDataChrono.hasPassed(autoSendDataInteval,true)) {
        //Delay the Tx until the K30 CO2 lamp is not active (should only be 10s of ms)
        #if CO2_ON
        while (analogRead(CO2_LAMP_PIN)>1000) {
          delay(5);
        }
        #endif
        //Make a blank mavlink message
        mavlink_message_t MavlinkMsg;             //struct to encode MAVLink data into
        uint16_t MavlinkMsg_Length;               //size of the mavlink message
        uint8_t buf[250];                         //buffer to hold packed message before sending

        #if RTC_ON
          //Pack the data into the mavlink object
          mavlink_msg_dl_internal_temp_pack(0, 0, &MavlinkMsg,
                                            (uint32_t)tempValidTime.unixtime(),
                                            (uint16_t)(latestIntenalTemp*1E3));
          //Pack into a byte buffer
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
    
          //Send to server
          //Two options to send new messages.
          //The 'Wait' one causes other messages to be ignored until an ACK or timeout comes back from the server.
          //linkmanager.sendtoWait(buf, MavlinkMsg_Length, SERVER_ADDRESS);
          linkmanager.sendto(buf, MavlinkMsg_Length, SERVER_ADDRESS);
        #endif
        
        #if ACCEL_ON
          float tmpAccel = sqrt(maxAccel_x*maxAccel_x + maxAccel_y*maxAccel_y + maxAccel_z*maxAccel_z);
          mavlink_msg_dl_accel_pack(0, 0, &MavlinkMsg,
                                     (uint32_t)accelValidTime.unixtime(),
                                     (uint32_t)(tmpAccel*1E3));
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, SERVER_ADDRESS);
        #endif

        #if CO2_ON
          mavlink_msg_dl_co2_pack(0, 0, &MavlinkMsg,
                                     (uint32_t)co2ValidTime.unixtime(),
                                     (uint16_t)latestCO2,
                                     (uint8_t)uniqueCO2);
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, SERVER_ADDRESS);
        #endif

        
          mavlink_msg_dl_voltage_pack(0, 0, &MavlinkMsg,
                                     (uint32_t)voltageValidTime.unixtime(),
                                     (uint32_t)(latestVoltage*1E3));
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, SERVER_ADDRESS);
      }
    #endif
  #endif


  #if 0
  //#if LORA_ON
    // Update the radio (this is a DragonOS function which reads the RD modem and
    // attempts to recognise Dragon OS objects from the stream
    radio.update();
    switch (radio.available()) {
      case 0:
      break;
  
      case DragonObject_SET_CO2_POLL::getObjectID():
      {
        DragonObject_SET_CO2_POLL object;
        if (radio.recv(object)) {
          Serial.print("Received SET_CO2_POLL package... ");
          //Interpret the command
          uint32_t poll_time_ms = object.getFields()->Value;
          if (poll_time_ms > 10000) 
          {
            co2ReadInteval = 10000;
            readCO2Chrono.restart();
          }
          else {
            co2ReadInteval = poll_time_ms;
            readCO2Chrono.restart();
          }
        }
      }
  
      case DragonObject_LORACMD::getObjectID():
      {
        DragonObject_LORACMD object;
        if (radio.recv(object)) {
          Serial.print("Received LORACMD package... ");
          //Interpret the command
          switch (object.getFields()->CMDID) {
            case 1:
            {
              //Start sending measurement data
              Serial.print("start Tx...");
              sendData_on = true;
            }
            break;
            case 2:
            {
              //Stop sending measurement data
              Serial.print("stop Tx...");
              sendData_on = false;
            }
            break;
            case 3:
            {
              //Set send inteval to 1 second (time in ms)
              Serial.print("inteval 1s...");
              sendDataInteval = 1000;
            }
            break;
            case 4:
            {
              //Set send inteval to 60 seconds  (time in ms)
              Serial.print("inteval 60s...");
              sendDataInteval = 60*1000;
            }
            break;
            case 5:
            {
              //Send one CO2 reading asap.
              Serial.print("send CO2 data");
              DragonObject_LAST_CO2 createdObject(DRAGON_SYSTEMID_INT,latestCO2Time.unixtime(),latestCO2,uniqueCO2);
              radio.send(createdObject,DRAGON_TARGET_ID);
              datagram.waitPacketSent();
              Serial.println("Sent LAST_CO2 package");
            }
            break;
          }
          Serial.println("done");
        }
      }
      break;
  
      case DragonObject_ACK_LORACMD::getObjectID():
      {
        DragonObject_ACK_LORACMD object;
        if (radio.recv(object)) {
          Serial.println("Received ACK_LORACMD package");
          //Do something here!        
        }
      }
      
    } // end switch radio.available()
  
    //If the data send is enabled, then start sending at intevals
    if (sendData_on && sendDataChrono.hasPassed(sendDataInteval,true)) {
      #if ACCEL_ON
      {
      DragonObject_MAX_ACCEL createdObject(DRAGON_SYSTEMID_INT,maxAccel_x,maxAccel_y,maxAccel_z,maxAccelTime.unixtime());
      radio.send(createdObject,DRAGON_TARGET_ID);
      datagram.waitPacketSent();
      Serial.println("Sent MAX_ACCEL package");
      }
      #endif
      #if RTC_ON
      {
      DateTime now = rtc.now();
      DragonObject_RTC_TEMP createdObject(DRAGON_SYSTEMID_INT,now.unixtime(),rtc.getTemperature());
      radio.send(createdObject,DRAGON_TARGET_ID);
      datagram.waitPacketSent();
      Serial.println("Sent RTC_TEMP package");
      }
      #endif
      #if CO2_ON
      DragonObject_LAST_CO2 createdObject(DRAGON_SYSTEMID_INT,latestCO2Time.unixtime(),latestCO2,uniqueCO2);
      radio.send(createdObject,DRAGON_TARGET_ID);
      datagram.waitPacketSent();
      Serial.println("Sent LAST_CO2 package");
      #endif
    }
  #endif  //LORA_ON


  //int sleepMS = Watchdog.sleep(1000);   //The sleep works but kills the USB serial output.

  return true;    //TODO: this is for the IridiumSBD call back. Set false to cancel an Iridium Tx mid process.
} // end loop2()


#if SAT_ON
//Functions for the RockBlock satellite modem. For some buggy reason
//they are required to make the code work.
#if IRIDIUM_DIAGNOSTICS
void ISBDConsoleCallback(IridiumSBD *device, char c)
{
  Serial.write(c);
}

void ISBDDiagsCallback(IridiumSBD *device, char c)
{
  Serial.write(c);
}
#endif
#endif

//end of file
