/** @file
 *  @brief MAVLink comm protocol generated from dragon.xml
 *  @see http://mavlink.org
 */
#pragma once
#ifndef MAVLINK_DRAGON_H
#define MAVLINK_DRAGON_H

#ifndef MAVLINK_H
    #error Wrong include order: MAVLINK_DRAGON.H MUST NOT BE DIRECTLY USED. Include mavlink.h from the same directory instead or set ALL AND EVERY defines from MAVLINK.H manually accordingly, including the #define MAVLINK_H call.
#endif

#undef MAVLINK_THIS_XML_IDX
#define MAVLINK_THIS_XML_IDX 0

#ifdef __cplusplus
extern "C" {
#endif

// MESSAGE LENGTHS AND CRCS

#ifndef MAVLINK_MESSAGE_LENGTHS
#define MAVLINK_MESSAGE_LENGTHS {}
#endif

#ifndef MAVLINK_MESSAGE_CRCS
#define MAVLINK_MESSAGE_CRCS {{9000, 187, 6, 6, 0, 0, 0}, {9001, 30, 8, 8, 0, 0, 0}, {9002, 1, 4, 4, 0, 0, 0}, {9003, 84, 7, 7, 0, 0, 0}, {9004, 45, 8, 8, 0, 0, 0}}
#endif

#include "../protocol.h"

#define MAVLINK_ENABLED_DRAGON

// ENUM DEFINITIONS



// MAVLINK VERSION

#ifndef MAVLINK_VERSION
#define MAVLINK_VERSION 2
#endif

#if (MAVLINK_VERSION == 0)
#undef MAVLINK_VERSION
#define MAVLINK_VERSION 2
#endif

// MESSAGE DEFINITIONS
#include "./mavlink_msg_dl_internal_temp.h"
#include "./mavlink_msg_dl_accel.h"
#include "./mavlink_msg_dl_time.h"
#include "./mavlink_msg_dl_co2.h"
#include "./mavlink_msg_dl_voltage.h"

// base include


#undef MAVLINK_THIS_XML_IDX
#define MAVLINK_THIS_XML_IDX 0

#if MAVLINK_THIS_XML_IDX == MAVLINK_PRIMARY_XML_IDX
# define MAVLINK_MESSAGE_INFO {MAVLINK_MESSAGE_INFO_DL_INTERNAL_TEMP, MAVLINK_MESSAGE_INFO_DL_ACCEL, MAVLINK_MESSAGE_INFO_DL_TIME, MAVLINK_MESSAGE_INFO_DL_CO2, MAVLINK_MESSAGE_INFO_DL_VOLTAGE}
# define MAVLINK_MESSAGE_NAMES {{ "DL_ACCEL", 9001 }, { "DL_CO2", 9003 }, { "DL_INTERNAL_TEMP", 9000 }, { "DL_TIME", 9002 }, { "DL_VOLTAGE", 9004 }}
# if MAVLINK_COMMAND_24BIT
#  include "../mavlink_get_info.h"
# endif
#endif

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // MAVLINK_DRAGON_H
