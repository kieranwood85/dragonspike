#pragma once
// MESSAGE DL_VOLTAGE PACKING

#define MAVLINK_MSG_ID_DL_VOLTAGE 9004


typedef struct __mavlink_dl_voltage_t {
 uint32_t time; /*<  UTC timestamp*/
 uint32_t voltage; /*<  Volatge (V*1E3)*/
} mavlink_dl_voltage_t;

#define MAVLINK_MSG_ID_DL_VOLTAGE_LEN 8
#define MAVLINK_MSG_ID_DL_VOLTAGE_MIN_LEN 8
#define MAVLINK_MSG_ID_9004_LEN 8
#define MAVLINK_MSG_ID_9004_MIN_LEN 8

#define MAVLINK_MSG_ID_DL_VOLTAGE_CRC 45
#define MAVLINK_MSG_ID_9004_CRC 45



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DL_VOLTAGE { \
    9004, \
    "DL_VOLTAGE", \
    2, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_voltage_t, time) }, \
         { "voltage", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_dl_voltage_t, voltage) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DL_VOLTAGE { \
    "DL_VOLTAGE", \
    2, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_voltage_t, time) }, \
         { "voltage", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_dl_voltage_t, voltage) }, \
         } \
}
#endif

/**
 * @brief Pack a dl_voltage message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  UTC timestamp
 * @param voltage  Volatge (V*1E3)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_voltage_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, uint32_t voltage)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_VOLTAGE_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint32_t(buf, 4, voltage);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_VOLTAGE_LEN);
#else
    mavlink_dl_voltage_t packet;
    packet.time = time;
    packet.voltage = voltage;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_VOLTAGE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_VOLTAGE;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DL_VOLTAGE_MIN_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_CRC);
}

/**
 * @brief Pack a dl_voltage message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  UTC timestamp
 * @param voltage  Volatge (V*1E3)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_voltage_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,uint32_t voltage)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_VOLTAGE_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint32_t(buf, 4, voltage);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_VOLTAGE_LEN);
#else
    mavlink_dl_voltage_t packet;
    packet.time = time;
    packet.voltage = voltage;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_VOLTAGE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_VOLTAGE;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DL_VOLTAGE_MIN_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_CRC);
}

/**
 * @brief Encode a dl_voltage struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param dl_voltage C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_voltage_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_dl_voltage_t* dl_voltage)
{
    return mavlink_msg_dl_voltage_pack(system_id, component_id, msg, dl_voltage->time, dl_voltage->voltage);
}

/**
 * @brief Encode a dl_voltage struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dl_voltage C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_voltage_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_dl_voltage_t* dl_voltage)
{
    return mavlink_msg_dl_voltage_pack_chan(system_id, component_id, chan, msg, dl_voltage->time, dl_voltage->voltage);
}

/**
 * @brief Send a dl_voltage message
 * @param chan MAVLink channel to send the message
 *
 * @param time  UTC timestamp
 * @param voltage  Volatge (V*1E3)
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_dl_voltage_send(mavlink_channel_t chan, uint32_t time, uint32_t voltage)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_VOLTAGE_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint32_t(buf, 4, voltage);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_VOLTAGE, buf, MAVLINK_MSG_ID_DL_VOLTAGE_MIN_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_CRC);
#else
    mavlink_dl_voltage_t packet;
    packet.time = time;
    packet.voltage = voltage;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_VOLTAGE, (const char *)&packet, MAVLINK_MSG_ID_DL_VOLTAGE_MIN_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_CRC);
#endif
}

/**
 * @brief Send a dl_voltage message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_dl_voltage_send_struct(mavlink_channel_t chan, const mavlink_dl_voltage_t* dl_voltage)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_dl_voltage_send(chan, dl_voltage->time, dl_voltage->voltage);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_VOLTAGE, (const char *)dl_voltage, MAVLINK_MSG_ID_DL_VOLTAGE_MIN_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_CRC);
#endif
}

#if MAVLINK_MSG_ID_DL_VOLTAGE_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This variant of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_dl_voltage_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, uint32_t voltage)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint32_t(buf, 4, voltage);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_VOLTAGE, buf, MAVLINK_MSG_ID_DL_VOLTAGE_MIN_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_CRC);
#else
    mavlink_dl_voltage_t *packet = (mavlink_dl_voltage_t *)msgbuf;
    packet->time = time;
    packet->voltage = voltage;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_VOLTAGE, (const char *)packet, MAVLINK_MSG_ID_DL_VOLTAGE_MIN_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_LEN, MAVLINK_MSG_ID_DL_VOLTAGE_CRC);
#endif
}
#endif

#endif

// MESSAGE DL_VOLTAGE UNPACKING


/**
 * @brief Get field time from dl_voltage message
 *
 * @return  UTC timestamp
 */
static inline uint32_t mavlink_msg_dl_voltage_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field voltage from dl_voltage message
 *
 * @return  Volatge (V*1E3)
 */
static inline uint32_t mavlink_msg_dl_voltage_get_voltage(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  4);
}

/**
 * @brief Decode a dl_voltage message into a struct
 *
 * @param msg The message to decode
 * @param dl_voltage C-struct to decode the message contents into
 */
static inline void mavlink_msg_dl_voltage_decode(const mavlink_message_t* msg, mavlink_dl_voltage_t* dl_voltage)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    dl_voltage->time = mavlink_msg_dl_voltage_get_time(msg);
    dl_voltage->voltage = mavlink_msg_dl_voltage_get_voltage(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DL_VOLTAGE_LEN? msg->len : MAVLINK_MSG_ID_DL_VOLTAGE_LEN;
        memset(dl_voltage, 0, MAVLINK_MSG_ID_DL_VOLTAGE_LEN);
    memcpy(dl_voltage, _MAV_PAYLOAD(msg), len);
#endif
}
