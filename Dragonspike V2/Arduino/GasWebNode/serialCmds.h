#ifndef SERIALCMDS_H
#define SERIALCMDS_H
extern SerialCommands serial_commands_;

//==========================================================
// SERIAL COMMAND FLAGS (these are to activate/deactivate various functions in the main)
// (start with cmd_ and end with _flag)
//==========================================================
bool cmd_setrtc_flag = false;
bool cmd_pumpon_flag = false;

bool cmd_txon_flag = false;
bool cmd_txoff_flag = false;
bool cmd_tx60_flag = false;
bool cmd_tx01_flag = false;
bool cmd_co2_flag = false;
bool cmd_auto_flag = false;
bool cmd_com_flag = false;
bool cmd_setpoll_flag = false;
bool cmd_sethomepoll_flag = false;

//==========================================================
// SERIAL COMMAND FUNCTIONS (start with cmd_)
//==========================================================
//This is the default handler, and gets called when no other command matches. 
void cmd_unrecognized(SerialCommands* sender, const char* cmd)
{
  sender->GetSerial()->print("Unrecognized command [");
  sender->GetSerial()->print(cmd);
  sender->GetSerial()->println("]");
}

//called for COM command
void cmd_com(SerialCommands* sender) {
  /*
  //Get the parameters of the command, should be the target ID as first param. 0 for all.
  String comment = ""; //defult is 0 for all
  char* pwm_str = sender->Next();
  if (pwm_str == NULL)
  {
    comment = "";
  }
  else {
    comment = pwm_str;
  }
  sender->GetSerial()->print("Comment: ");
  sender->GetSerial()->println(pwm_str);
  char writeBuf[64];
  comment.toCharArray(writeBuf, 60);
  //safeWrite(writeBuf,true);
  */
}

//called for SETPOLL command
void cmd_setpoll(SerialCommands* sender) {
  /*
  int tgt_id = 0;           //defult is 0 for all
  uint32_t pollms = 0;
  char* pwm_str = sender->Next();
  if (pwm_str == NULL)
  {
    sender->GetSerial()->println("No ParamCode");
    return;
  }
  else {
    tgt_id = atoi(pwm_str);
    char* pwm_str = sender->Next();
    if (pwm_str == NULL)
    {
      sender->GetSerial()->println("No ParamValue");
      return;
    }
    else 
    {
      pollms = atoi(pwm_str);
      //DragonObject_SET_CO2_POLL createdObject(DRAGON_SYSTEMID_INT,pollms);
      //radio.send(createdObject,tgt_id);
      //datagram.waitPacketSent();
      sender->GetSerial()->print("Set CO2 poll inteval to ");
      sender->GetSerial()->print(pollms);
      sender->GetSerial()->print(" ms on D:");
      sender->GetSerial()->println(tgt_id);
    }
  }
  */
}

//called for TXON command
void cmd_txon(SerialCommands* sender) {
  /*
  //Get the parameters of the command, should be the target ID as first param. 0 for all.
  int tgt_id = 0; //defult is 0 for all
  char* pwm_str = sender->Next();
  if (pwm_str == NULL)
  {
    sender->GetSerial()->println("ERROR NO TARGET ID");
    tgt_id = 0;
  }
  else {
    tgt_id = atoi(pwm_str);
  }
  //DragonObject_LORACMD createdObject(DRAGON_SYSTEMID_INT,01);
  //radio.send(createdObject,tgt_id);
  //datagram.waitPacketSent();
  sender->GetSerial()->print("Sensor Tx is on to D:");
  sender->GetSerial()->println(tgt_id);
  */
}

//called for TXOFF command
void cmd_txoff(SerialCommands* sender) {
  /*
  int tgt_id = 0; //defult is 0 for all
  char* pwm_str = sender->Next();
  if (pwm_str == NULL)
  {
    sender->GetSerial()->println("ERROR NO TARGET ID");
    tgt_id = 0;
  }
  else {
    tgt_id = atoi(pwm_str);
  }
  //DragonObject_LORACMD createdObject(DRAGON_SYSTEMID_INT,02);
  //radio.send(createdObject,tgt_id);
  //datagram.waitPacketSent();
  sender->GetSerial()->print("Sensor Tx is off to D:");
  sender->GetSerial()->println(tgt_id);
  */
}

//called for TX60 command
void cmd_tx60(SerialCommands* sender) {
  /*
  int tgt_id = 0; //defult is 0 for all
  char* pwm_str = sender->Next();
  if (pwm_str == NULL)
  {
    sender->GetSerial()->println("ERROR NO TARGET ID");
    tgt_id = 0;
  }
  else {
    tgt_id = atoi(pwm_str);
  }
  //DragonObject_LORACMD createdObject(DRAGON_SYSTEMID_INT,04);
  //radio.send(createdObject,tgt_id);
  //datagram.waitPacketSent();
  sender->GetSerial()->print("Sensor Tx inteval is 60s to D:");
  sender->GetSerial()->println(tgt_id);
  */
}

//called for TX01 command
void cmd_tx01(SerialCommands* sender) {
  /*
  int tgt_id = 0; //defult is 0 for all
  char* pwm_str = sender->Next();
  if (pwm_str == NULL)
  {
    sender->GetSerial()->println("ERROR NO TARGET ID");
    tgt_id = 0;
  }
  else {
    tgt_id = atoi(pwm_str);
  }
  //DragonObject_LORACMD createdObject(DRAGON_SYSTEMID_INT,03);
  //radio.send(createdObject,tgt_id);
  //datagram.waitPacketSent();
  sender->GetSerial()->print("Sensor Tx inteval is 1s to D:");
  sender->GetSerial()->println(tgt_id);
  */
}

//called for CO2 command
void cmd_co2(SerialCommands* sender) {
  /*
  int tgt_id = 0; //defult is 0 for all
  char* pwm_str = sender->Next();
  if (pwm_str == NULL)
  {
    sender->GetSerial()->println("ERROR NO TARGET ID");
    tgt_id = 0;
  }
  else {
    tgt_id = atoi(pwm_str);
  }
  //DragonObject_LORACMD createdObject(DRAGON_SYSTEMID_INT,05);
  //radio.send(createdObject,tgt_id);
  //datagram.waitPacketSent();
  sender->GetSerial()->print("Polled CO2 from D:");
  sender->GetSerial()->println(tgt_id);
  */
}

void cmd_auto(SerialCommands* sender) {
  /*
  int auto_on = 0; //defult is 0 for all
  char* pwm_str = sender->Next();
  if (pwm_str == NULL)
  {
    sender->GetSerial()->println("ERROR NO TARGET ID");
    auto_on = 0;
  }
  else {
    auto_on = atoi(pwm_str);
  }
  if (auto_on) {
    sender->GetSerial()->println("Set auto poll ON");
    autoPollOn = true;
  }
  else {
    sender->GetSerial()->println("Set auto poll OFF");
    autoPollOn = false;
  }
  */
}

void cmd_sethomepoll(SerialCommands* sender) {
  /*
  char* pwm_str = sender->Next();
  if (pwm_str == NULL)
  {
    sender->GetSerial()->println("ERROR NO POLL TIME");
  }
  else {
    pollTimerInteval = atoi(pwm_str);
  }
  */
}

//Called for setrtc command.
//example: SETRTC YYYY MM DD HH MM SS
void cmd_setrtc(SerialCommands* sender) {
  int newyear,newmonth,newday,newhour,newminute,newsecond;
  char* pwm_str = sender->Next();
  if (pwm_str == NULL) {
    sender->GetSerial()->println("ERROR NO YEAR GIVEN");
    newyear = 2000;
  }
  else {
    newyear = atoi(pwm_str);
    char* pwm_str = sender->Next();
    if (pwm_str == NULL) {
      sender->GetSerial()->println("ERROR NO MONTH GIVEN");
      newmonth = 1;
    }
    else {
      newmonth = atoi(pwm_str);
      char* pwm_str = sender->Next();
      if (pwm_str == NULL) {
        sender->GetSerial()->println("ERROR NO DAY GIVEN");
        newday = 1;
      }
      else {
        newday = atoi(pwm_str);
        char* pwm_str = sender->Next();
        if (pwm_str == NULL) {
          sender->GetSerial()->println("ERROR NO HOUR GIVEN");
          newhour = 0;
        }
        else {
          newhour = atoi(pwm_str);
          char* pwm_str = sender->Next();
          if (pwm_str == NULL) {
            sender->GetSerial()->println("ERROR NO MINUTE GIVEN");
            newminute = 0;
          }
          else {
            newminute = atoi(pwm_str);
            char* pwm_str = sender->Next();
            if (pwm_str == NULL) {
              sender->GetSerial()->println("ERROR NO SECOND GIVEN");
              newsecond = 0;
            }
            else {
              newsecond = atoi(pwm_str);
            }
          }
        }
      }
    }
  }
  #if RTC_ON
    rtc.adjust(DateTime(newyear,newmonth,newday,newhour,newminute,newsecond));
    sender->GetSerial()->print("Set DateTime: ");
    DateTime now = rtc.now();
    char date_format[] = {'Y','Y','-','M','M','-','D','D',' ','h','h',':','m','m',':','s','s','\0'};
    sender->GetSerial()->println(now.toString(date_format));
  #endif
}

//Called for pumpon command
void cmd_pumpon(SerialCommands* sender) {
  #if CO2_ON
  char* pwm_str = sender->Next();
  if (pwm_str == NULL) {
    digitalWrite(CO2_PUMP_PIN, LOW);
    sender->GetSerial()->println("Pump OFF");
  }
  else {
    int tmp = atoi(pwm_str);
    if (tmp==1) {
      digitalWrite(CO2_PUMP_PIN, HIGH);
      sender->GetSerial()->println("Pump ON");
    }
    else {
      digitalWrite(CO2_PUMP_PIN, LOW);
      sender->GetSerial()->println("Pump OFF");
    }
  }
  #endif
}

//==========================================================
// REGISTER TEXT COMMANDS (start with cmd_ and end with _)
//==========================================================
//Note: Commands are case sensitive
//See below for actual commands.
SerialCommand cmd_setrtc_("SETRTC", cmd_setrtc);
SerialCommand cmd_pumpon_("PUMPON", cmd_pumpon);

SerialCommand cmd_txon_("TXON", cmd_txon);
SerialCommand cmd_txoff_("TXOFF", cmd_txoff);
SerialCommand cmd_tx01_("TX01", cmd_tx01);
SerialCommand cmd_tx60_("TX60", cmd_tx60);
SerialCommand cmd_co2_("CO2", cmd_co2);
SerialCommand cmd_auto_("auto", cmd_auto);
SerialCommand cmd_com_("com", cmd_com);
SerialCommand cmd_setpoll_("setpoll", cmd_setpoll);
SerialCommand cmd_sethomepoll_("sethomepoll", cmd_sethomepoll);


void regAllCmds(void) {
  // Register all the serial commands
  serial_commands_.SetDefaultHandler(cmd_unrecognized);
  serial_commands_.AddCommand(&cmd_setrtc_);
  serial_commands_.AddCommand(&cmd_pumpon_);
  
  serial_commands_.AddCommand(&cmd_txon_);
  serial_commands_.AddCommand(&cmd_txoff_);
  serial_commands_.AddCommand(&cmd_tx01_);
  serial_commands_.AddCommand(&cmd_tx60_);
  serial_commands_.AddCommand(&cmd_co2_);
  serial_commands_.AddCommand(&cmd_auto_);
  serial_commands_.AddCommand(&cmd_com_);
  serial_commands_.AddCommand(&cmd_setpoll_);
  serial_commands_.AddCommand(&cmd_sethomepoll_);
}


#endif
//eof
