/** @file
 *  @brief MAVLink comm protocol generated from dragon.xml
 *  @see http://mavlink.org
 */
#pragma once
#ifndef MAVLINK_DRAGON_H
#define MAVLINK_DRAGON_H

#ifndef MAVLINK_H
    #error Wrong include order: MAVLINK_DRAGON.H MUST NOT BE DIRECTLY USED. Include mavlink.h from the same directory instead or set ALL AND EVERY defines from MAVLINK.H manually accordingly, including the #define MAVLINK_H call.
#endif

#undef MAVLINK_THIS_XML_IDX
#define MAVLINK_THIS_XML_IDX 0

#ifdef __cplusplus
extern "C" {
#endif

// MESSAGE LENGTHS AND CRCS

#ifndef MAVLINK_MESSAGE_LENGTHS
#define MAVLINK_MESSAGE_LENGTHS {}
#endif

#ifndef MAVLINK_MESSAGE_CRCS
#define MAVLINK_MESSAGE_CRCS {{9000, 187, 6, 6, 0, 0, 0}, {9001, 30, 8, 8, 0, 0, 0}, {9002, 1, 4, 4, 0, 0, 0}, {9003, 84, 7, 7, 0, 0, 0}, {9004, 45, 8, 8, 0, 0, 0}, {9005, 62, 24, 24, 0, 0, 0}, {9006, 146, 4, 4, 0, 0, 0}, {9007, 156, 4, 4, 0, 0, 0}, {9008, 195, 4, 4, 0, 0, 0}, {9009, 204, 16, 16, 0, 0, 0}, {9010, 39, 1, 1, 0, 0, 0}, {9011, 87, 4, 4, 0, 0, 0}, {9012, 245, 4, 4, 0, 0, 0}, {9013, 133, 4, 4, 0, 0, 0}, {9014, 153, 4, 4, 0, 0, 0}, {9015, 24, 1, 1, 0, 0, 0}, {9016, 228, 4, 4, 0, 0, 0}, {9017, 156, 20, 20, 0, 0, 0}}
#endif

#include "../protocol.h"

#define MAVLINK_ENABLED_DRAGON

// ENUM DEFINITIONS



// MAVLINK VERSION

#ifndef MAVLINK_VERSION
#define MAVLINK_VERSION 2
#endif

#if (MAVLINK_VERSION == 0)
#undef MAVLINK_VERSION
#define MAVLINK_VERSION 2
#endif

// MESSAGE DEFINITIONS
#include "./mavlink_msg_dl_internal_temp.h"
#include "./mavlink_msg_dl_accel.h"
#include "./mavlink_msg_dl_time.h"
#include "./mavlink_msg_dl_co2.h"
#include "./mavlink_msg_dl_voltage.h"
#include "./mavlink_msg_dl_setrtc.h"
#include "./mavlink_msg_dl_txon.h"
#include "./mavlink_msg_dl_toff.h"
#include "./mavlink_msg_dl_spol.h"
#include "./mavlink_msg_dl_accel3.h"
#include "./mavlink_msg_dl_set_acc_rnge.h"
#include "./mavlink_msg_dl_spol2.h"
#include "./mavlink_msg_dl_txon2.h"
#include "./mavlink_msg_dl_toff2.h"
#include "./mavlink_msg_dl_get_acc.h"
#include "./mavlink_msg_dl_reset.h"
#include "./mavlink_msg_dl_relay.h"
#include "./mavlink_msg_dl_accel3r.h"

// base include


#undef MAVLINK_THIS_XML_IDX
#define MAVLINK_THIS_XML_IDX 0

#if MAVLINK_THIS_XML_IDX == MAVLINK_PRIMARY_XML_IDX
# define MAVLINK_MESSAGE_INFO {MAVLINK_MESSAGE_INFO_DL_INTERNAL_TEMP, MAVLINK_MESSAGE_INFO_DL_ACCEL, MAVLINK_MESSAGE_INFO_DL_TIME, MAVLINK_MESSAGE_INFO_DL_CO2, MAVLINK_MESSAGE_INFO_DL_VOLTAGE, MAVLINK_MESSAGE_INFO_DL_SETRTC, MAVLINK_MESSAGE_INFO_DL_TXON, MAVLINK_MESSAGE_INFO_DL_TOFF, MAVLINK_MESSAGE_INFO_DL_SPOL, MAVLINK_MESSAGE_INFO_DL_ACCEL3, MAVLINK_MESSAGE_INFO_DL_SET_ACC_RNGE, MAVLINK_MESSAGE_INFO_DL_SPOL2, MAVLINK_MESSAGE_INFO_DL_TXON2, MAVLINK_MESSAGE_INFO_DL_TOFF2, MAVLINK_MESSAGE_INFO_DL_GET_ACC, MAVLINK_MESSAGE_INFO_DL_RESET, MAVLINK_MESSAGE_INFO_DL_RELAY, MAVLINK_MESSAGE_INFO_DL_ACCEL3R}
# define MAVLINK_MESSAGE_NAMES {{ "DL_ACCEL", 9001 }, { "DL_ACCEL3", 9009 }, { "DL_ACCEL3R", 9017 }, { "DL_CO2", 9003 }, { "DL_GET_ACC", 9014 }, { "DL_INTERNAL_TEMP", 9000 }, { "DL_RELAY", 9016 }, { "DL_RESET", 9015 }, { "DL_SETRTC", 9005 }, { "DL_SET_ACC_RNGE", 9010 }, { "DL_SPOL", 9008 }, { "DL_SPOL2", 9011 }, { "DL_TIME", 9002 }, { "DL_TOFF", 9007 }, { "DL_TOFF2", 9013 }, { "DL_TXON", 9006 }, { "DL_TXON2", 9012 }, { "DL_VOLTAGE", 9004 }}
# if MAVLINK_COMMAND_24BIT
#  include "../mavlink_get_info.h"
# endif
#endif

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // MAVLINK_DRAGON_H
