#pragma once
// MESSAGE DL_ACCEL3R PACKING

#define MAVLINK_MSG_ID_DL_ACCEL3R 9017


typedef struct __mavlink_dl_accel3r_t {
 uint32_t time; /*<  UTC timestamp*/
 uint32_t tgtid; /*<  Target ID to relay*/
 uint32_t accelx; /*<  Acceleration X (m/s*1E3)*/
 uint32_t accely; /*<  Acceleration Y (m/s*1E3)*/
 uint32_t accelz; /*<  Acceleration Z (m/s*1E3)*/
} mavlink_dl_accel3r_t;

#define MAVLINK_MSG_ID_DL_ACCEL3R_LEN 20
#define MAVLINK_MSG_ID_DL_ACCEL3R_MIN_LEN 20
#define MAVLINK_MSG_ID_9017_LEN 20
#define MAVLINK_MSG_ID_9017_MIN_LEN 20

#define MAVLINK_MSG_ID_DL_ACCEL3R_CRC 156
#define MAVLINK_MSG_ID_9017_CRC 156



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DL_ACCEL3R { \
    9017, \
    "DL_ACCEL3R", \
    5, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_accel3r_t, time) }, \
         { "tgtid", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_dl_accel3r_t, tgtid) }, \
         { "accelx", NULL, MAVLINK_TYPE_UINT32_T, 0, 8, offsetof(mavlink_dl_accel3r_t, accelx) }, \
         { "accely", NULL, MAVLINK_TYPE_UINT32_T, 0, 12, offsetof(mavlink_dl_accel3r_t, accely) }, \
         { "accelz", NULL, MAVLINK_TYPE_UINT32_T, 0, 16, offsetof(mavlink_dl_accel3r_t, accelz) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DL_ACCEL3R { \
    "DL_ACCEL3R", \
    5, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_accel3r_t, time) }, \
         { "tgtid", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_dl_accel3r_t, tgtid) }, \
         { "accelx", NULL, MAVLINK_TYPE_UINT32_T, 0, 8, offsetof(mavlink_dl_accel3r_t, accelx) }, \
         { "accely", NULL, MAVLINK_TYPE_UINT32_T, 0, 12, offsetof(mavlink_dl_accel3r_t, accely) }, \
         { "accelz", NULL, MAVLINK_TYPE_UINT32_T, 0, 16, offsetof(mavlink_dl_accel3r_t, accelz) }, \
         } \
}
#endif

/**
 * @brief Pack a dl_accel3r message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  UTC timestamp
 * @param tgtid  Target ID to relay
 * @param accelx  Acceleration X (m/s*1E3)
 * @param accely  Acceleration Y (m/s*1E3)
 * @param accelz  Acceleration Z (m/s*1E3)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_accel3r_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, uint32_t tgtid, uint32_t accelx, uint32_t accely, uint32_t accelz)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_ACCEL3R_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint32_t(buf, 4, tgtid);
    _mav_put_uint32_t(buf, 8, accelx);
    _mav_put_uint32_t(buf, 12, accely);
    _mav_put_uint32_t(buf, 16, accelz);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_ACCEL3R_LEN);
#else
    mavlink_dl_accel3r_t packet;
    packet.time = time;
    packet.tgtid = tgtid;
    packet.accelx = accelx;
    packet.accely = accely;
    packet.accelz = accelz;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_ACCEL3R_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_ACCEL3R;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DL_ACCEL3R_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_CRC);
}

/**
 * @brief Pack a dl_accel3r message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  UTC timestamp
 * @param tgtid  Target ID to relay
 * @param accelx  Acceleration X (m/s*1E3)
 * @param accely  Acceleration Y (m/s*1E3)
 * @param accelz  Acceleration Z (m/s*1E3)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_accel3r_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,uint32_t tgtid,uint32_t accelx,uint32_t accely,uint32_t accelz)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_ACCEL3R_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint32_t(buf, 4, tgtid);
    _mav_put_uint32_t(buf, 8, accelx);
    _mav_put_uint32_t(buf, 12, accely);
    _mav_put_uint32_t(buf, 16, accelz);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_ACCEL3R_LEN);
#else
    mavlink_dl_accel3r_t packet;
    packet.time = time;
    packet.tgtid = tgtid;
    packet.accelx = accelx;
    packet.accely = accely;
    packet.accelz = accelz;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_ACCEL3R_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_ACCEL3R;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DL_ACCEL3R_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_CRC);
}

/**
 * @brief Encode a dl_accel3r struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param dl_accel3r C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_accel3r_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_dl_accel3r_t* dl_accel3r)
{
    return mavlink_msg_dl_accel3r_pack(system_id, component_id, msg, dl_accel3r->time, dl_accel3r->tgtid, dl_accel3r->accelx, dl_accel3r->accely, dl_accel3r->accelz);
}

/**
 * @brief Encode a dl_accel3r struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dl_accel3r C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_accel3r_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_dl_accel3r_t* dl_accel3r)
{
    return mavlink_msg_dl_accel3r_pack_chan(system_id, component_id, chan, msg, dl_accel3r->time, dl_accel3r->tgtid, dl_accel3r->accelx, dl_accel3r->accely, dl_accel3r->accelz);
}

/**
 * @brief Send a dl_accel3r message
 * @param chan MAVLink channel to send the message
 *
 * @param time  UTC timestamp
 * @param tgtid  Target ID to relay
 * @param accelx  Acceleration X (m/s*1E3)
 * @param accely  Acceleration Y (m/s*1E3)
 * @param accelz  Acceleration Z (m/s*1E3)
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_dl_accel3r_send(mavlink_channel_t chan, uint32_t time, uint32_t tgtid, uint32_t accelx, uint32_t accely, uint32_t accelz)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_ACCEL3R_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint32_t(buf, 4, tgtid);
    _mav_put_uint32_t(buf, 8, accelx);
    _mav_put_uint32_t(buf, 12, accely);
    _mav_put_uint32_t(buf, 16, accelz);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_ACCEL3R, buf, MAVLINK_MSG_ID_DL_ACCEL3R_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_CRC);
#else
    mavlink_dl_accel3r_t packet;
    packet.time = time;
    packet.tgtid = tgtid;
    packet.accelx = accelx;
    packet.accely = accely;
    packet.accelz = accelz;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_ACCEL3R, (const char *)&packet, MAVLINK_MSG_ID_DL_ACCEL3R_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_CRC);
#endif
}

/**
 * @brief Send a dl_accel3r message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_dl_accel3r_send_struct(mavlink_channel_t chan, const mavlink_dl_accel3r_t* dl_accel3r)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_dl_accel3r_send(chan, dl_accel3r->time, dl_accel3r->tgtid, dl_accel3r->accelx, dl_accel3r->accely, dl_accel3r->accelz);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_ACCEL3R, (const char *)dl_accel3r, MAVLINK_MSG_ID_DL_ACCEL3R_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_CRC);
#endif
}

#if MAVLINK_MSG_ID_DL_ACCEL3R_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This variant of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_dl_accel3r_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, uint32_t tgtid, uint32_t accelx, uint32_t accely, uint32_t accelz)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint32_t(buf, 4, tgtid);
    _mav_put_uint32_t(buf, 8, accelx);
    _mav_put_uint32_t(buf, 12, accely);
    _mav_put_uint32_t(buf, 16, accelz);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_ACCEL3R, buf, MAVLINK_MSG_ID_DL_ACCEL3R_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_CRC);
#else
    mavlink_dl_accel3r_t *packet = (mavlink_dl_accel3r_t *)msgbuf;
    packet->time = time;
    packet->tgtid = tgtid;
    packet->accelx = accelx;
    packet->accely = accely;
    packet->accelz = accelz;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_ACCEL3R, (const char *)packet, MAVLINK_MSG_ID_DL_ACCEL3R_MIN_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_LEN, MAVLINK_MSG_ID_DL_ACCEL3R_CRC);
#endif
}
#endif

#endif

// MESSAGE DL_ACCEL3R UNPACKING


/**
 * @brief Get field time from dl_accel3r message
 *
 * @return  UTC timestamp
 */
static inline uint32_t mavlink_msg_dl_accel3r_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field tgtid from dl_accel3r message
 *
 * @return  Target ID to relay
 */
static inline uint32_t mavlink_msg_dl_accel3r_get_tgtid(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  4);
}

/**
 * @brief Get field accelx from dl_accel3r message
 *
 * @return  Acceleration X (m/s*1E3)
 */
static inline uint32_t mavlink_msg_dl_accel3r_get_accelx(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  8);
}

/**
 * @brief Get field accely from dl_accel3r message
 *
 * @return  Acceleration Y (m/s*1E3)
 */
static inline uint32_t mavlink_msg_dl_accel3r_get_accely(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  12);
}

/**
 * @brief Get field accelz from dl_accel3r message
 *
 * @return  Acceleration Z (m/s*1E3)
 */
static inline uint32_t mavlink_msg_dl_accel3r_get_accelz(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  16);
}

/**
 * @brief Decode a dl_accel3r message into a struct
 *
 * @param msg The message to decode
 * @param dl_accel3r C-struct to decode the message contents into
 */
static inline void mavlink_msg_dl_accel3r_decode(const mavlink_message_t* msg, mavlink_dl_accel3r_t* dl_accel3r)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    dl_accel3r->time = mavlink_msg_dl_accel3r_get_time(msg);
    dl_accel3r->tgtid = mavlink_msg_dl_accel3r_get_tgtid(msg);
    dl_accel3r->accelx = mavlink_msg_dl_accel3r_get_accelx(msg);
    dl_accel3r->accely = mavlink_msg_dl_accel3r_get_accely(msg);
    dl_accel3r->accelz = mavlink_msg_dl_accel3r_get_accelz(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DL_ACCEL3R_LEN? msg->len : MAVLINK_MSG_ID_DL_ACCEL3R_LEN;
        memset(dl_accel3r, 0, MAVLINK_MSG_ID_DL_ACCEL3R_LEN);
    memcpy(dl_accel3r, _MAV_PAYLOAD(msg), len);
#endif
}
