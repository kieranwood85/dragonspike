#pragma once
// MESSAGE DL_CO2 PACKING

#define MAVLINK_MSG_ID_DL_CO2 9003


typedef struct __mavlink_dl_co2_t {
 uint32_t time; /*<  UTC timestamp*/
 uint16_t co2; /*<  CO2 (ppm)*/
 uint8_t unique; /*<  Is the measurement new*/
} mavlink_dl_co2_t;

#define MAVLINK_MSG_ID_DL_CO2_LEN 7
#define MAVLINK_MSG_ID_DL_CO2_MIN_LEN 7
#define MAVLINK_MSG_ID_9003_LEN 7
#define MAVLINK_MSG_ID_9003_MIN_LEN 7

#define MAVLINK_MSG_ID_DL_CO2_CRC 84
#define MAVLINK_MSG_ID_9003_CRC 84



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DL_CO2 { \
    9003, \
    "DL_CO2", \
    3, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_co2_t, time) }, \
         { "co2", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_dl_co2_t, co2) }, \
         { "unique", NULL, MAVLINK_TYPE_UINT8_T, 0, 6, offsetof(mavlink_dl_co2_t, unique) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DL_CO2 { \
    "DL_CO2", \
    3, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_co2_t, time) }, \
         { "co2", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_dl_co2_t, co2) }, \
         { "unique", NULL, MAVLINK_TYPE_UINT8_T, 0, 6, offsetof(mavlink_dl_co2_t, unique) }, \
         } \
}
#endif

/**
 * @brief Pack a dl_co2 message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  UTC timestamp
 * @param co2  CO2 (ppm)
 * @param unique  Is the measurement new
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_co2_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, uint16_t co2, uint8_t unique)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_CO2_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint16_t(buf, 4, co2);
    _mav_put_uint8_t(buf, 6, unique);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_CO2_LEN);
#else
    mavlink_dl_co2_t packet;
    packet.time = time;
    packet.co2 = co2;
    packet.unique = unique;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_CO2_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_CO2;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DL_CO2_MIN_LEN, MAVLINK_MSG_ID_DL_CO2_LEN, MAVLINK_MSG_ID_DL_CO2_CRC);
}

/**
 * @brief Pack a dl_co2 message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  UTC timestamp
 * @param co2  CO2 (ppm)
 * @param unique  Is the measurement new
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_co2_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,uint16_t co2,uint8_t unique)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_CO2_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint16_t(buf, 4, co2);
    _mav_put_uint8_t(buf, 6, unique);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_CO2_LEN);
#else
    mavlink_dl_co2_t packet;
    packet.time = time;
    packet.co2 = co2;
    packet.unique = unique;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_CO2_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_CO2;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DL_CO2_MIN_LEN, MAVLINK_MSG_ID_DL_CO2_LEN, MAVLINK_MSG_ID_DL_CO2_CRC);
}

/**
 * @brief Encode a dl_co2 struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param dl_co2 C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_co2_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_dl_co2_t* dl_co2)
{
    return mavlink_msg_dl_co2_pack(system_id, component_id, msg, dl_co2->time, dl_co2->co2, dl_co2->unique);
}

/**
 * @brief Encode a dl_co2 struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dl_co2 C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_co2_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_dl_co2_t* dl_co2)
{
    return mavlink_msg_dl_co2_pack_chan(system_id, component_id, chan, msg, dl_co2->time, dl_co2->co2, dl_co2->unique);
}

/**
 * @brief Send a dl_co2 message
 * @param chan MAVLink channel to send the message
 *
 * @param time  UTC timestamp
 * @param co2  CO2 (ppm)
 * @param unique  Is the measurement new
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_dl_co2_send(mavlink_channel_t chan, uint32_t time, uint16_t co2, uint8_t unique)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_CO2_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint16_t(buf, 4, co2);
    _mav_put_uint8_t(buf, 6, unique);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_CO2, buf, MAVLINK_MSG_ID_DL_CO2_MIN_LEN, MAVLINK_MSG_ID_DL_CO2_LEN, MAVLINK_MSG_ID_DL_CO2_CRC);
#else
    mavlink_dl_co2_t packet;
    packet.time = time;
    packet.co2 = co2;
    packet.unique = unique;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_CO2, (const char *)&packet, MAVLINK_MSG_ID_DL_CO2_MIN_LEN, MAVLINK_MSG_ID_DL_CO2_LEN, MAVLINK_MSG_ID_DL_CO2_CRC);
#endif
}

/**
 * @brief Send a dl_co2 message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_dl_co2_send_struct(mavlink_channel_t chan, const mavlink_dl_co2_t* dl_co2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_dl_co2_send(chan, dl_co2->time, dl_co2->co2, dl_co2->unique);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_CO2, (const char *)dl_co2, MAVLINK_MSG_ID_DL_CO2_MIN_LEN, MAVLINK_MSG_ID_DL_CO2_LEN, MAVLINK_MSG_ID_DL_CO2_CRC);
#endif
}

#if MAVLINK_MSG_ID_DL_CO2_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This variant of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_dl_co2_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, uint16_t co2, uint8_t unique)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint16_t(buf, 4, co2);
    _mav_put_uint8_t(buf, 6, unique);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_CO2, buf, MAVLINK_MSG_ID_DL_CO2_MIN_LEN, MAVLINK_MSG_ID_DL_CO2_LEN, MAVLINK_MSG_ID_DL_CO2_CRC);
#else
    mavlink_dl_co2_t *packet = (mavlink_dl_co2_t *)msgbuf;
    packet->time = time;
    packet->co2 = co2;
    packet->unique = unique;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_CO2, (const char *)packet, MAVLINK_MSG_ID_DL_CO2_MIN_LEN, MAVLINK_MSG_ID_DL_CO2_LEN, MAVLINK_MSG_ID_DL_CO2_CRC);
#endif
}
#endif

#endif

// MESSAGE DL_CO2 UNPACKING


/**
 * @brief Get field time from dl_co2 message
 *
 * @return  UTC timestamp
 */
static inline uint32_t mavlink_msg_dl_co2_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field co2 from dl_co2 message
 *
 * @return  CO2 (ppm)
 */
static inline uint16_t mavlink_msg_dl_co2_get_co2(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  4);
}

/**
 * @brief Get field unique from dl_co2 message
 *
 * @return  Is the measurement new
 */
static inline uint8_t mavlink_msg_dl_co2_get_unique(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  6);
}

/**
 * @brief Decode a dl_co2 message into a struct
 *
 * @param msg The message to decode
 * @param dl_co2 C-struct to decode the message contents into
 */
static inline void mavlink_msg_dl_co2_decode(const mavlink_message_t* msg, mavlink_dl_co2_t* dl_co2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    dl_co2->time = mavlink_msg_dl_co2_get_time(msg);
    dl_co2->co2 = mavlink_msg_dl_co2_get_co2(msg);
    dl_co2->unique = mavlink_msg_dl_co2_get_unique(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DL_CO2_LEN? msg->len : MAVLINK_MSG_ID_DL_CO2_LEN;
        memset(dl_co2, 0, MAVLINK_MSG_ID_DL_CO2_LEN);
    memcpy(dl_co2, _MAV_PAYLOAD(msg), len);
#endif
}
