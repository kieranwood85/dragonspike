#pragma once
// MESSAGE DL_INTERNAL_TEMP PACKING

#define MAVLINK_MSG_ID_DL_INTERNAL_TEMP 9000


typedef struct __mavlink_dl_internal_temp_t {
 uint32_t time; /*<  UTC timestamp*/
 uint16_t temp; /*<  Temp (C*1E3)*/
} mavlink_dl_internal_temp_t;

#define MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN 6
#define MAVLINK_MSG_ID_DL_INTERNAL_TEMP_MIN_LEN 6
#define MAVLINK_MSG_ID_9000_LEN 6
#define MAVLINK_MSG_ID_9000_MIN_LEN 6

#define MAVLINK_MSG_ID_DL_INTERNAL_TEMP_CRC 187
#define MAVLINK_MSG_ID_9000_CRC 187



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DL_INTERNAL_TEMP { \
    9000, \
    "DL_INTERNAL_TEMP", \
    2, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_internal_temp_t, time) }, \
         { "temp", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_dl_internal_temp_t, temp) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DL_INTERNAL_TEMP { \
    "DL_INTERNAL_TEMP", \
    2, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_internal_temp_t, time) }, \
         { "temp", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_dl_internal_temp_t, temp) }, \
         } \
}
#endif

/**
 * @brief Pack a dl_internal_temp message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  UTC timestamp
 * @param temp  Temp (C*1E3)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_internal_temp_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, uint16_t temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint16_t(buf, 4, temp);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN);
#else
    mavlink_dl_internal_temp_t packet;
    packet.time = time;
    packet.temp = temp;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_INTERNAL_TEMP;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_MIN_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_CRC);
}

/**
 * @brief Pack a dl_internal_temp message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  UTC timestamp
 * @param temp  Temp (C*1E3)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_internal_temp_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,uint16_t temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint16_t(buf, 4, temp);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN);
#else
    mavlink_dl_internal_temp_t packet;
    packet.time = time;
    packet.temp = temp;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_INTERNAL_TEMP;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_MIN_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_CRC);
}

/**
 * @brief Encode a dl_internal_temp struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param dl_internal_temp C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_internal_temp_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_dl_internal_temp_t* dl_internal_temp)
{
    return mavlink_msg_dl_internal_temp_pack(system_id, component_id, msg, dl_internal_temp->time, dl_internal_temp->temp);
}

/**
 * @brief Encode a dl_internal_temp struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dl_internal_temp C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_internal_temp_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_dl_internal_temp_t* dl_internal_temp)
{
    return mavlink_msg_dl_internal_temp_pack_chan(system_id, component_id, chan, msg, dl_internal_temp->time, dl_internal_temp->temp);
}

/**
 * @brief Send a dl_internal_temp message
 * @param chan MAVLink channel to send the message
 *
 * @param time  UTC timestamp
 * @param temp  Temp (C*1E3)
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_dl_internal_temp_send(mavlink_channel_t chan, uint32_t time, uint16_t temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint16_t(buf, 4, temp);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_INTERNAL_TEMP, buf, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_MIN_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_CRC);
#else
    mavlink_dl_internal_temp_t packet;
    packet.time = time;
    packet.temp = temp;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_INTERNAL_TEMP, (const char *)&packet, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_MIN_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_CRC);
#endif
}

/**
 * @brief Send a dl_internal_temp message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_dl_internal_temp_send_struct(mavlink_channel_t chan, const mavlink_dl_internal_temp_t* dl_internal_temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_dl_internal_temp_send(chan, dl_internal_temp->time, dl_internal_temp->temp);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_INTERNAL_TEMP, (const char *)dl_internal_temp, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_MIN_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_CRC);
#endif
}

#if MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This variant of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_dl_internal_temp_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, uint16_t temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint16_t(buf, 4, temp);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_INTERNAL_TEMP, buf, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_MIN_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_CRC);
#else
    mavlink_dl_internal_temp_t *packet = (mavlink_dl_internal_temp_t *)msgbuf;
    packet->time = time;
    packet->temp = temp;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_INTERNAL_TEMP, (const char *)packet, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_MIN_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_CRC);
#endif
}
#endif

#endif

// MESSAGE DL_INTERNAL_TEMP UNPACKING


/**
 * @brief Get field time from dl_internal_temp message
 *
 * @return  UTC timestamp
 */
static inline uint32_t mavlink_msg_dl_internal_temp_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field temp from dl_internal_temp message
 *
 * @return  Temp (C*1E3)
 */
static inline uint16_t mavlink_msg_dl_internal_temp_get_temp(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  4);
}

/**
 * @brief Decode a dl_internal_temp message into a struct
 *
 * @param msg The message to decode
 * @param dl_internal_temp C-struct to decode the message contents into
 */
static inline void mavlink_msg_dl_internal_temp_decode(const mavlink_message_t* msg, mavlink_dl_internal_temp_t* dl_internal_temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    dl_internal_temp->time = mavlink_msg_dl_internal_temp_get_time(msg);
    dl_internal_temp->temp = mavlink_msg_dl_internal_temp_get_temp(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN? msg->len : MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN;
        memset(dl_internal_temp, 0, MAVLINK_MSG_ID_DL_INTERNAL_TEMP_LEN);
    memcpy(dl_internal_temp, _MAV_PAYLOAD(msg), len);
#endif
}
