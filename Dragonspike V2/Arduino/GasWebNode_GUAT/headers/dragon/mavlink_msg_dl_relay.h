#pragma once
// MESSAGE DL_RELAY PACKING

#define MAVLINK_MSG_ID_DL_RELAY 9016


typedef struct __mavlink_dl_relay_t {
 uint32_t tgtid; /*<  Target ID*/
} mavlink_dl_relay_t;

#define MAVLINK_MSG_ID_DL_RELAY_LEN 4
#define MAVLINK_MSG_ID_DL_RELAY_MIN_LEN 4
#define MAVLINK_MSG_ID_9016_LEN 4
#define MAVLINK_MSG_ID_9016_MIN_LEN 4

#define MAVLINK_MSG_ID_DL_RELAY_CRC 228
#define MAVLINK_MSG_ID_9016_CRC 228



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DL_RELAY { \
    9016, \
    "DL_RELAY", \
    1, \
    {  { "tgtid", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_relay_t, tgtid) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DL_RELAY { \
    "DL_RELAY", \
    1, \
    {  { "tgtid", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_relay_t, tgtid) }, \
         } \
}
#endif

/**
 * @brief Pack a dl_relay message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param tgtid  Target ID
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_relay_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t tgtid)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_RELAY_LEN];
    _mav_put_uint32_t(buf, 0, tgtid);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_RELAY_LEN);
#else
    mavlink_dl_relay_t packet;
    packet.tgtid = tgtid;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_RELAY_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_RELAY;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DL_RELAY_MIN_LEN, MAVLINK_MSG_ID_DL_RELAY_LEN, MAVLINK_MSG_ID_DL_RELAY_CRC);
}

/**
 * @brief Pack a dl_relay message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param tgtid  Target ID
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_relay_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t tgtid)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_RELAY_LEN];
    _mav_put_uint32_t(buf, 0, tgtid);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_RELAY_LEN);
#else
    mavlink_dl_relay_t packet;
    packet.tgtid = tgtid;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_RELAY_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_RELAY;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DL_RELAY_MIN_LEN, MAVLINK_MSG_ID_DL_RELAY_LEN, MAVLINK_MSG_ID_DL_RELAY_CRC);
}

/**
 * @brief Encode a dl_relay struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param dl_relay C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_relay_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_dl_relay_t* dl_relay)
{
    return mavlink_msg_dl_relay_pack(system_id, component_id, msg, dl_relay->tgtid);
}

/**
 * @brief Encode a dl_relay struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dl_relay C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_relay_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_dl_relay_t* dl_relay)
{
    return mavlink_msg_dl_relay_pack_chan(system_id, component_id, chan, msg, dl_relay->tgtid);
}

/**
 * @brief Send a dl_relay message
 * @param chan MAVLink channel to send the message
 *
 * @param tgtid  Target ID
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_dl_relay_send(mavlink_channel_t chan, uint32_t tgtid)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_RELAY_LEN];
    _mav_put_uint32_t(buf, 0, tgtid);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_RELAY, buf, MAVLINK_MSG_ID_DL_RELAY_MIN_LEN, MAVLINK_MSG_ID_DL_RELAY_LEN, MAVLINK_MSG_ID_DL_RELAY_CRC);
#else
    mavlink_dl_relay_t packet;
    packet.tgtid = tgtid;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_RELAY, (const char *)&packet, MAVLINK_MSG_ID_DL_RELAY_MIN_LEN, MAVLINK_MSG_ID_DL_RELAY_LEN, MAVLINK_MSG_ID_DL_RELAY_CRC);
#endif
}

/**
 * @brief Send a dl_relay message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_dl_relay_send_struct(mavlink_channel_t chan, const mavlink_dl_relay_t* dl_relay)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_dl_relay_send(chan, dl_relay->tgtid);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_RELAY, (const char *)dl_relay, MAVLINK_MSG_ID_DL_RELAY_MIN_LEN, MAVLINK_MSG_ID_DL_RELAY_LEN, MAVLINK_MSG_ID_DL_RELAY_CRC);
#endif
}

#if MAVLINK_MSG_ID_DL_RELAY_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This variant of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_dl_relay_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t tgtid)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, tgtid);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_RELAY, buf, MAVLINK_MSG_ID_DL_RELAY_MIN_LEN, MAVLINK_MSG_ID_DL_RELAY_LEN, MAVLINK_MSG_ID_DL_RELAY_CRC);
#else
    mavlink_dl_relay_t *packet = (mavlink_dl_relay_t *)msgbuf;
    packet->tgtid = tgtid;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_RELAY, (const char *)packet, MAVLINK_MSG_ID_DL_RELAY_MIN_LEN, MAVLINK_MSG_ID_DL_RELAY_LEN, MAVLINK_MSG_ID_DL_RELAY_CRC);
#endif
}
#endif

#endif

// MESSAGE DL_RELAY UNPACKING


/**
 * @brief Get field tgtid from dl_relay message
 *
 * @return  Target ID
 */
static inline uint32_t mavlink_msg_dl_relay_get_tgtid(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Decode a dl_relay message into a struct
 *
 * @param msg The message to decode
 * @param dl_relay C-struct to decode the message contents into
 */
static inline void mavlink_msg_dl_relay_decode(const mavlink_message_t* msg, mavlink_dl_relay_t* dl_relay)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    dl_relay->tgtid = mavlink_msg_dl_relay_get_tgtid(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DL_RELAY_LEN? msg->len : MAVLINK_MSG_ID_DL_RELAY_LEN;
        memset(dl_relay, 0, MAVLINK_MSG_ID_DL_RELAY_LEN);
    memcpy(dl_relay, _MAV_PAYLOAD(msg), len);
#endif
}
