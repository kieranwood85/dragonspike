#pragma once
// MESSAGE DL_RESET PACKING

#define MAVLINK_MSG_ID_DL_RESET 9015


typedef struct __mavlink_dl_reset_t {
 uint8_t reset; /*<  OnOff*/
} mavlink_dl_reset_t;

#define MAVLINK_MSG_ID_DL_RESET_LEN 1
#define MAVLINK_MSG_ID_DL_RESET_MIN_LEN 1
#define MAVLINK_MSG_ID_9015_LEN 1
#define MAVLINK_MSG_ID_9015_MIN_LEN 1

#define MAVLINK_MSG_ID_DL_RESET_CRC 24
#define MAVLINK_MSG_ID_9015_CRC 24



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DL_RESET { \
    9015, \
    "DL_RESET", \
    1, \
    {  { "reset", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_dl_reset_t, reset) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DL_RESET { \
    "DL_RESET", \
    1, \
    {  { "reset", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_dl_reset_t, reset) }, \
         } \
}
#endif

/**
 * @brief Pack a dl_reset message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param reset  OnOff
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_reset_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t reset)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_RESET_LEN];
    _mav_put_uint8_t(buf, 0, reset);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_RESET_LEN);
#else
    mavlink_dl_reset_t packet;
    packet.reset = reset;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_RESET_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_RESET;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DL_RESET_MIN_LEN, MAVLINK_MSG_ID_DL_RESET_LEN, MAVLINK_MSG_ID_DL_RESET_CRC);
}

/**
 * @brief Pack a dl_reset message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param reset  OnOff
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_reset_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t reset)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_RESET_LEN];
    _mav_put_uint8_t(buf, 0, reset);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_RESET_LEN);
#else
    mavlink_dl_reset_t packet;
    packet.reset = reset;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_RESET_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_RESET;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DL_RESET_MIN_LEN, MAVLINK_MSG_ID_DL_RESET_LEN, MAVLINK_MSG_ID_DL_RESET_CRC);
}

/**
 * @brief Encode a dl_reset struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param dl_reset C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_reset_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_dl_reset_t* dl_reset)
{
    return mavlink_msg_dl_reset_pack(system_id, component_id, msg, dl_reset->reset);
}

/**
 * @brief Encode a dl_reset struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dl_reset C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_reset_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_dl_reset_t* dl_reset)
{
    return mavlink_msg_dl_reset_pack_chan(system_id, component_id, chan, msg, dl_reset->reset);
}

/**
 * @brief Send a dl_reset message
 * @param chan MAVLink channel to send the message
 *
 * @param reset  OnOff
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_dl_reset_send(mavlink_channel_t chan, uint8_t reset)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_RESET_LEN];
    _mav_put_uint8_t(buf, 0, reset);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_RESET, buf, MAVLINK_MSG_ID_DL_RESET_MIN_LEN, MAVLINK_MSG_ID_DL_RESET_LEN, MAVLINK_MSG_ID_DL_RESET_CRC);
#else
    mavlink_dl_reset_t packet;
    packet.reset = reset;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_RESET, (const char *)&packet, MAVLINK_MSG_ID_DL_RESET_MIN_LEN, MAVLINK_MSG_ID_DL_RESET_LEN, MAVLINK_MSG_ID_DL_RESET_CRC);
#endif
}

/**
 * @brief Send a dl_reset message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_dl_reset_send_struct(mavlink_channel_t chan, const mavlink_dl_reset_t* dl_reset)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_dl_reset_send(chan, dl_reset->reset);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_RESET, (const char *)dl_reset, MAVLINK_MSG_ID_DL_RESET_MIN_LEN, MAVLINK_MSG_ID_DL_RESET_LEN, MAVLINK_MSG_ID_DL_RESET_CRC);
#endif
}

#if MAVLINK_MSG_ID_DL_RESET_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This variant of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_dl_reset_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t reset)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint8_t(buf, 0, reset);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_RESET, buf, MAVLINK_MSG_ID_DL_RESET_MIN_LEN, MAVLINK_MSG_ID_DL_RESET_LEN, MAVLINK_MSG_ID_DL_RESET_CRC);
#else
    mavlink_dl_reset_t *packet = (mavlink_dl_reset_t *)msgbuf;
    packet->reset = reset;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_RESET, (const char *)packet, MAVLINK_MSG_ID_DL_RESET_MIN_LEN, MAVLINK_MSG_ID_DL_RESET_LEN, MAVLINK_MSG_ID_DL_RESET_CRC);
#endif
}
#endif

#endif

// MESSAGE DL_RESET UNPACKING


/**
 * @brief Get field reset from dl_reset message
 *
 * @return  OnOff
 */
static inline uint8_t mavlink_msg_dl_reset_get_reset(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Decode a dl_reset message into a struct
 *
 * @param msg The message to decode
 * @param dl_reset C-struct to decode the message contents into
 */
static inline void mavlink_msg_dl_reset_decode(const mavlink_message_t* msg, mavlink_dl_reset_t* dl_reset)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    dl_reset->reset = mavlink_msg_dl_reset_get_reset(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DL_RESET_LEN? msg->len : MAVLINK_MSG_ID_DL_RESET_LEN;
        memset(dl_reset, 0, MAVLINK_MSG_ID_DL_RESET_LEN);
    memcpy(dl_reset, _MAV_PAYLOAD(msg), len);
#endif
}
