#pragma once
// MESSAGE DL_SET_ACC_RNGE PACKING

#define MAVLINK_MSG_ID_DL_SET_ACC_RNGE 9010


typedef struct __mavlink_dl_set_acc_rnge_t {
 uint8_t range; /*<  Acceleration Range (2g,4g,8g)*/
} mavlink_dl_set_acc_rnge_t;

#define MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN 1
#define MAVLINK_MSG_ID_DL_SET_ACC_RNGE_MIN_LEN 1
#define MAVLINK_MSG_ID_9010_LEN 1
#define MAVLINK_MSG_ID_9010_MIN_LEN 1

#define MAVLINK_MSG_ID_DL_SET_ACC_RNGE_CRC 39
#define MAVLINK_MSG_ID_9010_CRC 39



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DL_SET_ACC_RNGE { \
    9010, \
    "DL_SET_ACC_RNGE", \
    1, \
    {  { "range", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_dl_set_acc_rnge_t, range) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DL_SET_ACC_RNGE { \
    "DL_SET_ACC_RNGE", \
    1, \
    {  { "range", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_dl_set_acc_rnge_t, range) }, \
         } \
}
#endif

/**
 * @brief Pack a dl_set_acc_rnge message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param range  Acceleration Range (2g,4g,8g)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_set_acc_rnge_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t range)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN];
    _mav_put_uint8_t(buf, 0, range);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN);
#else
    mavlink_dl_set_acc_rnge_t packet;
    packet.range = range;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_SET_ACC_RNGE;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_MIN_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_CRC);
}

/**
 * @brief Pack a dl_set_acc_rnge message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param range  Acceleration Range (2g,4g,8g)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_set_acc_rnge_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t range)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN];
    _mav_put_uint8_t(buf, 0, range);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN);
#else
    mavlink_dl_set_acc_rnge_t packet;
    packet.range = range;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_SET_ACC_RNGE;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_MIN_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_CRC);
}

/**
 * @brief Encode a dl_set_acc_rnge struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param dl_set_acc_rnge C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_set_acc_rnge_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_dl_set_acc_rnge_t* dl_set_acc_rnge)
{
    return mavlink_msg_dl_set_acc_rnge_pack(system_id, component_id, msg, dl_set_acc_rnge->range);
}

/**
 * @brief Encode a dl_set_acc_rnge struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dl_set_acc_rnge C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_set_acc_rnge_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_dl_set_acc_rnge_t* dl_set_acc_rnge)
{
    return mavlink_msg_dl_set_acc_rnge_pack_chan(system_id, component_id, chan, msg, dl_set_acc_rnge->range);
}

/**
 * @brief Send a dl_set_acc_rnge message
 * @param chan MAVLink channel to send the message
 *
 * @param range  Acceleration Range (2g,4g,8g)
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_dl_set_acc_rnge_send(mavlink_channel_t chan, uint8_t range)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN];
    _mav_put_uint8_t(buf, 0, range);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SET_ACC_RNGE, buf, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_MIN_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_CRC);
#else
    mavlink_dl_set_acc_rnge_t packet;
    packet.range = range;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SET_ACC_RNGE, (const char *)&packet, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_MIN_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_CRC);
#endif
}

/**
 * @brief Send a dl_set_acc_rnge message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_dl_set_acc_rnge_send_struct(mavlink_channel_t chan, const mavlink_dl_set_acc_rnge_t* dl_set_acc_rnge)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_dl_set_acc_rnge_send(chan, dl_set_acc_rnge->range);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SET_ACC_RNGE, (const char *)dl_set_acc_rnge, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_MIN_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_CRC);
#endif
}

#if MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This variant of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_dl_set_acc_rnge_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t range)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint8_t(buf, 0, range);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SET_ACC_RNGE, buf, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_MIN_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_CRC);
#else
    mavlink_dl_set_acc_rnge_t *packet = (mavlink_dl_set_acc_rnge_t *)msgbuf;
    packet->range = range;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SET_ACC_RNGE, (const char *)packet, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_MIN_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_CRC);
#endif
}
#endif

#endif

// MESSAGE DL_SET_ACC_RNGE UNPACKING


/**
 * @brief Get field range from dl_set_acc_rnge message
 *
 * @return  Acceleration Range (2g,4g,8g)
 */
static inline uint8_t mavlink_msg_dl_set_acc_rnge_get_range(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Decode a dl_set_acc_rnge message into a struct
 *
 * @param msg The message to decode
 * @param dl_set_acc_rnge C-struct to decode the message contents into
 */
static inline void mavlink_msg_dl_set_acc_rnge_decode(const mavlink_message_t* msg, mavlink_dl_set_acc_rnge_t* dl_set_acc_rnge)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    dl_set_acc_rnge->range = mavlink_msg_dl_set_acc_rnge_get_range(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN? msg->len : MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN;
        memset(dl_set_acc_rnge, 0, MAVLINK_MSG_ID_DL_SET_ACC_RNGE_LEN);
    memcpy(dl_set_acc_rnge, _MAV_PAYLOAD(msg), len);
#endif
}
