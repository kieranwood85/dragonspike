#pragma once
// MESSAGE DL_SETRTC PACKING

#define MAVLINK_MSG_ID_DL_SETRTC 9005


typedef struct __mavlink_dl_setrtc_t {
 uint32_t year; /*<  Year*/
 uint32_t month; /*<  Month*/
 uint32_t day; /*<  Day*/
 uint32_t hour; /*<  Hour*/
 uint32_t minute; /*<  Minute*/
 uint32_t second; /*<  Second*/
} mavlink_dl_setrtc_t;

#define MAVLINK_MSG_ID_DL_SETRTC_LEN 24
#define MAVLINK_MSG_ID_DL_SETRTC_MIN_LEN 24
#define MAVLINK_MSG_ID_9005_LEN 24
#define MAVLINK_MSG_ID_9005_MIN_LEN 24

#define MAVLINK_MSG_ID_DL_SETRTC_CRC 62
#define MAVLINK_MSG_ID_9005_CRC 62



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DL_SETRTC { \
    9005, \
    "DL_SETRTC", \
    6, \
    {  { "year", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_setrtc_t, year) }, \
         { "month", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_dl_setrtc_t, month) }, \
         { "day", NULL, MAVLINK_TYPE_UINT32_T, 0, 8, offsetof(mavlink_dl_setrtc_t, day) }, \
         { "hour", NULL, MAVLINK_TYPE_UINT32_T, 0, 12, offsetof(mavlink_dl_setrtc_t, hour) }, \
         { "minute", NULL, MAVLINK_TYPE_UINT32_T, 0, 16, offsetof(mavlink_dl_setrtc_t, minute) }, \
         { "second", NULL, MAVLINK_TYPE_UINT32_T, 0, 20, offsetof(mavlink_dl_setrtc_t, second) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DL_SETRTC { \
    "DL_SETRTC", \
    6, \
    {  { "year", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_setrtc_t, year) }, \
         { "month", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_dl_setrtc_t, month) }, \
         { "day", NULL, MAVLINK_TYPE_UINT32_T, 0, 8, offsetof(mavlink_dl_setrtc_t, day) }, \
         { "hour", NULL, MAVLINK_TYPE_UINT32_T, 0, 12, offsetof(mavlink_dl_setrtc_t, hour) }, \
         { "minute", NULL, MAVLINK_TYPE_UINT32_T, 0, 16, offsetof(mavlink_dl_setrtc_t, minute) }, \
         { "second", NULL, MAVLINK_TYPE_UINT32_T, 0, 20, offsetof(mavlink_dl_setrtc_t, second) }, \
         } \
}
#endif

/**
 * @brief Pack a dl_setrtc message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param year  Year
 * @param month  Month
 * @param day  Day
 * @param hour  Hour
 * @param minute  Minute
 * @param second  Second
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_setrtc_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minute, uint32_t second)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_SETRTC_LEN];
    _mav_put_uint32_t(buf, 0, year);
    _mav_put_uint32_t(buf, 4, month);
    _mav_put_uint32_t(buf, 8, day);
    _mav_put_uint32_t(buf, 12, hour);
    _mav_put_uint32_t(buf, 16, minute);
    _mav_put_uint32_t(buf, 20, second);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_SETRTC_LEN);
#else
    mavlink_dl_setrtc_t packet;
    packet.year = year;
    packet.month = month;
    packet.day = day;
    packet.hour = hour;
    packet.minute = minute;
    packet.second = second;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_SETRTC_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_SETRTC;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DL_SETRTC_MIN_LEN, MAVLINK_MSG_ID_DL_SETRTC_LEN, MAVLINK_MSG_ID_DL_SETRTC_CRC);
}

/**
 * @brief Pack a dl_setrtc message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param year  Year
 * @param month  Month
 * @param day  Day
 * @param hour  Hour
 * @param minute  Minute
 * @param second  Second
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_setrtc_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t year,uint32_t month,uint32_t day,uint32_t hour,uint32_t minute,uint32_t second)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_SETRTC_LEN];
    _mav_put_uint32_t(buf, 0, year);
    _mav_put_uint32_t(buf, 4, month);
    _mav_put_uint32_t(buf, 8, day);
    _mav_put_uint32_t(buf, 12, hour);
    _mav_put_uint32_t(buf, 16, minute);
    _mav_put_uint32_t(buf, 20, second);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_SETRTC_LEN);
#else
    mavlink_dl_setrtc_t packet;
    packet.year = year;
    packet.month = month;
    packet.day = day;
    packet.hour = hour;
    packet.minute = minute;
    packet.second = second;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_SETRTC_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_SETRTC;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DL_SETRTC_MIN_LEN, MAVLINK_MSG_ID_DL_SETRTC_LEN, MAVLINK_MSG_ID_DL_SETRTC_CRC);
}

/**
 * @brief Encode a dl_setrtc struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param dl_setrtc C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_setrtc_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_dl_setrtc_t* dl_setrtc)
{
    return mavlink_msg_dl_setrtc_pack(system_id, component_id, msg, dl_setrtc->year, dl_setrtc->month, dl_setrtc->day, dl_setrtc->hour, dl_setrtc->minute, dl_setrtc->second);
}

/**
 * @brief Encode a dl_setrtc struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dl_setrtc C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_setrtc_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_dl_setrtc_t* dl_setrtc)
{
    return mavlink_msg_dl_setrtc_pack_chan(system_id, component_id, chan, msg, dl_setrtc->year, dl_setrtc->month, dl_setrtc->day, dl_setrtc->hour, dl_setrtc->minute, dl_setrtc->second);
}

/**
 * @brief Send a dl_setrtc message
 * @param chan MAVLink channel to send the message
 *
 * @param year  Year
 * @param month  Month
 * @param day  Day
 * @param hour  Hour
 * @param minute  Minute
 * @param second  Second
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_dl_setrtc_send(mavlink_channel_t chan, uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minute, uint32_t second)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_SETRTC_LEN];
    _mav_put_uint32_t(buf, 0, year);
    _mav_put_uint32_t(buf, 4, month);
    _mav_put_uint32_t(buf, 8, day);
    _mav_put_uint32_t(buf, 12, hour);
    _mav_put_uint32_t(buf, 16, minute);
    _mav_put_uint32_t(buf, 20, second);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SETRTC, buf, MAVLINK_MSG_ID_DL_SETRTC_MIN_LEN, MAVLINK_MSG_ID_DL_SETRTC_LEN, MAVLINK_MSG_ID_DL_SETRTC_CRC);
#else
    mavlink_dl_setrtc_t packet;
    packet.year = year;
    packet.month = month;
    packet.day = day;
    packet.hour = hour;
    packet.minute = minute;
    packet.second = second;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SETRTC, (const char *)&packet, MAVLINK_MSG_ID_DL_SETRTC_MIN_LEN, MAVLINK_MSG_ID_DL_SETRTC_LEN, MAVLINK_MSG_ID_DL_SETRTC_CRC);
#endif
}

/**
 * @brief Send a dl_setrtc message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_dl_setrtc_send_struct(mavlink_channel_t chan, const mavlink_dl_setrtc_t* dl_setrtc)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_dl_setrtc_send(chan, dl_setrtc->year, dl_setrtc->month, dl_setrtc->day, dl_setrtc->hour, dl_setrtc->minute, dl_setrtc->second);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SETRTC, (const char *)dl_setrtc, MAVLINK_MSG_ID_DL_SETRTC_MIN_LEN, MAVLINK_MSG_ID_DL_SETRTC_LEN, MAVLINK_MSG_ID_DL_SETRTC_CRC);
#endif
}

#if MAVLINK_MSG_ID_DL_SETRTC_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This variant of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_dl_setrtc_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minute, uint32_t second)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, year);
    _mav_put_uint32_t(buf, 4, month);
    _mav_put_uint32_t(buf, 8, day);
    _mav_put_uint32_t(buf, 12, hour);
    _mav_put_uint32_t(buf, 16, minute);
    _mav_put_uint32_t(buf, 20, second);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SETRTC, buf, MAVLINK_MSG_ID_DL_SETRTC_MIN_LEN, MAVLINK_MSG_ID_DL_SETRTC_LEN, MAVLINK_MSG_ID_DL_SETRTC_CRC);
#else
    mavlink_dl_setrtc_t *packet = (mavlink_dl_setrtc_t *)msgbuf;
    packet->year = year;
    packet->month = month;
    packet->day = day;
    packet->hour = hour;
    packet->minute = minute;
    packet->second = second;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SETRTC, (const char *)packet, MAVLINK_MSG_ID_DL_SETRTC_MIN_LEN, MAVLINK_MSG_ID_DL_SETRTC_LEN, MAVLINK_MSG_ID_DL_SETRTC_CRC);
#endif
}
#endif

#endif

// MESSAGE DL_SETRTC UNPACKING


/**
 * @brief Get field year from dl_setrtc message
 *
 * @return  Year
 */
static inline uint32_t mavlink_msg_dl_setrtc_get_year(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field month from dl_setrtc message
 *
 * @return  Month
 */
static inline uint32_t mavlink_msg_dl_setrtc_get_month(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  4);
}

/**
 * @brief Get field day from dl_setrtc message
 *
 * @return  Day
 */
static inline uint32_t mavlink_msg_dl_setrtc_get_day(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  8);
}

/**
 * @brief Get field hour from dl_setrtc message
 *
 * @return  Hour
 */
static inline uint32_t mavlink_msg_dl_setrtc_get_hour(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  12);
}

/**
 * @brief Get field minute from dl_setrtc message
 *
 * @return  Minute
 */
static inline uint32_t mavlink_msg_dl_setrtc_get_minute(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  16);
}

/**
 * @brief Get field second from dl_setrtc message
 *
 * @return  Second
 */
static inline uint32_t mavlink_msg_dl_setrtc_get_second(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  20);
}

/**
 * @brief Decode a dl_setrtc message into a struct
 *
 * @param msg The message to decode
 * @param dl_setrtc C-struct to decode the message contents into
 */
static inline void mavlink_msg_dl_setrtc_decode(const mavlink_message_t* msg, mavlink_dl_setrtc_t* dl_setrtc)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    dl_setrtc->year = mavlink_msg_dl_setrtc_get_year(msg);
    dl_setrtc->month = mavlink_msg_dl_setrtc_get_month(msg);
    dl_setrtc->day = mavlink_msg_dl_setrtc_get_day(msg);
    dl_setrtc->hour = mavlink_msg_dl_setrtc_get_hour(msg);
    dl_setrtc->minute = mavlink_msg_dl_setrtc_get_minute(msg);
    dl_setrtc->second = mavlink_msg_dl_setrtc_get_second(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DL_SETRTC_LEN? msg->len : MAVLINK_MSG_ID_DL_SETRTC_LEN;
        memset(dl_setrtc, 0, MAVLINK_MSG_ID_DL_SETRTC_LEN);
    memcpy(dl_setrtc, _MAV_PAYLOAD(msg), len);
#endif
}
