#pragma once
// MESSAGE DL_SPOL PACKING

#define MAVLINK_MSG_ID_DL_SPOL 9008


typedef struct __mavlink_dl_spol_t {
 uint32_t rate; /*<  Inteval in tenths seconds*/
} mavlink_dl_spol_t;

#define MAVLINK_MSG_ID_DL_SPOL_LEN 4
#define MAVLINK_MSG_ID_DL_SPOL_MIN_LEN 4
#define MAVLINK_MSG_ID_9008_LEN 4
#define MAVLINK_MSG_ID_9008_MIN_LEN 4

#define MAVLINK_MSG_ID_DL_SPOL_CRC 195
#define MAVLINK_MSG_ID_9008_CRC 195



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DL_SPOL { \
    9008, \
    "DL_SPOL", \
    1, \
    {  { "rate", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_spol_t, rate) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DL_SPOL { \
    "DL_SPOL", \
    1, \
    {  { "rate", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_spol_t, rate) }, \
         } \
}
#endif

/**
 * @brief Pack a dl_spol message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param rate  Inteval in tenths seconds
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_spol_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t rate)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_SPOL_LEN];
    _mav_put_uint32_t(buf, 0, rate);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_SPOL_LEN);
#else
    mavlink_dl_spol_t packet;
    packet.rate = rate;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_SPOL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_SPOL;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DL_SPOL_MIN_LEN, MAVLINK_MSG_ID_DL_SPOL_LEN, MAVLINK_MSG_ID_DL_SPOL_CRC);
}

/**
 * @brief Pack a dl_spol message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param rate  Inteval in tenths seconds
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_spol_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t rate)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_SPOL_LEN];
    _mav_put_uint32_t(buf, 0, rate);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_SPOL_LEN);
#else
    mavlink_dl_spol_t packet;
    packet.rate = rate;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_SPOL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_SPOL;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DL_SPOL_MIN_LEN, MAVLINK_MSG_ID_DL_SPOL_LEN, MAVLINK_MSG_ID_DL_SPOL_CRC);
}

/**
 * @brief Encode a dl_spol struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param dl_spol C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_spol_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_dl_spol_t* dl_spol)
{
    return mavlink_msg_dl_spol_pack(system_id, component_id, msg, dl_spol->rate);
}

/**
 * @brief Encode a dl_spol struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dl_spol C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_spol_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_dl_spol_t* dl_spol)
{
    return mavlink_msg_dl_spol_pack_chan(system_id, component_id, chan, msg, dl_spol->rate);
}

/**
 * @brief Send a dl_spol message
 * @param chan MAVLink channel to send the message
 *
 * @param rate  Inteval in tenths seconds
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_dl_spol_send(mavlink_channel_t chan, uint32_t rate)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_SPOL_LEN];
    _mav_put_uint32_t(buf, 0, rate);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SPOL, buf, MAVLINK_MSG_ID_DL_SPOL_MIN_LEN, MAVLINK_MSG_ID_DL_SPOL_LEN, MAVLINK_MSG_ID_DL_SPOL_CRC);
#else
    mavlink_dl_spol_t packet;
    packet.rate = rate;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SPOL, (const char *)&packet, MAVLINK_MSG_ID_DL_SPOL_MIN_LEN, MAVLINK_MSG_ID_DL_SPOL_LEN, MAVLINK_MSG_ID_DL_SPOL_CRC);
#endif
}

/**
 * @brief Send a dl_spol message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_dl_spol_send_struct(mavlink_channel_t chan, const mavlink_dl_spol_t* dl_spol)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_dl_spol_send(chan, dl_spol->rate);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SPOL, (const char *)dl_spol, MAVLINK_MSG_ID_DL_SPOL_MIN_LEN, MAVLINK_MSG_ID_DL_SPOL_LEN, MAVLINK_MSG_ID_DL_SPOL_CRC);
#endif
}

#if MAVLINK_MSG_ID_DL_SPOL_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This variant of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_dl_spol_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t rate)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, rate);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SPOL, buf, MAVLINK_MSG_ID_DL_SPOL_MIN_LEN, MAVLINK_MSG_ID_DL_SPOL_LEN, MAVLINK_MSG_ID_DL_SPOL_CRC);
#else
    mavlink_dl_spol_t *packet = (mavlink_dl_spol_t *)msgbuf;
    packet->rate = rate;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_SPOL, (const char *)packet, MAVLINK_MSG_ID_DL_SPOL_MIN_LEN, MAVLINK_MSG_ID_DL_SPOL_LEN, MAVLINK_MSG_ID_DL_SPOL_CRC);
#endif
}
#endif

#endif

// MESSAGE DL_SPOL UNPACKING


/**
 * @brief Get field rate from dl_spol message
 *
 * @return  Inteval in tenths seconds
 */
static inline uint32_t mavlink_msg_dl_spol_get_rate(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Decode a dl_spol message into a struct
 *
 * @param msg The message to decode
 * @param dl_spol C-struct to decode the message contents into
 */
static inline void mavlink_msg_dl_spol_decode(const mavlink_message_t* msg, mavlink_dl_spol_t* dl_spol)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    dl_spol->rate = mavlink_msg_dl_spol_get_rate(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DL_SPOL_LEN? msg->len : MAVLINK_MSG_ID_DL_SPOL_LEN;
        memset(dl_spol, 0, MAVLINK_MSG_ID_DL_SPOL_LEN);
    memcpy(dl_spol, _MAV_PAYLOAD(msg), len);
#endif
}
