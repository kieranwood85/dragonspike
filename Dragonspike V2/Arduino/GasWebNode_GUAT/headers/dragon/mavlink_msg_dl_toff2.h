#pragma once
// MESSAGE DL_TOFF2 PACKING

#define MAVLINK_MSG_ID_DL_TOFF2 9013


typedef struct __mavlink_dl_toff2_t {
 uint32_t onoff; /*<  OnOff*/
} mavlink_dl_toff2_t;

#define MAVLINK_MSG_ID_DL_TOFF2_LEN 4
#define MAVLINK_MSG_ID_DL_TOFF2_MIN_LEN 4
#define MAVLINK_MSG_ID_9013_LEN 4
#define MAVLINK_MSG_ID_9013_MIN_LEN 4

#define MAVLINK_MSG_ID_DL_TOFF2_CRC 133
#define MAVLINK_MSG_ID_9013_CRC 133



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DL_TOFF2 { \
    9013, \
    "DL_TOFF2", \
    1, \
    {  { "onoff", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_toff2_t, onoff) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DL_TOFF2 { \
    "DL_TOFF2", \
    1, \
    {  { "onoff", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_dl_toff2_t, onoff) }, \
         } \
}
#endif

/**
 * @brief Pack a dl_toff2 message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param onoff  OnOff
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_toff2_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t onoff)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_TOFF2_LEN];
    _mav_put_uint32_t(buf, 0, onoff);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_TOFF2_LEN);
#else
    mavlink_dl_toff2_t packet;
    packet.onoff = onoff;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_TOFF2_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_TOFF2;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DL_TOFF2_MIN_LEN, MAVLINK_MSG_ID_DL_TOFF2_LEN, MAVLINK_MSG_ID_DL_TOFF2_CRC);
}

/**
 * @brief Pack a dl_toff2 message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param onoff  OnOff
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_dl_toff2_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t onoff)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_TOFF2_LEN];
    _mav_put_uint32_t(buf, 0, onoff);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DL_TOFF2_LEN);
#else
    mavlink_dl_toff2_t packet;
    packet.onoff = onoff;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DL_TOFF2_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DL_TOFF2;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DL_TOFF2_MIN_LEN, MAVLINK_MSG_ID_DL_TOFF2_LEN, MAVLINK_MSG_ID_DL_TOFF2_CRC);
}

/**
 * @brief Encode a dl_toff2 struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param dl_toff2 C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_toff2_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_dl_toff2_t* dl_toff2)
{
    return mavlink_msg_dl_toff2_pack(system_id, component_id, msg, dl_toff2->onoff);
}

/**
 * @brief Encode a dl_toff2 struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dl_toff2 C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_dl_toff2_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_dl_toff2_t* dl_toff2)
{
    return mavlink_msg_dl_toff2_pack_chan(system_id, component_id, chan, msg, dl_toff2->onoff);
}

/**
 * @brief Send a dl_toff2 message
 * @param chan MAVLink channel to send the message
 *
 * @param onoff  OnOff
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_dl_toff2_send(mavlink_channel_t chan, uint32_t onoff)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DL_TOFF2_LEN];
    _mav_put_uint32_t(buf, 0, onoff);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_TOFF2, buf, MAVLINK_MSG_ID_DL_TOFF2_MIN_LEN, MAVLINK_MSG_ID_DL_TOFF2_LEN, MAVLINK_MSG_ID_DL_TOFF2_CRC);
#else
    mavlink_dl_toff2_t packet;
    packet.onoff = onoff;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_TOFF2, (const char *)&packet, MAVLINK_MSG_ID_DL_TOFF2_MIN_LEN, MAVLINK_MSG_ID_DL_TOFF2_LEN, MAVLINK_MSG_ID_DL_TOFF2_CRC);
#endif
}

/**
 * @brief Send a dl_toff2 message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_dl_toff2_send_struct(mavlink_channel_t chan, const mavlink_dl_toff2_t* dl_toff2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_dl_toff2_send(chan, dl_toff2->onoff);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_TOFF2, (const char *)dl_toff2, MAVLINK_MSG_ID_DL_TOFF2_MIN_LEN, MAVLINK_MSG_ID_DL_TOFF2_LEN, MAVLINK_MSG_ID_DL_TOFF2_CRC);
#endif
}

#if MAVLINK_MSG_ID_DL_TOFF2_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This variant of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_dl_toff2_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t onoff)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, onoff);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_TOFF2, buf, MAVLINK_MSG_ID_DL_TOFF2_MIN_LEN, MAVLINK_MSG_ID_DL_TOFF2_LEN, MAVLINK_MSG_ID_DL_TOFF2_CRC);
#else
    mavlink_dl_toff2_t *packet = (mavlink_dl_toff2_t *)msgbuf;
    packet->onoff = onoff;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DL_TOFF2, (const char *)packet, MAVLINK_MSG_ID_DL_TOFF2_MIN_LEN, MAVLINK_MSG_ID_DL_TOFF2_LEN, MAVLINK_MSG_ID_DL_TOFF2_CRC);
#endif
}
#endif

#endif

// MESSAGE DL_TOFF2 UNPACKING


/**
 * @brief Get field onoff from dl_toff2 message
 *
 * @return  OnOff
 */
static inline uint32_t mavlink_msg_dl_toff2_get_onoff(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Decode a dl_toff2 message into a struct
 *
 * @param msg The message to decode
 * @param dl_toff2 C-struct to decode the message contents into
 */
static inline void mavlink_msg_dl_toff2_decode(const mavlink_message_t* msg, mavlink_dl_toff2_t* dl_toff2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    dl_toff2->onoff = mavlink_msg_dl_toff2_get_onoff(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DL_TOFF2_LEN? msg->len : MAVLINK_MSG_ID_DL_TOFF2_LEN;
        memset(dl_toff2, 0, MAVLINK_MSG_ID_DL_TOFF2_LEN);
    memcpy(dl_toff2, _MAV_PAYLOAD(msg), len);
#endif
}
