from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
from pyqtgraph.ptime import time
import serial
import winsound

app = QtGui.QApplication([])

p = pg.plot()
p.setWindowTitle('live plot from serial')
p.setBackground('w')
p.addLegend()
p.showGrid(x=True, y=True)
p.setLabel('left', "Accel. (G*1E3)")
p.setLabel('bottom', "Up Time (s*10)")
curve1 = p.plot(pen=pg.mkPen('r', width=15),name="N10")
curve2 = p.plot(pen=pg.mkPen('g', width=15),name="N11")
curve3 = p.plot(pen=pg.mkPen('y', width=15),name="N12")
curve4 = p.plot(pen=pg.mkPen('b', width=15),name="N13")
curve5 = p.plot(pen=pg.mkPen('k', width=15),name="N15")

data = []
xs = []
n10 = []
n11 = []
n12 = []
n13 = []
n15 = []
raw=serial.Serial('COM9', 9600)
print('Setting base data mode')
raw.write("plotout 1\r\n".encode())

doOnce = [1,0]


def update():

    global curve, data
    line = raw.readline()
    linecomp=line.split(b',')
    xs.append(int(linecomp[0]))
    n10.append(float(linecomp[1]))
    n11.append(float(linecomp[2]))
    n12.append(float(linecomp[3]))
    n13.append(float(linecomp[4]))
    n15.append(float(linecomp[6]))
    maxLen = 500
    if (len(xs)>maxLen):
        xs.pop(0)
        n10.pop(0)
        n11.pop(0)
        n12.pop(0)
        n13.pop(0)
        n15.pop(0)
        
    #Sound for threshold crossing
    if (n11[-1]>4000):
        print('EVENT!')
        winsound.Beep(1000,100)
    
    if (doOnce[0]):
        doOnce.pop(0)
    else:
        xdata = np.array(xs, dtype='float64')
        ydata10 = np.array(n10, dtype='float64')
        ydata11 = np.array(n11, dtype='float64')
        ydata12 = np.array(n12, dtype='float64')
        ydata13 = np.array(n13, dtype='float64')
        ydata15 = np.array(n15, dtype='float64')
        curve1.setData(xdata,ydata10)
        curve2.setData(xdata,ydata11)
        curve3.setData(xdata,ydata12)
        curve4.setData(xdata,ydata13)
        curve5.setData(xdata,ydata15)
        app.processEvents()

timer = QtCore.QTimer()
timer.timeout.connect(update)
timer.start(0)

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()