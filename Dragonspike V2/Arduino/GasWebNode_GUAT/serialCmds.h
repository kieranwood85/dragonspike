#ifndef SERIALCMDS_H
#define SERIALCMDS_H
extern SerialCommands serial_commands_;

//This converts four chars into a unique number
#define FOURCC(a,b,c,d) ( (uint32_t) (((d)<<24) | ((c)<<16) | ((b)<<8) | (a)) )
enum COMMAND_NAME
{  
  ID_SRTC = FOURCC('s','r','t','c'),    //set rtc clock
  
  ID_TXON = FOURCC('t','x','o','n'),    //turn on primary
  ID_TOFF = FOURCC('t','o','f','f'),    //turn off primary
  ID_SPOL = FOURCC('s','p','o','l'),    //primary rate
  
  ID_TXO2 = FOURCC('t','x','o','2'),    //turn on secondary
  ID_TOF2 = FOURCC('t','o','f','2'),    //turn off secondary
  ID_SPO2 = FOURCC('s','p','o','2'),    //secondary rate

  ID_GACC = FOURCC('g','a','c','c'),    //request accel data
  ID_SACC = FOURCC('s','a','c','c'),    //set accel sensitivity (2,4,8)
  
  ID_REST = FOURCC('r','e','s','t'),    //reset

  ID_RELY = FOURCC('r','e','l','y')     //relay mode
};

//==========================================================
// SERIAL COMMAND FLAGS (these are to activate/deactivate various functions in the main)
// (start with cmd_ and end with _flag)
//==========================================================
bool cmd_setrtc_flag = false;
bool cmd_com_flag = false;
bool cmd_pumpon_flag = false;
bool cmd_plotout_flag = false;

//==========================================================
// SERIAL COMMAND FUNCTIONS (start with cmd_)
//==========================================================
//This is the default handler, and gets called when no other command matches. 
void cmd_unrecognized(SerialCommands* sender, const char* cmd)
{
  sender->GetSerial()->print("Unrecognized command [");
  sender->GetSerial()->print(cmd);
  sender->GetSerial()->println("]");
}

//called for COM command
//example: com tgt_id cmd_name param1 param2 param3
void cmd_com(SerialCommands* sender) {
  //Get the parameters of the command, should be the target ID as first param. 0 for all.
  int tgt_id = 0;
  char* pwm_str = sender->Next();
  if (pwm_str == NULL) {
    //Do nothing
    sender->GetSerial()->println("ERROR: no target given");
  }
  else {
    //Store the ID and check the command type
    tgt_id = atoi(pwm_str);
    char* pwm_str = sender->Next();
    if (pwm_str == NULL) {
      //Do nothing
      sender->GetSerial()->println("ERROR: no command given");
    }
    else {
      uint32_t CMD_ID = FOURCC(pwm_str[0],pwm_str[1],pwm_str[2],pwm_str[3]);
      //Switch the command
      switch (CMD_ID) {
        case ID_TXON:
        {
          mavlink_message_t MavlinkMsg;
          uint16_t MavlinkMsg_Length;
          uint8_t buf[250];
          mavlink_msg_dl_txon_pack(0, 0, &MavlinkMsg,1);
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, tgt_id);
        }
        break;
        
        case ID_TOFF:
        {
          mavlink_message_t MavlinkMsg;
          uint16_t MavlinkMsg_Length;
          uint8_t buf[250];
          mavlink_msg_dl_toff_pack(0, 0, &MavlinkMsg,0);
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, tgt_id);
        }
        break;

        case ID_TXO2:
        {
          mavlink_message_t MavlinkMsg;
          uint16_t MavlinkMsg_Length;
          uint8_t buf[250];
          mavlink_msg_dl_txon2_pack(0, 0, &MavlinkMsg,1);
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, tgt_id);
        }
        break;
        
        case ID_TOF2:
        {
          mavlink_message_t MavlinkMsg;
          uint16_t MavlinkMsg_Length;
          uint8_t buf[250];
          mavlink_msg_dl_toff2_pack(0, 0, &MavlinkMsg,0);
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, tgt_id);
        }
        break;
        
        case ID_SRTC:
        {
          int newyear,newmonth,newday,newhour,newminute,newsecond;
          char* pwm_str = sender->Next();
          newyear = atoi(pwm_str);
          pwm_str = sender->Next();
          newmonth = atoi(pwm_str);
          pwm_str = sender->Next();
          newday = atoi(pwm_str);
          pwm_str = sender->Next();
          newhour = atoi(pwm_str);
          pwm_str = sender->Next();
          newminute = atoi(pwm_str);
          pwm_str = sender->Next();
          newsecond = atoi(pwm_str);
          
          mavlink_message_t MavlinkMsg;
          uint16_t MavlinkMsg_Length;
          uint8_t buf[250];
          mavlink_msg_dl_setrtc_pack(0, 0, &MavlinkMsg,
                               (uint32_t)newyear,(uint32_t)newmonth,(uint32_t)newday,(uint32_t)newhour,(uint32_t)newminute,(uint32_t)newsecond);
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, tgt_id);
        }
        break;
        
        case ID_SPOL:
        {
          char* pwm_str = sender->Next();
          int inteval = atoi(pwm_str);

          if (inteval>5) 
          {
            mavlink_message_t MavlinkMsg;
            uint16_t MavlinkMsg_Length;
            uint8_t buf[250];
            mavlink_msg_dl_spol_pack(0, 0, &MavlinkMsg,(uint32_t)inteval);
            MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
            linkmanager.sendto(buf, MavlinkMsg_Length, tgt_id);
          }
          else
          {
            sender->GetSerial()->println("ERROR Inteval too short");
          }
        }
        break;

        case ID_SPO2:
        {
          char* pwm_str = sender->Next();
          int inteval = atoi(pwm_str);
          
          mavlink_message_t MavlinkMsg;
          uint16_t MavlinkMsg_Length;
          uint8_t buf[250];
          mavlink_msg_dl_spol2_pack(0, 0, &MavlinkMsg,(uint32_t)inteval);
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, tgt_id);
        }
        break;

        case ID_GACC:
        {
          mavlink_message_t MavlinkMsg;
          uint16_t MavlinkMsg_Length;
          uint8_t buf[250];
          mavlink_msg_dl_get_acc_pack(0, 0, &MavlinkMsg,(uint32_t)1);
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, tgt_id);
        }
        break;

        case ID_SACC:
        {
          char* pwm_str = sender->Next();
          int acc_range = atoi(pwm_str);
          
          mavlink_message_t MavlinkMsg;
          uint16_t MavlinkMsg_Length;
          uint8_t buf[250];
          mavlink_msg_dl_set_acc_rnge_pack(0, 0, &MavlinkMsg,(uint8_t)acc_range);
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, tgt_id);
        }
        break;

        case ID_REST:
        {          
          mavlink_message_t MavlinkMsg;
          uint16_t MavlinkMsg_Length;
          uint8_t buf[250];
          mavlink_msg_dl_reset_pack(0, 0, &MavlinkMsg,(uint8_t)1);
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, tgt_id);
        }
        break;

        case ID_RELY:
        {
          char* pwm_str = sender->Next();
          int relay_tgt = atoi(pwm_str);
          
          mavlink_message_t MavlinkMsg;
          uint16_t MavlinkMsg_Length;
          uint8_t buf[250];
          mavlink_msg_dl_relay_pack(0, 0, &MavlinkMsg,(uint32_t)relay_tgt);
          MavlinkMsg_Length = mavlink_msg_to_send_buffer(buf, &MavlinkMsg);
          linkmanager.sendto(buf, MavlinkMsg_Length, tgt_id);
        }
        break;
        
        default:
        {
          int tmp = 1;
        }
        break;
      }
    }
  }
}

//Called for setrtc command.
//example: SETRTC YYYY MM DD HH MM SS
void cmd_setrtc(SerialCommands* sender) {
  int newyear,newmonth,newday,newhour,newminute,newsecond;
  char* pwm_str = sender->Next();
  if (pwm_str == NULL) {
    sender->GetSerial()->println("ERROR NO YEAR GIVEN");
    newyear = 2000;
  }
  else {
    newyear = atoi(pwm_str);
    char* pwm_str = sender->Next();
    if (pwm_str == NULL) {
      sender->GetSerial()->println("ERROR NO MONTH GIVEN");
      newmonth = 1;
    }
    else {
      newmonth = atoi(pwm_str);
      char* pwm_str = sender->Next();
      if (pwm_str == NULL) {
        sender->GetSerial()->println("ERROR NO DAY GIVEN");
        newday = 1;
      }
      else {
        newday = atoi(pwm_str);
        char* pwm_str = sender->Next();
        if (pwm_str == NULL) {
          sender->GetSerial()->println("ERROR NO HOUR GIVEN");
          newhour = 0;
        }
        else {
          newhour = atoi(pwm_str);
          char* pwm_str = sender->Next();
          if (pwm_str == NULL) {
            sender->GetSerial()->println("ERROR NO MINUTE GIVEN");
            newminute = 0;
          }
          else {
            newminute = atoi(pwm_str);
            char* pwm_str = sender->Next();
            if (pwm_str == NULL) {
              sender->GetSerial()->println("ERROR NO SECOND GIVEN");
              newsecond = 0;
            }
            else {
              newsecond = atoi(pwm_str);
            }
          }
        }
      }
    }
  }
  #if RTC_ON
    rtc.adjust(DateTime(newyear,newmonth,newday,newhour,newminute,newsecond));
    sender->GetSerial()->print("Set DateTime: ");
    DateTime now = rtc.now();
    char date_format[] = {'Y','Y','-','M','M','-','D','D',' ','h','h',':','m','m',':','s','s','\0'};
    sender->GetSerial()->println(now.toString(date_format));
  #endif
}

//Called for pumpon command
void cmd_pumpon(SerialCommands* sender) {
  #if CO2_ON
  char* pwm_str = sender->Next();
  if (pwm_str == NULL) {
    digitalWrite(CO2_PUMP_PIN, LOW);
    sender->GetSerial()->println("Pump OFF");
  }
  else {
    int tmp = atoi(pwm_str);
    if (tmp==1) {
      digitalWrite(CO2_PUMP_PIN, HIGH);
      sender->GetSerial()->println("Pump ON");
    }
    else {
      digitalWrite(CO2_PUMP_PIN, LOW);
      sender->GetSerial()->println("Pump OFF");
    }
  }
  #endif
}

//Change the serial output formatting for plotting
void cmd_plotout(SerialCommands* sender) {
  char* pwm_str = sender->Next();
  int tmp = atoi(pwm_str);
  if (tmp==1) 
  {
    plottingOn = true;
  }
  else 
  {
    plottingOn = false;
  }
}

//==========================================================
// REGISTER TEXT COMMANDS (start with cmd_ and end with _)
//==========================================================
//Note: Commands are case sensitive
//See below for actual commands.
SerialCommand cmd_setrtc_("SETRTC", cmd_setrtc);
SerialCommand cmd_com_("com", cmd_com);
SerialCommand cmd_pumpon_("pumpon", cmd_pumpon);
SerialCommand cmd_plotout_("plotout", cmd_plotout);

void regAllCmds(void) {
  // Register all the serial commands
  serial_commands_.SetDefaultHandler(cmd_unrecognized);
  serial_commands_.AddCommand(&cmd_setrtc_);
  serial_commands_.AddCommand(&cmd_com_);
  serial_commands_.AddCommand(&cmd_pumpon_);
  serial_commands_.AddCommand(&cmd_plotout_);
}

#endif
//eof
