classdef DTENode < handle & matlab.mixin.Heterogeneous
%DTENODE: Abstract node inherited by all other subclass nodes
%Description:
%   Inherited by all other node types to allow multiple types of node to be contained within the
%   same heterogeneous array.
    
    methods (Sealed)
    % Sealed methods cannot be overwritten by subclasses
        
        function parsedString = render(obj,inputString,schemaStruct)
        %RENDER: Heterogeneous wrapper function for the internal render function of each node
        %Description:
        %   In order to call a method on members of a heterogeneous array the method must be sealed. This
        %   method is sealed allowing render to be called directly on a heterogeneous array, and is used to
        %   wrap the subclass specific internalRender method.
        %Input Args:
        %   inputString(string): The template string to be rendered
        %   schemaStruct(struct): The structure containing data to be injected into the template string
        %Output Args:
        %   parsedString(string): The parsed template string
            
            parsedString = obj.internalRender(inputString,schemaStruct);
            
        end
        
    end
    
    methods (Access = protected, Abstract)
    % Protected methods cannot be accessed externally from this class and its subclasses
        
        % Internal render class to be reimplemented by subclass nodes
        parsedString = internalRender(obj,inputString,schemaStruct);
  
    end
    
end

