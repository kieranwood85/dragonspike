classdef DTENodeConditional < DTENode
%DTENODECONDITIONAL: Conditional node representing the ?{expression<?>true<?>...}? tags
%Description:
%   Node representing ?{expression<?>true<?>...}? tags within a template string. When rendered will
%   evaluate as an if-elseif-else stack and inject the appropriate result into the template.
    
    properties
        children    %Child nodes
        startTag    %Index of start tag
        endTag      %Index of end tag
        startPos    %Start position of tag pair within template string
        endPos      %End position of tag pair within template string
    end
    
    methods
    % Publically accessible methods
       
        function obj = DTENodeConditional(inputString,tagIdx,startTag)
        %DTENODECONDITIONAL: Generates a conditional node
        %Description:
        %   Parses the input string and generates a conditional node to represent the ?{expression<?>true<?>...}? tags
        %Input Args:
        %   inputString(string): The template string to be rendered
        %   tagIdx(int[]): Array containing the starting character position of each tag in the template string
        %   startTag: Index of the start tag for this node within tagIdx
           
            % Loop through all tags
            i = startTag;
            firstTag = 1;
            while i <= length(tagIdx)
               
                % Read first 2 characters of tag
                tag = inputString(tagIdx(i):tagIdx(i)+1);
                
                % Handle all tag types with custom errors for unexpected tags
                switch tag
                    
                    % Throw error
                    case '${'
                        error(['Variable start tag ''${'' at character index ' num2str(tagIdx(i)) ' should not occur within a conditional expression']);
                        
                    % Throw error
                    case '#{'
                        error(['Iterator start tag ''#{'' at character index ' num2str(tagIdx(i)) ' should not occur within a conditional expression']);
                        
                    % Create the first conditional element
                    case '?{'
                        if firstTag
                            obj.children = DTENodeConditionalElement(inputString,tagIdx,startTag,firstTag);
                            i = obj.children(end).endTag-1;
                        else
                            error(['Conditional start tag ''?{'' at character index ' num2str(tagIdx(i)) ' should not occur within a conditional expression']);
                        end
                        
                    % Throw error
                    case '}$'
                        error(['Variable end tag ''}$'' at character index ' num2str(tagIdx(i)) ' has no matching start tag']);
                        
                    % Throw error
                    case '}#'
                        error(['Iterator end tag ''}#'' at character index ' num2str(tagIdx(i)) ' has no matching start tag']);
                        
                    % End of the conditional node has been found so construct and return 
                    case '}?'
                        if firstTag
                            error(['Conditional starting at ' num2str(tagIdx(startTag)) ' and ending at ' num2str(tagIdx(i)) ' has no delimiter tags']);
                        else
                            obj.startTag = startTag;
                            obj.endTag = i;
                            obj.startPos = tagIdx(startTag);
                            obj.endPos = tagIdx(i)+1;
                            % Loop through children and determine which are text and which are expressions
                            for j=1:1:length(obj.children)
                                if j==1
                                    obj.children(j).setToExpression(tagIdx);
                                elseif mod(j,2) && j ~= length(obj.children)
                                    obj.children(j).setToExpression(tagIdx);
                                end
                            end
                            return;
                        end
                        
                    % Throw error
                    case '<#'
                        error(['Iterator delimiter ''<#>'' at character index ' num2str(tagIdx(endTag)) ' should not occur within a conditional expression']);
                        
                    % Create a new conditional element
                    case '<?'
                        if firstTag
                            error(['This shouldn''nt even be possible!']);
                        else
                            obj.children = [obj.children DTENodeConditionalElement(inputString,tagIdx,i,firstTag)];
                            i = obj.children(end).endTag-1;
                        end
                        
                    % Throw error
                    otherwise
                        error(['Tag at character index ' num2str(tagIdx(i)) ' is of unknown type']);
                        
                end
                
                i=i+1;
                firstTag = 0;
                
            end
            
            % This node is unterminated and an error is thrown
            error(['Template contains an unterminated conditional start tag ''?{'' at character index ' num2str(tagIdx(startTag))]);
            
        end
        
    end
    
    methods (Access = protected)
    % Protected methods cannot be accessed externally from this class and its subclasses
        
        function parsedString = internalRender(obj,inputString,schemaStruct)
        %INTERNALRENDER: Renders the conditional node
        %Description:
        %   Evaluates the contents of the conditional node and renders the result to a string
        %Input Args:
        %   inputString(string): The template string to be rendered
        %   schemaStruct(struct): The structure containing data to be injected into the template string
        %Output Args:
        %   parsedString(string): The parsed string for this node
            
            % Initialise an empty string
            parsedString = [];
            % Initialise some housekeeping variables
            i = 1;
            startTagLen = 2;
            endTagLen = 3;
            
            % Loop through all child nodes
            while i <= length(obj.children)
                
                % Try to evaluate the current child node as an expression
                try
                    startingPos = obj.children(i).startPos+startTagLen;
                    endingPos = obj.children(i).endPos-endTagLen;
                    evalString = inputString(startingPos:endingPos);
                    result = DragonTE.evaluate(evalString,schemaStruct);
                catch
                    error(['Conditional expression starting at character position ' num2str(startingPos) ' and ending at ' num2str(endingPos) ' could not be evaluated']);
                end
                
                % Choose which nodes to parse based on the result of the previous evaluation
                if result
                    % Expression was true so parse next child
                    parsedString = obj.children(i+1).render(inputString,schemaStruct);
                    return;
                elseif i+1 == length(obj.children)
                    % Expression was false and there is no else child supplied so return an empty string
                    return;
                elseif i+2 == length(obj.children)
                    % Expression was false and there is an else child available 
                    parsedString = obj.children(i+2).render(inputString,schemaStruct);
                    return;
                else
                    % Expression was false and there is an elseif child available
                    i=i+2;
                end
                
                startTagLen = 3;
                
            end
            
        end
        
    end
    
end

