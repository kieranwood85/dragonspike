classdef DTENodeConditionalElement < DTENode
%DTENODECONDITIONALELEMENT: Conditional element node representing the <?>expression<?> tags
%Description:
%   Node representing <?>expression<?> tags within a conditional node. When rendered will evaluate the
%   expression and inject the result into the template.
    
    properties
        children                %Child nodes
        isExpression            %Is this node an expression or text to be parsed
        hasChildren = false;    %Does this node contain child nodes
        startTag                %Index of start tag
        startTagLen             %Length of start tag
        endTag                  %Index of end tag
        endTagLen               %Length of end tag
        startPos                %Start position of tag pair within template string
        endPos                  %End position of tag pair within template string
    end
    
    methods
    % Publically accessible methods
        
        function obj = DTENodeConditionalElement(inputString,tagIdx,startTag,firstElem)
        %DTENODECONDITIONALELEMENT: Generates a conditional element node
        %Description:
        %   Parses the input string and generates a conditional element node
        %Input Args:
        %   inputString(string): The template string to be rendered
        %   tagIdx(int[]): Array containing the starting character position of each tag in the template string
        %   startTag: Index of the start tag for this node within tagIdx
            
            % If this is the first conditional element in a conditional the start tag is only 2 chars long
            if firstElem
                obj.startTagLen = 2;
            else
                obj.startTagLen = 3;
            end
            
            % Loop through all tags
            i = startTag+1;
            while i <= length(tagIdx)
               
                % Read first 2 characters of tag
                tag = inputString(tagIdx(i):tagIdx(i)+1);
                
                % Handle all tag types with custom errors for unexpected tags
                switch tag
                    
                    % Found the start of a variable node. Construct the node and add to children.
                    case '${'
                            obj.children = [obj.children DTENodeVariable(inputString,tagIdx,i)];
                            i = obj.children(end).endTag;
                            obj.hasChildren = true;
                        
                    % Found the start of an iterator node. Construct the node and add to children.
                    case '#{'
                            obj.children = [obj.children DTENodeIterator(inputString,tagIdx,i)];
                            i = obj.children(end).endTag;
                            obj.hasChildren = true;
                            
                    % Found the start of a conditional node. Construct the node and add to children.
                    case '?{'
                            obj.children = [obj.children DTENodeConditional(inputString,tagIdx,i)];
                            i = obj.children(end).endTag;
                            obj.hasChildren = true;
                        
                    % Throw error
                    case '}$'
                        error(['Variable end tag ''}$'' at character index ' num2str(tagIdx(i)) ' has no matching start tag']);
                      
                    % Throw error
                    case '}#'
                        error(['Iterator end tag ''}#'' at character index ' num2str(tagIdx(i)) ' has no matching start tag']);
                    
                    % End of conditional element found so construct the node and return
                    case '}?'
                        obj.startTag = startTag;
                        obj.endTag = i;
                        obj.startPos = tagIdx(startTag);
                        obj.endPos = tagIdx(i)+1;
                        obj.endTagLen = 2;
                        return;
                        
                    % Throw error
                    case '<#'
                        error(['Iterator delimiter ''<#>'' at character index ' num2str(tagIdx(i)) ' does not fall within iterator tags']);
                        
                    % End of conditional element found so construct the node and return
                    case '<?'
                        obj.startTag = startTag;
                        obj.endTag = i;
                        obj.startPos = tagIdx(startTag);
                        obj.endPos = tagIdx(i)+2;
                        obj.endTagLen = 3;
                        return;
                        
                    % Throw error (should not be possible)
                    otherwise
                        error(['Tag at character index ' num2str(tagIdx(i)) ' is of unknown type']);
                        
                end
                
                i=i+1;
                
            end
            
            % This node is unterminated and an error is thrown
            error(['Template contains an unterminated conditional start tag ''?{'' at character index ' num2str(tagIdx(startTag))]);
            
        end
        
        function setToExpression(obj,tagIdx)
        %SETTOEXPRESSION: Sets this node to be an expression
        %Description:
        %   Sets this node to be an expression node. If this node contains children then it cannot also be
        %   an expression node and an error is thrown
            
            % Set to an expression node and thrown an error if required
            obj.isExpression = true;
            if obj.isExpression && obj.hasChildren
                error(['Conditional expression starting at character index ' num2str(tagIdx(obj.startTag)) ' contains other tags']);
            end
        end
        
    end
    
    methods (Access=protected)
    % Protected methods cannot be accessed externally from this class and its subclasses
        
        function parsedString = internalRender(obj,inputString,schemaStruct)
        %INTERNALRENDER: Renders the conditional element node
        %Description:
        %   Evaluates the contents of the conditional element node and renders the result to a string
        %Input Args:
        %   inputString(string): The template string to be rendered
        %   schemaStruct(struct): The structure containing data to be injected into the template string
        %Output Args:
        %   parsedString(string): The parsed string for this node
            
            % Initialise an empty string
            parsedString = [];
            % Get end position of last tag
            endingPos = obj.startPos+obj.startTagLen-1;
            
            % Loop through all child nodes
            for i=1:1:length(obj.children)
                % Find starting position of next node
                startingPos = obj.children(i).startPos;
                % Add text up to next child node and the rendered text of the next child node to the parsed string
                parsedString = [parsedString inputString(endingPos+1:startingPos-1) obj.children(i).render(inputString,schemaStruct)];
                % Find ending position of next node
                endingPos = obj.children(i).endPos;
            end
            
            % Add text from end of last node to the end of the conditional element node to the parsed string
            parsedString = [parsedString inputString(endingPos+1:obj.endPos-obj.endTagLen)];
            
        end
        
    end
    
end

