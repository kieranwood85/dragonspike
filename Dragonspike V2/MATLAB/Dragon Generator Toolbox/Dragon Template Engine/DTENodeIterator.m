classdef DTENodeIterator < DTENode
%DTENODEITERATOR: Iterator node representing the #{iter=indexes<#>text}# tags
%Description:
%   Node representing #{iter=indexes<#>text}# tags within a template string. When rendered will
%   evaluate the text  for each index in iter and inject the results into the template.
    
    properties
        children    %Child nodes
        startTag    %Index of start tag
        delimTag    %Index of delimiter tag
        endTag      %Index of end tag
        startPos    %Start position of tag pair within template string
        delimPos    %Position of delimiter within template string
        endPos      %End position of tag pair within template string
    end
    
    methods
    % Publically accessible methods
       
        function obj = DTENodeIterator(inputString,tagIdx,startTag)
        %DTENODEITERATOR: Generates an iterator node
        %Description:
        %   Parses the input string and generates an iterator node to represent the #{iter=indexes<#>text}# tags
        %Input Args:
        %   inputString(string): The template string to be rendered
        %   tagIdx(int[]): Array containing the starting character position of each tag in the template string
        %   startTag: Index of the start tag for this node within tagIdx
            
            % Loop through all tags
            i = startTag+1;
            firstTag = 1;
            while i <= length(tagIdx)
               
                % Read first 2 characters of tag
                tag = inputString(tagIdx(i):tagIdx(i)+1);
                
                % Handle all tag types with custom errors for unexpected tags
                switch tag
                    
                    % Throw error if before the delimiter, else create a new variable child node
                    case '${'
                        if firstTag
                            error(['Variable start tag ''${'' at character index ' num2str(tagIdx(i)) ' should not occur within an iterator index section']);
                        else
                            obj.children = [obj.children DTENodeVariable(inputString,tagIdx,i)];
                            i = obj.children(end).endTag;
                        end
                        
                    % Throw error if before the delimiter, else create a new iterator child node
                    case '#{'
                        if firstTag
                            error(['Iterator start tag ''#{'' at character index ' num2str(tagIdx(i)) ' should not occur within an iterator index section']);
                        else
                            obj.children = [obj.children DTENodeIterator(inputString,tagIdx,i)];
                            i = obj.children(end).endTag;
                        end
                        
                    % Throw error if before the delimiter, else create a new conditional child node
                    case '?{'
                        if firstTag
                            error(['Conditional start tag ''?{'' at character index ' num2str(tagIdx(i)) ' should not occur within an iterator index section']);
                        else
                            obj.children = [obj.children DTENodeConditional(inputString,tagIdx,i)];
                            i = obj.children(end).endTag;
                        end
                        
                    % Throw error
                    case '}$'
                        error(['Variable end tag ''}$'' at character index ' num2str(tagIdx(i)) ' has no matching start tag']);
                        
                    % Throw error if before the delimiter, else end of iterator node has been found so construct and return
                    case '}#'
                        if firstTag
                            error(['Iterator starting at ' num2str(tagIdx(startTag)) ' and ending at ' num2str(tagIdx(i)) ' has no delimiter tag']);
                        else
                            obj.startTag = startTag;
                            obj.endTag = i;
                            obj.startPos = tagIdx(startTag);
                            obj.endPos = tagIdx(i)+1;
                            return;
                        end
                        
                    % Throw error
                    case '}?'
                        error(['Conditional end tag ''}?'' at character index ' num2str(tagIdx(i)) ' has no matching start tag']);
                        
                    % If first tag this is the delimiter tag, else throw error
                    case '<#'
                        if firstTag
                           obj.delimTag = i;
                           obj.delimPos = tagIdx(i);
                        else
                           error(['Iterator delimiter <#> at character index' num2str(tagIdx(i)) ' is a duplicate']);
                        end
                        
                    % Throw error
                    case '<?'
                        error(['Conditional delimiter ''<?>'' at character index ' num2str(tagIdx(i)) ' does not fall within conditional tags']);
                        
                    % Throw error (should be impossible)
                    otherwise
                        error(['Tag at character index ' num2str(tagIdx(i)) ' is of unknown type']);
                        
                end
                
                i=i+1;
                firstTag = 0;
                
            end

            % This node is unterminated and an error is thrown
            error(['Template contains an unterminated iterator start tag ''#{'' at character index ' num2str(tagIdx(startTag))]);

        end
        
    end
    
    methods (Access = protected)
    % Protected methods cannot be accessed externally from this class and its subclasses
        
        function parsedString = internalRender(obj,inputString,schemaStruct)
        %INTERNALRENDER: Renders the iterator node
        %Description:
        %   Evaluates the contents of the iterator text section for each index in iter and renders the results to a string
        %Input Args:
        %   inputString(string): The template string to be rendered
        %   schemaStruct(struct): The structure containing data to be injected into the template string
        %Output Args:
        %   parsedString(string): The parsed string for this node
            
            % Initialise an empty string
            parsedString = [];
            % Get the string before the delimiter
            indexString = inputString(obj.startPos+2:obj.delimPos-1);
  
            % Try to split the section before the delimiter into an iterator variable name and set of indexes
            try
                indexString = strsplit(indexString,'=');
                indexVar = indexString{1};
                indexIdx = indexString{2};
            catch
                error(['Iterator indexes starting at character position ' num2str(obj.startPos) ' and ending at ' num2str(obj.delimPos) ' could not be evaluated']);
            end
            
            % If the iterator variable name conflicts with a field in the schema throw an error
            if isfield(schemaStruct,indexVar)
                error(['Iterator variable in indexes starting at character position ' num2str(obj.startPos) ' and ending at ' num2str(obj.delimPos) ' is already in use']);
            end
            
            % Try to evaluate the indexes and return an error if unsuccessful
            try
                indexes = DragonTE.evaluate(indexIdx,schemaStruct);
            catch
                error(['Iterator indexes starting at character position ' num2str(obj.startPos) ' and ending at ' num2str(obj.delimPos) ' could not be evaluated']);
            end
            
            % Loop through all of the indexes
            for i=1:1:length(indexes)
               
                % Set the starting position of the iterator text section
                endingPos = obj.delimPos+2;
                % Create the iterator variable within the schema and set its value for this loop
                schemaStruct.(indexVar) = indexes(i);
                
                % Loop through all child nodes
                for j=1:1:length(obj.children)
                    % Find starting position of next node
                    startingPos = obj.children(j).startPos;
                    % Add text up to next child node and the rendered text of the next child node to the parsed string
                    parsedString = [parsedString inputString(endingPos+1:startingPos-1) obj.children(j).render(inputString,schemaStruct)];
                    % Find ending position of next node
                    endingPos = obj.children(j).endPos;
                end
                
                % Add text from end of last node to the end of the iterator text section to the parsed string
                parsedString = [parsedString inputString(endingPos+1:obj.endPos-2)];
                
            end
            
        end
        
    end
    
end

