classdef DTENodeRoot < DTENode
%DTENODEROOT: Root node representing the entire template string
%Description:
%   This is the top level node of the node tree created when parsing a template string. It is used
%   to store all other nodes in a heirarchical structure.
    
    properties
        children    %Child nodes
        tagIdx      %Tag starting character indexes found in the template string
    end
    
    methods
    % Publically accessible methods
       
        function obj = DTENodeRoot(inputString)
        %DTENODEROOT: Generates a root node
        %Description:
        %   Parses the input string and generates a root node to represent the entire template
        %Input Args:
        %   inputString(string): The template string to be rendered
            
            % Find locations of all template engine tags and sotrt into character index order
            tagIdx = sort([strfind(inputString,'${') strfind(inputString,'#{') strfind(inputString,'?{') ...
                strfind(inputString,'}$') strfind(inputString,'}#') strfind(inputString,'}?') ...
                strfind(inputString,'<#>') strfind(inputString,'<?>')]);
            
            % Loop through all tags
            i = 1;
            while i <= length(tagIdx)
               
                % Read first 2 characters of tag
                tag = inputString(tagIdx(i):tagIdx(i)+1);
                
                % Handle all tag types with custom errors for unexpected tags
                switch tag
                    
                    % Found the start of a variable node. Construct the node and add to children.
                    case '${'
                        obj.children = [obj.children DTENodeVariable(inputString,tagIdx,i)];
                        i = obj.children(end).endTag;
                        
                    % Found the start of an iterator node. Construct the node and add to children.
                    case '#{'
                        obj.children = [obj.children DTENodeIterator(inputString,tagIdx,i)];
                        i = obj.children(end).endTag;
                        
                    % Found the start of a conditional node. Construct the node and add to children.
                    case '?{'
                        obj.children = [obj.children DTENodeConditional(inputString,tagIdx,i)];
                        i = obj.children(end).endTag;
                        
                    % Throw error
                    case '}$'
                        error(['Variable end tag ''}$'' at character index ' num2str(tagIdx(i)) ' has no matching start tag']);
                        
                    % Throw error
                    case '}#'
                        error(['Iterator end tag ''}#'' at character index ' num2str(tagIdx(i)) ' has no matching start tag']);
                        
                    % Throw error
                    case '}?'
                        error(['Conditional end tag ''}?'' at character index ' num2str(tagIdx(i)) ' has no matching start tag']);
                        
                    % Throw error
                    case '<#'
                        error(['Iterator delimiter ''<#>'' at character index ' num2str(tagIdx(i)) ' does not match format']);
                        
                    % Throw error
                    case '<?'
                        error(['Conditional delimiter ''<?>'' at character index ' num2str(tagIdx(i)) ' does not match format']);
                        
                    % Throw error (should not be possible)
                    otherwise
                        error(['Tag at character index ' num2str(tagIdx(i)) ' is of unknown type']);
                        
                end
                
                i=i+1;
                
            end
            
            obj.tagIdx = tagIdx;
            
        end
        
    end
    
    methods (Access = protected)
    % Protected methods cannot be accessed externally from this class and its subclasses
        
        function parsedString = internalRender(obj,inputString,schemaStruct)
        %INTERNALRENDER: Renders the root node
        %Description:
        %   Evaluates the contents of the root node and renders the result to a string
        %Input Args:
        %   inputString(string): The template string to be rendered
        %   schemaStruct(struct): The structure containing data to be injected into the template string
        %Output Args:
        %   parsedString(string): The parsed string for this node
            
            % Initialise an empty string
            parsedString = [];
            % Set end position of last tag to 0
            endPos = 0;
            
            % Loop through all child nodes
            for i=1:1:length(obj.children)
                % Find starting position of next node
                startPos = obj.children(i).startPos;
                % Add text up to next child node and the rendered text of the next child node to the parsed string
                parsedString = [parsedString inputString(endPos+1:startPos-1) obj.children(i).render(inputString,schemaStruct)];
                % Find ending position of next node
                endPos = obj.children(i).endPos;
            end
            
            % Add text from end of last node to the end of the template string to the parsed string
            parsedString = [parsedString inputString(endPos+1:end)];
            
        end
        
    end
    
end

