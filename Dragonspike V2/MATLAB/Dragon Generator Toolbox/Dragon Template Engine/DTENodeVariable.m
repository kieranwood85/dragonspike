classdef DTENodeVariable < DTENode
%DTENODEVARIABLE: Variable node representing the ${expression}$ tags
%Description:
%   Node representing ${expression}$ tags within a template string. When rendered will evaluate the
%   expression and inject the result into the template.
    
    properties
        startTag    %Index of start tag
        endTag      %Index of end tag
        startPos    %Start position of tag pair within template string
        endPos      %End position of tag pair within template string
    end
    
    methods
    % Publically accessible methods
       
        function obj = DTENodeVariable(inputString,tagIdx,startTag)
        %DTENODEVARIABLE: Generates a variable node
        %Description:
        %   Parses the input string and generates a variable node to represent the ${expression}$ tags
        %Input Args:
        %   inputString(string): The template string to be rendered
        %   tagIdx(int[]): Array containing the starting character position of each tag in the template string
        %   startTag: Index of the start tag for this node within tagIdx

            % Get the next tag, which should be the end tag
            endTag = startTag + 1;
            
            % If no tags follow then this node is unterminated and an error is thrown
            if endTag > length(tagIdx)
                error(['Template contains an unterminated variable start tag ''${'' at character index ' num2str(tagIdx(startTag))]);
            end
               
            % Read first 2 characters of next tag
            tag = inputString(tagIdx(endTag):tagIdx(endTag)+1);

            % Handle all tag types with custom errors for unexpected tags
            switch tag
                
                % Throw error
                case '${'
                    error(['Variable start tag ''${'' at character index ' num2str(tagIdx(endTag)) ' should not follow a variable start tag']);

                % Throw error
                case '#{'
                    error(['Iterator start tag ''#{'' at character index ' num2str(tagIdx(endTag)) ' should not follow a variable start tag']);

                % Throw error
                case '?{'
                    error(['Conditional start tag ''?{'' at character index ' num2str(tagIdx(endTag)) ' should not follow a variable start tag']);

                % End of the variable node has been found so construct and return
                case '}$'
                    obj.startTag = startTag;
                    obj.endTag = endTag;
                    obj.startPos = tagIdx(startTag);
                    obj.endPos = tagIdx(endTag)+1;

                % Throw error
                case '}#'
                    error(['Iterator end tag ''}#'' at character index ' num2str(tagIdx(endTag)) ' should not follow a variable start tag']);

                % Throw error
                case '}?'
                    error(['Conditional end tag ''}?'' at character index ' num2str(tagIdx(endTag)) ' should not follow a variable start tag']);

                % Throw error
                case '<#'
                    error(['Iterator delimiter ''<#>'' at character index ' num2str(tagIdx(endTag)) ' should not follow a variable start tag']);

                % Throw error
                case '<?'
                    error(['Conditional delimiter ''<?>'' at character index ' num2str(tagIdx(endTag)) ' should not follow a variable start tag']);

                % Throw error (should be impossible)
                otherwise
                    error(['Tag at character index ' num2str(tagIdx) ' is of unknown type']);

            end
            
        end
        
    end
    
    methods (Access = protected)
    % Protected methods cannot be accessed externally from this class and its subclasses
        
        function parsedString = internalRender(obj,inputString,schemaStruct)
        %INTERNALRENDER: Renders the variable node
        %Description:
        %   Evaluates the contents of the variable node and renders the result to a string
        %Input Args:
        %   inputString(string): The template string to be rendered
        %   schemaStruct(struct): The structure containing data to be injected into the template string
        %Output Args:
        %   parsedString(string): The parsed string for this node
            
            % Extract the expression string for this node from the template string
            varString = inputString(obj.startPos+2:obj.endPos-2);
            
            % Try to evaluate the expression and return an error if unsuccessful
            try
                evalOut = DragonTE.evaluate(varString,schemaStruct);
            catch
                error(['Variable tag starting at character position ' num2str(obj.startPos) ' and ending at ' num2str(obj.endPos) ' could not be evaluated']);
            end
            
            % Convert evaluation to a string and return
            parsedString = num2str(evalOut);
            
        end
        
    end
    
end

