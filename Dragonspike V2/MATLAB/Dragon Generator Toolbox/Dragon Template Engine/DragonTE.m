classdef DragonTE
%DRAGONTE: The Dragon Template Engine
%Description:
%   Injects data from a supplied schema data structure into a template string
%   
%   Variable Tag - ${expression}$
%   Evaluates expression between tags and injects the result into the template
%   
%   Iterator Tag - #{iter=indexes<#>text}#
%   Processes text for each index in the defined iter variable
%   
%   If Tag - ?{expression<?>true}?
%   If expression then process true
%   
%   If-Else Tag - ?{expression<?>true<?>false}?
%   If expression then process true else process false
%   
%   If-ElseIf-Else Tag - ?{expression<?>true<expression<?>true<?>false}?
%   Also supports elseif functionality
    
    methods (Static)
        
        function [parsedString,root] = parse(inputString,schemaStruct)
        %PARSE: Processes a single template string
        %Description:
        %   Parses the supplied template string and injects data into tags as supplied within the schema
        %   struct. Returns the parsed string and the node tree if requested.
        %Input Args:
        %   inputString(string): The template string to be parsed
        %   schemaStruct(struct): The structure containing data to be injected into the template
        %Output Args:
        %   parsedString(string): The parsed template string
        %   root(DTENodeRoot): The node tree constructed when parsing the template string
            
            % Sanitise input string
            inputString = strrep(inputString,'%','%%');
        
            % Create root node (will recursively create all nodes that follow)
            root = DTENodeRoot(inputString);
            
            % Render all nodes into parsed string
            parsedString = root.render(inputString,schemaStruct);
        
        end
        
        function DTEOut = evaluate(DTEInput,DTEStruct)
        %EVALUATE: Evaluates the input string using data contained in the supplied struct
        %Description:
        %   Evaluates the input string as a MATLAB expression. Exposes the top level fields of the supplied
        %   struct as variables within this context. Allows the struct to act as a psuedo-workspace. Note
        %   that the struct should contain no top level fields beginning with the DTE keyword.
        %Input Args:
        %   DTEInput(string): The string to be evaluated as a MATLAB expression
        %   DTEStruct(struct): Structure that will provide the workspace variables for evaluation
        %Output Args:
        %   DTEOut(any): The result of the evaluation of the input string. Can be of any data type.
            
            % Find the top level field names in the structure
            DTEFields = fieldnames(DTEStruct);
            
            % Loop through the top level fields and expose them as workspace variables within this function
            for DTEIter=1:1:length(DTEFields)
                feval(@() assignin('caller',DTEFields{DTEIter},DTEStruct.(DTEFields{DTEIter})));
            end
            
            % Evaluate the input string using the new workspace variables
            DTEOut = eval(DTEInput);
            
        end
        
    end
    
end

