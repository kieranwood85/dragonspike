classdef DragonGen
% DRAGONGEN: Code generator for C++ implementation of DragonOS
% Description:
%   This program is used to generate the DragonOS library in the C++ language. Configuration of OS settings and
%   definitions of data objects are provided in the form of an XML document. These are then used to automatic code an
%   implementation of the library. The OS includes autocoded serialisable data objects, a file system manager, and a
%   radio manager.
    
    properties
    end
    
    methods(Static, Access=public)
    
        function generate(xmlName)
        % GENERATE: Generates a C++ implementation of the DragonOS library
        % Description:
        %   Generates a c++ implementation of the DragonOS library from a provided XML document. 
        % Arguments:
        %   xmlName(string): Name of the XML configuration file to parse within the input folder
        
            %% Input argument processing
            switch nargin
                %No XML name provided so use default
                case 0
                    xmlName = 'Input/Default.xml';
                %XML name provided with or without .xml extension
                case 1
                    xmlName = ['Input/' xmlName];
                    if ~strcmp(xmlName(end-3:end),'.xml')
                        xmlName = [xmlName '.xml'];
                    end
                %Unsupported number of input arguments
                otherwise
                    error('ERROR: DragonGen.generate(): Greater than one input argument not expected');   
            end
            
            disp(['Building from ' xmlName]);
            
            %% Initial clean-up
            fclose('all');
            
            %% Dragon code modules missing
            if ~exist('Dragon Template Engine','dir'), error('ERROR: DragonGen.generate(): Dragon Template Engine module is missing!'); end
            if ~exist('Dragon XML Parser','dir'), error('ERROR: DragonGen.generate(): Dragon XML Parser module is missing!'); end
            
            %% Generate any missing folders
            if ~exist('Input','dir'), mkdir('Input'); end
            if ~exist('Output','dir'), mkdir('Output'); end
            if ~exist('Templates','dir'), mkdir('Templates'); end
            if ~exist('Masters','dir'), mkdir('Masters'); end
            
            %% Update MATLAB paths
            addpath('Dragon Template Engine');
            addpath('Dragon XML Parser');
            addpath('Input');
            addpath('Output');
            addpath('Masters');
            addpath('Templates');
           
            %% Check for existing generated implementation and request permission to overwrite
            % Check if the DragonOS directory exists
            if exist('Output/DragonOS','dir')
                % User input loop asking for confirmation
                while true
                    % Display user prompt and capture input
                    userInput = input('\nA DragonOS implementation already exists in the output folder which will be overwritten.\nPlease confirm to continue code generation (Y/N): ','s');
                    % User confirmed code generation so delete the old implementation
                    if strcmp(userInput,'Y') || strcmp(userInput,'y')
                        fprintf('\nConfirmed. Generating DragonOS implementation.\n\n');
                        rmdir('Output/DragonOS','s');
                        break;
                    % User aborted code generation
                    elseif strcmp(userInput,'N') || strcmp(userInput,'n')
                        fprintf('\nAborted. Halting code generation.\n\n');
                        return;
                    % User gave an invalid response to request answer again
                    else
                        fprintf('\nInvalid response.\n\n');
                    end
                end
            else
                fprintf('\nGenerating DragonOS implementation.\n\n');
            end
            
            masterTimer = tic();
            
            %% Create directories for the DragonOS implementation
            DragonGen.generateDirectories();
            
            %% Parse the XML configuration file and generate the structure
            % Check whether the file exists
            if ~exist(xmlName,'file')
                if strcmp(xmlName,'Input/Default')
                    error('ERROR: DragonGen.generate(): Default XML config file is missing!'); 
                else
                    error('ERROR: DragonGen.generate(): Specified XML file does not exist');
                end
            end
            % Create the DragonXML object by parsing XML
            xmlData = DragonXML(xmlName);
            xmlData = xmlData.findAll('DragonOS');
            if length(xmlData) ~= 1, error('ERROR: DragonGen.generate(): XML file does not contain a unique DragonOS definition'); end
            
            %% Load top level DragonOS settings
            DragonOS_Settings = DragonGen.parseSettings(xmlData);
            
            %% Load and create array of defined DragonOS objects
            DragonOS_Objects = DragonGen.parseObjects(xmlData);
            
            %% Generate DragonOS derived object classes
            DragonGen.generateDragonObjects(DragonOS_Objects,DragonOS_Settings);
            
            %% Generate DragonRadio class
            DragonGen.generateDragonRadio(DragonOS_Settings);
            
            %% Generate DragonStore class
            DragonGen.generateDragonStore(DragonOS_Settings);
            
            %% Generate DragonOS class
            DragonGen.generateDragonOS(DragonOS_Objects,DragonOS_Settings);
            
            %% Copy Masters
            DragonGen.copyMasters();
            
            %% Completion message
            fprintf(['\nGeneration of DragonOS Successful!\nTotal time elapsed: ' num2str(toc(masterTimer)) ' seconds\n\n']);
            fprintf('-----------------END-----------------\n\n');
            
            
            
        end
        
    end
    
    methods(Static, Access=private)
        
        function generateDirectories()
        % GENERATEDIRECTORIES: Generates the directory file structure of the DragonOS library
        % Description:
        %   Generates folder structure of the DragonOS library in a form that is supported by the Arduino IDE.
           
            % Top level directories
            mkdir('Output/DragonOS/help');
            mkdir('Output/DragonOS/src');
            
            % Dragon objects
            mkdir('Output/DragonOS/src/DragonObjects');
            
            % Dragon file system
            mkdir('Output/DragonOS/src/DragonStore');
            
            % Dragon radio
            mkdir('Output/DragonOS/src/DragonRadio');
            
            % Dragon utils
            mkdir('Output/DragonOS/src/DragonUtils');
            
        end
        
        function DragonOS_Settings = parseSettings(xmlData)
        % PARSESETTINGS: Parse a DragonXML object for DragonOS settings
        % Description:
        %   Finds the settings field for the DragonOS implementation and creates a settings structure
        % Arguments:
        %   xmlData(DragonXML): A DragonXML searchable representation of an XML file
        
            % Find the settings node of the XML document
            settingsData = xmlData.findAll('settings');
            if length(settingsData) ~= 1, error('ERROR: DragonGen.parseSettings(): XML file does not contain a unique DragonOS settings definition'); end

            % Load all object settings
            object_type_size = settingsData.findAll('object_type_size');
            if length(object_type_size) ~= 1, error('ERROR: DragonGen.parseSettings(): Must contain a unique definition of ''object_type_size'' setting'); end
            DragonOS_Settings.object_type_size = object_type_size.attributes.value;
            
            object_type_sysid = settingsData.findAll('object_type_sysid');
            if length(object_type_sysid) ~= 1, error('ERROR: DragonGen.parseSettings(): Must contain a unique definition of ''object_type_sysid'' setting'); end
            DragonOS_Settings.object_type_sysid = object_type_sysid.attributes.value;

            object_type_id = settingsData.findAll('object_type_id');
            if length(object_type_id) ~= 1, error('ERROR: DragonGen.parseSettings(): Must contain a unique definition of ''object_type_id'' setting'); end
            DragonOS_Settings.object_type_id = object_type_id.attributes.value;
            
            % Load all radio settings
            radio_type_target = settingsData.findAll('radio_type_target');
            if length(radio_type_target) ~= 1, error('ERROR: DragonGen.parseSettings(): Must contain a unique definition of ''radio_type_target'' setting'); end
            DragonOS_Settings.radio_type_target = radio_type_target.attributes.value;

        end
        
        function DragonOS_Objects = parseObjects(xmlData)
        % PARSEOBJECTS: Parse a DragonXML object for DragonOS objects
        % Description:
        %   Finds all object fields for the DragonOS implementation and creates an array of structures
        % Arguments:
        %   xmlData(DragonXML): A DragonXML searchable representation of an XML file
            
            objectData = xmlData.find('objects');
            if isempty(objectData), error('ERROR: DragonGen.parseObjects(): XML file does not contain any DragonOS object definitions'); end
            objectData = objectData.children;
            
            for i=1:1:length(objectData)
               DragonOS_Objects(i) = DragonGen.parseObject(objectData(i)); 
            end

        end
        
        function DragonOS_Object = parseObject(objectData)
        % PARSEOBJECT: Parse a single DragonOS objects
        % Description:
        %   Parses a single DragonXML object definition and creates a structure of the appropriate form
        % Arguments:
        %   objectData(DragonXML): A DragonXML searchable representation of an XML file
        
            % Get the object name
            if isfield(objectData.attributes,'name')
                DragonOS_Object.name = objectData.attributes.name;
            else
                error('ERROR: DragonGen.parseObject(): An XML object definition does not have a ''name'' attribute');
            end
            
            % Get the object ID
            if isfield(objectData.attributes,'id')
                DragonOS_Object.id = objectData.attributes.id;
            else
                error('ERROR: DragonGen.parseObject(): An XML object definition does not have a ''id'' attribute');
            end
        
            % Get description if available, otherwise set to default
            if ~isempty(objectData.find('description'))
                DragonOS_Object.desc = strrep(objectData.find('description').text,'\n','');
            else
                DragonOS_Object.desc = 'No description available';
            end
            
            % Get all object fields
            fieldData = objectData.findAll('field');
            if ~isempty(fieldData)
                for i=1:1:length(fieldData)
                   DragonOS_Object.fields(i) = DragonGen.parseField(fieldData(i));
                end
            else
                error('ERROR: DragonGen.parseObject(): An XML object definition does not have any fields');
            end
        
        end
        
        function DragonOS_Field = parseField(fieldData)
        % PARSEFIELD: Parse a single DragonOS object field
        % Description:
        %   Parses a single DragonXML field definition and creates a structure of the appropriate form
        % Arguments:
        %   fieldData(DragonXML): A DragonXML searchable representation of an XML file
        
            % Get the field name
            if isfield(fieldData.attributes,'name')
                DragonOS_Field.name = fieldData.attributes.name;
            else
                error('ERROR: DragonGen.parseField(): An XML field definition does not have a ''name'' attribute');
            end
            
            % Get description if available, otherwise set to default
            if fieldData.hasText
                DragonOS_Field.desc = fieldData.text;
            else
                DragonOS_Field.desc = 'No description available';
            end
            
            % Get the type of the field
            if isfield(fieldData.attributes,'type')
                fieldType = strsplit(fieldData.attributes.type,'[');
            else
                error('ERROR: DragonGen.parseField(): An XML field definition does not have a ''type'' attribute');
            end
            
            % Process array type declarations
            if size(fieldType,2) > 1
                arraySize = strsplit(fieldType{2},']');
                DragonOS_Field.arraySize = arraySize{1};
                DragonOS_Field.isArray = 1;
            else
                DragonOS_Field.arraySize = '1';
                DragonOS_Field.isArray = 0;
            end
            
            % Finalise field type
            DragonOS_Field.type = fieldType{1};
            
        end
        
        function generateDragonRadio(DragonOS_Settings)
        % GENERATEDRAGONRADIO: Generates the base DragonOS DragonRadio class from a template and schema
        % Description:
        %   Loads the radio template file and writes a c++ header file for the class by using the DragonTE template
        %   engine with supplied schema data
        % Arguments:
        %   DragonOS_Settings(DragonXML): Settings data loaded from the XML file
            
            % User notification
            fprintf('\nGenerating ''DragonRadio'' Header File:\n');
            
            % Load the template data as a string
            templateFile = fopen('Templates/DragonRadio.tpl');
            template = fread(templateFile,[1 inf],'*char');
            fclose(templateFile);
            
            % Generate the DragonObject header
            timer = tic;
            DragonOS_Schema.settings = DragonOS_Settings;
            objectFile = fopen('Output/DragonOS/src/DragonRadio/DragonRadio.h','w');
            fprintf(objectFile,DragonTE.parse(template,DragonOS_Schema));
            fclose(objectFile);
            fprintf(['Generated in ' num2str(toc(timer)) ' seconds\n']);
            
        end
        
        function generateDragonStore(DragonOS_Settings)
        % GENERATEDRAGONSTORE: Generates the base DragonOS DragonStore class from a template and schema
        % Description:
        %   Loads the store template file and writes a c++ header file for the class by using the DragonTE template
        %   engine with supplied schema data
        % Arguments:
        %   DragonOS_Settings(DragonXML): Settings data loaded from the XML file
            
            % User notification
            fprintf('\nGenerating ''DragonStore'' Header File:\n');
            
            % Load the template data as a string
            templateFile = fopen('Templates/DragonStore.tpl');
            template = fread(templateFile,[1 inf],'*char');
            fclose(templateFile);
            
            % Generate the DragonObject header
            timer = tic;
            DragonOS_Schema.settings = DragonOS_Settings;
            objectFile = fopen('Output/DragonOS/src/DragonStore/DragonStore.h','w');
            fprintf(objectFile,DragonTE.parse(template,DragonOS_Schema));
            fclose(objectFile);
            fprintf(['Generated in ' num2str(toc(timer)) ' seconds\n']);
            
        end
        
        function generateDragonObjects(DragonOS_Objects,DragonOS_Settings)
        % GENERATEDRAGONOBJECTS: Generate DragonOS object classes from a template and schema
        % Description:
        %   Loads the object template file and writes a C++ header file per object class by using the DragonTE template
        %   engine with supplied schema data
        % Arguments:
        %   DragonOS_Objects(DragonXML): Object data loaded from the XML file
        %   DragonOS_Settings(DragonXML): Settings data loaded from the XML file 
            
            % User notification
            fprintf('\nGenerating DragonOS Object Files:\n');
        
            % Load the template data as a string
            templateFile = fopen('Templates/DragonObject.tpl');
            template = fread(templateFile,[1 inf],'*char');
            fclose(templateFile);
            
            % Generate an object header file per DragonOS object definition
            for i=1:length(DragonOS_Objects)
                timer = tic;
                DragonOS_Schema.settings = DragonOS_Settings;
                DragonOS_Schema.object = DragonOS_Objects(i);
                objectFile = fopen(['Output/DragonOS/src/DragonObjects/DragonObject_' DragonOS_Schema.object.name '.h'],'w');
                fprintf(objectFile,DragonTE.parse(template,DragonOS_Schema));
                fclose(objectFile);
                fprintf(['Generated ''' DragonOS_Schema.object.name ''' object file in ' num2str(toc(timer)) ' seconds\n']);
            end  
        
        end
        
        function generateDragonOS(DragonOS_Objects,DragonOS_Settings)
        % GENERATEDRAGONOS: Generate the top level DragonOS class
        % Description:
        %   Loads the DragonOS template file and writes the c++ header file for the top level of the implementation
        %   using the DragonTE template engine with supplied schema data
        %   DragonOS_Objects(DragonXML): Object data loaded from the XML file
        %   DragonOS_Settings(DragonXML): Settings data loaded from the XML file 
        
            % User notification
            fprintf('\nGenerating ''DragonOS'' Header File:\n');
            
            % Load the template data as a string
            templateFile = fopen('Templates/DragonOS.tpl');
            template = fread(templateFile,[1 inf],'*char');
            fclose(templateFile);
            
            % Generate the DragonObject header
            timer = tic;
            DragonOS_Schema.settings = DragonOS_Settings;
            DragonOS_Schema.objects = DragonOS_Objects;
            objectFile = fopen('Output/DragonOS/src/DragonOS.h','w');
            fprintf(objectFile,DragonTE.parse(template,DragonOS_Schema));
            fclose(objectFile);
            fprintf(['Generated in ' num2str(toc(timer)) ' seconds\n']);
            
        end
        
        function copyMasters()
        % COPYMASTERS: Copy master files that do not require a template
        
            copyfile('Masters/library.properties','Output/DragonOS/library.properties');
            
        end  
        
    end
    
end

