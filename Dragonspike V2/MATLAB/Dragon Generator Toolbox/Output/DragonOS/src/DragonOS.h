/*
    DragonOS class autocoded in MATLAB

    Template Update History:
        V1.0 - Samuel Pownall - Initial Release
    
    Name: DragonOS
    Description: Operating system for the Dragon Egg research platform
    
    Autocoded using MATLAB
*/

#ifndef DragonOS_h

#define DragonOS_h
#include "Arduino.h"

class DragonOS {

  private:

	static uint8_t& systemID() {
		static uint8_t value = 0;
		return value;
	}

  public:

    static uint8_t getSystemID() {
      return systemID();
    }
	
	static uint8_t setSystemID(uint8_t value) {
      systemID() = value;
    }
  
};

#include "DragonObjects/DragonObject_SimpleExample.h"
#include "DragonObjects/DragonObject_ArrayExample.h"
#include "DragonObjects/DragonObject_LORACMD.h"
#include "DragonObjects/DragonObject_ACK_LORACMD.h"
#include "DragonObjects/DragonObject_MAX_ACCEL.h"
#include "DragonObjects/DragonObject_RTC_TEMP.h"
#include "DragonObjects/DragonObject_LAST_CO2.h"
#include "DragonObjects/DragonObject_SET_CO2_POLL.h"

#include "DragonRadio/DragonRadio.h"
#include "DragonStore/DragonStore.h"

#endif