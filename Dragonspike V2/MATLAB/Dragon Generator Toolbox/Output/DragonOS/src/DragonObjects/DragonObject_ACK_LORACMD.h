/*
    DragonOS object class autocoded in MATLAB

    Template Update History:
        V1.0 - Samuel Pownall - Initial Release

    Name: DragonObject_ACK_LORACMD
    ID: 4
    Description: Acknowledgements of LORA command
    
    Fields:

        Name: DEVID
        Type: uint8_t
        Description: The sending device ID

        Name: CMDID
        Type: uint8_t
        Description: The command code

    Autocoded using MATLAB
*/

#ifndef DragonObject_ACK_LORACMD_h

#define DragonObject_ACK_LORACMD_h
#include "Arduino.h"
#include "DragonOS.h"
//#include <ArduinoSTL.h>

class DragonObject_ACK_LORACMD {

    public:

        //Struct containing field data
        struct Fields { 
            uint8_t DEVID = 0; 
            uint8_t CMDID = 0; 
        };

    protected:
        
        // Static header variables
        static const uint8_t headerSize = sizeof(uint8_t) + sizeof(uint8_t) + sizeof(uint8_t);
        static const uint8_t objectID = 4;

        // Non-Static header variables
        uint8_t systemID;
        uint8_t dataSize;

        // Payload
        Fields fields;

        // Housekeeping
        bool defined = false;

    public:       

        // Check if the object is fully defined
        bool isDefined() {
            return this->defined;
        }

        // Getter for constant expression headerSize
        constexpr static uint8_t getHeaderSize() {
            return headerSize;
        }

        // Getter for constant expression objectID
        constexpr static uint8_t getObjectID() {
            return objectID;
        }

        // Getter for systemID associated with this object
        uint8_t getSystemID() {
            if (this->defined) return systemID; 
            else return 0;
        }

        // Getter for dataSize of the fields structure of this object
        uint8_t getDataSize() {
            if (this->defined) return dataSize; 
            else return 0;
        }

        // Getter for the object totalSize
        uint32_t getTotalSize() {
            if (this->defined) return dataSize + headerSize;
            else return 0;
        }

        // Getter for a const pointer to the internal fields structure
        const Fields* getFields() const {
            return &fields;
        }

        // Default constructor
        DragonObject_ACK_LORACMD() {

        }

        // Construct a ACK_LORACMD object from individual fields
        DragonObject_ACK_LORACMD(uint8_t DEVID,uint8_t CMDID){
            
            uint32_t _dataSize = sizeof(uint8_t) + sizeof(uint8_t);

            if (_dataSize <= pow(2,8*sizeof(uint8_t))) {
            
                this->dataSize = _dataSize;
                this->systemID = DragonOS::getSystemID();

                // Dynamically allocate array storage (if applicable)

                // Copy fields into relevant storage
				memcpy(&this->fields.DEVID,&DEVID,sizeof(uint8_t));
				memcpy(&this->fields.CMDID,&CMDID,sizeof(uint8_t));

                this->defined = true;
            } 

        }

        // Construct a ACK_LORACMD object by deserialising from a vector with a header of the format [id,origin,size]
        DragonObject_ACK_LORACMD(const std::vector<uint8_t> &buffer) {

            // Working variables
            uint32_t index = 0;
            uint8_t checkID;
        
            // Deserialise header and check object id and size validity
            if (buffer.size() < this->headerSize) return;
            memcpy(&checkID,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            if (this->objectID != checkID) return;
            memcpy(&this->systemID,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            memcpy(&this->dataSize,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            if (this->dataSize != buffer.size() - this->headerSize) return; 

            // Deserialise the DEVID field
            if (index+sizeof(uint8_t) > buffer.size()) return; 
            memcpy(&this->fields.DEVID,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);

            // Deserialise the CMDID field
            if (index+sizeof(uint8_t) > buffer.size()) return; 
            memcpy(&this->fields.CMDID,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);

            // Final size check
            if (index != buffer.size() || this->dataSize != index - this->headerSize) return;

            this->defined = true;

        }

        // Serialise object fields into a byte buffer with a header of the format [id,origin,size]
        bool serialise(std::vector<uint8_t> &buffer) {
            
            if (!this->defined) return false;

            // Get the constant expression object ID into temporary storage
            uint8_t _objectID = this->objectID;
                
            buffer.reserve(this->getTotalSize());
                
            //Serialise the object header
            buffer.insert(buffer.end(),(uint8_t*)&_objectID,(uint8_t*)&_objectID+sizeof(uint8_t));
            buffer.insert(buffer.end(),(uint8_t*)&this->systemID,(uint8_t*)&this->systemID+sizeof(uint8_t));
            buffer.insert(buffer.end(),(uint8_t*)&this->dataSize,(uint8_t*)&this->dataSize+sizeof(uint8_t));

            //Serialise object fields and place into buffer
			buffer.insert(buffer.end(),(uint8_t*)&this->fields.DEVID,(uint8_t*)&this->fields.DEVID+sizeof(uint8_t));
			buffer.insert(buffer.end(),(uint8_t*)&this->fields.CMDID,(uint8_t*)&this->fields.CMDID+sizeof(uint8_t));

            return true;
        }

        // Copy constructor
        DragonObject_ACK_LORACMD(const DragonObject_ACK_LORACMD &object) {

            // Copy header data
            this->systemID = object.systemID;
            this->dataSize = object.dataSize;
            this->defined = object.defined;

            // Dynamically allocate array storage (if applicable)

            // Copy field data
			memcpy(&this->fields.DEVID,&object.fields.DEVID,sizeof(uint8_t));
			memcpy(&this->fields.CMDID,&object.fields.CMDID,sizeof(uint8_t));
          
        }

        // Copy assignment operator
        DragonObject_ACK_LORACMD& operator=(const DragonObject_ACK_LORACMD &object) {

            if (this == &object) return *this;

            // Copy header data
            this->systemID = object.systemID;
            this->dataSize = object.dataSize;
            this->defined = object.defined;

            // Dynamically allocate array storage (if applicable)

            // Copy field data
			memcpy(&this->fields.DEVID,&object.fields.DEVID,sizeof(uint8_t));
			memcpy(&this->fields.CMDID,&object.fields.CMDID,sizeof(uint8_t));
         
            return *this;

        }

        // Destructor memory management
        ~DragonObject_ACK_LORACMD() {


        }

};

#endif