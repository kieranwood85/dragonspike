/*
    DragonOS object class autocoded in MATLAB

    Template Update History:
        V1.0 - Samuel Pownall - Initial Release

    Name: DragonObject_ArrayExample
    ID: 2
    Description: Example of an object with a set of variable sized arrays
    
    Fields:

        Name: sizeA
        Type: uint8_t
        Description: Size of array A

        Name: sizeB
        Type: uint8_t
        Description: Size of array B

        Name: arrayA
        Type: uint16_t
        Description: Array A

        Name: arrayB
        Type: uint32_t
        Description: Array B

    Autocoded using MATLAB
*/

#ifndef DragonObject_ArrayExample_h

#define DragonObject_ArrayExample_h
#include "Arduino.h"
#include "DragonOS.h"
//#include <ArduinoSTL.h>

class DragonObject_ArrayExample {

    public:

        //Struct containing field data
        struct Fields { 
            uint8_t sizeA = 0; 
            uint8_t sizeB = 0; 
            uint16_t *arrayA = 0; 
            uint32_t *arrayB = 0; 
        };

    protected:
        
        // Static header variables
        static const uint8_t headerSize = sizeof(uint8_t) + sizeof(uint8_t) + sizeof(uint8_t);
        static const uint8_t objectID = 2;

        // Non-Static header variables
        uint8_t systemID;
        uint8_t dataSize;

        // Payload
        Fields fields;

        // Housekeeping
        bool defined = false;

    public:       

        // Check if the object is fully defined
        bool isDefined() {
            return this->defined;
        }

        // Getter for constant expression headerSize
        constexpr static uint8_t getHeaderSize() {
            return headerSize;
        }

        // Getter for constant expression objectID
        constexpr static uint8_t getObjectID() {
            return objectID;
        }

        // Getter for systemID associated with this object
        uint8_t getSystemID() {
            if (this->defined) return systemID; 
            else return 0;
        }

        // Getter for dataSize of the fields structure of this object
        uint8_t getDataSize() {
            if (this->defined) return dataSize; 
            else return 0;
        }

        // Getter for the object totalSize
        uint32_t getTotalSize() {
            if (this->defined) return dataSize + headerSize;
            else return 0;
        }

        // Getter for a const pointer to the internal fields structure
        const Fields* getFields() const {
            return &fields;
        }

        // Default constructor
        DragonObject_ArrayExample() {

        }

        // Construct a ArrayExample object from individual fields
        DragonObject_ArrayExample(uint8_t sizeA,uint8_t sizeB,uint16_t *arrayA,uint32_t *arrayB){
            
            uint32_t _dataSize = sizeof(uint8_t) + sizeof(uint8_t) + sizeA*sizeof(uint16_t) + sizeB*sizeof(uint32_t);

            if (_dataSize <= pow(2,8*sizeof(uint8_t))) {
            
                this->dataSize = _dataSize;
                this->systemID = DragonOS::getSystemID();

                // Dynamically allocate array storage (if applicable)
				this->fields.arrayA = new uint16_t[sizeA];
				this->fields.arrayB = new uint32_t[sizeB];

                // Copy fields into relevant storage
				memcpy(&this->fields.sizeA,&sizeA,sizeof(uint8_t));
				memcpy(&this->fields.sizeB,&sizeB,sizeof(uint8_t));
				memcpy(this->fields.arrayA,arrayA,this->fields.sizeA*sizeof(uint16_t));
				memcpy(this->fields.arrayB,arrayB,this->fields.sizeB*sizeof(uint32_t));

                this->defined = true;
            } 

        }

        // Construct a ArrayExample object by deserialising from a vector with a header of the format [id,origin,size]
        DragonObject_ArrayExample(const std::vector<uint8_t> &buffer) {

            // Working variables
            uint32_t index = 0;
            uint8_t checkID;
        
            // Deserialise header and check object id and size validity
            if (buffer.size() < this->headerSize) return;
            memcpy(&checkID,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            if (this->objectID != checkID) return;
            memcpy(&this->systemID,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            memcpy(&this->dataSize,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            if (this->dataSize != buffer.size() - this->headerSize) return; 

            // Deserialise the sizeA field
            if (index+sizeof(uint8_t) > buffer.size()) return; 
            memcpy(&this->fields.sizeA,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);

            // Deserialise the sizeB field
            if (index+sizeof(uint8_t) > buffer.size()) return; 
            memcpy(&this->fields.sizeB,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);

            // Deserialise the arrayA field
            if (index+this->fields.sizeA*sizeof(uint16_t) > buffer.size()) return; 
            this->fields.arrayA = new uint16_t[this->fields.sizeA];
            memcpy(this->fields.arrayA,&buffer[index],this->fields.sizeA*sizeof(uint16_t)); index += this->fields.sizeA*sizeof(uint16_t);

            // Deserialise the arrayB field
            if (index+this->fields.sizeB*sizeof(uint32_t) > buffer.size()) return; 
            this->fields.arrayB = new uint32_t[this->fields.sizeB];
            memcpy(this->fields.arrayB,&buffer[index],this->fields.sizeB*sizeof(uint32_t)); index += this->fields.sizeB*sizeof(uint32_t);

            // Final size check
            if (index != buffer.size() || this->dataSize != index - this->headerSize) return;

            this->defined = true;

        }

        // Serialise object fields into a byte buffer with a header of the format [id,origin,size]
        bool serialise(std::vector<uint8_t> &buffer) {
            
            if (!this->defined) return false;

            // Get the constant expression object ID into temporary storage
            uint8_t _objectID = this->objectID;
                
            buffer.reserve(this->getTotalSize());
                
            //Serialise the object header
            buffer.insert(buffer.end(),(uint8_t*)&_objectID,(uint8_t*)&_objectID+sizeof(uint8_t));
            buffer.insert(buffer.end(),(uint8_t*)&this->systemID,(uint8_t*)&this->systemID+sizeof(uint8_t));
            buffer.insert(buffer.end(),(uint8_t*)&this->dataSize,(uint8_t*)&this->dataSize+sizeof(uint8_t));

            //Serialise object fields and place into buffer
			buffer.insert(buffer.end(),(uint8_t*)&this->fields.sizeA,(uint8_t*)&this->fields.sizeA+sizeof(uint8_t));
			buffer.insert(buffer.end(),(uint8_t*)&this->fields.sizeB,(uint8_t*)&this->fields.sizeB+sizeof(uint8_t));
			buffer.insert(buffer.end(),(uint8_t*)this->fields.arrayA,(uint8_t*)this->fields.arrayA+this->fields.sizeA*sizeof(uint16_t));
			buffer.insert(buffer.end(),(uint8_t*)this->fields.arrayB,(uint8_t*)this->fields.arrayB+this->fields.sizeB*sizeof(uint32_t));

            return true;
        }

        // Copy constructor
        DragonObject_ArrayExample(const DragonObject_ArrayExample &object) {

            // Copy header data
            this->systemID = object.systemID;
            this->dataSize = object.dataSize;
            this->defined = object.defined;

            // Dynamically allocate array storage (if applicable)
			this->fields.arrayA = new uint16_t[object.fields.sizeA];
			this->fields.arrayB = new uint32_t[object.fields.sizeB];

            // Copy field data
			memcpy(&this->fields.sizeA,&object.fields.sizeA,sizeof(uint8_t));
			memcpy(&this->fields.sizeB,&object.fields.sizeB,sizeof(uint8_t));
			memcpy(this->fields.arrayA,object.fields.arrayA,this->fields.sizeA*sizeof(uint16_t));
			memcpy(this->fields.arrayB,object.fields.arrayB,this->fields.sizeB*sizeof(uint32_t));
          
        }

        // Copy assignment operator
        DragonObject_ArrayExample& operator=(const DragonObject_ArrayExample &object) {

            if (this == &object) return *this;

            // Copy header data
            this->systemID = object.systemID;
            this->dataSize = object.dataSize;
            this->defined = object.defined;

            // Dynamically allocate array storage (if applicable)
			this->fields.arrayA = new uint16_t[object.fields.sizeA];
			this->fields.arrayB = new uint32_t[object.fields.sizeB];

            // Copy field data
			memcpy(&this->fields.sizeA,&object.fields.sizeA,sizeof(uint8_t));
			memcpy(&this->fields.sizeB,&object.fields.sizeB,sizeof(uint8_t));
			memcpy(this->fields.arrayA,object.fields.arrayA,this->fields.sizeA*sizeof(uint16_t));
			memcpy(this->fields.arrayB,object.fields.arrayB,this->fields.sizeB*sizeof(uint32_t));
         
            return *this;

        }

        // Destructor memory management
        ~DragonObject_ArrayExample() {

			delete [] fields.arrayA;
			delete [] fields.arrayB;

        }

};

#endif