/*
    DragonOS object class autocoded in MATLAB

    Template Update History:
        V1.0 - Samuel Pownall - Initial Release

    Name: DragonObject_RTC_TEMP
    ID: 6
    Description: The time and temp from the RTC chip
    
    Fields:

        Name: DEVID
        Type: uint8_t
        Description: The sending device ID

        Name: UnixT
        Type: uint32_t
        Description: Unix Time Stamp

        Name: Temp
        Type: float
        Description: RTC chip temp

    Autocoded using MATLAB
*/

#ifndef DragonObject_RTC_TEMP_h

#define DragonObject_RTC_TEMP_h
#include "Arduino.h"
#include "DragonOS.h"
//#include <ArduinoSTL.h>

class DragonObject_RTC_TEMP {

    public:

        //Struct containing field data
        struct Fields { 
            uint8_t DEVID = 0; 
            uint32_t UnixT = 0; 
            float Temp = 0; 
        };

    protected:
        
        // Static header variables
        static const uint8_t headerSize = sizeof(uint8_t) + sizeof(uint8_t) + sizeof(uint8_t);
        static const uint8_t objectID = 6;

        // Non-Static header variables
        uint8_t systemID;
        uint8_t dataSize;

        // Payload
        Fields fields;

        // Housekeeping
        bool defined = false;

    public:       

        // Check if the object is fully defined
        bool isDefined() {
            return this->defined;
        }

        // Getter for constant expression headerSize
        constexpr static uint8_t getHeaderSize() {
            return headerSize;
        }

        // Getter for constant expression objectID
        constexpr static uint8_t getObjectID() {
            return objectID;
        }

        // Getter for systemID associated with this object
        uint8_t getSystemID() {
            if (this->defined) return systemID; 
            else return 0;
        }

        // Getter for dataSize of the fields structure of this object
        uint8_t getDataSize() {
            if (this->defined) return dataSize; 
            else return 0;
        }

        // Getter for the object totalSize
        uint32_t getTotalSize() {
            if (this->defined) return dataSize + headerSize;
            else return 0;
        }

        // Getter for a const pointer to the internal fields structure
        const Fields* getFields() const {
            return &fields;
        }

        // Default constructor
        DragonObject_RTC_TEMP() {

        }

        // Construct a RTC_TEMP object from individual fields
        DragonObject_RTC_TEMP(uint8_t DEVID,uint32_t UnixT,float Temp){
            
            uint32_t _dataSize = sizeof(uint8_t) + sizeof(uint32_t) + sizeof(float);

            if (_dataSize <= pow(2,8*sizeof(uint8_t))) {
            
                this->dataSize = _dataSize;
                this->systemID = DragonOS::getSystemID();

                // Dynamically allocate array storage (if applicable)

                // Copy fields into relevant storage
				memcpy(&this->fields.DEVID,&DEVID,sizeof(uint8_t));
				memcpy(&this->fields.UnixT,&UnixT,sizeof(uint32_t));
				memcpy(&this->fields.Temp,&Temp,sizeof(float));

                this->defined = true;
            } 

        }

        // Construct a RTC_TEMP object by deserialising from a vector with a header of the format [id,origin,size]
        DragonObject_RTC_TEMP(const std::vector<uint8_t> &buffer) {

            // Working variables
            uint32_t index = 0;
            uint8_t checkID;
        
            // Deserialise header and check object id and size validity
            if (buffer.size() < this->headerSize) return;
            memcpy(&checkID,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            if (this->objectID != checkID) return;
            memcpy(&this->systemID,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            memcpy(&this->dataSize,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            if (this->dataSize != buffer.size() - this->headerSize) return; 

            // Deserialise the DEVID field
            if (index+sizeof(uint8_t) > buffer.size()) return; 
            memcpy(&this->fields.DEVID,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);

            // Deserialise the UnixT field
            if (index+sizeof(uint32_t) > buffer.size()) return; 
            memcpy(&this->fields.UnixT,&buffer[index],sizeof(uint32_t)); index += sizeof(uint32_t);

            // Deserialise the Temp field
            if (index+sizeof(float) > buffer.size()) return; 
            memcpy(&this->fields.Temp,&buffer[index],sizeof(float)); index += sizeof(float);

            // Final size check
            if (index != buffer.size() || this->dataSize != index - this->headerSize) return;

            this->defined = true;

        }

        // Serialise object fields into a byte buffer with a header of the format [id,origin,size]
        bool serialise(std::vector<uint8_t> &buffer) {
            
            if (!this->defined) return false;

            // Get the constant expression object ID into temporary storage
            uint8_t _objectID = this->objectID;
                
            buffer.reserve(this->getTotalSize());
                
            //Serialise the object header
            buffer.insert(buffer.end(),(uint8_t*)&_objectID,(uint8_t*)&_objectID+sizeof(uint8_t));
            buffer.insert(buffer.end(),(uint8_t*)&this->systemID,(uint8_t*)&this->systemID+sizeof(uint8_t));
            buffer.insert(buffer.end(),(uint8_t*)&this->dataSize,(uint8_t*)&this->dataSize+sizeof(uint8_t));

            //Serialise object fields and place into buffer
			buffer.insert(buffer.end(),(uint8_t*)&this->fields.DEVID,(uint8_t*)&this->fields.DEVID+sizeof(uint8_t));
			buffer.insert(buffer.end(),(uint8_t*)&this->fields.UnixT,(uint8_t*)&this->fields.UnixT+sizeof(uint32_t));
			buffer.insert(buffer.end(),(uint8_t*)&this->fields.Temp,(uint8_t*)&this->fields.Temp+sizeof(float));

            return true;
        }

        // Copy constructor
        DragonObject_RTC_TEMP(const DragonObject_RTC_TEMP &object) {

            // Copy header data
            this->systemID = object.systemID;
            this->dataSize = object.dataSize;
            this->defined = object.defined;

            // Dynamically allocate array storage (if applicable)

            // Copy field data
			memcpy(&this->fields.DEVID,&object.fields.DEVID,sizeof(uint8_t));
			memcpy(&this->fields.UnixT,&object.fields.UnixT,sizeof(uint32_t));
			memcpy(&this->fields.Temp,&object.fields.Temp,sizeof(float));
          
        }

        // Copy assignment operator
        DragonObject_RTC_TEMP& operator=(const DragonObject_RTC_TEMP &object) {

            if (this == &object) return *this;

            // Copy header data
            this->systemID = object.systemID;
            this->dataSize = object.dataSize;
            this->defined = object.defined;

            // Dynamically allocate array storage (if applicable)

            // Copy field data
			memcpy(&this->fields.DEVID,&object.fields.DEVID,sizeof(uint8_t));
			memcpy(&this->fields.UnixT,&object.fields.UnixT,sizeof(uint32_t));
			memcpy(&this->fields.Temp,&object.fields.Temp,sizeof(float));
         
            return *this;

        }

        // Destructor memory management
        ~DragonObject_RTC_TEMP() {


        }

};

#endif