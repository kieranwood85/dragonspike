/*
    DragonOS object class autocoded in MATLAB

    Template Update History:
        V1.0 - Samuel Pownall - Initial Release

    Name: DragonObject_SimpleExample
    ID: 1
    Description: Simple example object containing a number of fields of different types
    
    Fields:

        Name: fieldA
        Type: uint8_t
        Description: Example of uint8_t field

        Name: fieldB
        Type: uint16_t
        Description: Example of uint16_t field

        Name: fieldC
        Type: uint32_t
        Description: Example of uint32_t field

        Name: fieldD
        Type: float
        Description: Example of float field

    Autocoded using MATLAB
*/

#ifndef DragonObject_SimpleExample_h

#define DragonObject_SimpleExample_h
#include "Arduino.h"
#include "DragonOS.h"
//#include <ArduinoSTL.h>

class DragonObject_SimpleExample {

    public:

        //Struct containing field data
        struct Fields { 
            uint8_t fieldA = 0; 
            uint16_t fieldB = 0; 
            uint32_t fieldC = 0; 
            float fieldD = 0; 
        };

    protected:
        
        // Static header variables
        static const uint8_t headerSize = sizeof(uint8_t) + sizeof(uint8_t) + sizeof(uint8_t);
        static const uint8_t objectID = 1;

        // Non-Static header variables
        uint8_t systemID;
        uint8_t dataSize;

        // Payload
        Fields fields;

        // Housekeeping
        bool defined = false;

    public:       

        // Check if the object is fully defined
        bool isDefined() {
            return this->defined;
        }

        // Getter for constant expression headerSize
        constexpr static uint8_t getHeaderSize() {
            return headerSize;
        }

        // Getter for constant expression objectID
        constexpr static uint8_t getObjectID() {
            return objectID;
        }

        // Getter for systemID associated with this object
        uint8_t getSystemID() {
            if (this->defined) return systemID; 
            else return 0;
        }

        // Getter for dataSize of the fields structure of this object
        uint8_t getDataSize() {
            if (this->defined) return dataSize; 
            else return 0;
        }

        // Getter for the object totalSize
        uint32_t getTotalSize() {
            if (this->defined) return dataSize + headerSize;
            else return 0;
        }

        // Getter for a const pointer to the internal fields structure
        const Fields* getFields() const {
            return &fields;
        }

        // Default constructor
        DragonObject_SimpleExample() {

        }

        // Construct a SimpleExample object from individual fields
        DragonObject_SimpleExample(uint8_t fieldA,uint16_t fieldB,uint32_t fieldC,float fieldD){
            
            uint32_t _dataSize = sizeof(uint8_t) + sizeof(uint16_t) + sizeof(uint32_t) + sizeof(float);

            if (_dataSize <= pow(2,8*sizeof(uint8_t))) {
            
                this->dataSize = _dataSize;
                this->systemID = DragonOS::getSystemID();

                // Dynamically allocate array storage (if applicable)

                // Copy fields into relevant storage
				memcpy(&this->fields.fieldA,&fieldA,sizeof(uint8_t));
				memcpy(&this->fields.fieldB,&fieldB,sizeof(uint16_t));
				memcpy(&this->fields.fieldC,&fieldC,sizeof(uint32_t));
				memcpy(&this->fields.fieldD,&fieldD,sizeof(float));

                this->defined = true;
            } 

        }

        // Construct a SimpleExample object by deserialising from a vector with a header of the format [id,origin,size]
        DragonObject_SimpleExample(const std::vector<uint8_t> &buffer) {

            // Working variables
            uint32_t index = 0;
            uint8_t checkID;
        
            // Deserialise header and check object id and size validity
            if (buffer.size() < this->headerSize) return;
            memcpy(&checkID,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            if (this->objectID != checkID) return;
            memcpy(&this->systemID,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            memcpy(&this->dataSize,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            if (this->dataSize != buffer.size() - this->headerSize) return; 

            // Deserialise the fieldA field
            if (index+sizeof(uint8_t) > buffer.size()) return; 
            memcpy(&this->fields.fieldA,&buffer[index],sizeof(uint8_t)); index += sizeof(uint8_t);

            // Deserialise the fieldB field
            if (index+sizeof(uint16_t) > buffer.size()) return; 
            memcpy(&this->fields.fieldB,&buffer[index],sizeof(uint16_t)); index += sizeof(uint16_t);

            // Deserialise the fieldC field
            if (index+sizeof(uint32_t) > buffer.size()) return; 
            memcpy(&this->fields.fieldC,&buffer[index],sizeof(uint32_t)); index += sizeof(uint32_t);

            // Deserialise the fieldD field
            if (index+sizeof(float) > buffer.size()) return; 
            memcpy(&this->fields.fieldD,&buffer[index],sizeof(float)); index += sizeof(float);

            // Final size check
            if (index != buffer.size() || this->dataSize != index - this->headerSize) return;

            this->defined = true;

        }

        // Serialise object fields into a byte buffer with a header of the format [id,origin,size]
        bool serialise(std::vector<uint8_t> &buffer) {
            
            if (!this->defined) return false;

            // Get the constant expression object ID into temporary storage
            uint8_t _objectID = this->objectID;
                
            buffer.reserve(this->getTotalSize());
                
            //Serialise the object header
            buffer.insert(buffer.end(),(uint8_t*)&_objectID,(uint8_t*)&_objectID+sizeof(uint8_t));
            buffer.insert(buffer.end(),(uint8_t*)&this->systemID,(uint8_t*)&this->systemID+sizeof(uint8_t));
            buffer.insert(buffer.end(),(uint8_t*)&this->dataSize,(uint8_t*)&this->dataSize+sizeof(uint8_t));

            //Serialise object fields and place into buffer
			buffer.insert(buffer.end(),(uint8_t*)&this->fields.fieldA,(uint8_t*)&this->fields.fieldA+sizeof(uint8_t));
			buffer.insert(buffer.end(),(uint8_t*)&this->fields.fieldB,(uint8_t*)&this->fields.fieldB+sizeof(uint16_t));
			buffer.insert(buffer.end(),(uint8_t*)&this->fields.fieldC,(uint8_t*)&this->fields.fieldC+sizeof(uint32_t));
			buffer.insert(buffer.end(),(uint8_t*)&this->fields.fieldD,(uint8_t*)&this->fields.fieldD+sizeof(float));

            return true;
        }

        // Copy constructor
        DragonObject_SimpleExample(const DragonObject_SimpleExample &object) {

            // Copy header data
            this->systemID = object.systemID;
            this->dataSize = object.dataSize;
            this->defined = object.defined;

            // Dynamically allocate array storage (if applicable)

            // Copy field data
			memcpy(&this->fields.fieldA,&object.fields.fieldA,sizeof(uint8_t));
			memcpy(&this->fields.fieldB,&object.fields.fieldB,sizeof(uint16_t));
			memcpy(&this->fields.fieldC,&object.fields.fieldC,sizeof(uint32_t));
			memcpy(&this->fields.fieldD,&object.fields.fieldD,sizeof(float));
          
        }

        // Copy assignment operator
        DragonObject_SimpleExample& operator=(const DragonObject_SimpleExample &object) {

            if (this == &object) return *this;

            // Copy header data
            this->systemID = object.systemID;
            this->dataSize = object.dataSize;
            this->defined = object.defined;

            // Dynamically allocate array storage (if applicable)

            // Copy field data
			memcpy(&this->fields.fieldA,&object.fields.fieldA,sizeof(uint8_t));
			memcpy(&this->fields.fieldB,&object.fields.fieldB,sizeof(uint16_t));
			memcpy(&this->fields.fieldC,&object.fields.fieldC,sizeof(uint32_t));
			memcpy(&this->fields.fieldD,&object.fields.fieldD,sizeof(float));
         
            return *this;

        }

        // Destructor memory management
        ~DragonObject_SimpleExample() {


        }

};

#endif