/*
    DragonOS radio manager class autocoded in MATLAB

    Template Update History:
        V1.0 - Samuel Pownall - Initial Release
    
    Name: DragonRadio
    Description: Radio manager capable of transmitting and receiving data in DragonObject format
    
    Autocoded using MATLAB
*/

#ifndef DragonRadio_h

#define DragonRadio_h
#include "Arduino.h"
#include "DragonOS.h"

//#include <ArduinoSTL.h>
#include <list>
#include <iterator>

class DragonRadio {

    private:

        static const uint8_t headerSize = sizeof(uint8_t) + sizeof(uint8_t);
        static const uint8_t start = 123;

        RHDatagram *manager = 0;

        // Addresses
        std::vector<uint8_t> secondaryAddresses;

        // Received object cache
        std::list<std::vector<uint8_t>> objects;
        std::list<uint8_t> targetIDs;
        std::list<uint8_t> objectIDs;
        uint8_t cacheSize;

        // Check whether this system is a target of the radio packet
        bool isTarget(uint8_t targetID) {

            if (DragonOS::getSystemID() == targetID) return true;
            if (this->secondaryAddresses.empty()) return false;
            
            for (std::vector<uint8_t>::iterator it = this->secondaryAddresses.begin(); it != this->secondaryAddresses.end(); ++it) {
                if (*it == targetID) return true;
            }    

            return false;

        }

    public:
        
        // Construct a DragonRadio object
        DragonRadio(RHDatagram *manager, uint8_t cacheSize) {

            this->manager = manager;
            this->cacheSize = cacheSize;

        }

        // Add a secondary address
        void addSecondaryAddress(uint8_t address) {
            this->secondaryAddresses.push_back(address);
        }

        // Check if an object is available
        uint8_t available() {
            
            if (!this->objectIDs.empty()) {
                return this->objectIDs.back();
            } else {
                return 0;
            }

        }

        // Delete oldest object in cache
        void next() {

            if (!objects.empty()) {
                objects.pop_back();
                objectIDs.pop_back();
                targetIDs.pop_back();
            }

        }

        // Send a DragonObject to the specified address
        template <class Object>
        bool send(Object &object,uint8_t targetID) {

            if (!object.isDefined()) return false;

            std::vector<uint8_t> buffer;
            buffer.reserve(this->headerSize+object.getTotalSize());
            
            uint8_t start = this->start;
            buffer.insert(buffer.end(),(uint8_t*)&start,(uint8_t*)&start+sizeof(uint8_t));
            buffer.insert(buffer.end(),(uint8_t*)&targetID,(uint8_t*)&targetID+sizeof(uint8_t));

            object.serialise(buffer);

            manager->sendto(&buffer[0],buffer.size(),255);

            return true;

        }

        // Receive a DragonObject of the appropriate type
        template <class Object>
        bool recv(Object &object) {
            
            std::vector<uint8_t> buffer;

            if (!this->objects.empty() && this->objectIDs.back() == Object::getObjectID()) {
                buffer = this->objects.back();
                next();
            }

            object = Object(buffer);

            if (object.isDefined()) return true;
            else return false;

        }

        // Check the radio driver for new data and attempt to parse a new packet
        void update() {

            uint8_t len = 251;
            uint8_t buf[len];

            uint8_t start;
            uint8_t targetID;
            uint8_t objectID;

            if (!this->manager->available()) {
                return;
            }

            if (manager->recvfrom(buf,&len)) {

                if (len < this->headerSize + 1) return;

                uint32_t index = 0;
                memcpy(&start,&buf[index],sizeof(uint8_t)); index += sizeof(uint8_t);
                memcpy(&targetID,&buf[index],sizeof(uint8_t)); index += sizeof(uint8_t);

                if (!isTarget(targetID)) return;

                std::vector<uint8_t> object(&buf[index],&buf[len]);
                memcpy(&objectID,&buf[index],sizeof(uint8_t));

                if (this->objects.size() == this->cacheSize) {
                    next();
                }

                this->targetIDs.push_front(targetID);
                this->objectIDs.push_front(objectID);
                this->objects.push_front(object);

            }

        }

        ~DragonRadio() {

            delete manager;

        }

};

#endif