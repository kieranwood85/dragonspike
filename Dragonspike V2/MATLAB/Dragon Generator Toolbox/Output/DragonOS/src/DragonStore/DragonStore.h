/*
    DragonOS storage manager class autocoded in MATLAB

    Template Update History:
        V1.0 - Samuel Pownall - Initial Release
        V1.1 - Kieran Wood - Added comments
    
    Name: DragonStore
    Description: Storage manager capable of saving and loading data in DragonObject format
    
    Autocoded using MATLAB
*/

#ifndef DragonStore_h

#define DragonStore_h
#include "Arduino.h"
#include "DragonOS.h"

#include "math.h"

//#include <ArduinoSTL.h>
#include <SdFat.h>
#include <list>
#include <iterator>

class DragonStore {

    private:

        static const uint8_t headerSize = sizeof(uint8_t);
        static const uint8_t start = 123;

		//By creating a DragonStore object, the normal underlying microcontroller SdFat object 
		//is also created allowing file access, etc..
        SdFat sd;
        SPISettings settings;
        uint8_t csPin;

        bool begun = false;

        uint32_t registerObject(uint32_t fileIndex) {
			//After data has been written to the binary file store, the file index location 
			//is parsed into a storage ID.
			
			//Open, or create an index file and move to the end.
            File file = sd.open("INDEX.dat",O_WRONLY | O_CREAT | O_APPEND);
            file.seekEnd();

			//If the current position in the file is not a multiple of 4 
			//bytes then something is wrong.
            uint32_t position = file.curPosition();
            if (position % 4 != 0) {
                file.close();
                return 0;
            }
            
			//Convert the 32 bit file index into 4 bytes and write to the index file at the end.
            uint8_t buf[4];
            memcpy(buf,(uint8_t*)&fileIndex,sizeof(uint32_t));
            if (file.write(buf,sizeof(uint32_t)) != sizeof(uint32_t)) {
                file.close();
                return 0;
            }

			//Return the storage index (a counter starting at 1)
            file.close();
            return floor(position/4) + 1;

        }

        bool lookupObject(uint32_t storeID, uint32_t &fileIndex) {

            if (storeID == 0) return false;

            File file = sd.open("INDEX.dat",O_RDONLY);
            file.seekSet(sizeof(uint32_t)*(storeID-1));

            if (file.curPosition() != sizeof(uint32_t)*(storeID-1)) {
                file.close();
                return 0;
            }

            if (file.read(&fileIndex,sizeof(uint32_t)) != sizeof(uint32_t)) {
                file.close();
                return 0;
            }

            file.close();
            return true;

        }

    public:
        
        // Construct a DragonStore object
        DragonStore(uint8_t csPin, uint32_t freqMHz) {
            
            this->settings = SPISettings(1000000*freqMHz,MSBFIRST,SPI_MODE0);
            this->csPin = csPin;

        }

        // Begin storage system
        bool begin() {
            
			//Begin an instance of the SD card storage
            this->begun = sd.begin(this->csPin,this->settings);
            if (!sd.exists("DragonOS")) sd.mkdir("DragonOS");
            if (!sd.exists("DragonOS/Objects")) sd.mkdir("DragonOS/Objects");
            sd.chdir();
            return this->begun;

        }

        // Save a DragonObject
        template <class Object>
        uint32_t save(Object &object) {

			//Ensure the file system has been initialised and the object to be saved is valid.
            if (!this->begun) return 0;
            if (!object.isDefined()) return 0;

			//Move to root?
            sd.chdir();

			//Make a buffer of bytes to hold the object to be stored plus its header.
            std::vector<uint8_t> buffer;
            buffer.reserve(this->headerSize+object.getTotalSize());
            
            uint8_t start = this->start;
            buffer.insert(buffer.end(),(uint8_t*)&start,(uint8_t*)&start+sizeof(uint8_t));

			//Convert the object data into a byte array and copy into the buffer.
            object.serialise(buffer);

			//Create a subdir based upon the incomming object ID (see xml definitions for IDs)
            char folderPath[30];
            snprintf(folderPath,sizeof(folderPath),"DragonOS/Objects/%08x",Object::getObjectID());
            if (!sd.exists(folderPath)) sd.mkdir(folderPath);
            sd.chdir(folderPath);

			//Open (or create) a file to hold raw data. If it exists, move to the end.
            File file = sd.open("DATA.dat",O_WRONLY | O_CREAT | O_APPEND);
            file.seekEnd();

			//Write the buffer to the end of the file. If ok, convert the file index into 
			//a storage counter via the registerObject function.
            uint32_t fileIndex = file.curPosition();
            if (file.write(&buffer[0],buffer.size()) != buffer.size()) {
                file.close();
                sd.chdir();
                return 0;
            } else {
                file.close();
                uint32_t storeID = registerObject(fileIndex);
                sd.chdir();
                return storeID;
            }

        }

        // Load a DragonObject
        template <class Object>
        bool load(Object &object, uint32_t storeID) {
			//Check everything is initilised
            if (!this->begun) return false;

			//Move to root
            sd.chdir();

			//Move to the object specific directory. Create if it doesn't exist.
            char folderPath[30];
            snprintf(folderPath,sizeof(folderPath),"DragonOS/Objects/%08x",Object::getObjectID());
            if (!sd.exists(folderPath)) sd.mkdir(folderPath);
            sd.chdir(folderPath);

			//Convert the required store ID into a file system index via the lookupObject() function
            uint32_t fileIndex;
            if (!lookupObject(storeID,fileIndex)) return false;

			//Move to the required position in the storgae file.
            File file = sd.open("DATA.dat",O_RDONLY);
            if (!file.seekSet(fileIndex)) {file.close(); return false;}
            
			//Copy the required number of bytes (object size plus header) into a buffer.
            uint8_t bufSize = this->headerSize + Object::getHeaderSize();
            uint8_t headerBuf[bufSize];
            if (file.read(headerBuf,bufSize) != bufSize) {file.close(); return false;}

            uint8_t start;
            uint8_t objectID;
            uint8_t systemID;
            uint8_t dataSize;

            uint32_t index = 0;
            memcpy((uint8_t*)&start,&headerBuf[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            memcpy((uint8_t*)&objectID,&headerBuf[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            memcpy((uint8_t*)&systemID,&headerBuf[index],sizeof(uint8_t)); index += sizeof(uint8_t);
            memcpy((uint8_t*)&dataSize,&headerBuf[index],sizeof(uint8_t)); index += sizeof(uint8_t);

            if (objectID != Object::getObjectID()) {file.close(); return false;}

            uint8_t dataBuf[dataSize];
            if (file.read(dataBuf,dataSize) != dataSize) {file.close(); return false;}

            std::vector<uint8_t> buffer;

            buffer.insert(buffer.end(),(uint8_t*)&objectID,(uint8_t*)&objectID+sizeof(uint8_t));
            buffer.insert(buffer.end(),(uint8_t*)&systemID,(uint8_t*)&systemID+sizeof(uint8_t));
            buffer.insert(buffer.end(),(uint8_t*)&dataSize,(uint8_t*)&dataSize+sizeof(uint8_t));
            buffer.insert(buffer.end(), dataBuf, dataBuf+dataSize);

            object = Object(buffer);

            if (object.isDefined()) return true;
            else return false;

        }

        ~DragonStore() {

        }

};

#endif