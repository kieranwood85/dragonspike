/*
    DragonOS class autocoded in MATLAB

    Template Update History:
        V1.0 - Samuel Pownall - Initial Release
    
    Name: DragonOS
    Description: Operating system for the Dragon Egg research platform
    
    Autocoded using MATLAB
*/

#ifndef DragonOS_h

#define DragonOS_h
#include "Arduino.h"

class DragonOS {

  private:

	static ${settings.object_type_sysid}$& systemID() {
		static ${settings.object_type_sysid}$ value = 0;
		return value;
	}

  public:

    static ${settings.object_type_sysid}$ getSystemID() {
      return systemID();
    }
	
	static ${settings.object_type_sysid}$ setSystemID(${settings.object_type_sysid}$ value) {
      systemID() = value;
    }
  
};

#{i=1:length(objects)<#>#include "DragonObjects/DragonObject_${objects(i).name}$.h"\n}#
#include "DragonRadio/DragonRadio.h"
#include "DragonStore/DragonStore.h"

#endif