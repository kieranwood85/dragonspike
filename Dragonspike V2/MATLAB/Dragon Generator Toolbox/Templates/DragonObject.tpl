/*
    DragonOS object class autocoded in MATLAB

    Template Update History:
        V1.0 - Samuel Pownall - Initial Release

    Name: DragonObject_${object.name}$
    ID: ${object.id}$
    Description: ${object.desc}$
    
    Fields:
#{i=1:length(object.fields)<#>
        Name: ${object.fields(i).name}$
        Type: ${object.fields(i).type}$
        Description: ${object.fields(i).desc}$
}#
    Autocoded using MATLAB
*/

#ifndef DragonObject_${object.name}$_h

#define DragonObject_${object.name}$_h
#include "Arduino.h"
#include "DragonOS.h"
//#include <ArduinoSTL.h>

class DragonObject_${object.name}$ {

    public:

        //Struct containing field data
        struct Fields { #{i=1:length(object.fields)<#>
            ${object.fields(i).type}$ ?{object.fields(i).isArray<?>*}?${object.fields(i).name}$ = 0; }#
        };

    protected:
        
        // Static header variables
        static const uint8_t headerSize = sizeof(${settings.object_type_id}$) + sizeof(${settings.object_type_sysid}$) + sizeof(${settings.object_type_size}$);
        static const ${settings.object_type_id}$ objectID = ${object.id}$;

        // Non-Static header variables
        ${settings.object_type_sysid}$ systemID;
        ${settings.object_type_size}$ dataSize;

        // Payload
        Fields fields;

        // Housekeeping
        bool defined = false;

    public:       

        // Check if the object is fully defined
        bool isDefined() {
            return this->defined;
        }

        // Getter for constant expression headerSize
        constexpr static uint8_t getHeaderSize() {
            return headerSize;
        }

        // Getter for constant expression objectID
        constexpr static ${settings.object_type_id}$ getObjectID() {
            return objectID;
        }

        // Getter for systemID associated with this object
        ${settings.object_type_sysid}$ getSystemID() {
            if (this->defined) return systemID; 
            else return 0;
        }

        // Getter for dataSize of the fields structure of this object
        ${settings.object_type_size}$ getDataSize() {
            if (this->defined) return dataSize; 
            else return 0;
        }

        // Getter for the object totalSize
        uint32_t getTotalSize() {
            if (this->defined) return dataSize + headerSize;
            else return 0;
        }

        // Getter for a const pointer to the internal fields structure
        const Fields* getFields() const {
            return &fields;
        }

        // Default constructor
        DragonObject_${object.name}$() {

        }

        // Construct a ${object.name}$ object from individual fields
        DragonObject_${object.name}$(#{i=1:length(object.fields)<#>?{i~=1<?>,}?${object.fields(i).type}$ ?{object.fields(i).isArray<?>*}?${object.fields(i).name}$}#){
            
            uint32_t _dataSize = #{i=1:length(object.fields)<#>?{i>1<?> + }??{object.fields(i).isArray<?>${object.fields(i).arraySize}$*}?sizeof(${object.fields(i).type}$)}#;

            if (_dataSize <= pow(2,8*sizeof(${settings.object_type_size}$))) {
            
                this->dataSize = _dataSize;
                this->systemID = DragonOS::getSystemID();

                // Dynamically allocate array storage (if applicable)
#{i=1:length(object.fields)<#>?{object.fields(i).isArray<?>\t\t\t\tthis->fields.${object.fields(i).name}$ = new ${object.fields(i).type}$[${object.fields(i).arraySize}$];\n}?}#
                // Copy fields into relevant storage
#{i=1:length(object.fields)<#>\t\t\t\tmemcpy(?{~object.fields(i).isArray<?>&}?this->fields.${object.fields(i).name}$,?{~object.fields(i).isArray<?>&}?${object.fields(i).name}$,?{object.fields(i).isArray<?>this->fields.${object.fields(i).arraySize}$*}?sizeof(${object.fields(i).type}$));\n}#
                this->defined = true;
            } 

        }

        // Construct a ${object.name}$ object by deserialising from a vector with a header of the format [id,origin,size]
        DragonObject_${object.name}$(const std::vector<uint8_t> &buffer) {

            // Working variables
            uint32_t index = 0;
            ${settings.object_type_id}$ checkID;
        
            // Deserialise header and check object id and size validity
            if (buffer.size() < this->headerSize) return;
            memcpy(&checkID,&buffer[index],sizeof(${settings.object_type_id}$)); index += sizeof(${settings.object_type_id}$);
            if (this->objectID != checkID) return;
            memcpy(&this->systemID,&buffer[index],sizeof(${settings.object_type_sysid}$)); index += sizeof(${settings.object_type_sysid}$);
            memcpy(&this->dataSize,&buffer[index],sizeof(${settings.object_type_size}$)); index += sizeof(${settings.object_type_size}$);
            if (this->dataSize != buffer.size() - this->headerSize) return; #{i=1:length(object.fields)<#>

            // Deserialise the ${object.fields(i).name}$ field
            if (index+?{object.fields(i).isArray<?>this->fields.${object.fields(i).arraySize}$*}?sizeof(${object.fields(i).type}$) > buffer.size()) return; ?{object.fields(i).isArray<?>
            this->fields.${object.fields(i).name}$ = new ${object.fields(i).type}$[this->fields.${object.fields(i).arraySize}$];
            memcpy(this->fields.${object.fields(i).name}$,&buffer[index],this->fields.${object.fields(i).arraySize}$*sizeof(${object.fields(i).type}$)); index += this->fields.${object.fields(i).arraySize}$*sizeof(${object.fields(i).type}$);<?>
            memcpy(&this->fields.${object.fields(i).name}$,&buffer[index],sizeof(${object.fields(i).type}$)); index += sizeof(${object.fields(i).type}$);}?}#

            // Final size check
            if (index != buffer.size() || this->dataSize != index - this->headerSize) return;

            this->defined = true;

        }

        // Serialise object fields into a byte buffer with a header of the format [id,origin,size]
        bool serialise(std::vector<uint8_t> &buffer) {
            
            if (!this->defined) return false;

            // Get the constant expression object ID into temporary storage
            uint8_t _objectID = this->objectID;
                
            buffer.reserve(this->getTotalSize());
                
            //Serialise the object header
            buffer.insert(buffer.end(),(uint8_t*)&_objectID,(uint8_t*)&_objectID+sizeof(${settings.object_type_id}$));
            buffer.insert(buffer.end(),(uint8_t*)&this->systemID,(uint8_t*)&this->systemID+sizeof(${settings.object_type_sysid}$));
            buffer.insert(buffer.end(),(uint8_t*)&this->dataSize,(uint8_t*)&this->dataSize+sizeof(${settings.object_type_size}$));

            //Serialise object fields and place into buffer
#{i=1:length(object.fields)<#>\t\t\tbuffer.insert(buffer.end(),(uint8_t*)?{~object.fields(i).isArray<?>&}?this->fields.${object.fields(i).name}$,(uint8_t*)?{~object.fields(i).isArray<?>&}?this->fields.${object.fields(i).name}$+?{object.fields(i).isArray<?>this->fields.${object.fields(i).arraySize}$*}?sizeof(${object.fields(i).type}$));\n}#
            return true;
        }

        // Copy constructor
        DragonObject_${object.name}$(const DragonObject_${object.name}$ &object) {

            // Copy header data
            this->systemID = object.systemID;
            this->dataSize = object.dataSize;
            this->defined = object.defined;

            // Dynamically allocate array storage (if applicable)
#{i=1:length(object.fields)<#>?{object.fields(i).isArray<?>\t\t\tthis->fields.${object.fields(i).name}$ = new ${object.fields(i).type}$[object.fields.${object.fields(i).arraySize}$];\n}?}#
            // Copy field data
#{i=1:length(object.fields)<#>\t\t\tmemcpy(?{~object.fields(i).isArray<?>&}?this->fields.${object.fields(i).name}$,?{~object.fields(i).isArray<?>&}?object.fields.${object.fields(i).name}$,?{object.fields(i).isArray<?>this->fields.${object.fields(i).arraySize}$*}?sizeof(${object.fields(i).type}$));\n}#          
        }

        // Copy assignment operator
        DragonObject_${object.name}$& operator=(const DragonObject_${object.name}$ &object) {

            if (this == &object) return *this;

            // Copy header data
            this->systemID = object.systemID;
            this->dataSize = object.dataSize;
            this->defined = object.defined;

            // Dynamically allocate array storage (if applicable)
#{i=1:length(object.fields)<#>?{object.fields(i).isArray<?>\t\t\tthis->fields.${object.fields(i).name}$ = new ${object.fields(i).type}$[object.fields.${object.fields(i).arraySize}$];\n}?}#
            // Copy field data
#{i=1:length(object.fields)<#>\t\t\tmemcpy(?{~object.fields(i).isArray<?>&}?this->fields.${object.fields(i).name}$,?{~object.fields(i).isArray<?>&}?object.fields.${object.fields(i).name}$,?{object.fields(i).isArray<?>this->fields.${object.fields(i).arraySize}$*}?sizeof(${object.fields(i).type}$));\n}#         
            return *this;

        }

        // Destructor memory management
        ~DragonObject_${object.name}$() {

#{i=1:length(object.fields)<#>?{object.fields(i).isArray<?>\t\t\tdelete [] fields.${object.fields(i).name}$;\n<?>}?}#
        }

};

#endif