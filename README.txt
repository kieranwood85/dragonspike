The code is for the Arduino IDE. This is how to setup the IDE.

==========================
1) Installs
Install a version of the IDE (1.8.5 used here).
Install Teenduino (1.41) - I always have this installed and don't know if it changes anything.
Install Adafruit drivers 2.1.1.0 (an .exe installer)

(also change the user Arduino library location to C:/Arduino or something else on the root drive - this helps compile speed)

==========================
2) Libraries
(instructions here
https://learn.adafruit.com/adafruit-feather-m0-basic-proto/using-with-arduino-ide)

In Preferences -> add "https://adafruit.github.io/arduino-board-index/package_adafruit_index.json" as an external boards manager.

In Boards Manager -> Arduino SAMD 1.8.3
In Boards Manager -> Adafruit SAMD 1.5.2

Restart IDE

Copy any required downloaded libraries into the previously selected library location.

==========================
3) Compile + Flash
Tools -> Board -> Adafruit Feather M0
Port -> COMX assigned by OS
Compile and upload

